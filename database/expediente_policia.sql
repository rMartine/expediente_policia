-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 03, 2015 at 07:53 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `expediente_policia`
--

-- --------------------------------------------------------

--
-- Table structure for table `adscripciones`
--

CREATE TABLE IF NOT EXISTS `adscripciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `causas`
--

CREATE TABLE IF NOT EXISTS `causas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `causa_sancion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `convocatorias`
--

CREATE TABLE IF NOT EXISTS `convocatorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folio` text NOT NULL,
  `apertura` date NOT NULL,
  `cierre` date NOT NULL,
  `fallo` date NOT NULL,
  `contenido` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `convocatorias`
--

INSERT INTO `convocatorias` (`id`, `folio`, `apertura`, `cierre`, `fallo`, `contenido`) VALUES
(1, '1-2005', '2015-01-01', '2015-02-01', '2015-02-15', 'texto  de la convocatoria'),
(2, '2-2015', '2015-05-01', '0201-05-01', '2015-06-15', 'Texto de la convocatoria');

-- --------------------------------------------------------

--
-- Table structure for table `cursos`
--

CREATE TABLE IF NOT EXISTS `cursos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numerico` tinyint(1) NOT NULL,
  `idEtapa` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `duracion` int(11) NOT NULL,
  `importancia` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cursos_etapas1_idx` (`idEtapa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `elementos`
--

CREATE TABLE IF NOT EXISTS `elementos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folio` int(11) NOT NULL,
  `clave_empleado` varchar(20) DEFAULT NULL,
  `nombre` varchar(60) NOT NULL,
  `apellido_paterno` varchar(30) NOT NULL,
  `apellido_materno` varchar(30) NOT NULL,
  `fecha_nac` date NOT NULL,
  `municipio_nac` varchar(30) NOT NULL,
  `estado_nac` varchar(30) NOT NULL,
  `rfc` varchar(13) DEFAULT NULL,
  `curp` varchar(18) NOT NULL,
  `edad` int(11) NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `movil` varchar(10) DEFAULT NULL,
  `domicilio_next` int(11) NOT NULL,
  `domicilio_nint` int(11) NOT NULL,
  `domicilio_calle` varchar(45) NOT NULL,
  `domicilio_col` varchar(45) NOT NULL,
  `domicilio_cp` int(11) NOT NULL,
  `domicilio_mpo` varchar(30) NOT NULL,
  `domicilio_edo` varchar(30) NOT NULL,
  `estado_civil` varchar(20) NOT NULL,
  `email` varchar(40) DEFAULT NULL,
  `fotografia` text,
  `pulgar_der` text,
  `indice_der` text,
  `medio_der` text,
  `anular_der` text,
  `menique_der` text,
  `pulgar_izq` text,
  `indice_izq` text,
  `medio_izq` text,
  `anular_izq` text,
  `menique_izq` text,
  `idEstatus` int(11) NOT NULL,
  `idEstudios` int(11) NOT NULL,
  `idEtapa` int(11) NOT NULL,
  `idConvocatoria` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `folio_UNIQUE` (`folio`),
  UNIQUE KEY `clave_empleado_UNIQUE` (`clave_empleado`),
  KEY `fk_elementos_estatus1_idx` (`idEstatus`),
  KEY `fk_elementos_estudios1_idx` (`idEstudios`),
  KEY `fk_elementos_etapas1_idx` (`idEtapa`),
  KEY `fk_elementos_convocatorias1_idx` (`idConvocatoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `elementos`
--

INSERT INTO `elementos` (`id`, `folio`, `clave_empleado`, `nombre`, `apellido_paterno`, `apellido_materno`, `fecha_nac`, `municipio_nac`, `estado_nac`, `rfc`, `curp`, `edad`, `sexo`, `telefono`, `movil`, `domicilio_next`, `domicilio_nint`, `domicilio_calle`, `domicilio_col`, `domicilio_cp`, `domicilio_mpo`, `domicilio_edo`, `estado_civil`, `email`, `fotografia`, `pulgar_der`, `indice_der`, `medio_der`, `anular_der`, `menique_der`, `pulgar_izq`, `indice_izq`, `medio_izq`, `anular_izq`, `menique_izq`, `idEstatus`, `idEstudios`, `idEtapa`, `idConvocatoria`) VALUES
(1, 1001, NULL, 'Robertoni', 'Hernandez', 'Perez', '1980-08-15', 'Tecoman', 'Colima', 'TRDM102030', 'TRDM102030HCM12306', 20, 'M', '313415241', '314524561', 0, 0, '', '', 0, 'Manzanillo', 'Colima', 'Soltero', 'ccc@gn.com', 'no.png', '1_pulgar_der', '1_indice_der', '1_medio_der', '1_anular_der', '1_menique_der', '1_pulgar_izq', '1_indice_izq', '1_medio_izq', '1_anular_izq', '1_menique_izq', 1, 3, 1, 1),
(2, 1002, NULL, 'Juan', 'Penas', 'Teodoro', '1979-06-01', 'Manzanillo', 'Colima', 'RFDS-890723', '890723snn06', 29, 'M', '314555555', '314552255', 0, 0, '', '', 0, 'Manzanillo', 'Colima', 'Casado', 'csa@fff.com', 'no.png', '2_pulgar_der', '2_indice_der', '2_medio_der', '2_anular_der', '2_menique_der', '2_pulgar_izq', '2_indice_izq', '2_medio_izq', '2_anular_izq', '1_menique_izq', 1, 4, 1, 2),
(3, 1003, NULL, 'Piter', 'Picapiedra', 'del Toro', '1985-03-15', 'Coquiranch', 'Colima', 'RODO-851204', 'RODO-851204SNN06', 30, 'M', '314502010', '314602123', 0, 0, '', '', 0, 'Manzanillo', 'Colima', 'Soltero', 'xxxx@yahoo.com', 'no.png', '3_pulgar_der', '3_indice_der', '3_medio_der', '3_anular_der', '3_menique_der', '3_pulgar_izq', '3_indice_izq', '3_medio_izq', '3_anular_izq', '3_menique_izq', 1, 3, 1, 2),
(4, 1004, NULL, 'Piter', 'Picapiedra', 'del Toro', '1985-03-15', 'Coquiranch', 'Colima', 'RODO-851204', 'RODO-851204SNN06', 30, 'M', '314502010', '314602123', 0, 0, '', '', 0, 'Manzanillo', 'Colima', 'Soltero', 'xxxx@yahoo.com', 'no.png', '3_pulgar_der', '3_indice_der', '3_medio_der', '3_anular_der', '3_menique_der', '3_pulgar_izq', '3_indice_izq', '3_medio_izq', '3_anular_izq', '3_menique_izq', 1, 3, 1, 2),
(5, 1005, NULL, 'Piter', 'Picapiedra', 'del Toro', '1985-03-15', 'Coquiranch', 'Colima', 'RODO-851204', 'RODO-851204SNN06', 30, 'M', '314502010', '314602123', 0, 0, '', '', 0, 'Manzanillo', 'Colima', 'Soltero', 'xxxx@yahoo.com', 'no.png', '3_pulgar_der', '3_indice_der', '3_medio_der', '3_anular_der', '3_menique_der', '3_pulgar_izq', '3_indice_izq', '3_medio_izq', '3_anular_izq', '3_menique_izq', 1, 3, 1, 2),
(6, 1006, NULL, 'Piter', 'Picapiedra', 'del Toro', '1985-03-15', 'Coquiranch', 'Colima', 'RODO-851204', 'RODO-851204SNN06', 30, 'M', '314502010', '314602123', 0, 0, '', '', 0, 'Manzanillo', 'Colima', 'Soltero', 'xxxx@yahoo.com', 'no.png', '3_pulgar_der', '3_indice_der', '3_medio_der', '3_anular_der', '3_menique_der', '3_pulgar_izq', '3_indice_izq', '3_medio_izq', '3_anular_izq', '3_menique_izq', 1, 3, 1, 2),
(7, 1007, NULL, 'Piter', 'Picapiedra', 'del Toro', '1985-03-15', 'Coquiranch', 'Colima', 'RODO-851204', 'RODO-851204SNN06', 30, 'M', '314502010', '314602123', 0, 0, '', '', 0, 'Manzanillo', 'Colima', 'Soltero', 'xxxx@yahoo.com', 'no.png', '3_pulgar_der', '3_indice_der', '3_medio_der', '3_anular_der', '3_menique_der', '3_pulgar_izq', '3_indice_izq', '3_medio_izq', '3_anular_izq', '3_menique_izq', 1, 3, 1, 2),
(8, 1008, NULL, 'Piter', 'Picapiedra', 'del Toro', '1985-03-15', 'Coquiranch', 'Colima', 'RODO-851204', 'RODO-851204SNN06', 30, 'M', '314502010', '314602123', 0, 0, '', '', 0, 'Manzanillo', 'Colima', 'Soltero', 'xxxx@yahoo.com', 'no.png', '3_pulgar_der', '3_indice_der', '3_medio_der', '3_anular_der', '3_menique_der', '3_pulgar_izq', '3_indice_izq', '3_medio_izq', '3_anular_izq', '3_menique_izq', 1, 3, 1, 2),
(9, 1009, NULL, 'Piter', 'Picapiedra', 'del Toro', '1985-03-15', 'Coquiranch', 'Colima', 'RODO-851204', 'RODO-851204SNN06', 30, 'M', '314502010', '314602123', 0, 0, '', '', 0, 'Manzanillo', 'Colima', 'Soltero', 'xxxx@yahoo.com', 'no.png', '3_pulgar_der', '3_indice_der', '3_medio_der', '3_anular_der', '3_menique_der', '3_pulgar_izq', '3_indice_izq', '3_medio_izq', '3_anular_izq', '3_menique_izq', 1, 3, 1, 2),
(10, 1010, NULL, 'Piter', 'Picapiedra', 'del Toro', '1985-03-15', 'Coquiranch', 'Colima', 'RODO-851204', 'RODO-851204SNN06', 30, 'M', '314502010', '314602123', 0, 0, '', '', 0, 'Manzanillo', 'Colima', 'Soltero', 'xxxx@yahoo.com', 'no.png', '3_pulgar_der', '3_indice_der', '3_medio_der', '3_anular_der', '3_menique_der', '3_pulgar_izq', '3_indice_izq', '3_medio_izq', '3_anular_izq', '3_menique_izq', 1, 3, 1, 2),
(11, 1011, NULL, 'Piter', 'Picapiedra', 'del Toro', '1985-03-15', 'Coquiranch', 'Colima', 'RODO-851204', 'RODO-851204SNN06', 30, 'M', '314502010', '314602123', 0, 0, '', '', 0, 'Manzanillo', 'Colima', 'Soltero', 'xxxx@yahoo.com', 'no.png', '3_pulgar_der', '3_indice_der', '3_medio_der', '3_anular_der', '3_menique_der', '3_pulgar_izq', '3_indice_izq', '3_medio_izq', '3_anular_izq', '3_menique_izq', 1, 3, 1, 2),
(12, 1012, NULL, 'Piter', 'Picapiedra', 'del Toro', '1985-03-15', 'Coquiranch', 'Colima', 'RODO-851204', 'RODO-851204SNN06', 30, 'M', '314502010', '314602123', 0, 0, '', '', 0, 'Manzanillo', 'Colima', 'Soltero', 'xxxx@yahoo.com', 'no.png', '3_pulgar_der', '3_indice_der', '3_medio_der', '3_anular_der', '3_menique_der', '3_pulgar_izq', '3_indice_izq', '3_medio_izq', '3_anular_izq', '3_menique_izq', 1, 3, 1, 2),
(13, 1013, NULL, 'Piter', 'Picapiedra', 'del Toro', '1985-03-15', 'Coquiranch', 'Colima', 'RODO-851204', 'RODO-851204SNN06', 30, 'M', '314502010', '314602123', 0, 0, '', '', 0, 'Manzanillo', 'Colima', 'Soltero', 'xxxx@yahoo.com', 'no.png', '3_pulgar_der', '3_indice_der', '3_medio_der', '3_anular_der', '3_menique_der', '3_pulgar_izq', '3_indice_izq', '3_medio_izq', '3_anular_izq', '3_menique_izq', 1, 3, 1, 2),
(14, 1014, NULL, 'Piter', 'Picapiedra', 'del Toro', '1985-03-15', 'Coquiranch', 'Colima', 'RODO-851204', 'RODO-851204SNN06', 30, 'M', '314502010', '314602123', 0, 0, '', '', 0, 'Manzanillo', 'Colima', 'Soltero', 'xxxx@yahoo.com', 'no.png', '3_pulgar_der', '3_indice_der', '3_medio_der', '3_anular_der', '3_menique_der', '3_pulgar_izq', '3_indice_izq', '3_medio_izq', '3_anular_izq', '3_menique_izq', 1, 3, 1, 2),
(15, 1015, NULL, 'Sergio Humberto', 'Reynaga', 'Ibarra', '1988-02-11', 'Hermosillo', 'Sonora', 'REIS880211NLA', 'REIS880211HSRYBR08', 27, 'M', '6622158327', '6621574103', 307, 0, 'Articulo 123', 'Ley 57', 83100, 'Hermosillo', 'Sonora', 'Soltero', 'sergio.reynaga@gmail.com', 'sergio-reynaga.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 4, 6, 1),
(17, 1016, NULL, 'Pablito', 'Aguirre', 'Lopez', '0000-00-00', '', '', NULL, '', 0, '', NULL, NULL, 0, 0, '', '', 0, '', '', '', NULL, 'no.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 3, 6, 2),
(18, 1017, NULL, 'Fulanito', 'Perez', 'Cabo', '0000-00-00', '', '', NULL, '', 0, '', NULL, NULL, 0, 0, '', '', 0, '', '', '', NULL, 'no.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `estatus`
--

CREATE TABLE IF NOT EXISTS `estatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `estatus`
--

INSERT INTO `estatus` (`id`, `nombre`) VALUES
(1, 'Aspirante'),
(2, 'Activo'),
(3, 'Inactivo');

-- --------------------------------------------------------

--
-- Table structure for table `estimulos`
--

CREATE TABLE IF NOT EXISTS `estimulos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `recompensa` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `estudios`
--

CREATE TABLE IF NOT EXISTS `estudios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grado_estudios` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `estudios`
--

INSERT INTO `estudios` (`id`, `grado_estudios`) VALUES
(1, 'Primaria'),
(2, 'Secundaria'),
(3, 'Preparatoria'),
(4, 'Licenciatura'),
(5, 'Maestría'),
(6, 'Doctorado');

-- --------------------------------------------------------

--
-- Table structure for table `etapas`
--

CREATE TABLE IF NOT EXISTS `etapas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `etapas`
--

INSERT INTO `etapas` (`id`, `nombre`, `orden`) VALUES
(1, 'Datos generales', 1),
(2, 'Reclutamiento', 2),
(3, 'Selección', 3),
(4, 'Formación inicial', 4),
(5, 'Nombramiento y certificación', 5),
(6, 'Activo', 6);

-- --------------------------------------------------------

--
-- Table structure for table `evaluaciones`
--

CREATE TABLE IF NOT EXISTS `evaluaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idTipo` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `duracion` int(11) NOT NULL,
  `importancia` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_evaluaciones_tipos_evaluacion1_idx` (`idTipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `log_adscripciones`
--

CREATE TABLE IF NOT EXISTS `log_adscripciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idElemento` int(11) NOT NULL,
  `idAdscripcion` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `observaciones` text,
  PRIMARY KEY (`id`),
  KEY `fk_log_adscripciones_elementos_idx` (`idElemento`),
  KEY `fk_log_adscripciones_adscripciones1_idx` (`idAdscripcion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `log_bajas`
--

CREATE TABLE IF NOT EXISTS `log_bajas` (
  `id` int(11) NOT NULL,
  `idElemento` int(11) NOT NULL,
  `idMotivo` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `observaciones` text,
  PRIMARY KEY (`id`),
  KEY `fk_log_bajas_motivos1_idx` (`idMotivo`),
  KEY `fk_log_bajas_elementos1_idx` (`idElemento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `log_cursos`
--

CREATE TABLE IF NOT EXISTS `log_cursos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idElemento` int(11) NOT NULL,
  `idCurso` int(11) NOT NULL,
  `calificacion` decimal(4,2) DEFAULT NULL,
  `cumplimiento` tinyint(1) NOT NULL,
  `fecha` date NOT NULL,
  `observaciones` text,
  PRIMARY KEY (`id`),
  KEY `fk_log_cursos_elementos1_idx` (`idElemento`),
  KEY `fk_log_cursos_cursos1_idx` (`idCurso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `log_estimulos`
--

CREATE TABLE IF NOT EXISTS `log_estimulos` (
  `id` int(11) NOT NULL,
  `idElemento` int(11) NOT NULL,
  `idEstimulo` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `observaciones` text,
  PRIMARY KEY (`id`),
  KEY `fk_log_estimulos_elementos1_idx` (`idElemento`),
  KEY `fk_log_estimulos_estimulos1_idx` (`idEstimulo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `log_evaluaciones`
--

CREATE TABLE IF NOT EXISTS `log_evaluaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idElemento` int(11) NOT NULL,
  `idEvaluacion` int(11) NOT NULL,
  `cumplimiento` tinyint(1) NOT NULL,
  `observaciones` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_log_evaluaciones_evaluaciones1_idx` (`idEvaluacion`),
  KEY `fk_log_evaluaciones_elementos1_idx` (`idElemento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `log_nombramientos`
--

CREATE TABLE IF NOT EXISTS `log_nombramientos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idElemento` int(11) NOT NULL,
  `idTipo` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `observaciones` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_log_nombramientos_nombramientos1_idx` (`idTipo`),
  KEY `fk_log_nombramientos_elementos1_idx` (`idElemento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `log_sanciones`
--

CREATE TABLE IF NOT EXISTS `log_sanciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idElemento` int(11) NOT NULL,
  `idSancion` int(11) NOT NULL,
  `idCausa` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `duracion` int(11) NOT NULL,
  `observaciones` text NOT NULL,
  `sanciones_id` int(11) NOT NULL,
  `causas_id` int(11) NOT NULL,
  `elementos_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_log_sanciones_sanciones1_idx` (`sanciones_id`),
  KEY `fk_log_sanciones_causas1_idx` (`causas_id`),
  KEY `fk_log_sanciones_elementos1_idx` (`elementos_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `username`, `password`, `created`, `updated`) VALUES
(1, 'ivan', 'a2cae7a9e64402c30efb5b9727b1cd95e517879e', '2015-03-21 04:49:01', '2015-03-21 08:39:47'),
(2, 'sreynaga', 'a7250ae5b41a9d616460897e8ff8be341d9215d6', '2015-03-21 04:49:01', '2015-03-21 08:39:47'),
(3, 'roberto', 'a2cae7a9e64402c30efb5b9727b1cd95e517879e', '2015-03-21 04:49:01', '2015-03-21 08:39:47');

-- --------------------------------------------------------

--
-- Table structure for table `motivos`
--

CREATE TABLE IF NOT EXISTS `motivos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `motivo` varchar(45) NOT NULL,
  `descripcion` text NOT NULL,
  `ordinaria` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `motivos`
--

INSERT INTO `motivos` (`id`, `motivo`, `descripcion`, `ordinaria`) VALUES
(1, 'La renuncia formulada por el policía', 'Motivo ordinario 1', 1),
(2, 'Antigüedad en el servicio', 'Motivo ordinario 2', 1),
(3, 'Incapacidad Permanente para el desempeño de s', 'Motivo ordinario 3', 1),
(4, 'La pensión por Jubilación', 'Motivo ordinario 4', 1),
(5, 'Retiro', 'Motivo ordinario 5', 1),
(6, 'Edad y tiempo de servicio', 'Motivo ordinario 6', 1),
(7, 'Invalides', 'Motivo ordinario 7', 1),
(8, 'Cesantía en edad avanzada', 'Motivo ordinario 8', 1),
(9, 'Indemnización global', 'Motivo ordinario 9', 1),
(10, 'Muerte del Policía', 'Motivo ordinario 10', 1),
(11, 'No Mantener en todo caso vigente los requisit', 'Motivo extraordinario 1', 0),
(12, 'No Aprobar las evaluaciones relativas a la fo', 'Motivo extraordinario 2', 0),
(13, 'No Aprobar los exámenes periódicos...', 'Motivo extraordinario 3', 0),
(14, 'No Aprobar las evaluaciones a que se refiere ', 'Motivo extraordinario 4', 0),
(15, 'Exceder la edad máxima de permanencia para el', 'Motivo extraordinario 5', 0),
(16, 'No aprobar el examen toxicológico', 'Motivo extraordinario 6', 0),
(17, 'Observar las prohibiciones...', 'Motivo extraordinario 7', 0);

-- --------------------------------------------------------

--
-- Table structure for table `nombramientos`
--

CREATE TABLE IF NOT EXISTS `nombramientos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grado` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `paquetes_cursos`
--

CREATE TABLE IF NOT EXISTS `paquetes_cursos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `idEtapa` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_paquetes_cursos_etapas1_idx` (`idEtapa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sanciones`
--

CREATE TABLE IF NOT EXISTS `sanciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sancion` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tipos_evaluacion`
--

CREATE TABLE IF NOT EXISTS `tipos_evaluacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cursos`
--
ALTER TABLE `cursos`
  ADD CONSTRAINT `fk_cursos_etapas1` FOREIGN KEY (`idEtapa`) REFERENCES `etapas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `elementos`
--
ALTER TABLE `elementos`
  ADD CONSTRAINT `fk_elementos_convocatorias1` FOREIGN KEY (`idConvocatoria`) REFERENCES `convocatorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_elementos_estatus1` FOREIGN KEY (`idEstatus`) REFERENCES `estatus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_elementos_estudios1` FOREIGN KEY (`idEstudios`) REFERENCES `estudios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_elementos_etapas1` FOREIGN KEY (`idEtapa`) REFERENCES `etapas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `evaluaciones`
--
ALTER TABLE `evaluaciones`
  ADD CONSTRAINT `fk_evaluaciones_tipos_evaluacion1` FOREIGN KEY (`idTipo`) REFERENCES `tipos_evaluacion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `log_adscripciones`
--
ALTER TABLE `log_adscripciones`
  ADD CONSTRAINT `fk_log_adscripciones_adscripciones1` FOREIGN KEY (`idAdscripcion`) REFERENCES `adscripciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_log_adscripciones_elementos` FOREIGN KEY (`idElemento`) REFERENCES `elementos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `log_bajas`
--
ALTER TABLE `log_bajas`
  ADD CONSTRAINT `fk_log_bajas_elementos1` FOREIGN KEY (`idElemento`) REFERENCES `elementos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_log_bajas_motivos1` FOREIGN KEY (`idMotivo`) REFERENCES `motivos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `log_cursos`
--
ALTER TABLE `log_cursos`
  ADD CONSTRAINT `fk_log_cursos_cursos1` FOREIGN KEY (`idCurso`) REFERENCES `cursos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_log_cursos_elementos1` FOREIGN KEY (`idElemento`) REFERENCES `elementos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `log_estimulos`
--
ALTER TABLE `log_estimulos`
  ADD CONSTRAINT `fk_log_estimulos_elementos1` FOREIGN KEY (`idElemento`) REFERENCES `elementos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_log_estimulos_estimulos1` FOREIGN KEY (`idEstimulo`) REFERENCES `estimulos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `log_evaluaciones`
--
ALTER TABLE `log_evaluaciones`
  ADD CONSTRAINT `fk_log_evaluaciones_elementos1` FOREIGN KEY (`idElemento`) REFERENCES `elementos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_log_evaluaciones_evaluaciones1` FOREIGN KEY (`idEvaluacion`) REFERENCES `evaluaciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `log_nombramientos`
--
ALTER TABLE `log_nombramientos`
  ADD CONSTRAINT `fk_log_nombramientos_elementos1` FOREIGN KEY (`idElemento`) REFERENCES `elementos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_log_nombramientos_nombramientos1` FOREIGN KEY (`idTipo`) REFERENCES `nombramientos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `log_sanciones`
--
ALTER TABLE `log_sanciones`
  ADD CONSTRAINT `fk_log_sanciones_causas1` FOREIGN KEY (`causas_id`) REFERENCES `causas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_log_sanciones_elementos1` FOREIGN KEY (`elementos_id`) REFERENCES `elementos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_log_sanciones_sanciones1` FOREIGN KEY (`sanciones_id`) REFERENCES `sanciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `paquetes_cursos`
--
ALTER TABLE `paquetes_cursos`
  ADD CONSTRAINT `fk_paquetes_cursos_etapas1` FOREIGN KEY (`idEtapa`) REFERENCES `etapas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
