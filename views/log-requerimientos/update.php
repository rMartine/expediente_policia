<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\LogRequerimientos $model
 */

$this->title = 'Log Requerimientos Update ' . $model->id . '';
$this->params['breadcrumbs'][] = ['label' => 'Log Requerimientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="log-requerimientos-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
