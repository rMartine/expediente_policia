<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\LogRechazos $model
 */

$this->title = 'Log Rechazos Update ' . $model->id . '';
$this->params['breadcrumbs'][] = ['label' => 'Log Rechazos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="log-rechazos-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
