<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\LogSeleccionSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="log-seleccion-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'idElemento') ?>

		<?= $form->field($model, 'idSeleccion') ?>

		<?= $form->field($model, 'observaciones') ?>

		<?= $form->field($model, 'resultados') ?>

		<?php // echo $form->field($model, 'fecha') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
