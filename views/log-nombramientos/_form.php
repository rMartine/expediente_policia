<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Adscripciones;
use app\models\Nombramientos;
/**
* @var yii\web\View $this
* @var app\models\LogSanciones $model
* @var yii\widgets\ActiveForm $form
*/
$cssButton = $model->isNewRecord ? "btn btn-app" : "btn btn-app hidden";
?>

<div class="log-sanciones-form">
    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => true, 'id' => 'form-log']); ?>
    <div class="row">
        <?php echo $form->errorSummary($model, ['header' => 'Por favor corrija los errores:']); ?>
    </div>
    <div class="">
        <ul id="w0" class="nav nav-tabs"><li class="active"><a href="#w0-tab0" data-toggle="tab">Nombramientos</a></li></ul>
        <p>
            <?= $form->field($model->idElemento0, 'nombreText')->label('Nombre del elemento')->textInput(['readonly' => true]) ?>

            <?= $form->field($model, 'idTipo')
                ->label('Nombramiento')
                ->dropDownList(
                    ArrayHelper::map(Nombramientos::find()->all(), 'id', 'grado'),
                    ['prompt' => '']
                )
            ?>
            <?= $form->field($model, 'idAdscripcion')
                ->label('Adscripcion')
                ->dropDownList(
                    ArrayHelper::map(Adscripciones::find()->all(), 'id', 'nombre')
                )
            ?>
            <?= $form->field($model, 'fecha')->textInput(['type' => 'date']); ?>
            <?= $form->field($model, 'comision')->textInput() ?>
            <?= $form->field($model, 'formacion_inicial')->textInput(['maxlength' => 4]) ?>
            <?= $form->field($model, 'oficio_nombramiento')->textInput(['maxlength' => 50]) ?>
            <?= $form->field($model, 'observaciones')->textarea(['rows' => 6]) ?>
        </p>        
        <hr/>

        <?= Html::submitButton('<span class="fa fa-save"></span> '.'Guardar', ['class' => $cssButton]) ?>
        <a href="<?php echo Url::previous(); ?>" class="<?php echo $cssButton ?>">
            <i class="fa fa-times"></i>Cancelar
        </a>

        <?php ActiveForm::end(); ?>

    </div>
</div>

