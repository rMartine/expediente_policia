<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LogNombramientos */

$this->title = 'Actualiza Nombramiento: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Log Nombramientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualiza';
?>
<div class="log-nombramientos-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>