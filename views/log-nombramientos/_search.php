<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/**
 * @var yii\web\View $this
 * @var app\models\LogNombramientosSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="log-nombramientos-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'idElemento') ?>

		<?= $form->field($model, 'idTipo') ?>

		<?= $form->field($model, 'idAdscripcion') ?>

		<?= $form->field($model, 'fecha') ?>

		<?php // echo $form->field($model, 'comision') ?>

		<?php // echo $form->field($model, 'formacion_inicial') ?>

		<?php // echo $form->field($model, 'oficio_nombramiento') ?>

		<?php // echo $form->field($model, 'observaciones') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
