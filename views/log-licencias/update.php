<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\LogLicencias $model
 */

$this->title = 'Actualiza Licencia ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Log Licencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualiza';
?>
<div class="log-licencias-update">

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
