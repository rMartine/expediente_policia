<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Licencias;

/**
* @var yii\web\View $this
* @var yii\data\ActiveDataProvider $dataProvider
* @var app\models\LogLicenciasSearch $searchModel
*/
$this->registerJsFile(Yii::$app->homeUrl.'js/datatables/tablas.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->homeUrl . '/css/activos/planes.css');
$urlIndex = Url::to(['licencias/']);
$this->title = 'Licencias';
$blockName = 'licencias';
?>

<section class="content-header">

    <h1>
        <?php echo $this->title; ?>
        <small></small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="<?php echo Yii::$app->homeUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
        <li class="active"><?php echo $this->title; ?></li>
    </ol>

</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <?= $this->render('//planes/formacion_header', ['elemento' => $elemento]); ?>
        </div>
        <div class="box-body">
            <div class="bg-navy-active">
                <h5>Agregar una nueva licencia, permiso, comisión o incapacidad</h5>
            </div>
            <?php $form = ActiveForm::begin([
                'action' => Url::to(['log-licencias/', 'id' => $elemento->id]),
                'method' => 'post',
                'enableClientValidation' => true]);
            ?>
            <div class="row">
                <?php echo $form->errorSummary($model, ['header' => 'Por favor corrija los errores:']); ?>
            </div>
            <div>
                <?php $this->beginBlock($blockName); ?>
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($model, 'idLicencia')
                            ->label('Tipo de Evento')
                            ->dropDownList(
                                ArrayHelper::map(Licencias::find()->all(), 'id', 'tipo')
                            )
                        ?>
                    </div>
                    <div class="col-md-3">  
                        <?= $form->field($model, 'fecha_insercion')->label('Fecha de Iserción')->textInput(['type' => 'date', 'readonly' => 'true', 'value' => date('Y-m-d', time())]); ?>
                    </div>
                    <div class="col-md-3">  
                        <?= $form->field($model, 'fecha_inicio')->label('Inicio del Evento')->textInput(['type' => 'date']); ?>
                    </div>
                    <div class="col-md-3">  
                        <?= $form->field($model, 'fecha_fin')->label('Fin del Evento')->textInput(['type' => 'date']); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($model, 'motivo')->label('Concepto')->textInput(); ?>
                    </div>
                    <div class="col-md-9">
                        <?= $form->field($model, 'observaciones')->label('observaciones')->textarea(); ?>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label"></label>
                        <span class="">
                            <?php echo Html::submitButton('', ['class' => 'btn btn-info btn-block fa fa-plus-circle']); ?>
                        </span>
                        <?= $form->field($model, 'idElemento')->label(false)->textInput(['type' => 'hidden', 'value' => $elemento->id]); ?>
                    </div>
                </div>
                <?php $this->endBlock(); ?>
                <?php ActiveForm::end(); ?>
                <p>
                    <?= $this->blocks[$blockName]; ?>
                </p>
            </div>
            <h4>Histórico de Incapacidades, Licencias, Permisos y Comisiones</h4>
            <?= $this->render('@app/views/layouts/modal', []); ?>
            <div class="bg-navy-active">
                <h5>Histórico de Incapacidades, Licencias, Permisos y Comisiones</h5>
            </div>
            <table id="resultados" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tipo de Evento</th>
                        <th>Motivo</th>
                        <th>Fecha de Inserción</th>
                        <th>Inicio de Evento</th>
                        <th>Fin de Evento</th>
                        <th>Observaciones</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $contador = 1; ?>
                    <?php foreach ( $elemento->logLicencias as $log ) { ?>
                        <tr>
                            <td><?php echo $contador++; ?></td>
                            <td><?php echo $log->idLicencia0->tipo; ?></td>
                            <td><?php echo $log->motivo; ?></td>
                            <td><?php echo $log->fecha_insercion; ?></td>
                            <td><?php echo $log->fecha_inicio; ?></td>
                            <td><?php echo $log->fecha_fin; ?></td>
                            <td><?php echo $log->observaciones; ?></td>
                            <td>
                                <a class="btn btn-block" data-toggle="modal" data-target="#modal-model" data-modal-type="edit" data-action="<?php echo Url::to(['log-licencias/update', 'id' => $log->id]) ?>">
                                    <span data-toggle="tooltip" data-placement="left" title="Editar" class="glyphicon glyphicon-pencil"></span>
                                </a>
                            </td>
                            <td>
                                <a class="btn btn-block" data-toggle="modal" data-target="#modal-model" data-modal-type="remove" data-action="<?php echo Url::to(['log-licencias/delete', 'id' => $log->id, 'idElemento'=>$elemento->id]) ?>">
                                    <span data-toggle="tooltip" data-placement="left" title="Eliminar" class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
        <div class="box-footer">
            <a style="visibility:show" href="<?php echo $urlIndex; ?>" class="btn btn-app">
                <i class="fa fa-arrow-left"></i>Regresar
            </a>
        </div>
    </div>
</div>