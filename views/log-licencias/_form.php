<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Licencias;
use yii\helpers\Url;

/**
* @var yii\web\View $this
* @var app\models\LogLicencias $model
* @var yii\widgets\ActiveForm $form
*/
$cssButton = $model->isNewRecord ? "btn btn-app" : "btn btn-app hidden";
?>

<div class="log-licencias-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => true, 'id' => 'form-log']); ?>

     <div class="row">
        <?php echo $form->errorSummary($model, ['header' => 'Por favor corrija los errores:']); ?>
    </div>
    <div class="">
        <ul id="w0" class="nav nav-tabs"><li class="active"><a href="#w0-tab0" data-toggle="tab">Licencias</a></li></ul>

        <p>
            
			<?= $form->field($model->idElemento0, 'nombreText')->label('Nombre del elemento')->textInput(['readonly' => true]) ?>
			<?= $form->field($model, 'idLicencia')
                ->label('Tipo de Evento')
                ->dropDownList(
                    ArrayHelper::map(Licencias::find()->all(), 'id', 'tipo')
                )
            ?>
			<?= $form->field($model, 'fecha_insercion')->label('Fecha de Iserción')->textInput(['type' => 'date', 'disabled' => '']); ?>
			<?= $form->field($model, 'fecha_inicio')->label('Inicio del Evento')->textInput(['type' => 'date']); ?>
			<?= $form->field($model, 'fecha_fin')->label('Fin del Evento')->textInput(['type' => 'date']); ?>
			<?= $form->field($model, 'motivo')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'observaciones')->textarea(['rows' => 6]) ?>
        </p>
        <hr/>

        <?= Html::submitButton('<span class="fa fa-save"></span> '.'Guardar', ['class' => $cssButton]) ?>
        <a href="<?php echo Url::previous(); ?>" class="<?php echo $cssButton ?>">
            <i class="fa fa-times"></i>Cancelar
        </a>
        <?php ActiveForm::end(); ?>

    </div>

</div>
