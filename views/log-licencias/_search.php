<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\LogLicenciasSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="log-licencias-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'idElemento') ?>

		<?= $form->field($model, 'idLicencia') ?>

		<?= $form->field($model, 'motivo') ?>

		<?= $form->field($model, 'fecha_insercion') ?>

		<?php // echo $form->field($model, 'fecha_inicio') ?>

		<?php // echo $form->field($model, 'fecha_duracion') ?>

		<?php // echo $form->field($model, 'observaciones') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
