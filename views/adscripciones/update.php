<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\adscripciones $model
 */

$this->title = 'Adscripciones Update ' . $model->id . '';
$this->params['breadcrumbs'][] = ['label' => 'Adscripciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="adscripciones-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
