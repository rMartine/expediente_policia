<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Cursos;
/**
* @var yii\web\View $this
* @var app\models\LogCursos $model
* @var yii\widgets\ActiveForm $form
*/
$cssButton = $model->isNewRecord ? "btn btn-app" : "btn btn-app hidden";
$attributes = $formType == "view" ? ['disabled' => ''] : [];
?>

<div class="log-cursos-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model, ['header' => 'Por favor corrija los errores:']); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            <label class="control-label col-sm-3">
                Elemento
            </label>
            <input disabled type="text" value="<?= $model->idElemento0->nombre . " " . $model->idElemento0->apellido_paterno . " " . $model->idElemento0->apellido_materno; ?>">
			<?= $form->field($model, 'idElemento')->textInput(['type'=>'hidden'])->label(false) ?>
			<?= $form->field($model, 'idCurso')
                ->label('Nombre del curso')
                ->dropDownList(ArrayHelper::map(Cursos::find()->all(), 'id', 'nombre'),
                    ['prompt' => '--cursos--']
                )
            ?>
            <?= $form->field($model, 'fecha')->textInput(['type' => 'date']); ?>
			<?= $form->field($model, 'calificacion')->textInput(['maxlength' => 4, 'type'=>'number']) ?>
			<?= $form->field($model, 'observaciones')->textarea(['rows' => 6]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    \yii\bootstrap\Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ [
    'label'   => 'Curso',
    'content' => $this->blocks['main'],
    'active'  => true,
], ]
                 ]
    );
    ?>
        <hr/>
        <?= Html::submitButton('<span class="fa fa-save"></span> '.'Guardar', ['class' => $cssButton]) ?>
        <a href="<?php echo Url::previous(); ?>" class="<?php echo $cssButton ?>">
            <i class="fa fa-times"></i>Cancelar
        </a>
        <?php ActiveForm::end(); ?>

    </div>

</div>
