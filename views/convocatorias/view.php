<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\convocatorias $model
 */

$this->title = 'Convocatoria' . $model->id . '';
$this->params['breadcrumbs'][] = ['label' => 'Convocatorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="convocatorias-update">
    <?php echo $this->render('_form', [
        'model' => $model,
        'requerimientosConvocatoria' => $requerimientosConvocatoria, 
        'formType' => 'view',
    ]); ?>
</div>