<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\convocatorias $model
*/

$this->title = 'Nueva convocatoria';
$this->params['breadcrumbs'][] = ['label' => 'Convocatoria', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">

    <h1>
        <?php echo $this->title; ?>
        <small></small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="<?php echo Yii::$app->homeUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
        <li class="active"><?php echo $this->title; ?></li>
    </ol>

</section>

<section class="content">
	<div class="box">
		<div class="box-header">
	        <div class="bg-navy-active">
	            <h5>Datos de la Convocatoria</h5>
	        </div>
	    </div>
	    <div class="box-body">
			<div class="convocatorias-create">
			    <?php echo $this->render('_form', [
			    	'model' => $model, 
			    	'requerimientosConvocatoria' => $requerimientosConvocatoria, 
			    	'formType' => 'create'
			    	]);
			    ?>
			</div>
		</div>
	</div>
</section>