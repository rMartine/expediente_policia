
<?php
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->registerCssFile(Yii::$app->homeUrl.'css/bootstrap3-wysihtml5.min.css');
$this->registerJsFile(Yii::$app->homeUrl.'js/datatables/tablas.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->homeUrl.'js/bootstrap3-wysihtml5.all.min.js', ['depends' => [\yii\web\JqueryAsset::className()], 'position' => $this::POS_END ]);
$this->title = 'Convocatorias';
?>

<section class="content-header">

	<h1>
		<?php echo $this->title; ?>
		<small></small>
	</h1>

	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::$app->homeUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
		<li class="active"><?php echo $this->title; ?></li>
	</ol>

</section>

<section class="content">
	<?= $this->render('@app/views/layouts/modal', []); ?>
	<div class="box box-primary">
		<div class="box-header with-border">
			<?= $this->render('_search');  ?>
		</div>

		<div class="box-body">
			<div class="box">
				<div class="bg-navy-active">
					<h5>Resultados de la búsqueda</h5>
				</div>
				<table id="resultados" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Folio</th>
							<th>Apertura</th>
							<th>Cierre</th>
							<th>Fallo</th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php if (count($convocatorias) > 0) { ?>
							<?php $contador = 1; ?>
							<?php foreach ( $convocatorias as $convocatoria ) { ?>
								<tr>
									<td><?php echo $contador++; ?></td>
									<td><?php echo $convocatoria->folio; ?></td>
									<td><?php echo $convocatoria->apertura; ?></td>
									<td><?php echo $convocatoria->cierre; ?></td>
									<td><?php echo $convocatoria->fallo; ?></td>
									<td>
										<a class="btn btn-block" data-toggle="modal" data-target="#modal-model" data-modal-type="view" data-action="<?php echo Url::to(['convocatorias/view', 'id' => $convocatoria->id]); ?>">
											<span data-toggle="tooltip" data-placement="left" title="Ver" class="glyphicon glyphicon-eye-open"></span>
										</a>
									</td>
									<td>
										<a class="btn btn-block" data-toggle="modal" data-target="#modal-model" data-modal-type="edit" data-action="<?php echo Url::to(['convocatorias/update', 'id' => $convocatoria->id]); ?>">
											<span data-toggle="tooltip" data-placement="left" title="Editar" class="glyphicon glyphicon-pencil"></span>
										</a>
									</td>
									<?php $btnClass = count($convocatoria->elementos) > 0 ? "disabled" : "" ?>
									<?php $btnTooltip = $btnClass == "disabled" ? "La convocatoria tiene elementos asignados" : "Eliminar" ?>
									<td data-toggle="tooltip" data-placement="left" title="<?php echo $btnTooltip ?>">
										<a class="btn btn-block <?php echo $btnClass ?>" data-toggle="modal" data-target="#modal-model" data-modal-type="remove" data-action="<?php echo Url::to(['convocatorias/delete', 'id' => $convocatoria->id]); ?>">
											<span class="glyphicon glyphicon-trash"></span>
										</a>
									</td>
								</tr>
							<?php } ?>
						<?php } else { ?>
							<tr>
								<td></td>
								<td class="text-center"><i>Tu busqueda no arrojo resultados.</i></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						<?php } ?>
					</tbody>
					<tfoot>
                    </tfoot>
				</table>
			</div>
		</div>

		<div class="box-footer">
		</div>

	</div>
</section>