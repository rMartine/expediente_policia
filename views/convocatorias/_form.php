<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Requerimientos;
/**
* @var yii\web\View $this
* @var app\models\convocatorias $model
* @var yii\widgets\ActiveForm $form
*/
$this->registerJsFile(Yii::$app->homeUrl.'js/bootstrap3-wysihtml5.all.min.js', ['depends' => [\yii\web\JqueryAsset::className()], 'position' => $this::POS_END ]);
$this->registerCssFile(Yii::$app->homeUrl.'css/bootstrap3-wysihtml5.min.css');
$cssButton = $model->isNewRecord ? "btn" : "btn hidden";
$attributes = $formType == "view" ? ['disabled' => ''] : [];
?>

<div class="convocatorias-form">
    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>
    <div class="">
            <div class="row">
                <?php echo $form->errorSummary($model); ?>
            </div>
            <?php $this->beginBlock('main'); ?>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'folio')->textInput($attributes) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'apertura')->label('Fecha de apertura')->textInput(['type' => 'date'] + $attributes) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'cierre')->label('Fecha de cierre')->textInput(['type' => 'date'] + $attributes) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'fallo')->label('Fecha del fallo')->textInput(['type' => 'date'] + $attributes) ?>
                </div>
            </div>
            <div class="row col-md-12">
                <?= $form->field($model, 'contenido')->label('')->textarea(['rows' => 10] + $attributes) ?>
            </div>
            <div class="row">
                <div class="bg-navy-active">
                </div>
            </div>
            <div class="row">
                <div class="box-header">
                    <div class="bg-navy-active">
                        <h5>Requerimientos de la convocatoria</h5>
                    </div>
                </div>
                <div class="col-md-offset-2">
                    <?php $requerimientosAll = Requerimientos::find()->all() ?>
                    <?php if (count($requerimientosAll > 0)) { ?>
                        <?php $modelRequerimientos = $model->getRequerimientos(); 
                              $strSelectedFields = '';
                              $strUnSelectedFields = ''; 
                        ?>
                        
                        <?php foreach ( $requerimientosAll as $key => $requerimiento ) { ?>
                            
                            <?php if (array_key_exists($requerimiento->id, $modelRequerimientos)) {                                
                                 $classCheckbox = 'form-group field-requerimientosconvocatoria-requerimientos-' . $requerimiento->id;
                                 $idCheckbox = 'requerimientosconvocatoria-requerimientos-' . $requerimiento->id;
                                 $nameCheckbox = 'RequerimientosConvocatoria[requerimientos][' . $requerimiento->id . ']';
                                 $strSelectedFields .= '
                                    <div class="'.$classCheckbox.'">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <div class="checkbox">
                                                <label class="col-md-6">
                                                    <input type="hidden" name="'.$nameCheckbox.'" value="1">
                                                    <input checked type="checkbox" id="'.$idCheckbox.'" name="'.$nameCheckbox.'" value="1" '.http_build_query($attributes, '', '').' >
                                                    '.$requerimiento->nombre.'
                                                </label>
                                            </div>
                                        </div>
                                    </div>';
                                } else if($formType != "view") { 
                                 $strUnSelectedFields .= $form->field($requerimientosConvocatoria, "requerimientos[$requerimiento->id]")
                                    ->label($requerimiento->nombre, ['class' => 'col-md-6'])
                                    ->checkbox(['checked' => ''] + $attributes); ?>

                            <?php } ?>                            
                        <?php } ?>
                            <?=$strSelectedFields ?>
                            <?=$strUnSelectedFields ?>  
                    <?php } ?>
                </div>
            </div>
        <?php $this->endBlock(); ?>

        <?php if ($formType != 'create') {
            echo \yii\bootstrap\Tabs::widget(
                [
                   'encodeLabels' => false,
                     'items' => [ [
                    'label'   => 'Convocatoria',
                    'content' => $this->blocks['main'],
                    'active'  => true,
                    ], ]
                ]
            );
        ?>
        <?php } else { ?>
            <p>
                <?= $this->blocks['main']; ?>
            </p>
        <?php } ?>
        
        <hr/>

        <div class="row">
            <div class="col-xs-2 pull-right">
                <a href="<?php echo Url::previous(); ?>" class="<?php echo $cssButton . ' btn-default' ?>">
                    <i class="glyphicon glyphicon-repeat"></i>Cancelar
                </a>
            </div>
            <div class="col-xs-2 pull-right">
                <?= Html::submitButton('<span class="glyphicon glyphicon-floppy-disk"></span> '.'Guardar', ['class' => $cssButton.' btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>

<script type="text/javascript">
  $(document).ready(function () {
    var editor = $("#convocatorias-contenido");
    editor.wysihtml5();
    editor.parent().attr("class", "col-sm-12");

    $("input[type=checkbox]").on("click", function () {
        var checked = $(this).prop("checked");
        $(this).val(+checked);
        $(this).prev("input[type=hidden]").val(+checked);
    });
  });
</script>