<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\convocatoriasSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="convocatorias-search">
	<div class="col-md-2">
		<a href="<?php echo Url::to(['convocatorias/create']); ?>" class="btn btn-app">
			<i class="fa fa-plus"></i>Nueva Convocatoria
		</a>
	</div>
	<div class="col-md-6">
		<?php
			$f = ActiveForm::begin([
				'action'                 => Url::to(['convocatorias/']),
				'method'                 => 'post',
				'enableClientValidation' => true
			]);
		?>

		<div class="input-group input-group-sm">
			<input oninvalid="return false;" autofocus required maxLength="50" type="search" id="convocatoriasformsearch-search_element" class="form-control" name="ConvocatoriasFormSearch[search_element]" placeholder="Busqueda por folio o fecha de apertura">
			<span class="input-group-btn">
				<?php echo Html::submitButton('Buscar', ['class' => 'btn btn-info btn-flat']); ?>
			</span>
		</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>