<?php
use yii\helpers\Html;
?>
<div class="input-group input-group-sm">
	<input oninvalid="return false;" autofocus required maxLength="50" type="search" placeholder="Busqueda por numero de empleado o nombre" id="formsearch-search_element" class="form-control" name="FormSearch[search_element]">
	<span class="input-group-btn">
		<?php echo Html::submitButton('Buscar', ['class' => 'btn btn-info btn-flat']); ?>
	</span>
</div>