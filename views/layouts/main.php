<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <?= Html::csrfMetaTags() ?>
        <title>Expediente de Policias | <?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <!-- jQuery 1.11.2 JS -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <!-- favicon -->
        <link rel="shortcut icon" href="<?= Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />

        <!-- Ionicons -->
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <!-- DATA TABLES -->
        <link href="<?=Yii::$app->urlManager->createUrl(['css/datatables/dataTables.bootstrap.css'])?>" rel="stylesheet" type="text/css" />
        <link href="<?=Yii::$app->urlManager->createUrl(['css/datatables/tablas.css'])?>" rel="stylesheet" type="text/css" />

        <!-- Theme style -->
        <link href="<?=Yii::$app->urlManager->createUrl(['css/AdminLTE.min.css'])?>" rel="stylesheet" type="text/css" />

        <!-- Boostrap Datepicker -->
        <link href="<?=Yii::$app->urlManager->createUrl(['css/datepicker.css'])?>" rel="stylesheet" type="text/css" />

        <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
        <link href="<?=Yii::$app->urlManager->createUrl(['css/skins/_all-skins.min.css'])?>" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <?php $this->beginBody() ?>

        <!-- Start  - Wrapper -->
        <div class="wrapper">
            
            <!-- Start - Main Header -->
            <header class="main-header">
                
                <a href="" class="logo"><strong>Expediente</strong>Policia</a>

                <nav class="navbar navbar-static-top" role="navigation">

                    <!-- Start - Sidebar toggle button -->
                    <a href="" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Ver/Ocultar Menú</span>
                    </a>
                    <!-- End - Sidebar toggle button -->

                    <!-- Start - Navbar right menu -->
                    <div class="navbar-custom-menu">
                        
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <?php
                                    $avatar = \Yii::$app->user->identity->avatar;
                                    if(!empty($avatar)) {
                                    ?>
                                    <img src="<?php echo Yii::$app->urlManager->createUrl(['avatars/'.$avatar]); ?>" class="user-image" alt="<?=\Yii::$app->user->identity->username?>" />
                                    <?php } else { ?>
                                    <img src="<?php echo Yii::$app->urlManager->createUrl(['img/noAvatar.png']); ?>" class="user-image" alt="<?=\Yii::$app->user->identity->username?>" />
                                    <?php }?>
                                    <span class="hidden-xs"><?=\Yii::$app->user->identity->username?></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li class="user-header">
                                        <?php
                                        $avatar = \Yii::$app->user->identity->avatar;
                                        if(!empty($avatar)) {
                                        ?>
                                        <img src="<?php echo Yii::$app->urlManager->createUrl(['avatars/'.$avatar]); ?>" class="img-circle" alt="<?=\Yii::$app->user->identity->username?>" />
                                        <?php } else { ?>
                                        <img src="<?php echo Yii::$app->urlManager->createUrl(['img/noAvatar.png']); ?>" class="img-circle" alt="<?=\Yii::$app->user->identity->username?>" />
                                        <?php }?>
                                        <p>
                                            <?=\Yii::$app->user->identity->username?>
                                            <!--<small>Miembro desde Feb. 2012</small>-->
                                        </p>
                                    </li>
                                    <li class="user-body">
                                        <div class="col-xs-4 text-center">
                                            
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            
                                        </div>
                                    </li>
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="<?=Yii::$app->urlManager->createUrl(['user/member'])?>" class="btn btn-default btn-flat">Perfil</a>
                                        </div>
                                        <div class="pull-right">
                                            
                                        <?= (Yii::$app->user->isGuest)?Html::a('Iniciar sesion', 'site/login', ['class'=> 'btn btn-default btn-flat']):
                                             Html::a('Cerrar sesion', Yii::$app->urlManager->createUrl(['site/logout']), ['class'=> 'btn btn-default btn-flat'])?>                                          
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!-- End - Navbar right menu -->

                </nav>

            </header>
            <!-- End - Main Header -->

            <!-- Start - Main sidebar -->
            <aside class="main-sidebar">
                
                <section class="sidebar">
                    
                    <div class="user-panel">
                        <div class="pull-left image">
                            <?php
                            $avatar = \Yii::$app->user->identity->avatar;
                            if(!empty($avatar)) {
                            ?>
                            <img src="<?php echo Yii::$app->urlManager->createUrl(['avatars/'.$avatar]); ?>" class="img-circle" alt="<?=\Yii::$app->user->identity->username?>" />
                            <?php } else { ?>
                            <img src="<?php echo Yii::$app->urlManager->createUrl(['img/noAvatar.png']); ?>" class="img-circle" alt="<?=\Yii::$app->user->identity->username?>" />
                            <?php }?>
                        </div>
                        <div class="pull-left info">
                            <p><?= \Yii::$app->user->identity->username?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <ul class="sidebar-menu">
                        <li class="header">Menú Principal</li><i class="fa fa-angle-left pull-right"></i>
						<li <?php echo (strtolower(Yii::$app->controller->id) == 'aspirantes') ? 'class="active"' : ''; ?>><a href="<?php echo Yii::$app->urlManager->createUrl(['/aspirantes']); ?>"><i class="fa fa-users"></i> Aspirantes</a></li>
						<li class="treeview <?php echo (strtolower(Yii::$app->controller->id) == 'planes' || strtolower(Yii::$app->controller->id) == 'estimulos' || strtolower(Yii::$app->controller->id) == 'adscripciones' || strtolower(Yii::$app->controller->id) == 'sanciones' || strtolower(Yii::$app->controller->id) == 'nombramientos' || strtolower(Yii::$app->controller->id) == 'generales') ? ' active' : ''; ?>">
							<a>
								<i class="fa fa-shield"></i><span> Activos</span><i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
                                <li <?php echo (strtolower(Yii::$app->controller->id) == 'generales') ? 'class="active"' : ''; ?>><a href="<?php echo Yii::$app->urlManager->createUrl(['/generales']); ?>"><i class="fa fa-laptop"></i> Generales</a></li>
                                <li <?php echo (strtolower(Yii::$app->controller->id) == 'nombramientos') ? 'class="active"' : ''; ?>><a href="<?php echo Yii::$app->urlManager->createUrl(['/nombramientos']); ?>"><i class="fa fa-trophy"></i> Nombramientos</a></li>
								<li <?php echo (strtolower(Yii::$app->controller->id) == 'planes') ? 'class="active"' : ''; ?>><a href="<?php echo Yii::$app->urlManager->createUrl(['/planes']); ?>"><i class="fa fa-list"></i> Plan individual</a></li>
								<li <?php echo (strtolower(Yii::$app->controller->id) == 'estimulos') ? 'class="active"' : ''; ?>><a href="<?php echo Yii::$app->urlManager->createUrl(['/estimulos']); ?>"><i class="fa fa-thumbs-up"></i> Estímulos, reconocimientos, recompensas</a></li>
								<li <?php echo (strtolower(Yii::$app->controller->id) == 'adscripciones') ? 'class="active"' : ''; ?>><a href="<?php echo Yii::$app->urlManager->createUrl(['/adscripciones']); ?>"><i class="fa fa-building-o"></i> Cambio de adscripción</a></li>
								<li <?php echo (strtolower(Yii::$app->controller->id) == 'sanciones') ? 'class="active"' : ''; ?>><a href="<?php echo Yii::$app->urlManager->createUrl(['/sanciones']); ?>"><i class="fa fa-ban"></i> Sanciones</a></li>
                                <li <?php echo (strtolower(Yii::$app->controller->id) == 'licencias') ? 'class="active"' : ''; ?>><a href="<?php echo Yii::$app->urlManager->createUrl(['/licencias']); ?>"><i class="fa fa-wheelchair"></i> Licencias, permisos, comisiones e incapacidades</a></li>
							</ul>
						</li>
						<li <?php echo (strtolower(Yii::$app->controller->id) == 'bajas') ? 'class="active"' : ''; ?>><a href="<?php echo Yii::$app->urlManager->createUrl(['/bajas']); ?>"><i class="fa fa-thumbs-down"></i> Bajas</a></li>
                        <li <?php echo (strtolower(Yii::$app->controller->id) == 'historial') ? 'class="active"' : ''; ?>><a href="<?php echo Yii::$app->urlManager->createUrl(['/historial']); ?>"><i class="fa fa-clock-o"></i> Historial</a></li>
						<li <?php echo (strtolower(Yii::$app->controller->id) == 'convocatorias') ? 'class="active"' : ''; ?>><a href="<?php echo Yii::$app->urlManager->createUrl(['/convocatorias']); ?>"><i class="fa fa-pencil-square-o"></i> Convocatorias</a></li>
						<li <?php echo (strtolower(Yii::$app->controller->id) == 'configuracion') ? 'class="active"' : ''; ?>><a href="<?php echo Yii::$app->urlManager->createUrl(['/configuracion']); ?>"><i class="fa fa-gears"></i> Configuración</a></li>
                        <li <?php echo (strtolower(Yii::$app->controller->id) == 'acerca') ? 'class="active"' : ''; ?>><a href="<?php echo Yii::$app->urlManager->createUrl(['/acerca']); ?>"><i class="fa fa-info"></i> Acerca de</a></li>
                    </ul>

                </section>

            </aside>
            <!-- End - Main sidebar -->

            <!-- Start - Content Wrapper -->
            <div class="content-wrapper">
                
                <?= $content ?>
                
            </div>
            <!-- End - Content Wrapper -->

            <!-- Start - Footer -->
            <footer class="main-footer text-right">

                <strong>&copy; Unlimited Thinks <?= date('Y') ?></strong>

            </footer>
            <!-- End - Footer -->

        </div>
        <!-- End - Wrapper -->

        <?php $this->endBody() ?>

        <!-- Bootstrap 3.3.2 JS -->
        <script src="<?=Yii::$app->urlManager->createUrl(['js/bootstrap.min.js'])?>" type="text/javascript"></script>

        <!-- Boostrap Datepicker -->
        <script src="<?=Yii::$app->urlManager->createUrl(['js/bootstrap-datepicker.js'])?>" type="text/javascript"></script>

        <!-- Bootbox JS -->
        <script src="<?=Yii::$app->urlManager->createUrl(['js/bootbox.min.js'])?>" type="text/javascript"></script>

        <!-- jQuery Validate -->
        <script src="<?=Yii::$app->urlManager->createUrl(['js/jquery.validate.min.js'])?>" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="<?=Yii::$app->urlManager->createUrl(['js/app.js'])?>" type="text/javascript"></script>
        <script src="<?=Yii::$app->urlManager->createUrl(['js/daterangepicker.js'])?>" type="text/javascript"></script>
        <script src="<?=Yii::$app->urlManager->createUrl(['js/datatables/jquery.dataTables.js'])?>" type="text/javascript"></script>
        <script src="<?=Yii::$app->urlManager->createUrl(['js/datatables/dataTables.bootstrap.js'])?>" type="text/javascript"></script>
    </body>
</html>
<?php $this->endPage() ?>