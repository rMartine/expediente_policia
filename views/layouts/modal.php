<?php 
use yii\helpers\Html;
use yii\helpers\Url;
$this->registerJsFile(Yii::$app->homeUrl.'js/main/modals.js', ['depends' => [\yii\web\JqueryAsset::className()], 'position' => $this::POS_END]);
?>
<div class="modal fade" id="modal-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">Detalle</h4>
    </div>
    <div class="modal-body">
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-repeat"></i> Cancelar</button>
      <button type="button" class="btn btn-primary" data-post=""><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
    </div>
  </div>
</div>
</div>