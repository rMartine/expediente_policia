<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
	<head>
		<meta charset="<?= Yii::$app->charset ?>">
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<?= Html::csrfMetaTags() ?>
		<title>Expediente de Policias | Log in ?></title>

		<?php $this->head() ?>
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<!-- Font Awesome Icons -->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
                
		<!-- Theme style -->
                <link href="<?=Yii::$app->urlManager->createUrl(['/css/AdminLTE.min.css'])?>" rel="stylesheet" type="text/css" />

		<!-- iCheck -->
		<link href="<?=Yii::$app->urlManager->createUrl(['/css/iCheck/square/blue.css'])?>" rel="stylesheet" type="text/css" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="login-page">

		<?php $this->beginBody() ?>

		<?php echo $content; ?>
		
		<?php $this->endBody() ?>

		<!-- Bootstrap 3.3.2 JS -->
		<script src="<?=Yii::$app->urlManager->createUrl(['js/bootstrap.min.js'])?>" type="text/javascript"></script>

		<!-- iCheck -->
		<script src="<?=Yii::$app->urlManager->createUrl(['js/iCheck/icheck.min.js'])?>" type="text/javascript"></script>

		<script>
			$(function () {
				$('input').iCheck({
					checkboxClass : 'icheckbox_square-blue',
					radioClass    : 'iradio_square-blue',
					increaseArea  : '20%' // optional
				});
			});
		</script>

	</body>
</html>

<?php $this->endPage() ?>