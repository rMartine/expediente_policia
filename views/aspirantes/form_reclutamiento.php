<?php 
    use yii\bootstrap\ActiveForm;
    use app\models\LogRequerimientos;
    use app\models\Requerimientos;
    use app\models\Cumplimiento;
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;

    /* @var $this yii\web\View */
    $blockName = 'reclutamiento';
?>
<div class="row">
    <div class="col-xs-2 pull-right">
        <a class="btn btn-app" data-toggle="modal" data-target="#next-seleccion" <?= ((count($aspirante->logRequerimientosNo) > 0) || ($aspirante->idEtapa > 2)) ? 'disabled' : '' ?>>
            <i data-toggle="tooltip" data-placement="left" title="Siguiente etapa" class="glyphicon glyphicon-ok-sign"></i>Etapa Validada
        </a>
    </div>
    <div class="col-xs-2 pull-right">
        <a class="btn btn-app" data-toggle="modal" data-target="#modal-rechazar">
            <i data-toggle="tooltip" data-placement="left" title="Rechazar elemento" class="glyphicon glyphicon-ban-circle"></i>Rechazar Elemento
        </a>
    </div>
</div>
<div class="row">
    <div class="bg-navy-active">
        <h5>Lista de Requerimientos para el Aspirante</h5>
    </div>
</div>
<div class="col-xs-2 pull-left">
    <a class="btn btn-app" data-toggle="modal" data-target="#modal-reclutamiento-nuevo">
        <i data-toggle="tooltip" data-placement="left" title="Nuevo requerimiento" class="glyphicon glyphicon-plus-sign"></i>Nuevo Requerimiento
    </a>
</div>

<table id="tabla-reclutamiento" class="table table-bordered table-hover aspirante-tabla">
    <thead>
        <tr>
            <th>No</th>
            <th>Requerimiento</th>
            <th>Descripción</th>
            <th>Fecha</th>
            <th>Observaciones</th>
            <th>Cumplimiento</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php $contador = 1; ?>
        <?php foreach ( $aspirante->logRequerimientos as $log ) { ?>
        <tr requerimiento="<?= $log->id?>">
                <td><?php echo $contador++; ?></td>
                <td><?php echo $log->requerimiento->nombre; ?></td>
                <td><?php echo $log->requerimiento->descripcion ?></td>
                <td><?php echo $log->fechaText; ?></td>
                <td><?php echo $log->observaciones; ?></td>
                <td class="dato_cumplimiento"><?php echo $log->cumplimiento0->valor; ?></td>
                
                <td>
                    <a class="btn btn-block" data-toggle="modal" data-target="#modal-reclutamiento-<?= $log->id ?>">
                        <i data-toggle="tooltip" data-placement="left" title="Editar" class="glyphicon glyphicon-pencil"></i>
                    </a>
                </td>
                <td>
                    <a class="btn btn-block" data-toggle="modal" data-target="#modal-reclutamiento-elimina-<?= $log->id ?>">
                        <i data-toggle="tooltip" data-placement="left" title="Eliminar" class="glyphicon glyphicon-trash"></i>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
    <tfoot>
    </tfoot>
</table>

<div class="modal fade" id="next-seleccion" tabindex="-1" role="dialog" aria-labelledby="next-seleccion" aria-hidden="true">
    <div class="modal-dialog">
        <?php 
            $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'enableClientValidation' => false,
                'action'=>Yii::$app->homeUrl . 'aspirantes/habilita-seleccion?id=' . $aspirante->id,
                'id'=>'next-seleccion',
                'method'=>'post'
            ]);
        ?>
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Habilitar Selección</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-xs-11">
                        <p>
                            Se habilitará la siguiente etapa de captura para el aspirante <?= $aspirante->nombre . ' ' . $aspirante->apellido_paterno . ' ' . $aspirante->apellido_materno ?>, ¿Desea continuar?
                        </p>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary modal-input">SI</button>
                <button class="btn btn-default modal-input" data-dismiss="modal">NO</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<div class="modal fade" id="modal-rechazar" tabindex="-1" role="dialog" aria-labelledby="modal-rechazar" aria-hidden="true">
    <div class="modal-dialog">
        <?php 
            $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'enableClientValidation' => false,
                'action'=>Yii::$app->homeUrl . 'aspirantes/rechazar-aspirante?id=' . $aspirante->id,
                'id'=>'next-seleccion',
                'method'=>'post'
            ]);
        ?>
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Rechazar Elemento</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-xs-11">
                        <p>
                            El estatus del aspirante <?= $aspirante->nombre . ' ' . $aspirante->apellido_paterno . ' ' . $aspirante->apellido_materno ?> cambiará a RECHAZADO, ¿Desea continuar?
                        </p>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary modal-input">SI</button>
                <button class="btn btn-default modal-input" data-dismiss="modal">NO</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<div class="modal fade" id="modal-reclutamiento-nuevo" tabindex="-1" role="dialog" aria-labelledby="modal-reclutamiento-nuevo" aria-hidden="true">
    <div class="modal-dialog">
        <?php 
            $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'enableClientValidation' => false,
                'action'=>Yii::$app->urlManager->getBaseUrl() . '/log-requerimientos/create'
            ]);
        ?>
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Agregar Requerimiento a la Lista</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-xs-16">
                        <?= $form->field($log, 'idRequerimiento')
                                 ->dropDownList(
                                    ArrayHelper::map(Requerimientos::find()->select('id, nombre')
                                                                           ->all(), 'id', 'nombre'),
                                    ['class'=>'modal-input']
                                 )
                         ->label('Requerimiento');?>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">
                            Elemento
                        </label>
                        <div class="col-sm-6">
                            <input class="modal-input-info" readonly="true" type="text" value="<?= $aspirante->nombre . " " . $aspirante->apellido_paterno . " " . $aspirante->apellido_materno; ?>">
                            <input type="hidden" id="logrequerimientos-idelemento" class="form-control modal-input" name="LogRequerimientos[idElemento]" value="<?= $aspirante->id ?>">
                        </div>    
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">
                            Fecha
                        </label>
                        <div class="col-sm-8">
                            <input class="modal-input-info" readonly="true" type="text" value="<?= date('d-m-Y', time()) ?>">
                            <input type="hidden" id="logrequerimientos-fecha" class="form-control modal-input" name="LogRequerimientos[fecha]" value="<?= date('Y-m-d', time()) ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?= $form->field($log, 'cumplimiento')
                             ->dropDownList(
                                ArrayHelper::map(Cumplimiento::find()->select('id, valor')
                                                                      ->all(), 'id', 'valor'),
                                ['class'=>'modal-input'])
                             ->label('Cumplimiento');?>
                </div>
                <div class="row">
                    <?= $form->field($log, 'observaciones')
                             ->textarea(['rows' => 6,
                                         'class'=>'modal-input'])
                             ->label('Observaciones') ?>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="logrequerimientos-idelemento" class="form-control modal-input" name="idElemento" value="<?= $aspirante->id ?>">
                <button class="btn btn-primary modal-input"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                <button class="btn btn-default modal-input" data-dismiss="modal"><i class="glyphicon glyphicon-repeat"></i> Cancelar</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php foreach ( $aspirante->logRequerimientos as $log ) { ?>
    <div class="modal fade" id="modal-reclutamiento-elimina-<?= $log->id ?>" tabindex="-1" role="dialog" aria-labelledby="modal-reclutamiento-elimina-<?= $log->id ?>" aria-hidden="true">
        <div class="modal-dialog">
            <?php 
                $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'enableClientValidation' => false,
                    'action'=>Yii::$app->urlManager->getBaseUrl() . '/log-requerimientos/delete?id=' . $log->id,
                    'id'=>'next-seleccion',
                    'method'=>'post'
                ]);
            ?>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Requerimiento</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-xs-11">
                            <h5>Eliminará el requerimiento <?= $log->requerimiento->nombre ?> para el aspirante <?= $aspirante->nombre . ' ' . $aspirante->apellido_paterno . ' ' . $aspirante->apellido_materno ?>, ¿Desea continuar?</n5>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="logrequerimientos-idelemento" class="form-control modal-input" name="idElemento" value="<?= $aspirante->id ?>">
                    <button class="btn btn-danger modal-input glyphicon glyphicon-trash"> SI</button>
                    <button class="btn btn-default modal-input glyphicon glyphicon-repeat" data-dismiss="modal"> NO</button>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <div class="modal fade" id="modal-reclutamiento-<?= $log->id ?>" tabindex="-1" role="dialog" aria-labelledby="modal-reclutamiento-<?= $log->id ?>" aria-hidden="true">
        <div class="modal-dialog">
            <?php 
                $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'enableClientValidation' => false,
                    'action'=>Yii::$app->urlManager->getBaseUrl() . '/log-requerimientos/update?id=' . $log->id
                ]);
            ?>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edición de Requerimiento</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-sm-3">
                                Requerimiento
                            </label>
                            <div class="col-sm-6">
                                <input class="modal-input-info" type="text" readonly="true" value="<?= $log->requerimiento->nombre; ?>">
                                <input type="hidden" id="logrequerimientos-idrequerimiento" class="form-control modal-input" name="LogRequerimientos[idRequerimiento]" value="<?= $log->idRequerimiento ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-sm-3">
                                Elemento
                            </label>
                            <div class="col-sm-6">
                                <input class="modal-input-info" type="text" readonly="true" value="<?= $aspirante->nombre . " " . $aspirante->apellido_paterno . " " . $aspirante->apellido_materno; ?>">
                                <input type="hidden" id="logrequerimientos-idelemento" class="form-control modal-input" name="LogRequerimientos[idElemento]" value="<?= $log->idElemento ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-sm-3">
                                Fecha
                            </label>
                            <div class="col-sm-6">
                                <input class="modal-input-info" type="text" readonly="true" value="<?= $log->fechaText; ?>">
                                <input type="hidden" id="logrequerimientos-fecha" class="form-control modal-input" name="LogRequerimientos[fecha]" value="<?= $log->fecha ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?= $form->field($log, 'cumplimiento')
                                 ->dropDownList(
                                    ArrayHelper::map(Cumplimiento::find()->select('id, valor')
                                                                          ->all(), 'id', 'valor'),
                                    ['class'=>'modal-input'])
                                 ->label('Cumplimiento');?>
                    </div>
                    <div class="row">
                        <?= $form->field($log, 'observaciones')
                                 ->textarea(['rows' => 6, 'class'=>'modal-input'])
                                 ->label('Observaciones') ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="logrequerimientos-idelemento" class="form-control modal-input" name="idElemento" value="<?= $aspirante->id ?>">
                    <button class="btn btn-primary modal-input"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                    <button class="btn btn-default modal-input" data-dismiss="modal"><i class="glyphicon glyphicon-repeat"></i> Cancelar</button>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php } ?>
