<?php 
    use yii\bootstrap\ActiveForm;
    use app\models\LogRequerimientos;
    use app\models\Requerimientos;
    use app\models\ResultadosSeleccion;
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;

    /* @var $this yii\web\View */
    $blockName = 'reclutamiento';
?>

<div class="row">
    <div class="bg-navy-active">
        <h5>Posibles Resultados de las Evaluaciones</h5>
    </div>
</div>
<table id="tabla-resultados" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Concepto</th>
            <th>Descripcion</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Apto y/o Recomendable
            </td>
            <td>
                Aquel que refleja los resultados satisfactorios a los requerimientos de la totalidad de los exámenes de esta evaluación.
            </td>
        </tr>
        <tr>
            <td>
                Recomendable con Reservas
            </td>
            <td>
                Aquel que cumple con los parámetros de cualquiera de los exámenes, pero que existen características específicas, que deben marcarse y tomarse en cuenta para las actividades a desarrollar dentro de la corporación.
            </td>
        </tr>
        
        <tr>
            <td>
                No Apto
            </td>
            <td>
                Significa el incumplimiento a los requerimientos de cualquiera de los exámenes. Este resultado excluye de forma definitiva al aspirante en el procedimiento de ingreso, hasta en tanto no se expida otra convocatoria.
            </td>
        </tr>
    </tbody>
    <tfoot>
    </tfoot>
</table>
<div class="row">
    <div class="bg-navy-active">
        <h5>Evaluaciones del Proceso de Selección</h5>
    </div>
</div>

<table id="tabla-seleccion" class="table table-bordered table-hover aspirante-tabla">
    <thead>
        <tr>
            <th>No</th>
            <th>Evaluación</th>
            <th>Fecha</th>
            <th>Observaciones</th>
            <th>Resultado</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php $contador = 1; ?>
        <?php foreach ( $aspirante->logSeleccion as $log ) { ?>
            <tr  requerimiento="<?= $log->id?>">
                <td><?php echo $contador++; ?></td>
                <td><?php echo $log->idSeleccion0->nombre; ?></td>
                <td><?php echo $log->fechaText; ?></td>
                <td><?php echo $log->observaciones; ?></td>
                <td class="dato_cumplimiento" class="dato_cumplimiento"><?php echo $log->resultados0->valor; ?></td>
                <td>
                    <a class="btn btn-block" data-toggle="modal" data-target="#modal-seleccion-<?= $log->id ?>">
                        <i data-toggle="tooltip" data-placement="left" title="Editar" class="glyphicon glyphicon-pencil"></i>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
    <tfoot>
    </tfoot>
</table>

<?php foreach ( $aspirante->logSeleccion as $log ) { ?>
    <div class="modal fade" id="modal-seleccion-<?= $log->id ?>" tabindex="-1" role="dialog" aria-labelledby="modal-seleccion-<?= $log->id ?>" aria-hidden="true">
        <div class="modal-dialog">
            <?php 
                $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'enableClientValidation' => false,
                    'action'=>Yii::$app->urlManager->getBaseUrl() . '/log-seleccion/update?id=' . $log->id
                ]);
            ?>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edición de la evaluación: <?= $log->idSeleccion0->nombre ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-sm-3">
                                Evaluación
                            </label>
                            <div class="col-sm-6">
                                <input class="modal-input-info" type="text" value="<?= $log->idSeleccion0->nombre; ?>">
                                <input type="hidden" id="logseleccion-idrechazo" class="form-control modal-input" name="LogSeleccion[idSeleccion]" value="<?= $log->idSeleccion ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-sm-3">
                                Elemento
                            </label>
                            <div class="col-sm-6">
                                <input class="modal-input-info" type="text" value="<?= $aspirante->nombre . " " . $aspirante->apellido_paterno . " " . $aspirante->apellido_materno; ?>">
                                <input type="hidden" id="logseleccion-idelemento" class="form-control modal-input" name="LogSeleccion[idElemento]" value="<?= $log->idElemento ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-sm-3">
                                Fecha
                            </label>
                            <div class="col-sm-6">
                                <input class="modal-input-info" type="date" value="<?= $log->fecha; ?>" id="logseleccion-fecha" name="LogSeleccion[fecha]">                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?= $form->field($log, 'resultados')
                                 ->dropDownList(
                                    ArrayHelper::map(ResultadosSeleccion::find()->select('id, valor')
                                                                                ->all(), 'id', 'valor'),
                                    ['class'=>'modal-input'])
                                 ->label('Resultado');?>
                    </div>
                    <div class="row">
                        <?= $form->field($log, 'observaciones')
                                 ->textarea(['rows' => 6, 'class'=>'modal-input'])
                                 ->label('Observaciones') ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="logseleccion-idelemento" class="form-control modal-input" name="idElemento" value="<?= $aspirante->id ?>">
                    <button class="btn btn-primary modal-input"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                    <button class="btn btn-default modal-input" data-dismiss="modal"><i class="glyphicon glyphicon-repeat"></i> Cancelar</button>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php } ?>