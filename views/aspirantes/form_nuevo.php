<?php 
	use yii\helpers\Html;
	use yii\helpers\ArrayHelper;
	use yii\bootstrap\ActiveForm;
	use app\models\Estados;
	use app\models\CargaImagen;
	use app\models\Convocatorias;
	use app\models\Estudios;
	use yii\helpers\Url;
        use app\models\TiposElementos;
	/* @var $this yii\web\View */
	$blockName = 'nuevo';
?>

<?php
	$form = ActiveForm::begin([
		'enableClientValidation' => false,
		'id'=>'generales-form',
		'action' => Yii::$app->homeUrl . 'aspirantes/nuevo',
		'method' => 'post'
	]);
	echo $form->errorSummary($aspirante); 
	//$this->beginBlock('main'); 
?>
<div class="row">
	<div class="box-body"><div class="bg-navy-active color-palette"><h5>Datos del Aspirante</h5></div></div>
</div>
<div class="row">
	<div class="form-group col-xs-2">
		<?= $form->field($aspirante, 'rfc')
				 ->textInput([
				 	'placeholder' => 'RFC del aspirante...'
				 ])
				 ->label('RFC'); ?>
	</div>
	<div class="form-group col-xs-2">
		<?= $form->field($aspirante, 'curp')
				 ->textInput([
				 	'placeholder' => 'CURP del aspirante...'
				 ])
				 ->label('CURP'); ?>
	</div>
	<div class="form-group col-xs-2">
		<?= $form->field($aspirante, 'idConvocatoria')
				 ->dropDownList(
				 	ArrayHelper::map(Convocatorias::find()->select('id, folio')->all(), 'id', 'folio')
				 )
				 ->label('Convocatoria');?>
	</div>
	<div class="form-group col-xs-2">
		<?= $form->field($aspirante, 'idTipo')
				 ->dropDownList(
				 	ArrayHelper::map(TiposElementos::find()->select('id, nombre')->all(), 'id', 'nombre')
				 )
				 ->label('Departamento');?>
	</div>
</div>
<div class="row">
	<div class="form-group col-xs-2">
		<?= $form->field($aspirante, 'estado_civil')
				 ->dropDownList(['soltero'=>'Soltero',
				 				 'casado'=>'Casado',
				 				 'divorciado'=>'Divorciado',
				 				 'viudo'=>'Viudo'])
				 ->label('Estado Civil');?>
	</div>
	<div class="form-group col-xs-2">
		<?= $form->field($aspirante, 'fecha_nac')
			 ->textInput([
			 	'type' => 'date',
			 	'placeholder' => 'Fecha de nacimiento del aspirante...'
			 ])
			 ->label('Fecha de Nacimiento'); ?>
	</div>
	<div class="form-group col-xs-2">
		<?= $form->field($aspirante, 'idEstudios')
				 ->dropDownList(
				 	ArrayHelper::map(Estudios::find()->all(), 'id', 'grado_estudios')
				 )
				 ->label('Nivel Escolar');?>
	</div>
</div>
<div class="row">
	<div class="form-group col-xs-4">
		<?= $form->field($aspirante, 'nombre')
				 ->textInput([
				 	'placeholder' => 'Nombre del aspirante...'
				 ])
				 ->label('Nombre(s)'); ?>
	</div>
	<div class="form-group col-xs-4">
		<?= $form->field($aspirante, 'apellido_paterno')
				 ->textInput([
				 	'placeholder' => 'Apellido paterno del aspirante...'
				 ])
				 ->label('Apellido Paterno'); ?>
	</div>
	<div class="form-group col-xs-4">
		<?= $form->field($aspirante, 'apellido_materno')
				 ->textInput([
				 	'placeholder' => 'Apellido materno del aspirante...'
				 ])
				 ->label('Apellido Materno'); ?>
	</div>
</div>
<div class="row">
	<div class="form-group col-xs-2">
		<?php
			echo $form->field($aspirante, 'sexo')
					  ->dropDownList(['M'=>'Masculino', 'F'=>'Femenino'])
					  ->label('Sexo');
		?>
	</div>
	<div class="form-group col-xs-3">
		<?php
			$estados = new Estados;
			echo $form->field($aspirante, 'estado_nac')
				->dropDownList(ArrayHelper::map($estados->find()->all(), 'id', 'nombre')
				)
				->label('Estado de Nacimiento');
		?>
	</div>
	<div class="form-group col-xs-4">
		<?= $form->field($aspirante, 'municipio_nac')
				 ->textInput([
				 	'placeholder' => 'Municipio donde nació el aspirante...'
				 ])
				 ->label('Municipio de Nacimiento'); ?>
	</div>
</div>
<div class="row">
	<div class="form-group col-xs-2">
		<?= $form->field($aspirante, 'telefono')
				 ->textInput([
				 	'placeholder' => 'Número telefónico del aspirante...'
				 ])
				 ->label('Teléfono'); ?>
	</div>
	<div class="form-group col-xs-2">
		<?= $form->field($aspirante, 'movil')
				 ->textInput([
				 	'placeholder' => 'Número celular del aspirante...'
				 ])
				 ->label('Celular'); ?>
	</div>
	<div class="form-group col-xs-2">
		<?= $form->field($aspirante, 'email')
				 ->textInput([
				 	'placeholder' => 'Email del aspirante...'
				 ])
				 ->label('Correo Electrónico'); ?>
	</div>
</div>
<div class="row">
	<div class="box-body"><div class="bg-navy-active color-palette"><h5>Domicilio del Aspirante</h5></div></div>
</div>
<div class="row">
	<div class="form-group col-xs-2">
		<?php
			$estados = new Estados;
			echo $form->field($aspirante, 'domicilio_edo')
				->dropDownList(ArrayHelper::map($estados->find()->all(), 'id', 'nombre'))
				->label('Estado');
		?>
	</div>
	<div class="form-group col-xs-2">
		<?= $form->field($aspirante, 'domicilio_mpo')
				 ->textInput([
				 	'placeholder' => 'Domicilio...'
				 ])
				 ->label('Municipio'); ?>
	</div>
</div>
<div class="row">
	<div class="form-group col-xs-2">
		<?= $form->field($aspirante, 'domicilio_col')
				 ->textInput([
				 	'placeholder' => 'Domicilio...'
				 ])
				 ->label('Colonia'); ?>
	</div>
	<div class="form-group col-xs-2">
		<?= $form->field($aspirante, 'domicilio_cp')
				 ->textInput([
				 	'placeholder' => 'Domicilio...'
				 ])
				 ->label('C. P.'); ?>
	</div>
	<div class="form-group col-xs-2">
		<?= $form->field($aspirante, 'domicilio_calle')
				 ->textInput([
				 	'placeholder' => 'Domicilio...'
				 ])
				 ->label('Calle'); ?>
	</div>
	<div class="form-group col-xs-2">
		<?= $form->field($aspirante, 'domicilio_next')
				 ->textInput([
				 	'placeholder' => 'Domicilio...'
				 ])
				 ->label('Número Exterior'); ?>
	</div>
	<div class="form-group col-xs-2">
		<?= $form->field($aspirante, 'domicilio_nint')
				 ->textInput([
				 	'placeholder' => 'Domicilio...'
				 ])
				 ->label('Número Interior'); ?>
	</div>
</div>
<div class="row">
	<input type="hidden" name="op" id="op" value="" />
	<div class="col-xs-2 pull-right">
		<button type="button" id="btn-cancelar" class="btn btn-default"><i class="glyphicon glyphicon-repeat"></i> Cancelar</button>
	</div>
	<div class="col-xs-2 pull-right">
		<button type="button" id="btn-nuevo" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
	</div>
</div>
<?php ActiveForm::end(); ?>