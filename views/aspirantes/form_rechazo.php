<?php 
    use yii\bootstrap\ActiveForm;
    use app\models\LogSeleccion;
    use app\models\Requerimientos;
    use app\models\Cumplimiento;
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;

    /* @var $this yii\web\View */
    $blockName = 'reclutamiento';
?>
<div class="row">
    <div class="col-xs-2 pull-right">
        <a class="btn btn-app" data-toggle="modal" data-target="#next-formacion" <?= ((count($aspirante->logRechazosSi) > 0) || (count($aspirante->logSeleccionNo) > 0) || ($aspirante->idEtapa > 3)) ? 'disabled' : '' ?>>
            <i data-toggle="tooltip" data-placement="left" title="Siguiente etapa" class="glyphicon glyphicon-ok-sign"></i>Etapa Validada
        </a>
    </div>
    <div class="col-xs-2 pull-right">
        <a class="btn btn-app" data-toggle="modal" data-target="#modal-rechazar2">
            <i data-toggle="tooltip" data-placement="left" title="Rechazar elemento" class="glyphicon glyphicon-ban-circle"></i>Rechazar Elemento
        </a>
    </div>
</div>
<div class="row">
    <div class="bg-navy-active">
        <h5>Motivos de Desistimiento o Rechazo</h5>
    </div>
</div>

<table id="tabla-rechazo" class="table table-bordered table-hover aspirante-tabla">
    <thead>
        <tr>
            <th>No</th>
            <th>Motivo</th>
            <th>Fecha</th>
            <th>Observaciones</th>
            <th>Aplica</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php $contador = 1; ?>
        <?php foreach ( $aspirante->logRechazos as $log ) { ?>
            <tr requerimiento="<?= $log->id?>">
                <td><?php echo $contador++; ?></td>
                <td><?php echo $log->idRechazo0->nombre; ?></td>
                <td><?php echo $log->fechaText; ?></td>
                <td><?php echo $log->observaciones; ?></td>
                <td class="dato_cumplimiento" class="dato_cumplimiento"><?php echo $log->rechazado0->valor; ?></td>
                
                <td>
                    <a class="btn btn-block" data-toggle="modal" data-target="#modal-rechazo-<?= $log->id ?>">
                        <i data-toggle="tooltip" data-placement="left" title="Editar" class="glyphicon glyphicon-pencil"></i>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
    <tfoot>
    </tfoot>
</table>

<div class="modal fade" id="next-formacion" tabindex="-1" role="dialog" aria-labelledby="next-formacion" aria-hidden="true">
    <div class="modal-dialog">
        <?php 
            $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'enableClientValidation' => false,
                'action'=>Yii::$app->homeUrl . 'aspirantes/habilita-formacion?id=' . $aspirante->id,
                'id'=>'next-formacion',
                'method'=>'post'
            ]);
        ?>
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Habilitar Selección</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-xs-11">
                        <h5>Se habilitará la siguiente etapa de captura para el aspirante <?= $aspirante->nombre . ' ' . $aspirante->apellido_paterno . ' ' . $aspirante->apellido_materno ?>, ¿Desea continuar?</n5>
                        <div class="col-sm-6">
                            <?= $form->field($aspirante, 'id')
                                     ->textInput(['type'=>'hidden'])
                                     ->label(false); ?>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary modal-input">SI</button>
                <button class="btn btn-default modal-input" data-dismiss="modal">NO</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php foreach ( $aspirante->logRechazos as $log ) { ?>
    <div class="modal fade" id="modal-rechazo-<?= $log->id ?>" tabindex="-1" role="dialog" aria-labelledby="modal-rechazo-<?= $log->id ?>" aria-hidden="true">
        <div class="modal-dialog">
            <?php 
                $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'enableClientValidation' => false,
                    'action'=>Yii::$app->urlManager->getBaseUrl() . '/log-rechazos/update?id=' . $log->id
                ]);
            ?>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edición de los motivos de Desistimiento/Rechazo: <?= $log->idRechazo0->nombre ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-sm-3">
                                Motivo de Rechazo o Desistimiento
                            </label>
                            <div class="col-sm-6">
                                <input class="modal-input-info" type="text" value="<?= $log->idRechazo0->nombre; ?>">
                                <input type="hidden" id="logrechazos-idrechazo" class="form-control modal-input" name="LogRechazos[idRechazo]" value="<?= $log->idRechazo ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-sm-3">
                                Elemento
                            </label>
                            <div class="col-sm-6">
                                <input class="modal-input-info" type="text" value="<?= $aspirante->nombre . " " . $aspirante->apellido_paterno . " " . $aspirante->apellido_materno; ?>">
                                <input type="hidden" id="logrechazos-idelemento" class="form-control modal-input" name="LogRechazos[idElemento]" value="<?= $log->idElemento ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-sm-3">
                                Fecha
                            </label>
                            <div class="col-sm-6">
                                <input class="modal-input-info" type="date" value="<?= $log->fecha; ?>" id="logrechazos-fecha" name="LogRechazos[fecha]">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?= $form->field($log, 'rechazado')
                                 ->dropDownList(
                                    ArrayHelper::map(Cumplimiento::find()->select('id, valor')
                                                                          ->all(), 'id', 'valor'),
                                    ['class'=>'modal-input'])
                                 ->label('Aplica');?>
                    </div>
                    <div class="row">
                        <?= $form->field($log, 'observaciones')
                                 ->textarea(['rows' => 6, 'class'=>'modal-input'])
                                 ->label('Observaciones') ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="logrechazos-idelemento" class="form-control modal-input" name="idElemento" value="<?= $aspirante->id ?>">
                    <button class="btn btn-primary modal-input"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                    <button class="btn btn-default modal-input" data-dismiss="modal"><i class="glyphicon glyphicon-repeat"></i> Cancelar</button>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php } ?>

<div class="modal fade" id="modal-rechazar2" tabindex="-1" role="dialog" aria-labelledby="modal-rechazar2" aria-hidden="true">
    <div class="modal-dialog">
        <?php 
            $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'enableClientValidation' => false,
                'action'=>Yii::$app->homeUrl . 'aspirantes/rechazar-aspirante?id=' . $aspirante->id,
                'id'=>'next-seleccion',
                'method'=>'post'
            ]);
        ?>
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Rechazar Elemento</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-xs-11">
                        <p>
                            El estatus del aspirante <?= $aspirante->nombre . ' ' . $aspirante->apellido_paterno . ' ' . $aspirante->apellido_materno ?> cambiará a RECHAZADO, ¿Desea continuar?
                        </p>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary modal-input">SI</button>
                <button class="btn btn-default modal-input" data-dismiss="modal">NO</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>