
<?php
	use yii\helpers\Url;
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;

	/* @var $this yii\web\View */
	$this->registerJsFile(Yii::$app->homeUrl.'js/datatables/tablas.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
	$this->title = 'Aspirantes';
?>

<section class="content-header">

	<h1>
		<?php echo $this->title; ?>
		<small> Alta y seguimiento de nuevos aspirantes</small>
	</h1>

	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::$app->homeUrl; ?>"><i data-toggle="tooltip" data-placement="left" title="Regresar a la página de inicio" class="fa fa-home"></i> Inicio</a></li>
		<li class="active"><?php echo $this->title; ?></li>
	</ol>

</section>

<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<div class="col-md-2">
				<a href="<?php echo Yii::$app->homeUrl . 'aspirantes/nuevo'; ?>" class="btn btn-app">
					<i data-toggle="tooltip" data-placement="left" title="Agregar un nuevo aspirante" class="fa fa-plus"></i>Nuevo Aspirante
				</a>
			</div>
			<div class="col-md-6">
				<?php
					$f = ActiveForm::begin([
						'action'                 => Yii::$app->homeUrl . 'aspirantes',
						'method'                 => 'post',
						'enableClientValidation' => true
					]);
				?>

				<div class="input-group input-group-sm">
					<input type="search" id="bajasformsearch-search_element" class="form-control" name="BajasFormSearch[search_element]" placeholder="Busqueda por nombre, folio o convocatoria de un elemento...">
					<span class="input-group-btn">
						<?php echo Html::submitButton('Buscar', ['class' => 'btn btn-info btn-flat']); ?>
					</span>
				</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>

		<div class="box-body">
			<div class="box">
				<table id="resultados" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Aspirante</th>
							<th>Folio</th>
							<th>Convocatoria</th>
							<th>Fecha de Registro</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php $contador = 1; ?>
						<?php foreach ( $arrElementos as $oElemento ) { ?>
							<tr>
								<td><?php echo $contador++; ?></td>
								<td><?php echo $oElemento->nombre . ' ' . $oElemento->apellido_paterno . ' ' . $oElemento->apellido_materno; ?></td>
								<td><?php echo $oElemento->folio; ?></td>
								<td><?php echo $oElemento->folioConvocatoria; ?></td>
								<td><?php echo $oElemento->fechaIngresoText; ?></td>
								<td>
									<?php
										$f = ActiveForm::begin([
											'action' => Yii::$app->homeUrl . 'aspirantes/detalle',
											'method' => 'post'
										]);
									?>
									<input type="hidden" name="idElemento" value="<?php echo $oElemento->id; ?>">
									<input type="hidden" name="campo" value="-1">
									<button class="btn btn-default"><i data-toggle="tooltip" data-placement="left" title="Abrir el expediente del aspirante" class="fa fa-folder-open"></i> Abrir expediente</button>
									<?php ActiveForm::end(); ?>
								</td>
							</tr>
						<?php } ?>
					</tbody>
					<tfoot>
                    </tfoot>
				</table>
			</div>
		</div>

		<div class="box-footer">
		</div>

	</div>
</section>