<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
$this->title = 'Aspirantes';
$this->registerJsFile(Yii::$app->urlManager->getBaseUrl() . '/js/aspirantes/aspirantes-nuevo.js');
?>

<section class="content-header">

	<h1>
		<?php echo $this->title . ':'; ?>
		<h4><i class="fa fa-plus"></i> Registro de un nuevo aspirante</h3>
	</h1>

	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::$app->homeUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
		<li><a href="<?php echo Yii::$app->homeUrl . 'aspirantes/'; ?>"><i class="fa fa-users"></i><?php echo $this->title; ?></a></li>
		<li class="active">Nuevo</li>
	</ol>

</section>

<section class="content">
	<div class="box">
		<div class="box-body">
			<?= $this->render('form_nuevo', ['aspirante' => $aspirante]); ?>
		</div>
		<div class="box-footer">
		</div>
	</div>
</section>