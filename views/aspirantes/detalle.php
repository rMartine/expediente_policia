<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
	$this->title = 'Aspirantes';
	$this->registerJsFile(Yii::$app->urlManager->getBaseUrl() . '/js/aspirantes/aspirantes.js');
	$this->registerCssFile(Yii::$app->urlManager->getBaseUrl() . '/css/aspirantes/aspirantes.css');
    $this->registerJsFile(Yii::$app->homeUrl.'js/datatables/aspirantes-tablas.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<section class="content-header">

	<h1>
		<?php echo $this->title . ':'; ?>
		<h4><i class="fa fa-folder-open"></i> <?php echo $aspirante->nombre . ' ' .  $aspirante->apellido_paterno . ' ' . $aspirante->apellido_materno; ?></h3>
	</h1>

	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::$app->homeUrl; ?>"><i data-toggle="tooltip" data-placement="left" title="Regresar a la página de inicio" class="fa fa-home"></i> Inicio</a></li>
		<li><a class='main_aspirantes_page' href="<?php echo Yii::$app->homeUrl . 'aspirantes/'; ?>"><i data-toggle="tooltip" data-placement="left" title="Regresar a la lista de aspirantes" class="fa fa-users"></i><?php echo $this->title; ?></a></li>
		<li class="active"><?php echo $aspirante->folio; ?></li>
	</ol>

</section>

<section class="content">
	<div class="box">
		<div class="box-body">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li <?= ($pestana == 'generales' ? 'class="active"' : '') ?>><a href="#tab_1" data-toggle="tab">Datos Generales</a></li>
					<li <?= ($pestana == 'reclutamiento' ? 'class="active"' : '') ?>><a href="#tab_2" data-toggle="tab">Reclutamiento</a></li>
					<?php if($aspirante->idEtapa >= 3) { ?><li <?= ($pestana == 'seleccion' ? 'class="active"' : '') ?>><a href="#tab_3" data-toggle="tab">Selección</a></li><?php } ?>
					<?php if($aspirante->idEtapa >= 4) { ?><li <?= ($pestana == 'formacion' ? 'class="active"' : '') ?>><a href="#tab_4" data-toggle="tab">Formación Inicial</a></li><?php } ?>
					<?php if($aspirante->idEtapa >= 5) { ?><li <?= ($pestana == 'nombramiento' ? 'class="active"' : '') ?>><a href="#tab_5" data-toggle="tab">Nombramiento y Certificación</a></li><?php } ?>
				</ul>
                <div class="tab-content main-aspirantes" memberId='<?= $aspirante->id?>' lastPage='<?=$pagina?>'>
					<div class="tab-pane<?= ($pestana == 'generales' ? ' active' : '') ?>" id="tab_1">
						<?= $this->render('form_detalle', ['aspirante' => $aspirante]); ?>
					</div><!-- /.tab-pane -->
					<div class="tab-pane<?= ($pestana == 'reclutamiento' ? ' active' : '') ?> form_reclutamiento" content="<?=Yii::$app->urlManager->getBaseUrl() . '/aspirantes/get-form-reclutamiento?idElemento='.$aspirante->id?>"  id="tab_2">
						<?= $this->render('form_reclutamiento', ['aspirante' => $aspirante]); ?>
					</div><!-- /.tab-pane -->
					<div class="tab-pane<?= ($pestana == 'seleccion' ? ' active' : '')  ?> " content="<?=Yii::$app->urlManager->getBaseUrl() . '/aspirantes/get-form-rechazo?idElemento='.$aspirante->id.'|'.Yii::$app->urlManager->getBaseUrl() . '/aspirantes/get-form-seleccion?idElemento='.$aspirante->id?>" id="tab_3">
						<?= $this->render('form_rechazo', ['aspirante' => $aspirante]); ?>
						<?= $this->render('form_seleccion', ['aspirante' => $aspirante]); ?>
					</div><!-- /.tab-pane -->
					<div class="tab-pane<?= ($pestana == 'formacion' ? ' active' : '') ?>" content="<?=Yii::$app->urlManager->getBaseUrl() . '/aspirantes/get-form-formacion?idElemento='.$aspirante->id?>" id="tab_4">
						<?= $this->render('form_formacion', ['logCursos' => $logCursos, 'cursos' => $cursos, 'aspirante' => $aspirante]); ?>
					</div><!-- /.tab-pane -->
					<div class="tab-pane<?= ($pestana == 'nombramiento' ? ' active' : '') ?>" id="tab_5">
						<?= $this->render('form_nombramientos', ['logNombramientos' => $logNombramientos, 'aspirante' => $aspirante]); ?>
					</div><!-- /.tab-pane -->
				</div><!-- /.tab-content -->
			</div><!-- nav-tabs-custom -->
		</div>
		<div class="box-footer">
		</div>
	</div>
</section>
<script>   
    
    $('body').on('submit','form',function(event){
        console.debug('submittites');
        event.preventDefault();
        action = $(this).attr('action'); 
        ajax = (action.indexOf("?") == -1)?'?ajax=1':'&ajax=1';
        action = $(this).attr('action') + ajax;
        
        idElemento = $('.main-aspirantes').attr('memberId');
        newPage = '/expediente_policia/web/aspirantes/detalle?idElemento='+idElemento;
        
        if(action.indexOf("log-requerimientos") != -1) {
            tableIndex = 'tabla-reclutamiento'; 
            pestana = 'reclutamiento';            
            curPage = window.location.pathname;
            page = tables[tableIndex].fnPagingInfo().iPage;
            newPage = curPage+'?idElemento='+idElemento+'&pestana='+pestana+'&pagina='+page;                  
        }
        
        if(action.indexOf("log-rechazos") != -1) {
            tableIndex = 'tabla-rechazo'; 
            pestana = 'seleccion';
            curPage = window.location.pathname;
            page = tables[tableIndex].fnPagingInfo().iPage;
            newPage = curPage+'?idElemento='+idElemento+'&pestana='+pestana+'&pagina='+page;            
        }
        if(action.indexOf("log-seleccion") != -1) {
            tableIndex = 'tabla-seleccion';
            pestana = 'seleccion';
            curPage = window.location.pathname;
            page = tables[tableIndex].fnPagingInfo().iPage;
            newPage = curPage+'?idElemento='+idElemento+'&pestana='+pestana+'&pagina='+page; 
        }
        
        console.log(action);
        console.log(newPage);
       
        
        $.ajax({
               type     :'POST',
               cache    : false,
               url  : action,//'user/create?partial=true',
               data : $(this).serialize(),
               success  : function(response) {
                    if(action.indexOf('rechazar-aspirante') != -1) {
                         window.location = $('.main_aspirantes_page').attr('href');
                     } else {
                         window.location = newPage;
                     }
                   
               }
               });
    });
    
    var loadie_index = 0
    var strInsert = '';
    function reloadGrid(target){
        daddy = $('.tab-pane.active');
        url = $('.tab-pane.active').attr('content');
        urls = url.split('|');
        loadie_index = 0;
        strInsert = '';
        loadie(urls,daddy);
    }
    
    function loadie(urls,daddy) {
        if(loadie_index < urls.length) {    
            url = urls[loadie_index];
            loadie_index++;
            $.ajax({
                 type     :'POST',
                 cache    : false,
                 url  : url,
                 success  : function(response) {/*
                     console.debug(response);*/
                    strInsert += response;         
                    loadie(urls,daddy);
                 }
            });
        } else {
            daddy.html(strInsert);
            $('.tab-pane.active .aspirante-tabla').dataTable({
                    "iDisplayLength": 5,
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "displayStart": 6,
                    "bInfo": true,
                    "bAutoWidth": false,
                    "oLanguage":{
                            "oPaginate":{
                                    "sFirst":"Primero",
                                    "sLast":"Último",
                                    "sNext":"Siguiente",
                                    "sPrevious":"Anterior"},
                            "sEmptyTable":"Su búsqueda no arrojó resultados",
                            "sInfo":"Mostrando _START_ a _END_ registros de un total de _TOTAL_",
                            "sInfoEmpty":"Mostrando 0 a 0 registros de un total de 0",
                            "sInfoFiltered":"(filtrando de un total de _MAX_ registros)"
                    }
            });
        }
    }
</script>
    
    