<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Nombramientos;
use app\models\Adscripciones;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
* @var yii\web\View $this
* @var app\models\LogNombramientos $logNombramientos
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="log-nombramientos-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal',
                                     'enableClientValidation' => false,
                                     'action'=>Yii::$app->homeUrl . 'aspirantes-nombramientos/create?idElemento=' . $aspirante->id,
                                     'id'=>'asigna-nombramiento',
                                     'method'=>'post']); ?>

    <div class="row">
        <?php echo $form->errorSummary($logNombramientos); ?>
        <p>
			<?= $form->field($logNombramientos, 'idElemento')->textInput(['type'=>'hidden', 'value'=>$aspirante->id])->label(false) ?>

            <div class="form-group required">
                <label class="control-label col-sm-3" for="lognombramientos-clave_empleado">Nueva Clave de Empleado</label>
                <div class="col-sm-6">
                    <input type="text" id="lognombramientos-clave_empleado" class="form-control" name="clave_empleado" maxlength="50">
                    <div class="help-block help-block-error "></div>
            </div>
            </div>

            <?= $form->field($logNombramientos, 'idTipo')
                ->label('Nombramiento Otorgado')
                ->dropDownList(
                    ArrayHelper::map(Nombramientos::find()->select('id, grado')->all(), 'id', 'grado'),
                    ['prompt' => '--Nombramiento--']
                )
            ?>
            <?= $form->field($logNombramientos, 'idAdscripcion')
                ->label('Adscripción del Elemento')
                ->dropDownList(
                    ArrayHelper::map(Adscripciones::find()->select('id, nombre')->all(), 'id', 'nombre'),
                    ['prompt' => '--Adscripcion--']
                )
            ?>
			<?= $form->field($logNombramientos, 'fecha')->textInput(['type'=>'date'])->label('Fecha de Ingreso') ?>
			<?= $form->field($logNombramientos, 'comision')->textarea(['rows' => 6])->label('Comisión Otorgada') ?>
			<?= $form->field($logNombramientos, 'formacion_inicial')->textInput(['maxlength' => 4,
                                                                                 'value'=>number_format($aspirante->promedioFormacionInicial(), 2, '.', ''),
                                                                                 'readonly'=>'true']) ?>
			<?= $form->field($logNombramientos, 'oficio_nombramiento')->textInput(['maxlength' => 50])->label('Oficio de Nombramiento') ?>
			<?= $form->field($logNombramientos, 'observaciones')->textarea(['rows' => 6]) ?>
        </p>

        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal-nombra"><i class="glyphicon glyphicon-certificate"></i> Otorgar Nombramiento</button>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<div class="modal fade" id="modal-nombra" tabindex="-1" role="dialog" aria-labelledby="modal-pulgar-der" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title modal-title-carga">Nombramiento del aspirante <?= $aspirante->nombre . ' ' . $aspirante->apellido_paterno . ' ' . $aspirante->apellido_materno; ?></h4>
            </div>
            <div class="modal-body">
                <p>¿Desea asignar un nuevo nombramiento? Una vez que el nombramiento ha sido otorgado, solo podrá dar seguimiento a la carrera del elemento desde el menú "ACTIVOS"</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="btn-nombrar">Si</button>
                <button class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>