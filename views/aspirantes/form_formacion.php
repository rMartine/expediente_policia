<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Cursos;
use app\models\LogCursos;
$this->registerJsFile(Yii::$app->homeUrl.'js/main/modals.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
//$this->registerJsFile(Yii::$app->homeUrl.'js/datatables/tablas.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$blockName = 'formacion';
?>
<div class="row">
	
    <div class="col-xs-2 pull-right">
        <a class="btn btn-app" data-toggle="modal" data-target="#next-nombramiento" <?= (($aspirante->reprobadoFormacionInicial() > 0) || ($aspirante->idEtapa > 4) || ($aspirante->promedioFormacionInicial() < 8)) ? 'disabled' : '' ?>>
            <i data-toggle="tooltip" data-placement="left" title="Siguiente etapa" class="glyphicon glyphicon-ok-sign"></i>Etapa Validada
        </a>
    </div>
    <div class="col-xs-2 pull-right">
	    <a class="btn btn-app" data-toggle="modal" data-target="#modal-rechazar3">
	        <i data-toggle="tooltip" data-placement="left" title="Rechazar elemento" class="glyphicon glyphicon-ban-circle"></i>Rechazar Elemento
	    </a>
	</div>
</div>
<div class="">
	<div class="bg-navy-active">
		<h5>Agregar nuevo curso al expediente del aspirante</h5>
	</div>
	<?php $form = ActiveForm::begin([
		'action' => Url::to(['aspirantes-formacion/create']),
		'method' => 'post',
		'enableClientValidation' => false]);
	?>
	<div class="row">
		<?php echo $form->errorSummary($logCursos); ?>
	</div>
	<div class="row">
		<?php $this->beginBlock($blockName); ?>
		<div class="col-md-2">
			<?= $form->field($logCursos, 'idCurso')
				->label('Nombre del curso')
		        ->dropDownList(
		        	ArrayHelper::map(Cursos::find()->where('idEtapa=4')->all(), 'id', 'nombre'),
		        	['prompt' => '--cursos--']
		        )
	        ?>
		</div>
		<div class="col-md-2">
			<?= $form->field($logCursos, 'calificacion')->textInput(['type'=>'number', 'max'=>'10', 'step'=>'0.01']); ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($logCursos, 'fecha')->textInput(['type' => 'date']); ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($logCursos, 'fecha_fin')->textInput(['type' => 'date']); ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($logCursos, 'observaciones')->textarea(); ?>
		</div>
		<div class="col-md-2">
			<label class="control-label"></label>
			<span class="">
				<?php echo Html::submitButton('', ['class' => 'btn btn-info btn-block fa fa-plus-circle']); ?>
			</span>
			<?= $form->field($logCursos, 'idElemento')->label(false)->textInput(['type' => 'hidden', 'value' => $aspirante->id]); ?>
			<?= $form->field($logCursos, 'cumplimiento')->label(false)->textInput(['type' => 'hidden', 'value' => 1]); ?>
		</div>
		<?php $this->endBlock(); ?>
		<?php ActiveForm::end(); ?>
		<p>
			<?= $this->blocks[$blockName]; ?>
		</p>
	</div>
	<div class="bg-navy-active">
		<h5>Histórico de formación continua</h5>
	</div>
	<?php
		if ($aspirante->promedioFormacionInicial() > 8) {
	?>
			<button class="btn bg-olive btn-flat margin pull-right">Promedio: <?= number_format($aspirante->promedioFormacionInicial(), 2, '.', ''); ?></button>
	<?php
		} elseif ($aspirante->promedioFormacionInicial() == 8) {
	?>
			<button class="btn bg-orange btn-flat margin pull-right">Promedio: <?= number_format($aspirante->promedioFormacionInicial(), 2, '.', ''); ?></button>
	<?php
		} else {
	?>
			<button class="btn bg-red btn-flat margin pull-right">Promedio: <?= number_format($aspirante->promedioFormacionInicial(), 2, '.', ''); ?></button>
	<?php
		}
	?>
	<table id="resultados" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th>Nombre del curso</th>
				<th>Duracion</th>
				<th>Importancia</th>
				<th>Calificacion</th>
				<th>Fecha</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($aspirante->logCursos) > 0) { ?>
				<?php $contador = 1; ?>
				<?php foreach ( $aspirante->logCursos as $logCurso ) { ?>
					<tr>
						<td><?php echo $contador++; ?></td>
						<td><?php echo $logCurso->curso->nombre; ?></td>
						<td><?php echo $logCurso->curso->duracionText ?></td>
						<td><?php echo $logCurso->curso->obligatorioText; ?></td>
						<td><?php echo $logCurso->calificacion; ?></td>
						<td><?php echo $logCurso->fechaText; ?></td>
						<td>
							<a class="btn btn-block" data-toggle="modal" data-target="#modal-model" data-modal-type="edit" data-action="<?php echo Url::to(['aspirantes-formacion/update', 'id' => $logCurso->id]) ?>">
								<span data-toggle="tooltip" data-placement="left" title="Editar" class="glyphicon glyphicon-pencil"></span>
							</a>
						</td>
						<td>
							<a class="btn btn-block" data-toggle="modal" data-target="#modal-model" data-modal-type="remove" data-action="<?php echo Url::to(['aspirantes-formacion/delete', 'id' => $logCurso->id]) ?>">
								<span data-toggle="tooltip" data-placement="left" title="Eliminar" class="glyphicon glyphicon-trash"></span>
							</a>
						</td>
					</tr>
				<?php } ?>
			<?php } ?>
		</tbody>
		<tfoot>
        </tfoot>
	</table>
</div>
<?= $this->render('@app/views/layouts/modal', []); ?>

<div class="modal fade" id="next-nombramiento" tabindex="-1" role="dialog" aria-labelledby="next-nombramiento" aria-hidden="true">
    <div class="modal-dialog">
        <?php 
            $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'enableClientValidation' => false,
                'action'=>Yii::$app->homeUrl . 'aspirantes/habilita-nombramiento?id=' . $aspirante->id,
                'id'=>'next-nombramiento',
                'method'=>'post'
            ]);
        ?>
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Habilitar Nombramiento</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-xs-11">
                        <h5>Se habilitará la siguiente etapa de captura para el aspirante <?= $aspirante->nombre . ' ' . $aspirante->apellido_paterno . ' ' . $aspirante->apellido_materno ?>, ¿Desea continuar?</n5>
                        <div class="col-sm-6">
                            <?= $form->field($aspirante, 'id')
                                     ->textInput(['type'=>'hidden'])
                                     ->label(false); ?>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary modal-input">SI</button>
                <button class="btn btn-default modal-input" data-dismiss="modal">NO</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<div class="modal fade" id="modal-rechazar3" tabindex="-1" role="dialog" aria-labelledby="modal-rechazar2" aria-hidden="true">
    <div class="modal-dialog">
        <?php 
            $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'enableClientValidation' => false,
                'action'=>Yii::$app->homeUrl . 'aspirantes/rechazar-aspirante?id=' . $aspirante->id,
                'id'=>'next-seleccion',
                'method'=>'post'
            ]);
        ?>
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Rechazar Elemento</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-xs-11">
                        <p>
                            El estatus del aspirante <?= $aspirante->nombre . ' ' . $aspirante->apellido_paterno . ' ' . $aspirante->apellido_materno ?> cambiará a RECHAZADO, ¿Desea continuar?
                        </p>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary modal-input">SI</button>
                <button class="btn btn-default modal-input" data-dismiss="modal">NO</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>