<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\LogSancionesSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="log-sanciones-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'idElemento') ?>

		<?= $form->field($model, 'idSancion') ?>

		<?= //$form->field($model, 'idCausa') ?>

		<?= $form->field($model, 'fecha') ?>

		<?php // echo $form->field($model, 'duracion') ?>

		<?php // echo $form->field($model, 'observaciones') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
