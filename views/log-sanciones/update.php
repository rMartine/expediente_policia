<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\LogSanciones $model
 */

$this->title = 'Log Sanciones Update ' . $model->id . '';
$this->params['breadcrumbs'][] = ['label' => 'Log Sanciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="log-sanciones-update">
    <?php echo $this->render('_form', [
		'model' => $model,
		'formType' => 'edit',
	]); ?>
</div>
