
<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Sanciones;
use app\models\Causas;

/* @var $this yii\web\View */
$this->registerJsFile(Yii::$app->homeUrl.'js/datatables/tablas.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->urlManager->getBaseUrl() . '/js/activos/sanciones.js');
$this->registerCssFile(Yii::$app->homeUrl . '/css/activos/planes.css');
$urlIndex = Url::to(['sanciones/']);
$this->title = 'Sanciones';
$blockName = 'sanciones';
?>
<section class="content-header">

    <h1>
        <?php echo $this->title; ?>
        <small></small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="<?php echo Yii::$app->homeUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
        <li class="active"><?php echo $this->title; ?></li>
    </ol>

</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <?= $this->render('//planes/formacion_header', ['elemento' => $elemento]); ?>
        </div>
        <div class="box-body">
            <div class="bg-navy-active">
                <h5>Agregar nueva sanción al elemento</h5>
            </div>
            <?php $form = ActiveForm::begin([
                'action' => Url::to(['log-sanciones/', 'id' => $elemento->id]),
                'method' => 'post',
                'enableClientValidation' => true]);
            ?>
            <div class="row">
                <?php echo $form->errorSummary($model, ['header' => 'Por favor corrija los errores:']); ?>
            </div>
            <div>
                <?php $this->beginBlock($blockName); ?>
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($model, 'idSancion')
                            ->label('Tipo de sanción')
                            ->dropDownList(
                                ArrayHelper::map(Sanciones::find()->all(), 'id', 'sancion'),
                                ['prompt' => 'Tipo de Sanción']
                            )
                        ?>
                    </div>
                    <div class="col-md-2 box-fecha-inicio">
                        <?= $form->field($model, 'fecha_inicio')->label('Fecha Inicio')->textInput(['type' => 'date']) ?>
                    </div>
                    <div class="col-md-2 box-fecha-fin">
                        <?= $form->field($model, 'fecha_fin')->label('Fecha Final')->textInput(['type' => 'date']) ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'causa')->label('Causa')->textInput(['type' => 'text']) ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model, 'fecha')->textInput(['type' => 'date']); ?>
                    </div>
                </div>
                <div class="row box-otra-sancion">
                    <div class="col-md-4">
                        <?= $form->field($model, 'otra_sancion')->label('Otra Sanción')->textInput(['type' => 'text', 'placeholder' => 'Otra Sanción']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <?= $form->field($model, 'observaciones')->textarea(); ?>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label"></label>
                        <span class="">
                            <?php echo Html::submitButton('', ['class' => 'btn btn-info btn-block fa fa-plus-circle']); ?>
                        </span>
                        <?= $form->field($model, 'idElemento')->label('')->textInput(['type' => 'hidden', 'value' => $elemento->id]); ?>
                    </div>
                </div>
                <?php $this->endBlock(); ?>
                <?php ActiveForm::end(); ?>
                <p>
                    <?= $this->blocks[$blockName]; ?>
                </p>
            </div>
            <h4>Histórico de Sanciones</h4>
            <?= $this->render('@app/views/layouts/modal', []); ?>
            <div class="bg-navy-active">
                <h5>Histórico de sanciones</h5>
            </div>
            <table id="resultados" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Tipo sanción</th>
                        <th class="text-center">Fecha Inicio</th>
                        <th class="text-center">Fecha Fin</th>
                        <th class="text-center">Causa</th>
                        <th class="text-center">Observaciones</th>
                        <th class="text-center">Fecha</th>
                        <th class="text-center"></th>
                        <th class="text-center"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($elemento->logSanciones) > 0) { ?>
                        <?php $contador = 1; ?>
                        <?php foreach ( $elemento->logSanciones as $log ) { ?>
                            <tr>
                                <td class="text-center"><?php echo $contador++; ?></td>
                                <td><?php echo ($log->idSancion == 6) ? $log->otra_sancion : $log->sancion->sancion; ?></td>
                                <td class="text-center"><?php echo ($log->fecha_inicio == '') ? 'N/A' : $log->fecha_inicio; ?></td>
                                <td class="text-center"><?php echo ($log->fecha_fin == '') ? 'N/A' : $log->fecha_fin; ?></td>
                                <td><?php echo $log->causa; ?></td>
                                <td><?php echo $log->observaciones; ?></td>
                                <td class="text-center"><?php echo $log->fecha; ?></td>
                                <td>
                                    <a class="btn btn-block" data-toggle="modal" data-target="#modal-model" data-modal-type="edit" data-action="<?php echo Url::to(['log-sanciones/update', 'id' => $log->id]) ?>">
                                        <span data-toggle="tooltip" data-placement="left" title="Editar" class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                </td>
                                <td>
                                    <a class="btn btn-block" data-toggle="modal" data-target="#modal-model" data-modal-type="remove" data-action="<?php echo Url::to(['log-sanciones/delete', 'id' => $log->id]) ?>">
                                        <span data-toggle="tooltip" data-placement="left" title="Eliminar" class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
        <div class="box-footer">
            <a style="visibility:show" href="<?php echo $urlIndex; ?>" class="btn btn-app">
                <i class="fa fa-arrow-left"></i>Regresar
            </a>
        </div>
    </div>
</div>