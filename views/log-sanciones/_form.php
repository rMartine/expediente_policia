<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Sanciones;
use app\models\Causas;
/**
* @var yii\web\View $this
* @var app\models\LogSanciones $model
* @var yii\widgets\ActiveForm $form
*/
$cssButton = $model->isNewRecord ? "btn btn-app" : "btn btn-app hidden";
$attributes = $formType == "view" ? ['disabled' => ''] : [];
?>

<div class="log-sanciones-form">
    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => true, 'id' => 'form-log']); ?>
    <div class="row">
        <?php echo $form->errorSummary($model, ['header' => 'Por favor corrija los errores:']); ?>
    </div>
    <div class="">
        <ul id="w0" class="nav nav-tabs"><li class="active"><a href="#w0-tab0" data-toggle="tab">Sancion</a></li></ul>

        <p>
            <?= $form->field($model->elementos, 'nombreText')->label('Nombre del elemento')->textInput(['readonly' => true]) ?>
			<?= $form->field($model, 'idSancion')
                ->label('Tipo de sanción')
                ->dropDownList(
                    ArrayHelper::map(Sanciones::find()->all(), 'id', 'sancion'),
                    ['prompt' => ''] + $attributes
                )
            ?>

            <?= $form->field($model, 'otra_sancion')->label('Otra Sanción')->textInput(['type' => 'text'] + $attributes) ?>

            <?= $form->field($model, 'causa')->label('Causa')->textInput(['type' => 'text'] + $attributes) ?>

			<?= $form->field($model, 'fecha')->textInput(['type' => 'date'] + $attributes); ?>
            <?= $form->field($model, 'fecha_inicio')->textInput(['type' => 'date'] + $attributes); ?>
            <?= $form->field($model, 'fecha_fin')->textInput(['type' => 'date'] + $attributes); ?>
			<?= $form->field($model, 'observaciones')->textarea(['rows' => 6] + $attributes) ?>
        </p>
        <hr/>

        <?= Html::submitButton('<span class="fa fa-save"></span> '.'Guardar', ['class' => $cssButton]) ?>
        <a href="<?php echo Url::previous(); ?>" class="<?php echo $cssButton ?>">
            <i class="fa fa-times"></i>Cancelar
        </a>

        <?php ActiveForm::end(); ?>

    </div>

</div>
