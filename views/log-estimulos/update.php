<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\LogEstimulos $model
 */

$this->title = 'Log Estimulos Update ' . $model->id . '';
$this->params['breadcrumbs'][] = ['label' => 'Log Estimulos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="log-estimulos-update">
    <?php echo $this->render('_form', [
		'model' => $model,
		'formType' => 'edit',
	]); ?>
</div>
