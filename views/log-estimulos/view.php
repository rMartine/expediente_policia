<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
$this->registerCssFile(Yii::$app->homeUrl . '/css/activos/planes.css');
$urlIndex = Url::to(['estimulos/']);
$this->title = 'Detalle de estimulos';
?>

<section class="content-header">
    <h1>
        <?php echo $this->title; ?>
        <small></small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="<?php echo Yii::$app->homeUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
        <li class="active"><?php echo $this->title; ?></li>
    </ol>

</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <div class="col-md-6 col-md-offset-2 search-box">
                <?php
                    $f = ActiveForm::begin([
                        'action'                 => $urlIndex,
                        'method'                 => 'post',
                        'enableClientValidation' => false
                    ]);
                ?>

                <div class="input-group input-group-sm">
                    <input autofocus type="search" placeholder="Busqueda por numero de empleado o nombre" id="formsearch-search_element" class="form-control" name="FormSearch[search_element]">
                    <span class="input-group-btn">
                        <?php echo Html::submitButton('Buscar', ['class' => 'btn btn-info btn-flat']); ?>
                    </span>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

        <div class="box-body">
            <div class="box">
                <?= $this->render('@app/views/layouts/modal', []); ?>
                <?= $this->render('index', ['model' => $model, 'elemento' => $elemento]); ?>
            </div>
        </div>

        <div class="box-footer">
            <a style="visibility:show" href="<?php echo $urlIndex; ?>" class="btn btn-app">
                <i class="fa fa-arrow-left"></i>Regresar
            </a>
        </div>

    </div>
</section>