
<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Estimulos;

/* @var $this yii\web\View */
$this->registerJsFile(Yii::$app->homeUrl.'js/datatables/tablas.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->homeUrl . '/css/activos/planes.css');
$urlIndex = Url::to(['estimulos/']);
$this->title = 'Estimulos ';
$blockName = 'estimulos';
?>
<section class="content-header">

    <h1>
        <?php echo $this->title; ?>
        <small></small>
    </h1>

    <ol class="breadcrumb">
        <li><a class='base_url' href="<?php echo Yii::$app->homeUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
        <li class="active"><?php echo $this->title; ?></li>
    </ol>

</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <?= $this->render('//planes/formacion_header', ['elemento' => $elemento]); ?>
        </div>
        <div class="box-body">
            <div class="bg-navy-active">
                <h5>Agregar nuevo estimulo al elemento</h5>
            </div>
            <?php $form = ActiveForm::begin([
                'action' => Url::to(['log-estimulos/', 'id' => $elemento->id]),
                'method' => 'post',
                'enableClientValidation' => true]);
            ?>
            <div class="row">
                <?php echo $form->errorSummary($model, ['header' => 'Por favor corrija los errores:']); ?>
            </div>
            <div>
                <?php $this->beginBlock($blockName); ?>
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label" for="logestimulos-idestimulo">Tipo</label>
                        <select id="tipos_estimulo" class="form-control">
                            <option value="">estimulo/reconocimiento/recompensa</option>
                            <option value="Estimulos">Estimulos</option>
                            <option value="Recompensa">Recompensa</option>
                            <option value="Reconocimiento">Reconocimiento</option>
                        </select>
                        <div class="help-block"></div>
                    </div>
                    <div class="col-md-6">
                        <label class="estimulos_tipo control-label" for="logestimulos-idestimulo">Estimulo/Reconocimiento/Recompensa</label>
                        <select id="logestimulos-idestimulo" class="form-control estimulos_holder" name="LogEstimulos[idEstimulo]">
  
                        </select>
                        <div class="help-block"></div>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'fecha')->textInput(['type' => 'date']); ?>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <?= $form->field($model, 'observaciones')->textarea(); ?>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label"></label>
                        <span class="">
                            <?php echo Html::submitButton('', ['class' => 'btn btn-info btn-block fa fa-plus-circle']); ?>
                        </span>
                        <?= $form->field($model, 'idElemento')->label('')->textInput(['type' => 'hidden', 'value' => $elemento->id]); ?>
                    </div>
                </div>
                <?php $this->endBlock(); ?>
                <?php ActiveForm::end(); ?>
                <p>
                    <?= $this->blocks[$blockName]; ?>
                </p>
            </div>
            <?= $this->render('@app/views/layouts/modal', []); ?>
            <div class="bg-navy-active">
                <h5>Histórico de estimulos</h5>
            </div>
            <table id="resultados" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tipo</th>
                        <th>Descripción</th>
                        <th>Fecha</th>
                        <th>Observación</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($elemento->logEstimulos) > 0) { ?>
                        <?php $contador = 1; ?>
                        <?php foreach ( $elemento->logEstimulos as $log ) { ?>
                            <tr>
                                <td><?php echo $contador++; ?></td>
                                <td><?php echo $log->estimulo->tipo; ?></td>
                                <td><?php echo $log->estimulo->concepto; ?></td>
                                <td><?php echo $log->fecha; ?></td>
                                <td><?php echo $log->observaciones; ?></td>
                                <td>
                                    <a class="btn btn-block" data-toggle="modal" data-target="#modal-model" data-modal-type="edit" data-action="<?php echo Url::to(['log-estimulos/update', 'id' => $log->id]) ?>">
                                        <span data-toggle="tooltip" data-placement="left" title="Editar" class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                </td>
                                <td>
                                    <a class="btn btn-block" data-toggle="modal" data-target="#modal-model" data-modal-type="remove" data-action="<?php echo Url::to(['log-estimulos/delete', 'id' => $log->id]) ?>">
                                        <span data-toggle="tooltip" data-placement="left" title="Eliminar" class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
        <div class="box-footer">
            <a style="visibility:show" href="<?php echo $urlIndex; ?>" class="btn btn-app">
                <i class="fa fa-arrow-left"></i>Regresar
            </a>
        </div>
    </div>
</div>
<script>
    $('#tipos_estimulo').change(function(){
        url = $('.base_url').attr('href') + '/log-estimulos/estimulo?estimulo='+$('#tipos_estimulo').val();
        
        $.ajax({
            type     :'POST',
            cache    : false,
            url  : url,
            success  : function(response) {     
                $('.estimulos_holder').html(response);                
            }
        });
        if($('#tipos_estimulo').val().length > 0) {
            $('.estimulos_tipo').html($('#tipos_estimulo').val());
        } else {
            $('.estimulos_tipo').html('Estimulo/Reconocimiento/Recompensa');
        }
    });
</script>