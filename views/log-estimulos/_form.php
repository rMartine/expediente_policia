<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Estimulos;
/**
* @var yii\web\View $this
* @var app\models\LogEstimulos $model
* @var yii\widgets\ActiveForm $form
*/
$cssButton = $model->isNewRecord ? "btn btn-app" : "btn btn-app hidden";
$attributes = $formType == "view" ? ['disabled' => ''] : [];
?>

<div class="log-estimulos-form">
    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => true, 'id' => 'form-log']); ?>
    <div class="">
        <?php echo $form->errorSummary($model, ['header' => 'Por favor corrija los errores:']); ?>
        <ul id="w0" class="nav nav-tabs"><li class="active"><a href="#w0-tab0" data-toggle="tab">Estimulo, Reconocimiento o Recompensa</a></li></ul>

        <p>
            <?= $form->field($model, 'idElemento')->label('')->textInput(['readonly' => true, 'type' => 'hidden']) ?>
            <?= $form->field($model->idElemento0, 'nombreText')->label('Nombre del elemento')->textInput(['readonly' => true]) ?>
			<?= $form->field($model, 'idEstimulo')
                ->label('Estimulo')
                ->dropDownList(
                    ArrayHelper::map(Estimulos::find()->all(), 'id', 'concepto'),
                    ['prompt' => 'estimulo/reconocimiento/recompensa'] + $attributes
                )
            ?>
			<?= $form->field($model, 'fecha')->textInput(['type' => 'date'] + $attributes) ?>
			<?= $form->field($model, 'observaciones')->textarea(['rows' => 6] + $attributes) ?>
        </p>
        <hr/>

        <?= Html::submitButton('<span class="fa fa-save"></span> '.'Guardar', ['class' => $cssButton]) ?>
        <a href="<?php echo Url::previous(); ?>" class="<?php echo $cssButton ?>">
            <i class="fa fa-times"></i>Cancelar
        </a>

        <?php ActiveForm::end(); ?>

    </div>

</div>
