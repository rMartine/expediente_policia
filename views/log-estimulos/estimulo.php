<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Estimulos;
?>

<?php 

$arrOptions = ArrayHelper::map(Estimulos::find()->andWhere(['tipo'=>$estimulo])->all(), 'id', 'concepto'); 
echo '<option value>'.$estimulo.'</option>';
foreach($arrOptions as $key=>$strOption) {
    echo '<option value="'.$key.'">'.$strOption.'</option>';
}

?>