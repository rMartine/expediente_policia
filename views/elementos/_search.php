<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\ElementosSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="elementos-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'folio') ?>

		<?= $form->field($model, 'clave_empleado') ?>

		<?= $form->field($model, 'nombre') ?>

		<?= $form->field($model, 'apellido_paterno') ?>

		<?php // echo $form->field($model, 'apellido_materno') ?>

		<?php // echo $form->field($model, 'fecha_nac') ?>

		<?php // echo $form->field($model, 'municipio_nac') ?>

		<?php // echo $form->field($model, 'estado_nac') ?>

		<?php // echo $form->field($model, 'rfc') ?>

		<?php // echo $form->field($model, 'curp') ?>

		<?php // echo $form->field($model, 'edad') ?>

		<?php // echo $form->field($model, 'sexo') ?>

		<?php // echo $form->field($model, 'telefono') ?>

		<?php // echo $form->field($model, 'movil') ?>

		<?php // echo $form->field($model, 'domicilio') ?>

		<?php // echo $form->field($model, 'domicilio_mpo') ?>

		<?php // echo $form->field($model, 'domicilio_edo') ?>

		<?php // echo $form->field($model, 'estado_civil') ?>

		<?php // echo $form->field($model, 'email') ?>

		<?php // echo $form->field($model, 'fotografia') ?>

		<?php // echo $form->field($model, 'pulgar_der') ?>

		<?php // echo $form->field($model, 'indice_der') ?>

		<?php // echo $form->field($model, 'medio_der') ?>

		<?php // echo $form->field($model, 'anular_der') ?>

		<?php // echo $form->field($model, 'menique_der') ?>

		<?php // echo $form->field($model, 'pulgar_izq') ?>

		<?php // echo $form->field($model, 'indice_izq') ?>

		<?php // echo $form->field($model, 'medio_izq') ?>

		<?php // echo $form->field($model, 'anular_izq') ?>

		<?php // echo $form->field($model, 'menique_izq') ?>

		<?php // echo $form->field($model, 'idEstatus') ?>

		<?php // echo $form->field($model, 'idEstudios') ?>

		<?php // echo $form->field($model, 'idEtapa') ?>

		<?php // echo $form->field($model, 'idConvocatoria') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
