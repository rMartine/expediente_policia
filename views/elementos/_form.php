<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Etapas;

/**
* @var yii\web\View $this
* @var app\models\Elementos $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="elementos-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            
			<?= $form->field($model, 'folio')->textInput(['maxlength' => 20]) ?>
			<?= $form->field($model, 'nombre')->textInput(['maxlength' => 60]) ?>
			<?= $form->field($model, 'apellido_paterno')->textInput(['maxlength' => 30]) ?>
			<?= $form->field($model, 'apellido_materno')->textInput(['maxlength' => 30]) ?>
			<?= $form->field($model, 'fecha_nac')->textInput() ?>
			<?= $form->field($model, 'municipio_nac')->textInput(['maxlength' => 30]) ?>
			<?= $form->field($model, 'estado_nac')->textInput(['maxlength' => 30]) ?>
			<?= $form->field($model, 'curp')->textInput(['maxlength' => 18]) ?>
			<?= $form->field($model, 'edad')->textInput() ?>
			<?= $form->field($model, 'sexo')->textInput(['maxlength' => 1]) ?>
			<?= $form->field($model, 'domicilio')->textInput() ?>
			<?= $form->field($model, 'domicilio_mpo')->textInput(['maxlength' => 30]) ?>
			<?= $form->field($model, 'domicilio_edo')->textInput(['maxlength' => 30]) ?>
			<?= $form->field($model, 'estado_civil')->textInput(['maxlength' => 20]) ?>
			<?= $form->field($model, 'idEstatus')->textInput() ?>
			<?= $form->field($model, 'idEstudios')->textInput() ?>
	        <?=
	        $form->field($model, 'idEtapa')->dropDownList(
	                    ArrayHelper::map(Etapas::find()->all(), 'id', 'nombre')
	                    )
	        ?>
			<?= $form->field($model, 'idConvocatoria')->textInput() ?>
			<?= $form->field($model, 'fotografia')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'pulgar_der')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'indice_der')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'medio_der')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'anular_der')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'menique_der')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'pulgar_izq')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'indice_izq')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'medio_izq')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'anular_izq')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'menique_izq')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'clave_empleado')->textInput(['maxlength' => 20]) ?>
			<?= $form->field($model, 'rfc')->textInput(['maxlength' => 13]) ?>
			<?= $form->field($model, 'telefono')->textInput(['maxlength' => 10]) ?>
			<?= $form->field($model, 'movil')->textInput(['maxlength' => 10]) ?>
			<?= $form->field($model, 'email')->textInput(['maxlength' => 40]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    \yii\bootstrap\Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ [
    'label'   => 'Elementos',
    'content' => $this->blocks['main'],
    'active'  => true,
], ]
                 ]
    );
    ?>
        <hr/>

        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? 'Create' : 'Save'), ['class' => $model->isNewRecord ?
        'btn btn-primary' : 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
