<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
* @var yii\web\View $this
* @var yii\data\ActiveDataProvider $dataProvider
* @var app\models\ElementosSearch $searchModel
*/

$this->title = 'Elementos';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="elementos-index">

    <?php //     echo $this->render('_search', ['model' =>$searchModel]);
    ?>

    <div class="clearfix">
        <p class="pull-left">
            <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Elementos', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <div class="pull-right">


                                                                                                                                                                                                                                                                                                                                            
            <?php 
            echo \yii\bootstrap\ButtonDropdown::widget(
                [
                    'id'       => 'giiant-relations',
                    'encodeLabel' => false,
                    'label'    => '<span class="glyphicon glyphicon-paperclip"></span> Relations',
                    'dropdown' => [
                        'options'      => [
                            'class' => 'dropdown-menu-right'
                        ],
                        'encodeLabels' => false,
                        'items'        => [
    [
        'label' => '<i class="glyphicon glyphicon-arrow-left"> Convocatorias</i>',
        'url' => [
            'convocatorias/index',
        ],
    ],
    [
        'label' => '<i class="glyphicon glyphicon-arrow-left"> Estatus</i>',
        'url' => [
            'estatus/index',
        ],
    ],
    [
        'label' => '<i class="glyphicon glyphicon-arrow-left"> Estudios</i>',
        'url' => [
            'estudios/index',
        ],
    ],
    [
        'label' => '<i class="glyphicon glyphicon-arrow-left"> Etapas</i>',
        'url' => [
            'etapas/index',
        ],
    ],
    [
        'label' => '<i class="glyphicon glyphicon-arrow-right"> Log Adscripciones</i>',
        'url' => [
            'log-adscripciones/index',
        ],
    ],
    [
        'label' => '<i class="glyphicon glyphicon-arrow-right"> Log Bajas</i>',
        'url' => [
            'log-bajas/index',
        ],
    ],
    [
        'label' => '<i class="glyphicon glyphicon-arrow-right"> Log Cursos</i>',
        'url' => [
            'log-cursos/index',
        ],
    ],
    [
        'label' => '<i class="glyphicon glyphicon-arrow-right"> Log Estimulos</i>',
        'url' => [
            'log-estimulos/index',
        ],
    ],
    [
        'label' => '<i class="glyphicon glyphicon-arrow-right"> Log Evaluaciones</i>',
        'url' => [
            'log-evaluaciones/index',
        ],
    ],
    [
        'label' => '<i class="glyphicon glyphicon-arrow-right"> Log Nombramientos</i>',
        'url' => [
            'log-nombramientos/index',
        ],
    ],
    [
        'label' => '<i class="glyphicon glyphicon-arrow-right"> Log Sanciones</i>',
        'url' => [
            'log-sanciones/index',
        ],
    ],
]                    ],
                ]
            );
            ?>        </div>
    </div>

            <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
        
			'id',
			'folio',
			'clave_empleado',
			'nombre',
			'apellido_paterno',
			'apellido_materno',
			'fecha_nac',
			/*'municipio_nac'*/
			/*'estado_nac'*/
			/*'rfc'*/
			/*'curp'*/
			/*'edad'*/
			/*'sexo'*/
			/*'telefono'*/
			/*'movil'*/
			/*'domicilio'*/
			/*'domicilio_mpo'*/
			/*'domicilio_edo'*/
			/*'estado_civil'*/
			/*'email:email'*/
			/*'fotografia:ntext'*/
			/*'pulgar_der:ntext'*/
			/*'indice_der:ntext'*/
			/*'medio_der:ntext'*/
			/*'anular_der:ntext'*/
			/*'menique_der:ntext'*/
			/*'pulgar_izq:ntext'*/
			/*'indice_izq:ntext'*/
			/*'medio_izq:ntext'*/
			/*'anular_izq:ntext'*/
			/*'menique_izq:ntext'*/
			/*'idEstatus'*/
			/*'idEstudios'*/
			/*'idEtapa'*/
			/*'idConvocatoria'*/
            [
                'class' => 'yii\grid\ActionColumn',
                'urlCreator' => function($action, $model, $key, $index) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
                    $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                    return \yii\helpers\Url::toRoute($params);
                },
                'contentOptions' => ['nowrap'=>'nowrap']
            ],
        ],
    ]); ?>
    
</div>
