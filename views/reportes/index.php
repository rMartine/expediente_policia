<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerCssFile(Yii::$app->urlManager->getBaseUrl() . '/css/configuracion/config.css');
$this->registerJsFile(Yii::$app->urlManager->getBaseUrl() . '/js/reportes/reportes.js');

/* @var $this yii\web\View */
$this->title = 'Reportes';
?>

<section class="content-header">

    <h1>
        <?php echo $this->title; ?>
        <small>Reportes</small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="<?php echo Yii::$app->homeUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="<?php echo Yii::$app->homeUrl . 'configuracion'; ?>">Configuraci&oacute;n</a></li>
        <li class="active"><?php echo $this->title; ?></li>
    </ol>

</section>

<section class="content">
    <div class="row">        
        <div class="col-md-12 main_box">
            <div class="box box-default box-bajas">
                <?php if (count($arrReportes)) { ?>
                    <?php foreach ($arrReportes as $key => $oReporte) { ?>
                        <?php
                        $strBackground = '';

                        switch ($key%5) {
                            case 0:
                                $strBackground = 'bg-aqua';
                                break;
                            case 1:
                                $strBackground = 'bg-green';
                                break;
                            case 2:
                                $strBackground = 'bg-orange';
                                break;
                            case 3:
                                $strBackground = 'bg-red';
                                break;
                            case 4:
                                $strBackground = 'bg-purple';
                                break;
                        }
                        ?>
                        <div class="col-md-4 col-sm-8 config-box">
                            <a class="btn-ver-reporte" href="<?php echo ($oReporte->parametros == 1) ? '#agregarParametro' : $oReporte->url; ?>" <?php echo ($oReporte->parametros == 1) ? 'data-toggle="modal"' : 'target="_blank"'; ?> url="<?php echo $oReporte->url; ?>">
                                <div class="info-box">
                                    <span class="info-box-icon <?php echo $strBackground; ?>"><i class="fa fa-book"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-number"><?php echo $oReporte->nombre; ?></span>
                                        <span class="info-box-more"><?php echo $oReporte->descripcion_role; ?></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <div class="col-md-12 col-sm-12 config-box">
                        <p>No se ha encontrado ningún reporte.</p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="agregarParametro" tabindex="-1" role="dialog" aria-labelledby="parametrosModal" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title modal-title-parametros" id="parametrosModal">Agregar parametro</h4>
            </div>

            <form action="" id="frm-agregar-parametro">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row subtitle-reportes">
                            <div class="col-md-12 text-left">Buscar clave de empleado:</div>
                        </div>
                        <div class="row reporte-elementos-table">
                            <div class="col-md-12">
                                <table id="reporte-elementos-table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">No</th>
                                            <th class="text-center">Clave de empleado</th>
                                            <th class="text-center">Nombre del Elemento</th>
                                            <th class="no-sort">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($arrElementos as $key => $oElemento) { ?>
                                            <tr>
                                                <td class="text-center"><?php echo $key + 1; ?></td>
                                                <td class="text-center"><?php echo $oElemento->clave_empleado; ?></td>
                                                <td><?php echo $oElemento->nombre . ' ' . $oElemento->apellido_paterno . ' '  . $oElemento->apellido_materno; ?></td>
                                                <td class="text-center"><button type="button" class="btn btn-sm btn-info btn-seleccionar-clave" claveEmpleado="<?php echo $oElemento->clave_empleado; ?>">Seleccionar</button></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row subtitle-reportes">
                            <div class="col-md-12 text-left">Clave de empleado seleccionada:</div>
                        </div>
                        <div class="row reporte-clave-empleado-container">
                            <div class="col-md-1"></div>
                            <div class="col-md-4 clave-empleado-container">
                                <strong>Clave de empleado:</strong>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="clave_empleado" name='clave_empleado' placeholder="Clave de empleado" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" data-toggle="modal">Ver reporte</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-repeat"></i> Cancelar</button>
                </div>
            </form>

        </div>
    </div>

</div>