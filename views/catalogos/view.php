<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Roles */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content">
    <div class="row">        
        <div class="col-md-12 main_box">
            <div class="box box-warning box-bajas roles-view">
                <div class="box-header ">

                    <div class="box-tools pull-right">      
                      <button type="button" class="close btn btn-box-tool" data-dismiss="modal" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                <?= $this->render('views/'.$stage, [
                    'model' => $model,
                ]) ?>  
                    <p class="buttoneer">
                        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary updater', 'stage'=>$stage]) ?>
                        <?= Html::a('Eliminar', ['delete', 'id' => $model->id, 'action'=>$stage], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </p>
                </div><!-- /.box-body -->
            </div>    
        </div>
    </div>
</section>