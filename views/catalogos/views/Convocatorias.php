<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

?>
<?php echo DetailView::widget([
'model' => $model,
'attributes' => [
                    'id',
                    'folio:ntext',
                    'apertura',
                    'cierre',
                    'fallo',
                    'contenido:ntext',
],
]); ?>