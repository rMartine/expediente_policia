<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

?>
<?= DetailView::widget([
    'options'=>['class' => 'table table-bordered table-hover'],
    'model' => $model,
    'attributes' => [
        'nombre',
        'orden',
    ],
]) ?>