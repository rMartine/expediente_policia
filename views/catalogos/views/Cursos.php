<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

?>
<?php echo DetailView::widget([
'options'=>['class' => 'table table-bordered table-hover'],    
'model' => $model,
'attributes' => [
                    'nombre',
                    'duracion',
                    ['attribute'=>'etapas.nombre','label'=>'Etapa'],
                    ['attribute'=>'categoriasCursos.nombre', 'label' => 'Categoria'],
                    ['attribute'=>function ($model) {
                    $return = ($model->numerico == "1")?"Si":"No";
                    return " " .
                    $return . '';
                    },'label'=>'Num&eacute;rico'],
                    ['attribute'=>function ($model) {
                    $return = ($model->obligatorio == "1")?"Si":"No";
                    return " " .
                    $return . '';
                    },'label'=>'Obligatorio'],
],
]); ?>