<?php

use yii\helpers\Html;
use yii\grid\GridView;
$dataProvider->pagination=['pagesize'=>10];
?>

<?php echo GridView::widget([
    'dataProvider' => $dataProvider,
    'tableOptions' => [
        'class' => 'table table-bordered table-hover',
    ],
    'columns' => [

                    ['class' => 'yii\grid\SerialColumn'],
                    'tipo',
                    'concepto',
        [
            'class' => 'yii\grid\ActionColumn',
            'urlCreator' => function($action, $model, $key, $index) {
                // using the column name as key, not mapping to 'id' like the standard generator
                $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
                $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                return \yii\helpers\Url::toRoute($params);
            },
            'contentOptions' => ['nowrap'=>'nowrap']
        ],
],
    'pager' => ['prevPageLabel' => '← Anterior', 'nextPageLabel' => 'Siguiente → '],
    'layout'=>"{items}\n{summary}{pager}"
]); ?>