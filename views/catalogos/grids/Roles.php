<?php

use yii\helpers\Html;
use yii\grid\GridView;
$dataProvider->pagination=['pagesize'=>10];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'tableOptions' => [
        'class' => 'table table-bordered table-hover',
    ],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'nombre',

        ['class' => 'yii\grid\ActionColumn'],
],
    'pager' => ['prevPageLabel' => '← Anterior', 'nextPageLabel' => 'Siguiente → '],
    'layout'=>"{items}\n{summary}{pager}"
]); ?>