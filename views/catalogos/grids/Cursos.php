<?php

use yii\helpers\Html;
use yii\grid\GridView;
$dataProvider->pagination=['pagesize'=>10];
?>

<?php echo GridView::widget([
    'dataProvider' => $dataProvider,
    'tableOptions' => [
        'class' => 'table table-bordered table-hover',
    ],
    'columns' => [

                    ['class' => 'yii\grid\SerialColumn'],
                    'nombre',
                    ['value'=>function ($model, $key, $index, $widget) {
                        $return = ($model->numerico == "1")?"Si":"No";
                        return " " .
                        $return . '';
                        },'header'=>'Num&eacute;rico'],
                    ['value'=>'etapas.nombre','header'=>'Etapa'],
                    ['value'=>'categoriasCursos.nombre','header'=>'Categoria'], 
                    ['value'=>function ($model, $key, $index, $widget) {
                        return $model->duracion . ' horas'; 
                        },'header'=>'Duracion'],
                    ['value'=>function ($model, $key, $index, $widget) {
                        $return = ($model->obligatorio == "1")?"Si":"No";
                        return " " .
                        $return . '';
                        },'header'=>'Obligatorio'],
        [
            'class' => 'yii\grid\ActionColumn',
            'urlCreator' => function($action, $model, $key, $index) {
                // using the column name as key, not mapping to 'id' like the standard generator
                $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
                $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                return \yii\helpers\Url::toRoute($params);
            },
            'contentOptions' => ['nowrap'=>'nowrap']
        ],
],
    'pager' => ['prevPageLabel' => '← Anterior', 'nextPageLabel' => 'Siguiente → '],
    'layout'=>"{items}\n{summary}{pager}" 
]); ?>