<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerCssFile(Yii::$app->urlManager->getBaseUrl() . '/css/configuracion/config.css');
$this->registerJsFile(Yii::$app->homeUrl.'js/configuracion/main.js', ['position' => $this::POS_END]);

/* @var $this yii\web\View */
$this->title = 'Cat&aacute;logos';

$oCatalogos = [               
       'Adscripciones'=>'Adscripciones',
       'Cursos'=>'Cursos',
       //'Estatus'=>'Estatus',
       'Estimulos'=>'Est&iacute;mulos',  
       //'Estudios'=>'Estudios',
       //'Etapas'=>'Etapas',
       'Evaluaciones'=>'Evaluaciones', 
       //'Motivos'=>'Motivos',
       'Nombramientos'=>'Nombramientos',
       //'PaquetesCursos'=>'Paquetes de Cursos',
       'Sanciones'=>'Sanciones',
       'Requerimientos'=>'Requerimientos',
       //'Roles'=>'Roles',
       'TiposEvaluacion'=>'Tipos de Evaluaci&oacute;n',
    ];

?>

<section class="content-header">

        <h1>
                <?php echo $this->title; ?>
            <small>Captura y Verificaci&oacute;n de datos</small>
        </h1>

        <ol class="breadcrumb">
                <li><a href="<?php echo Yii::$app->homeUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
                <li class="active"><?php echo $this->title; ?></li>
        </ol>

</section>

<section class="content main_grid">
    <input id="base_url" type="hidden" url="<?=Yii::$app->urlManager->getBaseUrl()?>">
    <div class="row">        
        <div class="col-md-12 main_box">
            <div class="box box-default box-bajas">
                <div class="panel-group" id="catalogos" role="tablist" aria-multiselectable="true">

                <?php 
                    foreach($oCatalogos as $key => $value) {
                ?>
                    <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="heading_<?=$key?>">
                        <h4 class="panel-title">
                            <a class="collapsed opener" id="<?=$key?>" data-toggle="collapse" data-parent="#catalogos" href="#collapse_<?=$key?>" aria-expanded="false" aria-controls="collapse_<?=$key?>">
                            <?=$value?>
                            </a>
                            <div class="creator_container">
                                <a class="btn btn-success creator" parent="<?=$key?>" href="javascript:;" title="Close" >Crear</a>
                            </div>
                        </h4>
                      </div>
                      <div id="collapse_<?=$key?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body" parent="<?=$key?>"></div>
                      </div>
                    </div>
                <?php 
                    }
                ?>                  
                    
                </div>
                
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="ajaxModal" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true" data-backdrop="false">
  <div class="modal-dialog">
    <!--<div class="modal-content">-->
        <div id="ajax_div" class="box-header">
      </div>
    <!--</div>-->    
  </div>
</div>
<script>

</script>