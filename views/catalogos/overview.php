<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\etapasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Etapas';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="box-body">
    <?= $this->render('grids/'.$stage, [
        'dataProvider' => $dataProvider,
    ]) ?>        
    </div><!-- /.box-body -->
</section>