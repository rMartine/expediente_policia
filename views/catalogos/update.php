<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\adscripciones $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => 'Adscripciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="row">        
        <div class="col-md-12 main_box">
            <div class="box box-warning box-bajas roles-view">
                <div class="box-header">
                    <div class="box-tools pull-right">      
                      <button type="button" class="close btn btn-box-tool" data-dismiss="modal" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                <div class="clearfix"></div>
                <input type="hidden" id="action_pointer" url="<?=Yii::$app->urlManager->getBaseUrl()?>" action="<?=$stage?>">
                <?php echo $this->render('insertor/'.$stage, [
                'model' => $model,
                ]); ?>
                </div><!-- /.box-body -->
            </div>    
        </div>
    </div>
</section>