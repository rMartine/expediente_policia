<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="convocatorias-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main', true); ?>
        
        <p>            
            <?= $form->field($model, 'folio')->textarea(['rows' => 6]) ?>
            <?= $form->field($model, 'apertura')->textInput() ?>
            <?= $form->field($model, 'cierre')->textInput() ?>
            <?= $form->field($model, 'fallo')->textInput() ?>
            <?= $form->field($model, 'contenido')->textarea(['rows' => 6]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <hr/>

        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? 'Create' : 'Save'), ['class' => $model->isNewRecord ?
        'btn btn-primary' : 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
