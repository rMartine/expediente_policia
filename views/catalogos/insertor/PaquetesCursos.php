<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
?>

<div class="paquetes-cursos-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main', true); ?>
        
        <p>            
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => 45]) ?>
            <?=
            $form->field($model, 'idEtapa')->dropDownList(
                            ArrayHelper::map(app\models\Etapas::find()->all(), 'id', 'nombre')
                        )->label('Etapa');
            ?>
            <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <hr/>

        <div class="buttoneer">
        <?= Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> Guardar', ['class' => 'btn btn-primary']) ?>
        <?= Html::button('<i class="glyphicon glyphicon-repeat"></i> Cancelar', ['class' => 'btn btn-default closer']) ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>

</div>
