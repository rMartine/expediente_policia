<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
?>

<div class="cursos-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main', true); ?>
        
        <p>   
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => 45]) ?>
            <?= $form->field($model, 'duracion')->textInput()->label('Duracion(Horas)'); ?>
            <?=
            $form->field($model, 'idEtapa')->dropDownList(
                            ArrayHelper::map(app\models\Etapas::find()->andOnCondition(['id'=>['4','6']])->all(), 'id', 'nombre')
                        )->label('Etapa');
            ?>
            <?=
            $form->field($model, 'idCategoria')->dropDownList(
                            ArrayHelper::map(app\models\CategoriasCursos::find()->all(), 'id', 'nombre')
                        )->label('Categoria');
            ?>
            <?php
            $model->numerico = $model->isNewRecord ? true:$model->numerico;
            echo $form->field($model, 'numerico')->checkbox(); 
            ?>
            <?php
            $model->obligatorio = $model->isNewRecord ? true:$model->obligatorio;
            echo $form->field($model, 'obligatorio')->checkbox(); 
            ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <hr/>
        <div class="buttoneer">
        <?= Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> Guardar', ['class' => 'btn btn-primary']) ?>
        <?= Html::button('<i class="glyphicon glyphicon-repeat"></i> Cancelar', ['class' => 'btn btn-default closer']) ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>

</div>
