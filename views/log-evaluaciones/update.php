<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\LogEvaluaciones $model
 */

$this->title = 'Log Evaluaciones Update ' . $model->id . '';
$this->params['breadcrumbs'][] = ['label' => 'Log Evaluaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="log-evaluaciones-update">
	<?php echo $this->render('_form', [
		'model' => $model,
		'formType' => 'edit',
	]); ?>
</div>
