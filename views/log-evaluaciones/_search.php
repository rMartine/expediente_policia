<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\LogEvaluacionesSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="log-evaluaciones-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'idElemento') ?>

		<?= $form->field($model, 'idEvaluacion') ?>

		<?= $form->field($model, 'cumplimiento') ?>

		<?= $form->field($model, 'observaciones') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
