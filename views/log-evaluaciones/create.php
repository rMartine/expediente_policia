<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\LogEvaluaciones $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => 'Log Evaluaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-evaluaciones-create">
    <?php echo $this->render('_form', [
    'model' => $model,
    'formType' => 'create'
    ]); ?>

</div>
