<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Evaluaciones;
/**
* @var yii\web\View $this
* @var app\models\LogEvaluaciones $model
* @var yii\widgets\ActiveForm $form
*/
$cssButton = $model->isNewRecord ? "btn" : "btn hidden";
$attributes = $formType == "view" ? ['disabled' => ''] : [];
?>

<div class="log-evaluaciones-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => true, 'id' => 'form-log']); ?>

    <div class="">
        <?php echo $form->errorSummary($model, ['header' => 'Por favor corrija los errores:']); ?>
<ul id="w0" class="nav nav-tabs"><li class="active"><a href="#w0-tab0" data-toggle="tab">Evaluacion</a></li></ul>

        <p>
            <?= $form->field($model, 'idElemento')->label('')->textInput(['readonly' => true, 'type' => 'hidden']) ?>
            <?= $form->field($model->idElemento0, 'nombreText')->label('Nombre del elemento')->textInput(['readonly' => true]) ?>
            <?= $form->field($model, 'idEvaluacion')
                ->label('Nombre de la evaluacion')
                ->dropDownList(ArrayHelper::map(Evaluaciones::find()->all(), 'id', 'nombre'),
                    ['prompt' => '--evaluacion--']
                )
            ?>
			<?= $form->field($model, 'cumplimiento')->dropDownList([0 => 'No', 1 => 'Si']); ?>
            <?= $form->field($model, 'fecha')->textInput(['type' => 'date']); ?>
			<?= $form->field($model, 'observaciones')->textarea(['rows' => 6]) ?>
        </p>

        <hr/>
        <div class="row">
            <div class="col-xs-2 pull-right">
                <a href="<?php echo Url::previous(); ?>" class="<?php echo $cssButton . ' btn-default' ?>">
                    <i class="glyphicon glyphicon-repeat"></i>Cancelar
                </a>
            </div>
            <div class="col-xs-2 pull-right">
                <?= Html::submitButton('<span class="glyphicon glyphicon-floppy-disk"></span> '.'Guardar', ['class' => $cssButton.' btn-primary']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>

</div>
