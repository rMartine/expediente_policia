<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerCssFile(Yii::$app->urlManager->getBaseUrl() . '/css/configuracion/config.css');

/* @var $this yii\web\View */
$this->title = 'Configuraci&oacute;n';
?>

<section class="content-header">

        <h1>
                <?php echo $this->title; ?>
                <small>Usuarios y Cat&aacute;logos</small>
        </h1>

        <ol class="breadcrumb">
                <li><a href="<?php echo Yii::$app->homeUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
                <li class="active"><?php echo $this->title; ?></li>
        </ol>

</section>

<section class="content">
    <div class="row">        
        <div class="col-md-12 main_box">
            <div class="box box-default box-bajas">
                <div class="col-md-3 col-sm-6 config-box">
                    <a href="<?= Yii::$app->urlManager->createUrl(['/user'])?>">
                        <div class="info-box">
                          <span class="info-box-icon bg-aqua"><i class="fa fa-group"></i></span>
                          <div class="info-box-content">
                            <span class="info-box-number">Usuarios</span>
                            <span class="info-box-more">Administraci&oacute;n</span>
                            <span class="info-box-more">Accesos</span>
                          </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                    </a>
                </div>     
                <div class="col-md-3 col-sm-6 config-box">
                    <a href="<?= Yii::$app->urlManager->createUrl(['/catalogos'])?>">
                        <div class="info-box">
                          <span class="info-box-icon bg-blue"><i class="fa fa-tasks"></i></span>
                          <div class="info-box-content">
                            <span class="info-box-number">Catalogos</span>
                            <span class="info-box-more">Captura de datos</span>
                            <span class="info-box-more">Verificar datos</span>
                          </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                    </a>    
                </div>    
                <div class="col-md-3 col-sm-6 config-box">
                    <a href="<?= Yii::$app->urlManager->createUrl(['/reportes'])?>">
                        <div class="info-box">
                          <span class="info-box-icon bg-orange"><i class="fa fa-book"></i></span>
                          <div class="info-box-content">
                            <span class="info-box-number">Reportes</span>
                            <span class="info-box-more"></span>
                          </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                    </a>    
                </div> 
                <div class="col-md-3 col-sm-6 config-box">
                    <a href="<?= Yii::$app->urlManager->createUrl(['/user/member'])?>">
                        <div class="info-box">
                          <span class="info-box-icon"><i class="fa fa-cogs"></i></span>
                          <div class="info-box-content">
                            <span class="info-box-number">Configuraci&oacute;n</span>
                            <span class="info-box-more">Datos Usuario</span>
                          </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                    </a>    
                </div>    
            </div>
        </div>
    </div>
</section>
