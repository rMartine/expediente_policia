<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Crear Usuario';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content">
    <div class="row">        
        <div class="col-md-12 main_box">
            <div class="box box-warning box-bajas user-index">
                <div class="box-header with-border">
                    <h1><?= Html::encode($this->title) ?></h1>
                    <div class="box-tools pull-right">      
                      <button type="button" class="close btn btn-box-tool" data-dismiss="modal" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div><!-- /.box-body -->
            </div>    
        </div>
    </div>
</section>