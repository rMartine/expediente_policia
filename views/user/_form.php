<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Roles;
use app\models\Nombramientos;
use app\models\Adscripciones;
use yii\helpers\ArrayHelper;
use kartik\switchinput\SwitchInput;

$this->registerCssFile(Yii::$app->urlManager->getBaseUrl() . '/css/configuracion/usuarios.css');
$this->registerJsFile(Yii::$app->homeUrl.'js/configuracion/fileReader.js', ['position' => $this::POS_END]);

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="left_group">
        <?= $form->field($model, 'username')->textInput(['maxlength' => 100])->label('Usuario') ?>

        <?php if(!$model->isNewRecord) {$model->password = '';} ?>
        <?= $form->field($model, 'password')->passwordInput(['maxlength' => 20]) ?>

        <?= $form->field($model, 'confirm_password')->passwordInput(['maxlength' => 20])
                ->label('Confirmar Password')
                ?>

        <?= $form->field($model, 'id_cargo')->dropDownList(
                            ArrayHelper::map(Nombramientos::find()->all(), 'id', 'grado')
                        )->label('Nombramiento');
            ?>

        <?= $form->field($model, 'id_adscripcion')->dropDownList(
                            ArrayHelper::map(Adscripciones::find()->all(), 'id', 'nombre')
                        )->label('Adscripci&oacute;n');
            ?>
        <?= $form->field($model, 'id_rol')->dropDownList(
                            ArrayHelper::map(Roles::find()->all(), 'id', 'nombre')
                        )->label('Rol');
            ?>
        <?php 
            $model->estatus_usr = $model->isNewRecord ? true:$model->estatus_usr;
            echo $form->field($model, 'estatus_usr')->checkbox(); 
            ?>
    </div>
    <div class="right_group">
        <div class='avatar_group'>
            <div class="avatar_container" id="avatar_container">
                <span>
                    <?php if($model->avatar){ ?>
                        <img class="thumb" src="<?= Yii::$app->urlManager->getBaseUrl().'/avatars/'.$model->avatar?>" title="Avatar">
                    <?php 
                        $model->avatar = '';
                    }?>                    
                </span>   
            </div>
            <div class="file_container">
                <label class="btn btn-app fotopload pull-left ui file_label">
                   <i class="glyphicon glyphicon-cloud-upload"></i>Fotografía
                   <input type="file" id="avatar_upload" class="Avatar" name="file_input" value> 
                   <?php echo $form->field($model, 'avatar',['inputOptions'=>['type' => 'hidden']])->label(false); ?>
                </label>
            </div>            
            <div class="generar_llave_cont <?= $model->isNewRecord?'no_display':'' ?>">
                <a href="<?= Yii::$app->urlManager->createUrl(['/user/getkey']).'?id='.$model->id ?>">
                    <i class="fa fa-lock"></i>
                    <div class="generar_llave">
                        Generar llave
                    </div>
                </a>
            </div>
        </div>
        <div class="form-group botones">
            <?= Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> Guardar', ['class' => 'btn btn-primary']) ?>
            <?= Html::button('<i class="glyphicon glyphicon-repeat"></i> Cancelar', ['class' => 'btn btn-default closer']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>