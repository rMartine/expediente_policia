<?php

use yii\helpers\Html;
use yii\grid\GridView;
$dataProvider->pagination=['pagesize'=>10];
//$dataProvider->estatus_usr = ($dataProvider->estatus_usr)?'Si':'No';
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'tableOptions' => [
        'class' => 'table table-bordered table-hover',
    ],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'username',
        ['value'=>'nombramientos.grado','header'=>'Cargo'],
        ['value'=>'adscripciones.nombre','header'=>'Adscripci&oacute;n'],
        ['value'=>'roles.nombre','header'=>'Rol'],
        ['value'=>function ($model, $key, $index, $widget) {
                $return = ($model->estatus_usr == "1")?"Si":"No";
                return " " .
                $return . '';
                },'header'=>'Activo'],
        ['class' => 'yii\grid\ActionColumn'],
    ],             
    'pager' => ['prevPageLabel' => '← Anterior', 'nextPageLabel' => 'Siguiente → '],
    'layout'=>"{items}\n{summary}{pager}"
]); ?>
