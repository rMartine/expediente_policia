<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="row">        
        <div class="col-md-12 main_box">
            <div class="box box-warning box-bajas user-index">
                <div class="box-header with-border">
                    <h1><?= Html::encode($this->title) ?></h1>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?= DetailView::widget([
                        'options'=>['class' => 'table table-bordered table-hover'],
                        'model' => $model,
                        'attributes' => [
                            ['attribute'=>'username','label'=>'Usuario'],
                            ['attribute'=>'nombramientos.grado','label'=>'Cargo'],
                            ['attribute'=>'adscripciones.nombre','label'=>'Adscripci&oacute;n'],
                            ['attribute'=>'roles.nombre','label'=>'Rol'],
                            ['attribute'=>function ($model) {
                            $return = ($model->estatus_usr == "1")?"Si":"No";
                            return " " .
                            $return . '';
                            },'label'=>'Activo'],
                        ],
                    ]) ?>
                    <div class="buttoneer">
                        <?= Html::a('Actualizar Password', ['memberupdate'], ['class' => 'btn btn-primary updater']) ?>
                    </div>
                </div><!-- /.box-body -->
            </div>    
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="ajaxModal" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true" data-backdrop="false">
  <div class="modal-dialog">
    <!--<div class="modal-content">-->
      <div id="ajax_div" class="box-header">
      </div>
    <!--</div>-->    
  </div>
</div>
<style>
    .buttoneer {
        float:right;
        margin-top:10px;
    }
    .box-body {
        overflow:hidden;
    }
</style>