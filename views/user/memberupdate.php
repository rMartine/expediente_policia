<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = ''
        . 'Editar Usuario: ' . ' ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<section class="content">
    <div class="row">        
        <div class="col-md-12 main_box">
            <div class="box box-warning box-bajas user-index">
                <div class="box-header with-border">
                    <h1><?= Html::encode($this->title) ?></h1>
                    <div class="box-tools pull-right">      
                      <button type="button" class="close btn btn-box-tool" data-dismiss="modal" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="user-form">
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                        <div class="left_group">
                            <?= $form->field($model, 'username')->textInput(['maxlength' => 100, 'readonly' => true])->label('Usuario') ?>

                            <?php if(!$model->isNewRecord) {$model->password = '';} ?>

                            <?= $form->field($model, 'password')->passwordInput(['maxlength' => 20]) ?>

                            <?= $form->field($model, 'confirm_password')->passwordInput(['maxlength' => 20])
                                    ->label('Confirmar Password')
                                    ?>
                        </div>
                            <div class="form-group botones buttoneer">
                                <?= Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> Guardar', ['class' => 'btn btn-primary']) ?>
                                <?= Html::a('<i class="glyphicon glyphicon-repeat"></i> Cancelar', ['member'], ['class' => 'btn btn-default closer']) ?>
                            </div>
                        <?php ActiveForm::end(); ?>

                    </div>
                </div><!-- /.box-body -->
            </div>    
        </div>
    </div>
</section>
<style>
    .buttoneer {
        float:right;
        margin-top:10px;
    }
    .box-body {
        overflow:hidden;
    }
</style>