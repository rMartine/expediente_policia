<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="row">        
        <div class="col-md-12 main_box">
            <div class="box box-warning box-bajas user-index">
                <div class="box-header with-border">
                    <h1><?= Html::encode($this->title) ?></h1>
                    <div class="box-tools pull-right">      
                      <button type="button" class="close btn btn-box-tool" data-dismiss="modal" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?= DetailView::widget([
                        'options'=>['class' => 'table table-bordered table-hover'],
                        'model' => $model,
                        'attributes' => [
                            ['attribute'=>'username','label'=>'Usuario'],
                            ['attribute'=>'nombramientos.grado','label'=>'Cargo'],
                            ['attribute'=>'adscripciones.nombre','label'=>'Adscripci&oacute;n'],
                            ['attribute'=>'roles.nombre','label'=>'Rol'],
                            ['attribute'=>function ($model) {
                            $return = ($model->estatus_usr == "1")?"Si":"No";
                            return " " .
                            $return . '';
                            },'label'=>'Activo'],
                            'creado',
                            'ultima_entrada',
                        ],
                    ]) ?>
                    <p class="buttoneer">
                        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary updater']) ?>
                        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                        <?= Html::a('Llave', ['getkey', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    </p>
                </div><!-- /.box-body -->
            </div>    
        </div>
    </div>
</section>