<?php

use yii\helpers\Html;
use yii\grid\GridView;


$this->title = 'Usuarios';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile(Yii::$app->urlManager->getBaseUrl() . '/css/configuracion/config.css');
$this->registerCssFile(Yii::$app->urlManager->getBaseUrl() . '/css/configuracion/usuarios.css');
$this->registerJsFile(Yii::$app->homeUrl.'js/ajaxupload.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->homeUrl.'js/usuarios/main.js', ['position' => $this::POS_END]);

?>
<section class="content-header">

    <h1><?= Html::encode($this->title) ?></h1>

</section>
<section class="content">
    <div class="row">        
        <div class="col-md-12 main_box">
            <div class="box box-default box-bajas user-index" id="members_grid">
                <div class="box-header with-border">
                    <p>
                        <?php
                            echo Html::a('Crear Usuario',['create'], [
                            'class' => 'btn btn-success',
                            'title' => Yii::t('yii', 'Close'),
                            'onclick'=>"
                                addLoading('#members_grid');
                                $.ajax({
                               type     :'POST',
                               cache    : false,
                               url  : $(this).attr('href')+'?partial=true',
                               success  : function(response) {                                   
                                   $('#ajax_div').html(response);
                                   $('#ajaxModal').modal();
                                   endLoading('#members_grid');
                               }
                               });return false;",
                               ]);
                        ?>    
                    </p>
                    <div class="box-tools pull-right">      
                      <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                      <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body" id="grid_cont">
                    <?php echo $this->render('grid', [
                    'dataProvider' => $dataProvider,
                    ]); ?>
                </div><!-- /.box-body -->
            </div>    
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="ajaxModal" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true" data-backdrop="false">
  <div class="modal-dialog">
    <!--<div class="modal-content">-->
      <div id="ajax_div" class="box-header">
      </div>
    <!--</div>-->    
  </div>
</div>