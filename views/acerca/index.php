<?php
  use yii\helpers\Url;
  use yii\helpers\Html;
  use yii\widgets\ActiveForm;

  /* @var $this yii\web\View */
  $this->title = 'Acerca de la aplicación';
?>

<section class="content-header">

  <h1>
    <?php echo $this->title; ?>
    <small> Equipo de desarrollo</small>
  </h1>

  <ol class="breadcrumb">
    <li><a href="<?php echo Yii::$app->homeUrl; ?>"><i data-toggle="tooltip" data-placement="left" title="Regresar a la página de inicio" class="fa fa-home"></i> Inicio</a></li>
    <li class="active"><?php echo $this->title; ?></li>
  </ol>

</section>

<section class="content">
  <div class="col-sm-7">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Equipo de Desarrollo</h3>
      </div><!-- /.box-header -->
      <div class="box-body">
        <a href="http://unlimitedthinks.com"><img src="<?php echo Yii::$app->urlManager->createUrl(['img/ut_logo.jpg']); ?>" class="admin-image" alt="Logo" /></a>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div>
  <div class="col-sm-5">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Manual de Usuario</h3>
        <span class="label label-primary pull-right"><i class="fa fa-book"></i></span>
      </div><!-- /.box-header -->
      <div class="box-body">
        <p>
          El siguiente manual le permitirá al usuario hacer uso correcto de la herramienta.
        </p>
        <a href="<?php echo Yii::$app->urlManager->createUrl(['manual.pdf']); ?>" class="btn btn-primary" target="_blank"><i class="fa fa-download"></i> Descargar</a>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div>
</section>
<style>
    .admin-image {
        max-width:300px;
        margin-left:auto;
        margin-right:auto;
        display:block;
    }
    .box-title {
        font-size:21px;
    }
</style>