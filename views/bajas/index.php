<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerCssFile(Yii::$app->urlManager->getBaseUrl() . '/css/bajas/bajas.css');
$this->registerJsFile(Yii::$app->urlManager->getBaseUrl() . '/js/bajas/bajas.js');

/* @var $this yii\web\View */
$this->title = 'Bajas';
?>

<section class="content-header">

	<h1>
		<?php echo $this->title; ?>
		<small>Personal policial</small>
	</h1>

	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::$app->homeUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
		<li class="active"><?php echo $this->title; ?></li>
	</ol>

</section>

<section class="content">
	
	<div class="row">

		<div class="col-md-12">
			
			<div class="box box-bajas">
				<div class="box-body">
					<?php
					$f = ActiveForm::begin([
						'action'                 => Url::toRoute(''),
						'method'                 => 'post',
						'enableClientValidation' => true
					]);
					?>
						<div class="col-md-2"></div>
						<div class="col-md-3 text-right label-search">
							<label for="search-element">Buscar elemento a dar de baja:</label>
						</div>
						<div class="input-group input-group-sm col-md-4">
							<?php echo $f->field($form, 'search_element')->input('search', ['class' => 'form-control', 'placeholder' => 'Busqueda por número de empleado o nombre']); ?>
							<span class="input-group-btn">
								<?php echo Html::submitButton('Buscar', ['class' => 'btn btn-info btn-flat btn-search-bajas']); ?>
							</span>
						</div>
						<div class="col-md-3"></div>
					<?php ActiveForm::end(); ?>
				</div>

				<div class="box-body box-body-bajas">
					
					<div class="box">
						<table id="elementos-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th class="text-center">No</th>
									<th class="text-center">Número de Empleado</th>
									<th class="text-center">Nombre del Elemento</th>
									<th class="text-center">Nombramiento</th>
									<th class="text-center">Adscripción</th>
									<th class="text-center">Estatus</th>
									<th class="text-center no-sort"></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ( $arrElementos as $key => $oElemento ) { ?>
									<tr>
										<td class="text-center"><?php echo $key + 1; ?></td>
										<td class="text-center"><?php echo $oElemento->folio; ?></td>
										<td><?php echo $oElemento->nombre . ' ' . $oElemento->apellido_paterno . ' ' . $oElemento->apellido_materno; ?></td>
										<td><?php echo (!is_null($oElemento->nombramiento->grado)) ? $oElemento->nombramiento->grado : 'Sin nombramiento'; ?></td>
										<td><?php echo (!is_null($oElemento->adscripcion->nombre)) ? $oElemento->adscripcion->nombre : 'Sin adscripción'; ?></td>
										<td class="text-center">
											<?php
											if ($oElemento->idEstatus == 2) {
												echo 'Activo';
											} else if ($oElemento->idEstatus == 3) {
												echo 'Baja';
											}
											?>
										</td>
										<td class="text-center">
											<?php if ($oElemento->idEstatus == 2) { ?>
												<button class="btn btn-block btn-danger" data-toggle="modal" href="#bajasModal-<?php echo $oElemento->id; ?>">Dar de baja</button>

												<?php
												$f = ActiveForm::begin([
													'action'                 => Url::toRoute('baja'),
													'method'                 => 'post',
													'enableClientValidation' => true
												]);
												?>
												
													<!-- Start - Popup Baja -->
													<div class="modal fade" id="bajasModal-<?php echo $oElemento->id; ?>" tabindex="-1" role="dialog" aria-labelledby="bajasModal" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																	<h4 class="modal-title modal-title-bajas" id="bajasModal">Bajas detalle</h4>
																</div>
																<div class="modal-body">
																
																	<div class="container-fluid">
																		<div class="row subtitle-baja">
																			<div class="col-md-12 text-left">Datos del activo</div>
																		</div>
																		<br>
																		<div class="row">
																			<div class="col-md-8">
																				<div class="row">
																					<div class="col-md-6 text-right field-name">Número de empleado:</div>
																					<div class="col-md-6 text-left" class="background-blue"><?php echo $oElemento->folio; ?></div>
																				</div>
																				<div class="row">
																					<div class="col-md-6 text-right field-name">Nombre del elemento:</div>
																					<div class="col-md-6 text-left"><?php echo $oElemento->nombre . ' ' . $oElemento->apellido_paterno . ' ' . $oElemento->apellido_materno; ?></div>
																				</div>
																				<div class="row">
																					<div class="col-md-6 text-right field-name">Nombramiento:</div>
																					<div class="col-md-6 text-left"><?php echo (!is_null($oElemento->nombramiento->grado)) ? $oElemento->nombramiento->grado : 'Sin nombramiento'; ?></div>
																				</div>
																				<div class="row">
																					<div class="col-md-6 text-right field-name">Adscripción:</div>
																					<div class="col-md-6 text-left"><?php echo (!is_null($oElemento->adscripcion->nombre)) ? $oElemento->adscripcion->nombre : 'Sin adscripción'; ?></div>
																				</div>
																			</div>
																			<div class="col-md-4 text-right">
																				<img src="<?php echo Yii::$app->urlManager->createUrl(['img/fotos_elementos/' . $oElemento->fotografia ]); ?>" width="100" height="100" class="img-thumbnail" />
																			</div>
																		</div>

																		<br>

																		<div class="row subtitle-baja">
																			<div class="col-md-12 text-left">Datos de la baja</div>
																		</div>
																		<br>
																		<div class="row datos-de-baja-cont">
																			<div class="col-md-3 field-name text-center">Tipo de baja:</div>
																			<div class="col-md-4 field-name text-center">Motivo:</div>
																			<div class="col-md-5 field-name text-center">Fecha:</div>
																		</div>
																		<div class="row">
																			<div class="col-md-4">
																				<select name="tipo_baja" class="form-control tipo-baja">
																					<option value="1">Ordinaria</option>
																					<option value="0">Extraordinaria</option>
																				</select>
																			</div>
																			<div class="col-md-4">
																				<select name="motivo_ordinario_baja" class="form-control dropdown-ordinarias">
																					<?php foreach ($arrMotivosOrdinarios as $oMotivoOrdinario) { ?>
																						<option value="<?php echo $oMotivoOrdinario->id; ?>"><?php echo $oMotivoOrdinario->motivo; ?></option>
																					<?php } ?>
																				</select>
																				<select name="motivo_extraordinario_baja" class="form-control dropdown-extraordinarias">
																					<?php foreach ($arrMotivosExtraordinarias as $oMotivoExtraordinario) { ?>
																						<option value="<?php echo $oMotivoExtraordinario->id; ?>"><?php echo $oMotivoExtraordinario->motivo; ?></option>
																					<?php } ?>
																				</select>
																			</div>
																			<div class="col-md-4">
																				<input type="date" name="fecha_baja" class="form-control input-fecha-baja" size="16" placeholder="dd/mm/aaaa" />
																			</div>
																		</div>
																		<div class="row" id="fecha-error">
																			<div class="col-md-7"></div>
																			<div class="col-md-5">
																				<div class="error-popup">Favor de seleccionar una fecha</div>
																			</div>
																		</div>

																		<br>

																		<div class="row otro-motivo-extraordinario">
																			<div class="col-md-1 text-right">
																				<label for="otro-motivo">Otro:</label>
																			</div>
																			<div class="col-md-11">
																				<input type="text" name="otro_motivo" id="otro-motivo" class="form-control" placeholder="Motivo extraordinario de baja" />
																			</div>
																		</div>
																		<div class="row" id="otro-motivo-error">
																			<div class="col-md-12 text-left">
																				<div class="error-popup">Favor de ingresar el motivo de baja</div>
																			</div>
																		</div>

																		<br>

																		<div class="row">
																			<div class="col-md-12 text-left field-name">Observaciones:</div>
																			<div class="clear"></div>
																			<div class="col-md-12">
																				<textarea name="observaciones_baja" class="form-control observaciones-baja" placeholder="Observaciones" rows="5"></textarea>
																				<div id="observaciones-error" class="error-popup text-left">Favor de ingresar las observaciones</div>
																			</div>
																		</div>
																	</div>

																</div>
																<div class="modal-footer">
																	<input type="hidden" name="idMotivoExtraordinarioOtro" id="idMotivoExtraordinarioOtro" value="<?php echo $numMotivoExtraordinarioOtro; ?>" />
																	<input type="hidden" name="idElemento" value="<?php echo $oElemento->id; ?>" />
																	<button type="button" class="btn btn-danger btn-baja-elemento" data-toggle="modal" data-id="<?php echo $oElemento->id; ?>">Aplicar Baja</button>
																	<button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-repeat"></i> Cancelar</button>
																</div>
															</div>
														</div>
													</div>
													<!-- End - Popup Baja -->

													<!-- Start - Popup Confirmacion -->
													<div class="modal fade confirmacion-popup" id="bajasConfirmacionModal-<?php echo $oElemento->id; ?>" tabindex="-1" role="dialog" aria-labelledby="bajasConfirmacionModal" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																	<h4 class="modal-title modal-title-bajas" id="bajasConfirmacionModal">Bajas de elementos</h4>
																</div>

																<div class="modal-body">
																	<div class="container-fluid">
																		<div class="row">
																			<div class="col-md-8">
																				<div class="row">
																					<div class="col-md-6 text-right field-name">Dará de baja a:</div>
																					<div class="col-md-6 text-left"><?php echo $oElemento->nombre . ' ' . $oElemento->apellido_paterno . ' ' . $oElemento->apellido_materno; ?></div>
																				</div>
																				<div class="row">
																					<div class="col-md-6 text-right field-name">Con número de folio:</div>
																					<div class="col-md-6 text-left"><?php echo $oElemento->folio; ?></div>
																				</div>
																				<div class="row">
																					<div class="col-md-6 text-right field-name">Adscrito a:</div>
																					<div class="col-md-6 text-left"><?php echo (!is_null($oElemento->adscripcion->nombre)) ? $oElemento->adscripcion->nombre : 'Sin adscripción'; ?></div>
																				</div>
																				<div class="row">
																					<div class="col-md-6 text-right field-name">Con nombramiento de:</div>
																					<div class="col-md-6 text-left"><?php echo (!is_null($oElemento->nombramiento->grado)) ? $oElemento->nombramiento->grado : 'Sin nombramiento'; ?></div>
																				</div>
																				<div class="ro">
																					<div class="col-md-6 text-right field-name">Con fecha de baja:</div>
																					<div class="col-md-6 text-left fecha-baja">DD/MM/YYYY</div>
																				</div>
																			</div>
																			<div class="col-md-4 text-right">
																				<img src="<?php echo Yii::$app->urlManager->createUrl(['img/fotos_elementos/' . $oElemento->fotografia ]); ?>" width="100" height="100" class="img-thumbnail" />
																			</div>
																		</div>

																		<br>

																		<div class="row">
																			<div class="col-md-12 text-center">
																				¿Desea proceder con la Baja?
																			</div>
																		</div>
																	</div>
																</div>

																<div class="modal-footer">
																	<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Si</button>
																	<button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-repeat"></i> No</button>
																</div>

															</div>
														</div>
													</div>
													<!-- End - Popup Confirmacion -->

												<?php ActiveForm::end(); ?>
											<?php } ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
							</tfoot>
						</table>
					</div>
				</div>

			</div>

		</div>

	</div>
</section>