<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\tiposEvaluacion $model
 */

$this->title = 'Tipos Evaluacion Update ' . $model->id . '';
$this->params['breadcrumbs'][] = ['label' => 'Tipos Evaluacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="tipos-evaluacion-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
