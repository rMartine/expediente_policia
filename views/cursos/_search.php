<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\cursosSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="cursos-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'numerico') ?>

		<?= $form->field($model, 'idEtapa') ?>

		<?= $form->field($model, 'nombre') ?>

		<?= $form->field($model, 'duracion') ?>

		<?php // echo $form->field($model, 'importancia') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
