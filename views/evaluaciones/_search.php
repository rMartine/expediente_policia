<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\evaluacionesSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="evaluaciones-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'idTipo') ?>

		<?= $form->field($model, 'nombre') ?>

		<?= $form->field($model, 'duracion') ?>

		<?= $form->field($model, 'importancia') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
