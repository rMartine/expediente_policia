<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerCssFile(Yii::$app->urlManager->getBaseUrl() . '/css/personal/personal.css');
$this->registerJsFile(Yii::$app->urlManager->getBaseUrl() . '/js/personal/personal.js');

/* @var $this yii\web\View */
$this->title = 'Historial';
?>

<section class="content-header">

	<h1>
		<?php echo $this->title; ?>
		<small>Personal policial</small>
	</h1>

	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::$app->homeUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
		<li class="active"><?php echo $this->title; ?></li>
	</ol>

</section>

<section class="content">
	
	<div class="row">

		<div class="col-md-12">
			
			<div class="box box-personal">
					<?php
					$f = ActiveForm::begin([
						'action'                 => Url::toRoute(''),
						'method'                 => 'post',
						'enableClientValidation' => true
					]);
					?>
						<div class="col-md-2"></div>
						<div class="col-md-3 text-right label-search">
							<label for="search-element">Buscar elemento:</label>
						</div>
						<div class="input-group input-group-sm col-md-4">
							<?php echo $f->field($form, 'search_element')->input('search', ['class' => 'form-control', 'placeholder' => 'Busqueda por número de empleado o nombre']); ?>
							<span class="input-group-btn">
								<?php echo Html::submitButton('Buscar', ['class' => 'btn btn-info btn-flat btn-search-personal']); ?>
							</span>
						</div>
						<div class="col-md-3"></div>
					<?php ActiveForm::end(); ?>
				</div>

				<div class="box-body box-body-personal">
					
					<div class="box">
						<table id="personal-table" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th class="text-center">No</th>
									<th class="text-center">Tipo</th>
									<th class="text-center">Empleado/Folio</th>
									<th class="text-center">Nombre del elemento</th>
									<th class="text-center">Nombramiento</th>
									<th class="text-center no-sort"></th>
								</tr>
							</thead>
							<tbody>
								<?php if (count($arrElementos) > 0) { ?>
									<?php foreach ( $arrElementos as $key => $oElemento ) { ?>
										<tr>
											<td class="text-center"><?php echo $key + 1; ?></td>
											<td><?php echo $oElemento->nombre_estatus; ?></td>
											<td class="text-center"><?php echo $oElemento->folio; ?></td>
											<td><?php echo $oElemento->nombre . ' ' . $oElemento->apellido_paterno . ' ' . $oElemento->apellido_materno; ?></td>
											<td><?php echo (!is_null($oElemento->nombramiento->grado)) ? $oElemento->nombramiento->grado : 'Sin nombramiento'; ?></td>
											<td class="text-center">
												<a href="<?php echo Yii::$app->homeUrl; ?>historial/detalle?elemento=<?php echo $oElemento->id; ?>" class="btn btn-default"><i class="fa fa-folder-open"></i> Abrir expediente</a>
											</td>
										</tr>
									<?php } ?>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>

			</div>

		</div>

	</div>
</section>