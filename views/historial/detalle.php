<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerCssFile(Yii::$app->urlManager->getBaseUrl() . '/css/personal/personal.css');
$this->registerJsFile(Yii::$app->urlManager->getBaseUrl() . '/js/personal/personal.js');
$this->registerJsFile(Yii::$app->urlManager->getBaseUrl() . '/js/aspirantes/aspirantes.js');
$this->registerCssFile(Yii::$app->urlManager->getBaseUrl() . '/css/aspirantes/aspirantes.css');

/* @var $this yii\web\View */
$this->title = 'Detalle historial';
?>

<section class="content-header">

	<h1>
		<?php echo $this->title; ?>:
	</h1>

	<br>

	<h1>
		<i class="fa fa-folder-open"></i>
		<small><?php echo $oElemento->nombre . ' ' . $oElemento->apellido_paterno . ' ' . $oElemento->apellido_materno; ?></small>
	</h1>

	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::$app->homeUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
		<li><a href="<?php echo Yii::$app->homeUrl; ?>historial"><i class="fa fa-clock-o"></i> Historial</a></li>
		<li class="active"><?php echo $oElemento->folio; ?></li>
	</ol>

</section>

<section class="content">
	
	<div class="content">
		<div class="box">
			<div class="nav-tabs-custom">

				<div class="tab-content">
					<div id="datos_generales">
						<?php echo $this->render('datos_generales', ['oElemento' => $oElemento]); ?>
					</div>
				</div>

			</div>
		</div>
	</div>

</section>