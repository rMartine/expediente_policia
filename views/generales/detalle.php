<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerCssFile(Yii::$app->urlManager->getBaseUrl() . '/css/personal/personal.css');
$this->registerJsFile(Yii::$app->urlManager->getBaseUrl() . '/js/personal/personal.js');
$this->registerJsFile(Yii::$app->urlManager->getBaseUrl() . '/js/aspirantes/aspirantes.js');
$this->registerCssFile(Yii::$app->urlManager->getBaseUrl() . '/css/aspirantes/aspirantes.css');

/* @var $this yii\web\View */
$this->title = 'Detalle generales';
?>

<section class="content-header">

	<h1>
		<?php echo $this->title; ?>:
	</h1>

	<br>

	<h1>
		<i class="fa fa-folder-open"></i>
		<small><?php echo $oElemento->nombre . ' ' . $oElemento->apellido_paterno . ' ' . $oElemento->apellido_materno; ?></small>
	</h1>

	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::$app->homeUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
		<li><a href="<?php echo Yii::$app->homeUrl; ?>personal"><i class="fa fa-laptop"></i> Personal</a></li>
		<li class="active"><?php echo $oElemento->folio; ?></li>
	</ol>

</section>

<section class="content">
	
	<div class="content">
		<div class="box">
			<div class="nav-tabs-custom">
				
				<ul class="nav nav-tabs">
					<li <?php echo ($tab == 1) ? 'class="active"' : ''; ?>><a href="#datos_generales" data-toggle="tab">Datos Generales</a></li>
					<li <?php echo ($tab == 2) ? 'class="active"' : ''; ?>><a href="#documentos" data-toggle="tab">Documentos</a></li>
				</ul>

				<!-- Start - User Messages -->
				<div class="user-messages">
					<div class="alert alert-success" <?php echo (Yii::$app->session->getFlash('success') != '') ? 'style="display: block;"' : ''; ?>>
						<i class="icon fa fa-check"></i> <span class="message"><?php echo Yii::$app->session->getFlash('success'); ?></span>
					</div>
					<div class="alert alert-danger" <?php echo (Yii::$app->session->getFlash('error') != '') ? 'style="display: block;"' : ''; ?>>
						<i class="icon fa fa-ban"></i> <span class="message"><?php echo Yii::$app->session->getFlash('error'); ?></span>
					</div>
				</div>
				<!-- End - User Messages -->

				<div class="tab-content">
					<div class="tab-pane <?php echo ($tab == 1) ? 'active' : ''; ?>" id="datos_generales">
						<?php echo $this->render('datos_generales', ['oElemento' => $oElemento]); ?>
					</div>
					<div class="tab-pane <?php echo ($tab == 2) ? 'active' : ''; ?>" id="documentos">
						<?php echo $this->render('documentos', ['oElemento' => $oElemento, 'arrRequerimientos' => $arrRequerimientos , 'arrDocumentos' => $arrDocumentos]); ?>
					</div>
				</div>

			</div>
		</div>
	</div>

</section>