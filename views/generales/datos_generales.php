<?php 
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\models\Estados;
use app\models\CargaImagen;
use app\models\Convocatorias;
use app\models\Estudios;
use yii\helpers\Url;
use app\models\Estatus;
use app\models\TiposElementos;

/* @var $this yii\web\View */
?>

<?php
$form = ActiveForm::begin([
	'enableClientValidation' => false,
	'id'                     => 'generales-form',
	'action'                 => Yii::$app->homeUrl . 'generales/save',
	'method'                 => 'post'
]);

echo $form->errorSummary($oElemento);
?>

	<!-- Start - Box Detalles Personal -->
	<div class="box-detalles-personal">
						
		<!-- Start - Fotografia Container -->
		<div class="fotografia-container">
			<div class="block-subtitle">Fotografía del Elemento</div>
			<div class="row">
				<div class="col-xs-3">
					<?php
						echo Html::img(Yii::$app->urlManager->getBaseUrl() . '/img/fotos_elementos/' . $oElemento->fotografia, [
							'alt'=>'Fotografía del elemento ' . $oElemento->folio,
							'class'=>'pull-right foto'
						]);
					?>
				</div>
				<div class="col-xs-3">
					<a class="btn btn-app pull-left aspirante-detalle-ui" data-toggle="modal" data-target="#carga-foto">
						<i data-toggle="tooltip" data-placement="left" title="Cargar fotografía" class="glyphicon glyphicon-cloud-upload"></i> Fotografía
					</a>
				</div>
				<div class="col-xs-2 pull-right">
					<a id="edit-button" class="btn btn-app">
						<i data-toggle="tooltip" data-placement="left" title="Editar datos del elemento" class="glyphicon glyphicon-pencil"></i> Editar
					</a>
				</div>
				<div class="col-xs-2 pull-right">
					<a href="http://unlimitedthinks.com:8080/birt/frameset?__report=activos.rptdesign&__format=pdf&claveEmpleado=<?= $oElemento->clave_empleado; ?>" id="reporte-button" class="btn btn-app" target="_blank">
						<i data-toggle="tooltip" data-placement="left" title="Imprimir expediente del elemento" class="glyphicon glyphicon-print"></i>Imprimir Generales
					</a>
				</div>
			</div>
		</div>
		<!-- End - Fotografia Container -->

		<!-- Start - Left Huellas Container -->
		<div class="left-huellas-container">
			<div class="block-subtitle">Huellas Digitales de la Mano Izquierda</div>
			<div class="row">
				<div class="col-xs-1">
					<h6>Meñique Izquierdo</h6>
					<div class="row">
						<?= Html::img(Yii::$app->urlManager->getBaseUrl() . '/img/fingerprints/' . $oElemento->menique_izq, [
							'alt'=>'Dedo meñique izquierdo del elemento ' . $oElemento->folio,
							'width'=>'100',
							'height'=>'100',
							'class'=>'img-thumbnail'
						]);?>
					</div>
					<div class="row">
						<a class="btn btn-default btn-block ui aspirante-detalle-ui" data-toggle="modal" data-target="#modal-menique-izq">
							<i data-toggle="tooltip" data-placement="left" title="Carga huella digital" class="glyphicon glyphicon-cloud-upload"></i>
						</a>
					</div>
				</div>

				<div class="col-xs-1">
					<h6>Anular Izquierdo</h6>
					<div class="row">
						<?= Html::img(Yii::$app->urlManager->getBaseUrl() . '/img/fingerprints/' . $oElemento->anular_izq, [
							'alt'=>'Dedo anular izquierdo del elemento ' . $oElemento->folio,
							'width'=>'100',
							'height'=>'100',
							'class'=>'img-thumbnail'
						]);?>
					</div>
					<div class="row">
						<a class="btn btn-default btn-block ui aspirante-detalle-ui" data-toggle="modal" data-target="#modal-anular-izq">
							<i data-toggle="tooltip" data-placement="left" title="Carga huella digital" class="glyphicon glyphicon-cloud-upload"></i>
						</a>
					</div>
				</div>

				<div class="col-xs-1">
					<h6>Medio Izquierdo</h6>
					<div class="row">
						<?= Html::img(Yii::$app->urlManager->getBaseUrl() . '/img/fingerprints/' . $oElemento->medio_izq, [
							'alt'=>'Dedo medio izquierdo del elemento ' . $oElemento->folio,
							'width'=>'100',
							'height'=>'100',
							'class'=>'img-thumbnail'
						]);?>
					</div>
					<div class="row">
						<a class="btn btn-default btn-block ui aspirante-detalle-ui" data-toggle="modal" data-target="#modal-medio-izq">
							<i data-toggle="tooltip" data-placement="left" title="Carga huella digital" class="glyphicon glyphicon-cloud-upload"></i>
						</a>
					</div>
				</div>

				<div class="col-xs-1">
					<h6>Índice Izquierdo</h6>
					<div class="row">
						<?= Html::img(Yii::$app->urlManager->getBaseUrl() . '/img/fingerprints/' . $oElemento->indice_izq, [
							'alt'=>'Dedo índice izquierdo del elemento ' . $oElemento->folio,
							'width'=>'100',
							'height'=>'100',
							'class'=>'img-thumbnail'
						]);?>
					</div>
					<div class="row">
						<a class="btn btn-default btn-block ui aspirante-detalle-ui" data-toggle="modal" data-target="#modal-indice-izq">
							<i data-toggle="tooltip" data-placement="left" title="Carga huella digital" class="glyphicon glyphicon-cloud-upload"></i>
						</a>
					</div>
				</div>

				<div class="col-xs-1">
					<h6>Pulgar Izquierdo</h6>
					<div class="row">
						<?= Html::img(Yii::$app->urlManager->getBaseUrl() . '/img/fingerprints/' . $oElemento->pulgar_izq, [
							'alt'=>'Dedo pulgar izquierdo del elemento ' . $oElemento->folio,
							'width'=>'100',
							'height'=>'100',
							'class'=>'img-thumbnail'
						]);?>
					</div>
					<div class="row">
						<a class="btn btn-default btn-block ui aspirante-detalle-ui" data-toggle="modal" data-target="#modal-pulgar-izq">
							<i data-toggle="tooltip" data-placement="left" title="Carga huella digital" class="glyphicon glyphicon-cloud-upload"></i>
						</a>
					</div>
				</div>

				<div class="col-xs-3 pull-right">
					<h6>Orientación de la Mano Izquierda</h6>
					<div class="row">
						<?= Html::img(Yii::$app->urlManager->getBaseUrl() . '/img/left.png', [
							'alt'=>'Orientación de las huellas digitales de la mano izquierda' . $oElemento->folio,
							'width'=>'50%',
							'height'=>'50%',
							'class'=>'margin'
						]);?>
					</div>
				</div>
			</div>
		</div>
		<!-- End - Left Huellas Container -->

		<!-- Start - Right Huellas Container -->
		<div class="right-huellas-container">
			<div class="block-subtitle">Huellas Digitales de la Mano Derecha</div>
			<div class="row">
				<div class="col-xs-1">
					<h6>Pulgar Derecho</h6>
					<div class="row">
						<?= Html::img(Yii::$app->urlManager->getBaseUrl() . '/img/fingerprints/' . $oElemento->pulgar_der, [
							'alt'=>'Dedo pulgar derecho del elemento ' . $oElemento->folio,
							'width'=>'100',
							'height'=>'100',
							'class'=>'img-thumbnail'
						]);?>
					</div>
					<div class="row">
						<a class="btn btn-default btn-block ui aspirante-detalle-ui" data-toggle="modal" data-target="#modal-pulgar-der">
							<i data-toggle="tooltip" data-placement="left" title="Carga huella digital" class="glyphicon glyphicon-cloud-upload"></i>
						</a>
					</div>
				</div>

				<div class="col-xs-1">
					<h6>Índice Derecho</h6>
					<div class="row">
						<?= Html::img(Yii::$app->urlManager->getBaseUrl() . '/img/fingerprints/' . $oElemento->indice_der, [
							'alt'=>'Dedo índice derecho del elemento ' . $oElemento->folio,
							'width'=>'100',
							'height'=>'100',
							'class'=>'img-thumbnail'
						]);?>
					</div>
					<div class="row">
						<a class="btn btn-default btn-block ui aspirante-detalle-ui" data-toggle="modal" data-target="#modal-indice-der">
							<i data-toggle="tooltip" data-placement="left" title="Carga huella digital" class="glyphicon glyphicon-cloud-upload"></i>
						</a>
					</div>
				</div>

				<div class="col-xs-1">
					<h6>Medio Derecho</h6>
					<div class="row">
						<?= Html::img(Yii::$app->urlManager->getBaseUrl() . '/img/fingerprints/' . $oElemento->medio_der, [
							'alt'=>'Dedo medio derecho del elemento ' . $oElemento->folio,
							'width'=>'100',
							'height'=>'100',
							'class'=>'img-thumbnail'
						]);?>
					</div>
					<div class="row">
						<a class="btn btn-default btn-block ui aspirante-detalle-ui" data-toggle="modal" data-target="#modal-medio-der">
							<i data-toggle="tooltip" data-placement="left" title="Carga huella digital" class="glyphicon glyphicon-cloud-upload"></i>
						</a>
					</div>
				</div>

				<div class="col-xs-1">
					<h6>Anular Derecho</h6>
					<div class="row">
						<?= Html::img(Yii::$app->urlManager->getBaseUrl() . '/img/fingerprints/' . $oElemento->anular_der, [
							'alt'=>'Dedo anular derecho del elemento ' . $oElemento->folio,
							'width'=>'100',
							'height'=>'100',
							'class'=>'img-thumbnail'
						]);?>
					</div>
					<div class="row">
						<a class="btn btn-default btn-block ui aspirante-detalle-ui" data-toggle="modal" data-target="#modal-anular-der">
							<i data-toggle="tooltip" data-placement="left" title="Carga huella digital" class="glyphicon glyphicon-cloud-upload"></i>
						</a>
					</div>
				</div>

				<div class="col-xs-1">
					<h6>Meñique Derecho</h6>
					<div class="row">
						<?= Html::img(Yii::$app->urlManager->getBaseUrl() . '/img/fingerprints/' . $oElemento->menique_der, [
							'alt'=>'Dedo meñique derecho del elemento ' . $oElemento->folio,
							'width'=>'100',
							'height'=>'100',
							'class'=>'img-thumbnail'
						]);?>
					</div>
					<div class="row">
						<a class="btn btn-default btn-block ui aspirante-detalle-ui" data-toggle="modal" data-target="#modal-menique-der">
							<i data-toggle="tooltip" data-placement="left" title="Carga huella digital" class="glyphicon glyphicon-cloud-upload"></i>
						</a>
					</div>
				</div>

				<div class="col-xs-3 pull-right">
					<h6>Orientación de la Mano Derecha</h6>
					<div class="row">
						<?= Html::img(Yii::$app->urlManager->getBaseUrl() . '/img/right.png', [
							'alt'=>'Orientación de las huellas digitales de la mano derecha' . $oElemento->folio,
							'width'=>'50%',
							'height'=>'50%',
							'class'=>'margin'
						]);?>
					</div>
				</div>
			</div>
		</div>
		<!-- End - Right Huellas Container -->

		<!-- Start - Datos Container -->
		<div class="datos-container">
			<div class="block-subtitle">Datos del Elemento</div>
			<div class="row">
				<div class="form-group col-xs-2">
					<?= $form->field($oElemento, 'idEstatus')
							 ->dropDownList(
							 	ArrayHelper::map(Estatus::find()->select('id, nombre')->all(), 'id', 'nombre'),
							 	['class'=>'form-control', 'disabled' => true, 'data-realestatus' => $oElemento->idEstatus, 'data-realidetapa' => $oElemento->idEtapa]
							 )
							 ->label('Estatus');?>
					<?= $form->field($oElemento, 'idEtapa')
							->label('')
							->textInput([
							 	'type' => 'hidden',
							 	'id' => 'input-id-etapa',
							 	'value' => ''
							 ])?>
				</div>
				<div class="form-group col-xs-2">
					<?= $form->field($oElemento, 'rfc')
							 ->textInput([
							 	'placeholder' => 'RFC del elemento...',
							 	'class'=>'form-control aspirante-detalle-ui'
							 ])
							 ->label('RFC'); ?>
				</div>
				<div class="form-group col-xs-2">
					<?= $form->field($oElemento, 'curp')
							 ->textInput([
							 	'placeholder' => 'CURP del elemento...',
							 	'class'=>'form-control aspirante-detalle-ui'
							 ])
							 ->label('CURP'); ?>
				</div>
				<div class="form-group col-xs-2">
					<?= $form->field($oElemento, 'cuip')
							 ->textInput([
							 	'placeholder' => 'CUIP del elemento...',
							 	'class'=>'form-control aspirante-detalle-ui'
							 ])
							 ->label('CUIP'); ?>
				</div>
				<div class="form-group col-xs-2">
					<?= $form->field($oElemento, 'idConvocatoria')
							 ->dropDownList(
							 	ArrayHelper::map(Convocatorias::find()->select('id, folio')->all(), 'id', 'folio'),
							 	['class'=>'form-control', 'readonly' => true,
							 	'disabled' => true]
							 )
							 ->label('Convocatoria');?>
				</div>
                                <div class="form-group col-xs-2">
                                        <?= $form->field($oElemento, 'idTipo')
                                                         ->dropDownList(
                                                                ArrayHelper::map(TiposElementos::find()->select('id, nombre')->all(), 'id', 'nombre'),
                                                                ['class'=>'form-control aspirante-detalle-ui']
                                                         )
                                                         ->label('Departamento');?>
                                </div>
			</div>
			<div class="row">
				<div class="form-group col-xs-2">
					<label class="control-label" for="elementos-fecha_ingreso">
						Fecha de Registro:
					</label>
					<label class="control-label" for="elementos-fecha_ingreso">
						<?= $oElemento->fechaIngresoText; ?>
					</label>
					<input type="hidden" id="elementos-fecha_ingreso" class="form-control" name="Elementos[fecha_ingreso]" value="<?= $oElemento->fecha_ingreso; ?>">
				</div>
				<div class="form-group col-xs-2">
					<?= $form->field($oElemento, 'estado_civil')
							 ->dropDownList(['soltero'=>'Soltero',
							 				 'casado'=>'Casado',
							 				 'divorciado'=>'Divorciado',
							 				 'viudo'=>'Viudo'],
							 				 ['class'=>'form-control aspirante-detalle-ui'])
							 ->label('Estado Civil');?>
				</div>
				<div class="form-group col-xs-2">
					<?= $form->field($oElemento, 'fecha_nac')
						 ->textInput([
						 	'type' => 'date',
						 	'placeholder' => 'Fecha de nacimiento del elemento...',
							'class'=>'form-control aspirante-detalle-ui'
						 ])
						 ->label('Fecha de Nacimiento'); ?>
				</div>
				<div class="form-group col-xs-2">
					<?php
						$edad = floor((time() - strtotime($oElemento->fecha_nac))/31536000);
						
					?>
					<label class="control-label" for="elementos-edad">Edad</label>
					<input type="text" id="elementos-edad" class="form-control aspirante-detalle-ui" name="Elementos[edad]" value="<?= $edad; ?>" maxlength="3" placeholder="Edad del elemento...">
				</div>
				<div class="form-group col-xs-2">
					<?= $form->field($oElemento, 'idEstudios')
							 ->dropDownList(
							 	ArrayHelper::map(Estudios::find()->all(), 'id', 'grado_estudios'),
							 	['class'=>'form-control aspirante-detalle-ui']
							 )
							 ->label('Nivel Escolar');?>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-4">
					<?= $form->field($oElemento, 'nombre')
							 ->textInput([
							 	'placeholder' => 'Nombre del elemento...',
							 	'class'=>'form-control aspirante-detalle-ui'
							 ])
							 ->label('Nombre(s)'); ?>
				</div>
				<div class="form-group col-xs-4">
					<?= $form->field($oElemento, 'apellido_paterno')
							 ->textInput([
							 	'placeholder' => 'Apellido paterno del elemento...',
							 	'class'=>'form-control aspirante-detalle-ui'
							 ])
							 ->label('Apellido Paterno'); ?>
				</div>
				<div class="form-group col-xs-4">
					<?= $form->field($oElemento, 'apellido_materno')
							 ->textInput([
							 	'placeholder' => 'Apellido materno del elemento...',
							 	'class'=>'form-control aspirante-detalle-ui'
							 ])
							 ->label('Apellido Materno'); ?>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-2">
					<?php
						echo $form->field($oElemento, 'sexo')
								  ->dropDownList(['M'=>'Masculino', 'F'=>'Femenino'],
								  				 ['class'=>'form-control aspirante-detalle-ui'])
								  ->label('Sexo');
					?>
				</div>
				<div class="form-group col-xs-3">
					<?php
						$estados = new Estados;
						echo $form->field($oElemento, 'estado_nac')
							->dropDownList(ArrayHelper::map($estados->find()->all(), 'id', 'nombre'),
										   ['class'=>'form-control aspirante-detalle-ui'])
							->label('Estado de Nacimiento');
					?>
				</div>
				<div class="form-group col-xs-4">
					<?= $form->field($oElemento, 'municipio_nac')
							 ->textInput([
							 	'placeholder' => 'Municipio donde nació el aspirante...',
							 	'class'=>'form-control aspirante-detalle-ui'
							 ])
							 ->label('Municipio de Nacimiento'); ?>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-2">
					<?= $form->field($oElemento, 'telefono')
							 ->textInput([
							 	'placeholder' => 'Número telefónico del elemento...',
							 	'class'=>'form-control aspirante-detalle-ui'
							 ])
							 ->label('Teléfono'); ?>
				</div>
				<div class="form-group col-xs-2">
					<?= $form->field($oElemento, 'movil')
							 ->textInput([
							 	'placeholder' => 'Número celular del elemento...',
							 	'class'=>'form-control aspirante-detalle-ui'
							 ])
							 ->label('Celular'); ?>
				</div>
				<div class="form-group col-xs-2">
					<?= $form->field($oElemento, 'email')
							 ->textInput([
							 	'placeholder' => 'Email del elemento...',
							 	'class'=>'form-control aspirante-detalle-ui'
							 ])
							 ->label('Correo Electrónico'); ?>
				</div>
			</div>
		</div>
		<!-- End - Datos Container -->

		<!-- Start - Domicilio Container -->
		<div class="domicilio-container">
			<div class="block-subtitle">Domicilio del Elemento</div>
			<div class="row">
				<div class="form-group col-xs-2">
					<?php
						$estados = new Estados;
						echo $form->field($oElemento, 'domicilio_edo')
							->dropDownList(ArrayHelper::map($estados->find()->all(), 'id', 'nombre'),
										  ['class'=>'form-control aspirante-detalle-ui'])
							->label('Estado');
					?>
				</div>
				<div class="form-group col-xs-2">
					<?= $form->field($oElemento, 'domicilio_mpo')
							 ->textInput([
							 	'placeholder' => 'Domicilio...',
							 	'class'=>'form-control aspirante-detalle-ui'
							 ])
							 ->label('Municipio'); ?>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-2">
					<?= $form->field($oElemento, 'domicilio_col')
							 ->textInput([
							 	'placeholder' => 'Domicilio...',
							 	'class'=>'form-control aspirante-detalle-ui'
							 ])
							 ->label('Colonia'); ?>
				</div>
				<div class="form-group col-xs-2">
					<?= $form->field($oElemento, 'domicilio_cp')
							 ->textInput([
							 	'placeholder' => 'Domicilio...',
							 	'class'=>'form-control aspirante-detalle-ui'
							 ])
							 ->label('C. P.'); ?>
				</div>
				<div class="form-group col-xs-2">
					<?= $form->field($oElemento, 'domicilio_calle')
							 ->textInput([
							 	'placeholder' => 'Domicilio...',
							 	'class'=>'form-control aspirante-detalle-ui'
							 ])
							 ->label('Calle'); ?>
				</div>
				<div class="form-group col-xs-2">
					<?= $form->field($oElemento, 'domicilio_next')
							 ->textInput([
							 	'placeholder' => 'Domicilio...',
							 	'class'=>'form-control aspirante-detalle-ui'
							 ])
							 ->label('Número Exterior'); ?>
				</div>
				<div class="form-group col-xs-2">
					<?= $form->field($oElemento, 'domicilio_nint')
							 ->textInput([
							 	'placeholder' => 'Domicilio...',
							 	'class'=>'form-control aspirante-detalle-ui'
							 ])
							 ->label('Número Interior'); ?>
				</div>
				
			</div>
		</div>
		<!-- End - Domicilio Container -->

	</div>
	<!-- End - Box Detalles Personal -->

	<!-- Start - Box Buttons Container -->
	<div class="box-buttons-container">

		<div class="row">
			<div class="col-md-12 text-right">
				<input type="hidden" name="idElemento" value="<?php echo $oElemento->id; ?>" />
				<input type="hidden" id="operacion" name="campo" value="2" />
				<button class="btn btn-primary aspirante-detalle-ui" id="btn-guardar" type="button"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
				<a href="<?php echo Yii::$app->homeUrl; ?>generales" class="btn btn-default aspirante-detalle-ui" id="btn-cancelar"><i class="glyphicon glyphicon-repeat"></i> Cancelar</a>
			</div>
		</div>

	</div>
	<!-- End - Box Buttons Container -->
<?php ActiveForm::end(); ?>

<!-- Modal de carga de Fotografía -->
<div class="modal fade" id="carga-foto" tabindex="-1" role="dialog" aria-labelledby="carga-foto" aria-hidden="true">
	<div class="modal-dialog">
		<?php
		$form = ActiveForm::begin([
			'action'  => Yii::$app->homeUrl . 'generales/save',
			'options' => ['enctype' => 'multipart/form-data']
		]);
		?>
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title modal-title-carga">Actualizar fotografía del elemento <?= $oElemento->folio; ?></h4>
				</div>
				<div class="modal-body">
					<?php $model = new CargaImagen(); ?>
					<?= $form->field($model, 'file')
							 ->fileInput(['accept'=>'image/*'])
							 ->label('Carga de fotografía') ?>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="idElemento" value="<?php echo $oElemento->id; ?>" />
					<input type="hidden" name="campo" value="0" />
					<button class="btn btn-primary">Guardar</button>
					<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>


<!-- Modales de carga de Huallas digitales mano izqaspirante-detalle-uierda -->
<div class="modal fade" id="modal-menique-izq" tabindex="-1" role="dialog" aria-labelledby="modal-menique-izq" aria-hidden="true">
	<div class="modal-dialog">
		<?php
		$form = ActiveForm::begin([
			'action'  => Yii::$app->homeUrl . 'generales/save',
			'options' => ['enctype' => 'multipart/form-data']
		]);
		?>
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title modal-title-carga">Carga de Meñique Izquierdo para el elemento <?= $oElemento->folio; ?></h4>
				</div>
				<div class="modal-body">
					<?php $model = new CargaImagen(); ?>
					<?= $form->field($model, 'file')
							 ->fileInput(['accept'=>'image/*'])
							 ->label('Carga de fotografía') ?>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="idElemento" value="<?php echo $oElemento->id; ?>" />
					<input type="hidden" name="campo" value="menique_izq" />
					<button class="btn btn-primary">Guardar</button>
					<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>

<div class="modal fade" id="modal-anular-izq" tabindex="-1" role="dialog" aria-labelledby="modal-anular-izq" aria-hidden="true">
	<div class="modal-dialog">
		<?php
		$form = ActiveForm::begin([
			'action'  => Yii::$app->homeUrl . 'generales/save',
			'options' => ['enctype' => 'multipart/form-data']
		]);
		?>
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title modal-title-carga">Carga de Anular Izquierdo para el elemento <?= $oElemento->folio; ?></h4>
				</div>
				<div class="modal-body">
					<?php $model = new CargaImagen(); ?>
					<?= $form->field($model, 'file')
							 ->fileInput(['accept'=>'image/*'])
							 ->label('Carga de fotografía') ?>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="idElemento" value="<?php echo $oElemento->id; ?>" />
					<input type="hidden" name="campo" value="anular_izq" />
					<button class="btn btn-primary">Guardar</button>
					<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>

<div class="modal fade" id="modal-medio-izq" tabindex="-1" role="dialog" aria-labelledby="modal-medio-izq" aria-hidden="true">
	<div class="modal-dialog">
		<?php
		$form = ActiveForm::begin([
			'action'  => Yii::$app->homeUrl . 'generales/save',
			'options' => ['enctype' => 'multipart/form-data']
		]);
		?>
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title modal-title-carga">Carga de Medio Izquierdo para el elemento <?= $oElemento->folio; ?></h4>
				</div>
				<div class="modal-body">
					<?php $model = new CargaImagen(); ?>
					<?= $form->field($model, 'file')
							 ->fileInput(['accept'=>'image/*'])
							 ->label('Carga de fotografía') ?>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="idElemento" value="<?php echo $oElemento->id; ?>" />
					<input type="hidden" name="campo" value="medio_izq" />
					<button class="btn btn-primary">Guardar</button>
					<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>

<div class="modal fade" id="modal-indice-izq" tabindex="-1" role="dialog" aria-labelledby="modal-indice-izq" aria-hidden="true">
	<div class="modal-dialog">
		<?php
		$form = ActiveForm::begin([
			'action'  => Yii::$app->homeUrl . 'generales/save',
			'options' => ['enctype' => 'multipart/form-data']
		]);
		?>
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title modal-title-carga">Carga de Índice Izquierdo para el elemento <?= $oElemento->folio; ?></h4>
				</div>
				<div class="modal-body">
					<?php $model = new CargaImagen(); ?>
					<?= $form->field($model, 'file')
							 ->fileInput(['accept'=>'image/*'])
							 ->label('Carga de fotografía') ?>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="idElemento" value="<?php echo $oElemento->id; ?>" />
					<input type="hidden" name="campo" value="indice_izq" />
					<button class="btn btn-primary">Guardar</button>
					<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>

<div class="modal fade" id="modal-pulgar-izq" tabindex="-1" role="dialog" aria-labelledby="modal-pulgar-izq" aria-hidden="true">
	<div class="modal-dialog">
		<?php
		$form = ActiveForm::begin([
			'action'  => Yii::$app->homeUrl . 'generales/save',
			'options' => ['enctype' => 'multipart/form-data']
		]);
		?>
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title modal-title-carga">Carga de Pulgar Izquierdo para el elemento <?= $oElemento->folio; ?></h4>
				</div>
				<div class="modal-body">
					<?php $model = new CargaImagen(); ?>
					<?= $form->field($model, 'file')
							 ->fileInput(['accept'=>'image/*'])
							 ->label('Carga de fotografía') ?>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="idElemento" value="<?php echo $oElemento->id; ?>" />
					<input type="hidden" name="campo" value="pulgar_izq" />
					<button class="btn btn-primary">Guardar</button>
					<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>


<!-- Modales de carga de Huellas digitales mano derecha -->
<div class="modal fade" id="modal-menique-der" tabindex="-1" role="dialog" aria-labelledby="modal-menique-der" aria-hidden="true">
	<div class="modal-dialog">
		<?php
		$form = ActiveForm::begin([
			'action'  => Yii::$app->homeUrl . 'generales/save',
			'options' => ['enctype' => 'multipart/form-data']
		]);
		?>
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title modal-title-carga">Carga de Meñique Derecho para el elemento <?= $oElemento->folio; ?></h4>
				</div>
				<div class="modal-body">
					<?php $model = new CargaImagen(); ?>
					<?= $form->field($model, 'file')
							 ->fileInput(['accept'=>'image/*'])
							 ->label('Carga de fotografía') ?>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="idElemento" value="<?php echo $oElemento->id; ?>" />
					<input type="hidden" name="campo" value="menique_der" />
					<button class="btn btn-primary">Guardar</button>
					<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>

<div class="modal fade" id="modal-anular-der" tabindex="-1" role="dialog" aria-labelledby="modal-anular-der" aria-hidden="true">
	<div class="modal-dialog">
		<?php
		$form = ActiveForm::begin([
			'action'  => Yii::$app->homeUrl . 'generales/save',
			'options' => ['enctype' => 'multipart/form-data']
		]);
		?>
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title modal-title-carga">Carga de Anular Derecho para el elemento <?= $oElemento->folio; ?></h4>
				</div>
				<div class="modal-body">
					<?php $model = new CargaImagen(); ?>
					<?= $form->field($model, 'file')
							 ->fileInput(['accept'=>'image/*'])
							 ->label('Carga de fotografía') ?>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="idElemento" value="<?php echo $oElemento->id; ?>" />
					<input type="hidden" name="campo" value="anular_der" />
					<button class="btn btn-primary">Guardar</button>
					<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>

<div class="modal fade" id="modal-medio-der" tabindex="-1" role="dialog" aria-labelledby="modal-medio-der" aria-hidden="true">
	<div class="modal-dialog">
		<?php
		$form = ActiveForm::begin([
			'action'  => Yii::$app->homeUrl . 'generales/save',
			'options' => ['enctype' => 'multipart/form-data']
		]);
		?>
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title modal-title-carga">Carga de Medio Derecho para el elemento <?= $oElemento->folio; ?></h4>
				</div>
				<div class="modal-body">
					<?php $model = new CargaImagen(); ?>
					<?= $form->field($model, 'file')
							 ->fileInput(['accept'=>'image/*'])
							 ->label('Carga de fotografía') ?>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="idElemento" value="<?php echo $oElemento->id; ?>" />
					<input type="hidden" name="campo" value="medio_der" />
					<button class="btn btn-primary">Guardar</button>
					<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>

<div class="modal fade" id="modal-indice-der" tabindex="-1" role="dialog" aria-labelledby="modal-indice-der" aria-hidden="true">
	<div class="modal-dialog">
		<?php
		$form = ActiveForm::begin([
			'action'  => Yii::$app->homeUrl . 'generales/save',
			'options' => ['enctype' => 'multipart/form-data']
		]);
		?>
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title modal-title-carga">Carga de Índice Derecho para el elemento <?= $oElemento->folio; ?></h4>
				</div>
				<div class="modal-body">
					<?php $model = new CargaImagen(); ?>
					<?= $form->field($model, 'file')
							 ->fileInput(['accept'=>'image/*'])
							 ->label('Carga de fotografía') ?>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="idElemento" value="<?php echo $oElemento->id; ?>" />
					<input type="hidden" name="campo" value="indice_der" />
					<button class="btn btn-primary">Guardar</button>
					<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>

<div class="modal fade" id="modal-pulgar-der" tabindex="-1" role="dialog" aria-labelledby="modal-pulgar-der" aria-hidden="true">
	<div class="modal-dialog">
		<?php
		$form = ActiveForm::begin([
			'action'  => Yii::$app->homeUrl . 'generales/save',
			'options' => ['enctype' => 'multipart/form-data']
		]);
		?>
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title modal-title-carga">Carga de Pulgar Derecho para el elemento <?= $oElemento->folio; ?></h4>
				</div>
				<div class="modal-body">
					<?php $model = new CargaImagen(); ?>
					<?= $form->field($model, 'file')
							 ->fileInput(['accept'=>'image/*'])
							 ->label('Carga de fotografía') ?>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="idElemento" value="<?php echo $oElemento->id; ?>" />
					<input type="hidden" name="campo" value="pulgar_der" />
					<button class="btn btn-primary">Guardar</button>
					<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>