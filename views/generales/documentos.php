<?php
use yii\bootstrap\ActiveForm;
use app\models\CargaPdf;
?>

<!-- Start - Box Detalles Personal -->
<div class="box-detalles-personal">

	<!-- Start - Documentos Container -->
	<div class="documentos-container">
		<div class="row">
			<div class="col-md-12 documentacion-title">
				<div class="col-md-2"></div>
				<div class="col-md-8"><label for="requerimientos">Documentación:</label></div>
				<div class="col-md-2"></div>
			</div>
			<div class="clear-both"></div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-7">
					<select name="requerimiento" id="requerimientos" class="form-control">
						<?php foreach($arrRequerimientos as $oRequerimiento) { ?>
							<option value="<?php echo $oRequerimiento->id ?>"><?php echo $oRequerimiento->nombre; ?></option>
						<?php } ?>
						<option value="-1">Otro</option>
					</select>
				</div>
				<div class="col-md-1">
					<a class="btn btn-default btn-carga-documento" data-toggle="modal">
						<i class="glyphicon glyphicon-cloud-upload"></i> Cargar
					</a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="clear-both"></div>
			<div class="col-md-12 nuevo-requerimiento">
				<div class="col-md-2"></div>
				<div class="col-md-1">
					<label for="descripcion-requerimiento">Descripción:</label>
				</div>
				<div class="col-md-6">
					<input type="text" name="descripcion-requerimiento" id="descripcion-requerimiento" class="form-control" placeholder="Descripción del documento" />
				</div>
				<div class="col-md-3"></div>
			</div>
			<div class="clear-both"></div>
		</div>
		<div class="block-subtitle">Documentos entregados</div>
		<div class="row">
			<div class="col-md-12">
				<table id="documentos-table" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th class="text-center">No</th>
							<th class="text-center">Documento</th>
							<th class="text-center">Fecha de carga</th>
							<th class="text-center no-sort"></th>
						</tr>
					</thead>
					<tbody>
						<?php if (count($arrDocumentos)) { ?>
							<?php foreach ($arrDocumentos as $key => $oDocumento) { ?>
								<tr>
									<td class="text-center"><?php echo $key + 1; ?></td>
									<td><?php echo $oDocumento->nombreDocumento; ?></td>
									<td class="text-center"><?php echo $oDocumento->fechaSubida; ?></td>
									<td class="text-center">
										<a href="<?php echo Yii::$app->homeUrl . 'documentos_elementos/' . $oDocumento->nombreArchivo; ?>" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a>
										<a href="<?php echo Yii::$app->homeUrl ?>generales/eliminar?elemento=<?php echo $oElemento->id; ?>&documento=<?php echo $oDocumento->id; ?>" class="btn-delete-documento"><i class="glyphicon glyphicon-trash"></i></a>
									</td>
								</tr>
							<?php } ?>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- End - Documentos Container -->

</div>
<!-- End - Box Detalles Personal -->

<!-- Start - Popups -->
<div class="popups-container">
	
	<!-- Start - Cargar Documento -->
	<div class="modal fade" id="cargar-documento" tabindex="-1" role="dialog" aria-labelledby="cargarDocumentoModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<?php
				$form = ActiveForm::begin([
					'id'      => 'frm-documentos',
					'action'  => Yii::$app->homeUrl . 'generales/cargar',
					'options' => ['enctype' => 'multipart/form-data']
				]);
				?>
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title modal-title-bajas" id="bajasModal">Cargar documento</h4>
					</div>
					<div class="modal-body">
						<div class="container-fluid">
							<?php
							$model = new CargaPdf();
							echo $form->field($model, 'file')->fileInput(['accept'=>'image/*'])->label('Carga de documento');
							?>
						</div>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="idElemento" value="<?php echo $oElemento->id; ?>" />
						<input type="hidden" name="nombreDocumento" id="nombreDocumento" value="" />
						<input type="hidden" name="otroDescripcion" id="otroDescripcion" value="" />
						<button type="button" class="btn btn-primary" id="btn-upload-file"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-repeat"></i> Cancelar</button>
					</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
	<!-- End - Cargar Documento -->

</div>
<!-- Start - Popups -->