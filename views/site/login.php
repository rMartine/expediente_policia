<?php
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->registerJsFile(Yii::$app->homeUrl.'js/login.js', ['position' => $this::POS_END]);
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Start - Login box -->
<div class="login-box">
    
    <div class="login-logo">

        <a><b>Expediente</b>Policia</a>
    </div>

    <div class="login-box-body">
        <p class="login-box-msg">Ingrese sus credenciales</p>

            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['class' => '', 'enctype' => 'multipart/form-data'],
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-13\">{input}</div>\n<div class=\"col-lg-13 back-out\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-1 control-label'],
                ],
            ]); ?>
            <div class="form-group has-feedback">
                <?= $form->field($model, 'username',['inputOptions'=>['class'=>'login-input required form-control', 'placeholder' => 'Nombre de usuario', 'autofocus' => '']])->label(false) ?>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <?= $form->field($model, 'password', ['inputOptions'=>['class'=>'login-input required passwf form-control', 'placeholder' => 'Contraseña']])->label(false)->passwordInput() ?>  
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8 file_container">
                        <label class="btn btn-app fotopload pull-left ui file_label key_label">
                            <i class="glyphicon glyphicon-lock"></i>Llave
                            <?php echo $form->field($model, 'keyfile',['inputOptions'=>['type' => 'file', 'class'=>'key_file']])->label(false) ?>

                        </label>
                        <?php
                            if(!empty($fileError)) {
                                echo $fileError;
                            }
                        ?>
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>
<!-- End - Login box -->
<style>
.key_label .key_file {
    display:none;
}
.key_label {
    margin-bottom:0px;
}
.file_container .fotopload {
    min-width: 0px;
    height: 35px;
    padding: 5px;
    margin: 0px;
    padding-bottom: 31px;
    margin-top: -4px;
}    

.file_container .fotopload i {
    font-size: 14px !important;
}

.key_on{
    color:green;
}
.key_off{
    color:red;
}
.error_message {
    margin:8px;
    color: #a94442;
}
</style>