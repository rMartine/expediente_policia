<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Adscripciones;
/**
* @var yii\web\View $this
* @var app\models\LogAdscripciones $model
* @var yii\widgets\ActiveForm $form
*/
$cssButton = $model->isNewRecord ? "btn btn-app" : "btn btn-app hidden";
$attributes = $formType == "view" ? ['disabled' => ''] : [];
?>

<div class="log-adscripciones-form">
    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => true, 'id' => 'form-log']); ?>
    <div class="">
        <?php echo $form->errorSummary($model, ['header' => 'Por favor corrija los errores:']); ?>
        <ul id="w0" class="nav nav-tabs"><li class="active"><a href="#w0-tab0" data-toggle="tab">Adscripcion</a></li></ul>

        <p>
            <?= $form->field($model, 'id')->label('')->textInput(['disabled' => '', 'type' => 'hidden']) ?>
			<?= $form->field($model->idElemento0, 'nombreText')->label('Nombre del elemento')->textInput(['readonly' => true]) ?>
			<?= $form->field($model, 'idAdscripcion')
                ->label('Adscripción')
                ->dropDownList(
                    ArrayHelper::map(Adscripciones::find()->all(), 'id', 'nombre'),
                    ['prompt' => ''] + $attributes
                )
            ?>
            <?= $form->field($model, 'oficio_comision')->textInput($attributes) ?>
            <?= $form->field($model, 'detalle_comision')->textInput($attributes) ?>
			<?= $form->field($model, 'fecha')->textInput(['type' => 'date'] + $attributes) ?>
			<?= $form->field($model, 'observaciones')->textarea(['rows' => 6] + $attributes) ?>
        </p>
        <hr/>

        <?= Html::submitButton('<span class="fa fa-save"></span> '.'Guardar', ['class' => $cssButton]) ?>
        <a href="<?php echo Url::previous(); ?>" class="<?php echo $cssButton ?>">
            <i class="fa fa-times"></i>Cancelar
        </a>

        <?php ActiveForm::end(); ?>

    </div>

</div>
