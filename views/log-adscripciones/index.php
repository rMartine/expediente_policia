
<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Adscripciones;

/* @var $this yii\web\View */
$this->registerJsFile(Yii::$app->homeUrl.'js/datatables/tablas.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->homeUrl . '/css/activos/planes.css');
$urlIndex = Url::to(['adscripciones/']);
$this->title = 'Adscripciones';
$blockName = 'adscripciones';
?>
<section class="content-header">

    <h1>
        <?php echo $this->title; ?>
        <small></small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="<?php echo Yii::$app->homeUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
        <li class="active"><?php echo $this->title; ?></li>
    </ol>

</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <?= $this->render('//planes/formacion_header', ['elemento' => $elemento]); ?>
        </div>
        <div class="box-body">
            <div class="bg-navy-active">
                <h5>Agregar nueva adscripción al elemento</h5>
            </div>
            <?php $form = ActiveForm::begin([
                'action' => Url::to(['log-adscripciones/', 'id' => $elemento->id]),
                'method' => 'post',
                'enableClientValidation' => true]);
            ?>
            <div class="row">
                <?php echo $form->errorSummary($model, ['header' => 'Por favor corrija los errores:']); ?>
            </div>
            <div>
                <?php $this->beginBlock($blockName); ?>
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($model, 'oficio_comision')->textInput(); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'idAdscripcion')
                            ->label('Nueva adscripción')
                            ->dropDownList(
                                ArrayHelper::map(Adscripciones::find()->all(), 'id', 'nombre'),
                                ['prompt' => '']
                            )
                        ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'fecha')->textInput(['type' => 'date']); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($model, 'detalle_comision')->textInput(); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'observaciones')->textarea(); ?>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label"></label>
                        <span class="">
                            <?php echo Html::submitButton('', ['class' => 'btn btn-info btn-block fa fa-plus-circle']); ?>
                        </span>
                        <?= $form->field($model, 'idElemento')->label('')->textInput(['type' => 'hidden', 'value' => $elemento->id]); ?>
                        <?= $form->field($model, 'idNombramiento')->label('')->textInput(['type' => 'hidden', 'value' => $elemento->getNombramiento()->id]); ?>
                    </div>
                </div>
                <?php $this->endBlock(); ?>
                <?php ActiveForm::end(); ?>
                <p>
                    <?= $this->blocks[$blockName]; ?>
                </p>
            </div>
            <h4>Histórico de adscripciones</h4>
            <?= $this->render('@app/views/layouts/modal', []); ?>
            <div class="bg-navy-active">
                <h5>Histórico de adscripciones</h5>
            </div>
            <table id="resultados" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Unidad Administrativa</th>
                        <th>Fecha</th>
                        <th>Comisión</th>
                        <th>Oficio</th>
                        <th>Cargo</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($elemento->logAdscripciones) > 0) { ?>
                        <?php $contador = 1; ?>
                        <?php foreach ( $elemento->logAdscripciones as $log ) { ?>
                            <tr>
                                <td><?php echo $contador++; ?></td>
                                <td><?php echo $log->adscripcion->nombre; ?></td>
                                <td><?php echo $log->fecha; ?></td>
                                <td><?php echo $log->detalle_comision; ?></td>
                                <td><?php echo $log->oficio_comision; ?></td>
                                <td><?php echo $elemento->getNombramiento()->grado ?></td>
                                <td>
                                    <a class="btn btn-block" data-toggle="modal" data-target="#modal-model" data-modal-type="edit" data-action="<?php echo Url::to(['log-adscripciones/update', 'id' => $log->id]) ?>">
                                        <span data-toggle="tooltip" data-placement="left" title="Editar" class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                </td>
                                <td>
                                    <a class="btn btn-block" data-toggle="modal" data-target="#modal-model" data-modal-type="remove" data-action="<?php echo Url::to(['log-adscripciones/delete', 'id' => $log->id]) ?>">
                                        <span data-toggle="tooltip" data-placement="left" title="Eliminar" class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
        <div class="box-footer">
            <a style="visibility:show" href="<?php echo $urlIndex; ?>" class="btn btn-app">
                <i class="fa fa-arrow-left"></i>Regresar
            </a>
        </div>
    </div>
</div>