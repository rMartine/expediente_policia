<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\LogAdscripciones $model
 */

$this->title = 'Log Adscripciones Update ' . $model->id . '';
$this->params['breadcrumbs'][] = ['label' => 'Log Adscripciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="log-adscripciones-update">
	<?php echo $this->render('_form', [
		'model' => $model,
		'formType' => 'edit',
	]); ?>
</div>
