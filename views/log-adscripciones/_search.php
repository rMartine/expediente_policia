<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\LogAdscripcionesSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="log-adscripciones-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'idElemento') ?>

		<?= $form->field($model, 'idAdscripcion') ?>

		<?= $form->field($model, 'fecha') ?>

		<?= $form->field($model, 'oficio_comision') ?>

		<?php // echo $form->field($model, 'detalle_comision') ?>

		<?php // echo $form->field($model, 'observaciones') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
