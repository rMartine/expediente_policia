
<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
$this->registerJsFile(Yii::$app->homeUrl.'js/datatables/tablas.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::$app->homeUrl . '/css/activos/planes.css');
$urlPlanes = Yii::$app->homeUrl . 'planes/';
$actionCursos = $urlPlanes . 'view?id=';
$this->title = 'Elementos activos';
?>

<section class="content-header">

	<h1>
		<?php echo $this->title; ?>
		<small></small>
	</h1>

	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::$app->homeUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
		<li class="active"><?php echo $this->title; ?></li>
	</ol>

</section>

<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<div class="col-md-6 col-md-offset-2 search-box">
				<?php
					$f = ActiveForm::begin([
						'action'                 => $urlPlanes,
						'method'                 => 'post',
						'enableClientValidation' => false
					]);
				?>

				<?= $this->render('//layouts/search_submit', []); ?>
				<?php ActiveForm::end(); ?>
			</div>
		</div>

		<div class="box-body">
			<div id="ajax-content" class="box">
				<div class="bg-navy-active">
					<h5>Resultados de la búsqueda</h5>
				</div>
				<table id="resultados" class="table table-bordered table-hover table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Número de empleado</th>
							<th>Nombre del elemento</th>
							<th>Nombramiento</th>
							<th>Adscripción</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php if (count($arrElementos) > 0) { ?>
							<?php $contador = 1; ?>
							<?php foreach ( $arrElementos as $oElemento ) { ?>
								<tr>
									<td><?php echo $contador++; ?></td>
									<td><?php echo $oElemento->clave_empleado; ?></td>
									<td><?php echo $oElemento->nombreText; ?></td>
									<td><?php echo $oElemento->getNombramiento()->grado; ?></td>
                                    <td><?php echo $oElemento->getAdscripcion()->nombre; ?></td>
									<td>
										<a class="btn btn-block" href="<?php echo $actionCursos.$oElemento->id ?>">
											<span data-toggle="tooltip" data-placement="left" title="Ver" class="glyphicon glyphicon-eye-open"></span>
										</a>
									</td>
								</tr>
							<?php } ?>
						<?php } ?>
					</tbody>
					<tfoot>
                    </tfoot>
				</table>
			</div>
		</div>

		<div class="box-footer">
		</div>

	</div>
</section>