<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Evaluaciones;
$this->registerJsFile(Yii::$app->homeUrl.'js/datatables/tablas.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$blockName = 'evaluacion-desempeno';
?>

<div class="">
	<div class="bg-navy-active">
		<h5>Agregar nueva evaluación al expediente del activo</h5>
	</div>
	<?php $form = ActiveForm::begin([
		'action' => Url::to(['log-evaluaciones/create']),
		'method' => 'post',
		'enableClientValidation' => true]);
	?>
	<div class="row">
		<?php echo $form->errorSummary($model, ['header' => 'Por favor corrija los errores']); ?>
	</div>
	<div class="row">
		<?php $this->beginBlock($blockName); ?>
		<div class="col-md-3">
			<?= $form->field($model, 'idEvaluacion')
				->label('Nombre de la evaluacion')
		        ->dropDownList(
		        	ArrayHelper::map(Evaluaciones::find()->where(['idTipo' => $tipoEvaluacion])->all(), 'id', 'nombre'),
		        	['prompt' => '--evaluacion--']
		        )
	        ?>
		</div>
		<div class="col-md-2">
			<?= $form->field($model, 'cumplimiento')->dropDownList([0 => 'No', 1 => 'Si']); ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($model, 'fecha')->textInput(['type' => 'date']); ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($model, 'observaciones')->textarea(); ?>
		</div>
		<div class="col-md-1">
			<label class="control-label"></label>
			<span class="">
				<?php echo Html::submitButton('', ['class' => 'btn btn-info btn-block fa fa-plus-circle']); ?>
			</span>
			<?= $form->field($model, 'idElemento')->label('')->textInput(['type' => 'hidden', 'value' => $elemento->id]); ?>
		</div>
		<?php $this->endBlock(); ?>
		<?php ActiveForm::end(); ?>
		<p>
			<?= $this->blocks[$blockName]; ?>
		</p>
	</div>
	<div class="bg-navy-active">
		<h5>Histórico de evaluaciones</h5>
	</div>
	<table id="resultados" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th>Evaluaciones</th>
				<th>Duracion</th>
				<th>Importancia</th>
				<th>Cumplimiento</th>
				<th>Fecha</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($elemento->logEvaluaciones) > 0) { ?>
				<?php $contador = 1; ?>
				<?php foreach ( $elemento->getFilteredEvaluaciones($tipoEvaluacion) as $log ) { ?>
					<tr>
						<td><?php echo $contador++; ?></td>
						<td><?php echo $log->evaluacion->nombre; ?></td>
						<td><?php echo $log->evaluacion->duracionText ?></td>
						<td><?php echo $log->evaluacion->importanciaText; ?></td>
						<td><?php echo $log->cumplimientoText; ?></td>
						<td><?php echo $log->fechaText; ?></td>
						<td>
							<a class="btn btn-block" data-toggle="modal" data-target="#modal-model" data-modal-type="edit" data-action="<?php echo Url::to(['log-evaluaciones/update', 'id' => $log->id]) ?>">
								<span data-toggle="tooltip" data-placement="left" title="Ver" class="glyphicon glyphicon-pencil"></span>
							</a>
						</td>
						<td>
							<a class="btn btn-block" data-toggle="modal" data-target="#modal-model" data-modal-type="remove" data-action="<?php echo Url::to(['log-evaluaciones/delete', 'id' => $log->id]) ?>">
								<span data-toggle="tooltip" data-placement="left" title="Eliminar" class="glyphicon glyphicon-trash"></span>
							</a>
						</td>
					</tr>
				<?php } ?>
			<?php } ?>
		</tbody>
		<tfoot>
        </tfoot>
	</table>
</div>