<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\TiposEvaluacion;

/* @var $this yii\web\View */
$this->registerCssFile(Yii::$app->homeUrl . '/css/activos/planes.css');
$this->registerJsFile(Yii::$app->homeUrl.'js/main/modals.js', ['depends' => [\yii\web\JqueryAsset::className()], 'position' => $this::POS_END]);

$urlPlanes = Yii::$app->homeUrl . 'planes';
$this->title = 'Plan individual';
?>

<section class="content-header">

	<h1>
		<?php echo $this->title; ?>
		<small></small>
	</h1>

	<ol class="breadcrumb">
		<li><a class='base_url' href="<?php echo Yii::$app->homeUrl; ?>"><i class="fa fa-home"></i> Inicio</a></li>
		<li class="active"><?php echo $this->title; ?></li>
	</ol>

</section>

<section class="content">
	<div class="box">
		<div class="box-body">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#tab_1" data-toggle="tab">Formacion Continua</a>
					</li>
					<li>
						<a href="#tab_2" data-toggle="tab">Evaluacion del desempeño</a>
					</li>
					<li>
						<a href="#tab_3" data-toggle="tab">Evaluacion<small> - destrezas,</small>
							<br><small>habilidades y conocimientos</small></a>
					</li>
					<?php if (\Yii::$app->user->identity->id_rol == 1) { ?>
						<li>
							<a href="#tab_4" data-toggle="tab">Evaluacion de control<br><small>y confianza</small></a>
						</li>
					<?php } ?>
				</ul>				
				<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<?= $this->render('formacion_header', ['elemento' => $elemento]); ?>
							<?= $this->render('formacion', ['model' => $model, 'cursos' => $cursos, 'elemento' => $elemento, 'arrCursos' => $arrCursos]); ?>
						</div>
						<div class="tab-pane" id="tab_2">
							<?= $this->render('formacion_header', ['elemento' => $elemento]); ?>
							<?= $this->render('evaluacion_desempeno', ['model' => $logEvaluacion, 'elemento' => $elemento, 
								'tipoEvaluacion' => TiposEvaluacion::DESEMPENO]); ?>
						</div>
						<div class="tab-pane" id="tab_3">
							<?= $this->render('formacion_header', ['elemento' => $elemento]); ?>
							<?= $this->render('evaluacion_desempeno', ['model' => $logEvaluacion, 'elemento' => $elemento, 
								'tipoEvaluacion' => TiposEvaluacion::DESTREZAS_HABILIDADES]); ?>
						</div>
						<?php if (\Yii::$app->user->identity->id_rol == 1) { ?>
							<div class="tab-pane" id="tab_4">
								<?= $this->render('formacion_header', ['elemento' => $elemento]); ?>
								<?= $this->render('evaluacion_desempeno', ['model' => $logEvaluacion, 'elemento' => $elemento,
									'tipoEvaluacion' => TiposEvaluacion::CONTROL_CONFIANZA]); ?>
							</div>
						<?php } ?>
					
				</div>
			</div>
		</div>

		<div class="box-footer">
			<a style="visibility:show" href="<?php echo $urlPlanes; ?>" class="btn btn-app">
				<i class="fa fa-arrow-left"></i>Regresar
			</a>
		</div>

	</div>
</section>
<div class="modal fade" id="modal-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">Detalle</h4>
    </div>
    <div class="modal-body">
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-repeat"></i> Cancelar</button>
      <button type="button" class="btn btn-primary" data-post=""><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
    </div>
  </div>
</div>
</div>
<script>
    $( document ).ready(function() {
        if(window.location.hash.length > 0) {
            $('a[href='+window.location.hash+']').click();
        }        
    });
</script>