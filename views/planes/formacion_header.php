<?php
use yii\helpers\Html;
$this->registerCssFile(Yii::$app->homeUrl . '/css/activos/planes.css');
?>
<div class="bg-navy-active">
	<h5>Datos del activo</h5>
</div>
<div class="row">
	<div class="col-md-6 col-md-offset-2">
		<div class="row">
			<div class="col-md-4">
				<p class="top-buffer10">Número de empleado:</p>
				<p class="top-buffer10">Nombre del elemento:</p>
				<p class="top-buffer10">Nombramiento:</p>
				<p class="top-buffer10">Adscripcion:</p>
			</div>
			<div class="col-md-6">
				<h4 class="control-label">
					<strong><?= $elemento->clave_empleado; ?></strong>
				</h4>
				<h4 class="control-label">
					<strong><?= $elemento->nombreText; ?></strong>
				</h4>
				<h4 class="control-label">
					<strong><?= $elemento->getNombramiento()->grado; ?></strong>
				</h4>
				<h4 class="control-label">
					<strong><?= $elemento->getAdscripcion()->nombre; ?></strong>
				</h4>
			</div>
		</div>
	</div>
	<div class="col-md-3">
	    <?= Html::img(Yii::$app->urlManager->getBaseUrl() . '/img/fotos_elementos/' . $elemento->fotografia, [
	        'alt'=>'Fotografía del elemento ' . $elemento->folio,
	        'width'=>'150',
	        'height'=>'150',
	        'class'=>'img-thumbnail pull-right'
	    ]);?>
	</div>
</div>