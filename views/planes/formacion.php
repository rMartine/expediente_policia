<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Cursos;
$this->registerJsFile(Yii::$app->homeUrl.'js/main/modals.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->homeUrl.'js/datatables/tablas.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$blockName = 'formacion';
?>
<div class="">
	<div class="bg-navy-active">
		<h5>Agregar nuevo curso al expediente del activo</h5>
	</div>
	<?php $form = ActiveForm::begin([
		'action' => Url::to(['planes/view', 'id' => $elemento->id]),
		'method' => 'post',
		'enableClientValidation' => true]);
	?>
	<div class="row">
		<?php echo $form->errorSummary($model, ['header' => 'Por favor corrija los errores:']); ?>
	</div>
	<div class="row">
		<?php $this->beginBlock($blockName); ?>
		<div class="col-md-2">
			<?= $form->field($model, 'idCurso')
				->label('Nombre del curso')
		        ->dropDownList(
		        	ArrayHelper::map(Cursos::find()->where(['idEtapa' => '6'])->all(), 'id', 'nombre'),
		        	['prompt' => '--cursos--']
		        )
	        ?>
		</div>
		<div class="col-md-2">
			<?= $form->field($model, 'cumplimiento')->dropDownList([0 => 'No', 1 => 'Si']); ?>
		</div>
                <span id="cond_calificacion">
                    <div class="col-md-2">
                            <?= $form->field($model, 'calificacion')->textInput(['type' => 'number', 'min'=>'0', 'max'=>'10', 'step'=>'0.01']); ?>
                    </div>
                </span>
		<div class="col-md-3">
			<?= $form->field($model, 'fecha')->textInput(['type' => 'date']); ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($model, 'fecha_fin')->textInput(['type' => 'date']); ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($model, 'observaciones')->textarea(); ?>
		</div>
		<div class="col-md-2">
			<label class="control-label"></label>
			<span class="">
				<?php echo Html::submitButton('', ['class' => 'btn btn-info btn-block fa fa-plus-circle']); ?>
			</span>
			<?= $form->field($model, 'idElemento')->label('')->textInput(['type' => 'hidden', 'value' => $elemento->id]); ?>
		</div>
		<?php $this->endBlock(); ?>
		<?php ActiveForm::end(); ?>
		<p>
			<?= $this->blocks[$blockName]; ?>
		</p>
	</div>
	<div class="bg-navy-active">
		<h5>Histórico de formación continua</h5>
	</div>
	<table id="resultados" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th>Nombre del curso</th>
				<th>Duracion</th>
				<th>Importancia</th>
				<th>Cumplimiento</th>
				<th>Calificacion</th>
				<th>Fecha Inicio</th>
				<th>Fecha Fin</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($arrCursos)) { ?>
				<?php foreach ( $arrCursos as $key => $oCurso ) { ?>
					<tr>
						<td><?php echo $key + 1; ?></td>
						<td><?php echo $oCurso->nombre_curso; ?></td>
						<td><?php echo $oCurso->getDuracionText(); ?></td>
						<td><?php echo $oCurso->getObligatorioText(); ?></td>
						<td><?php echo $oCurso->getCumplimientoText(); ?></td>
						<td><?php echo $oCurso->calificacion; ?></td>
						<td><?php echo $oCurso->getFechaText(); ?></td>
						<td><?php echo $oCurso->getFechaFinText(); ?></td>
						<td>
							<a class="btn btn-block" data-toggle="modal" data-target="#modal-model" data-modal-type="edit" data-action="<?php echo Url::to(['log-cursos/update-log', 'id' => $oCurso->id]) ?>">
								<span data-toggle="tooltip" data-placement="left" title="Editar" class="glyphicon glyphicon-pencil"></span>
							</a>
						</td>
						<td>
							<a class="btn btn-block" data-toggle="modal" data-target="#modal-model" data-modal-type="remove" data-action="<?php echo Url::to(['log-cursos/delete', 'id' => $oCurso->id]) ?>">
								<span data-toggle="tooltip" data-placement="left" title="Eliminar" class="glyphicon glyphicon-trash"></span>
							</a>
						</td>
					</tr>
				<?php } ?>
			<?php } ?>
		</tbody>
		<tfoot>
        </tfoot>
	</table>
</div>
<script>
    $(window).load(function() {
        if($('#logcursos-calificacion').val() == '') {
            loadHasGrade();
        }
        $('#logcursos-idcurso').change(function(){
            loadHasGrade();
        });
    });

    
    function loadHasGrade() { 
        curso = $('#logcursos-idcurso').val()|0;
        url = $('.base_url').attr('href') + 'log-cursos/curso?idCurso='+curso; 
        console.debug(url);
        $.ajax({
            type     :'POST',
            cache    : false,
            url  : url,
            success  : function(response) {     
                $('#cond_calificacion').html(response);                
            }
        });   
    }
</script>