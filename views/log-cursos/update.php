<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\LogCursos $model
 */

$this->title = 'Log Cursos Update ' . $model->id . '';
$this->params['breadcrumbs'][] = ['label' => 'Log Cursos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="log-cursos-update"> 
	<?php echo $this->render('_form', ['model' => $model, 'numerico' => $numerico , 'formType' => 'edit', 'noCal'=>(isset($noCal)&&$noCal == 1)?1:0]); ?>
</div>