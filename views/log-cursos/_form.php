<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Cursos;
use yii\bootstrap\Tabs;
/**
* @var yii\web\View $this
* @var app\models\LogCursos $model
* @var yii\widgets\ActiveForm $form
*/
$cssButton = $model->isNewRecord ? "btn btn-app" : "btn btn-app hidden";
$attributes = $formType == "view" ? ['disabled' => ''] : [];
?>

<div class="log-cursos-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => true, 'id' => 'form-log']); ?>

    <div class="">
        <?php echo $form->errorSummary($model, ['header' => 'Por favor corrija los errores:']); ?>
        <ul id="w0" class="nav nav-tabs"><li class="active"><a href="#w0-tab0" data-toggle="tab">Curso</a></li></ul>
        <p>
            <?= $form->field($model->idElemento0, 'nombreText')->label('Nombre del elemento')->textInput(['readonly' => true]) ?>
			<?= $form->field($model, 'idCurso')
                ->label('Nombre del curso')
                ->dropDownList(ArrayHelper::map(Cursos::find()->all(), 'id', 'nombre'),
                    ['prompt' => '--cursos--']
                )
            ?>
            <?= $form->field($model, 'cumplimiento')->dropDownList([0 => 'No', 1 => 'Si']); ?>
            <?= ($numerico)?$form->field($model, 'calificacion')->textInput(['type' => 'number', 'min'=>'0', 'max'=>'10', 'step'=>'0.01']):''; ?>
            <?= $form->field($model, 'fecha')->textInput(['type' => 'date']); ?>
            <?php 
                if(!$noCal) {
                    echo $form->field($model, 'calificacion')->textInput(['maxlength' => 4]);
                }
            ?>
                <?= $form->field($model, 'observaciones')->textarea(['rows' => 6]) ?>
        </p>
        
        <hr/>
        <?= Html::submitButton('<span class="fa fa-save"></span> '.'Guardar', ['class' => $cssButton]) ?>
        <a href="<?php echo Url::previous(); ?>" class="<?php echo $cssButton ?>">
            <i class="fa fa-times"></i>Cancelar
        </a>
        <?php ActiveForm::end(); ?>

    </div>

</div>
