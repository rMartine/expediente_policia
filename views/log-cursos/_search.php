<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\LogCursosSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="log-cursos-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'idElemento') ?>

		<?= $form->field($model, 'idCurso') ?>

		<?= $form->field($model, 'calificacion') ?>

		<?= $form->field($model, 'cumplimiento') ?>

		<?php // echo $form->field($model, 'fecha') ?>

		<?php // echo $form->field($model, 'observaciones') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
