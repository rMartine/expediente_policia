<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\models\Etapas;

/**
* @var yii\web\View $this
* @var app\models\paquetesCursos $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="paquetes-cursos-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            
			<?= $form->field($model, 'nombre')->textInput(['maxlength' => 45]) ?>
                        <?=
                        $form->field($model, 'idEtapa')->dropDownList(
                                        ArrayHelper::map(Etapas::find()->all(), 'id', 'nombre')
                                    )->label('Etapa');
                        ?>
			<?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    \yii\bootstrap\Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ [
    'label'   => 'PaquetesCursos',
    'content' => $this->blocks['main'],
    'active'  => true,
], ]
                 ]
    );
    ?>
        <hr/>

        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? 'Create' : 'Save'), ['class' => $model->isNewRecord ?
        'btn btn-primary' : 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
