<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\etapasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Etapas';
$this->params['breadcrumbs'][] = $this->title;
?>
 
<section class="content-header">

    <h1><?= Html::encode($this->title) ?></h1>

</section>
<section class="content">
    <div class="row">        
        <div class="col-md-12 main_box">
            <div class="box box-default box-bajas etapas-index">
                <div class="box-header with-border">
                    <p>
                        <?php
                            echo Html::a('Crear Etapa',['create'], [
                            'class' => 'btn btn-success',
                            'title' => Yii::t('yii', 'Close'),
                            'onclick'=>"
                                $.ajax({
                               type     :'POST',
                               cache    : false,
                               url  : ['etapas/create']+'?partial=true',
                               success  : function(response) {
                                   $('#ajax_div').html(response);
                                   $('#ajaxModal').modal();
                               }
                               });return false;",
                               ]);
                        ?>    
                    </p>
                    <div class="box-tools pull-right">      
                      <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                      <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'id',
                        'nombre',
                        'orden',
                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
                </div><!-- /.box-body -->
            </div>    
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="ajaxModal" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true" data-backdrop="false">
  <div class="modal-dialog">
    <!--<div class="modal-content">-->
        <div id="ajax_div" class="box-header">
      </div>
    <!--</div>-->    
  </div>
</div>