<?php

namespace app\controllers;

use app\models\LogNombramientos;
use app\models\Elementos;
use app\models\LogNombramientosSearch;
use app\models\LogAdscripciones;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use Yii;

/**
 * LogNombramientosController implements the CRUD actions for LogNombramientos model.
 */
class AspirantesNombramientosController extends Controller
{
	/**
	 * Lists all LogNombramientos models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new LogNombramientosSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single LogNombramientos model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new LogNombramientos model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate($idElemento)
	{
		$model = new LogNombramientos;
		$model2 = new LogAdscripciones;
		$model3 = new LogCursos;
		$elemento = $this->findElemento($idElemento);

		try {
            if ($model->load($_POST) && $model->save()) {

            	$elemento->clave_empleado = $_POST['clave_empleado'];
            	$elemento->idEstatus = 2;
            	$elemento->idEtapa = 6;
            	$elemento->save();

            	$model2->idElemento = $elemento->id;
            	$model2->idAdscripcion = $model->idAdscripcion;
            	$model2->idNombramiento = $model->idTipo;
            	$model2->fecha = $model->fecha;
            	$model2->oficio_comision = $model->oficio_nombramiento;
            	$model2->detalle_comision = $model->comision;
            	$model2->observaciones = $model->observaciones;
            	$model2->save();

            	$model3->idElemento = $elemento->id;
            	$model3->idCurso = 16;
            	$model3->calificacion = $elemento->promedioFormacionInicial();
            	$model3->cumplimiento = 1;
            	



                return $this->redirect(Yii::$app->homeUrl . 'generales');
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
		return $this->redirect(Yii::$app->homeUrl . 'aspirantes/detalle?idElemento=' . $idElemento . '&pestana=nombramiento');
	}

	/**
	 * Updates an existing LogNombramientos model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing LogNombramientos model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the Elementos model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return LogNombramientos the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findElemento($id)
	{
		if (($model = Elementos::findOne($id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}	

	/**
	 * Finds the LogNombramientos model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return LogNombramientos the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = LogNombramientos::findOne($id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
