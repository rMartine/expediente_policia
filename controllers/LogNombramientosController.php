<?php

namespace app\controllers;


use Yii;
use app\models\LogNombramientos;
use app\models\LogNombramientosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use app\models\FormSearch;
use app\models\Elementos;
use yii\web\HttpException;
use yii\filters\AccessControl;


/**
 * LogNombramientosController implements the CRUD actions for LogNombramientos model.
 */
class LogNombramientosController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * Lists all LogNombramientos models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        Url::remember();
        $model = new LogNombramientos();
        try {
           //if($model->load($_POST))print_r($model);exit;
            if ($model->load($_POST) && $model->save()) {
            	$model = new LogNombramientos();
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
        }

        $form     = new FormSearch;
        $elemento = Elementos::findOne($id);
        
        return $this->render('index', [
            'model' => $model,
            'form' => $form,
            'elemento' => $elemento
        ]);
    }

    /**
     * Displays a single LogNombramientos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LogNombramientos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LogNombramientos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LogNombramientos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
            $model = $this->findModel($id);

            if ($model->load($_POST) && $model->save()) {
        return $this->redirect(Url::previous());
            } else {
                    return $this->renderAjax('update', [
                            'model' => $model,
                    ]);
            }
    }

    /**
     * Deletes an existing LogNombramientos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $idElemento)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index?id=' . $idElemento]);
    }

    /**
     * Finds the LogNombramientos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogNombramientos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogNombramientos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}