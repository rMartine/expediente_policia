<?php

namespace app\controllers;

use yii\web\Controller;
use Yii;
use app\models\Elementos;
use app\models\PersonalFormSearch;
use yii\helpers\Html;
use app\models\CargaImagen;
use app\models\CargaPdf;
use yii\web\UploadedFile;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use app\models\Requerimientos;
use app\models\LogDocumentos;

class GeneralesController extends Controller
{

	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	public function actionIndex()
	{
		$oElementosModel = new Elementos();
		$arrElementos    = $oElementosModel->getElementosForPersonal();
		$form            = new PersonalFormSearch;
		
		$search = null;

		if ($form->load(Yii::$app->request->post())) {
			if ($form->validate()) {
				$search       = Html::encode($form->search_element);
				$arrElementos = $oElementosModel->searchPersonal($search);
			} else {
				$form->getErrors();
			}
		}

		return $this->render('index', [
			'arrElementos' => $arrElementos,
			'form'         => $form,
			'search'       => $search
		]);
	}

	public function actionDetalle($elemento = 0, $tab = 1)
	{
		if ($elemento > 0) {
			$oElemento         = Elementos::findOne($elemento);
			$arrRequerimientos = Requerimientos::find()->all();
			$arrDocumentos     = LogDocumentos::find()->where(['idElemento' => $elemento])->all();

			return $this->render('detalle', [
				'tab'               => $tab,
				'oElemento'         => $oElemento,
				'arrRequerimientos' => $arrRequerimientos,
				'arrDocumentos'     => $arrDocumentos
			]);
		} else {
			$this->redirect(Yii::$app->urlManager->getBaseUrl() . '/personal');
		}
	}

	public function actionSave()
	{
		$idElemento = (!is_null(Yii::$app->request->post('idElemento'))) ? (int) Yii::$app->request->post('idElemento') : 0;
		$campo = (!is_null(Yii::$app->request->post('campo'))) ? Yii::$app->request->post('campo') : '';

		$oElemento = $this->findModel($idElemento);

		if ($idElemento > 0 && Yii::$app->request->isPost) {
			$model = new CargaImagen();

			$model->file = UploadedFile::getInstance($model, 'file');

			if ($model->file) {
				if ($campo == '0') {
					$foto = 'foto_' . $idElemento . '_' . time() . '.' . $model->file->extension;
					$model->file->saveAs('img/fotos_elementos/' . $foto);
					$oElemento->fotografia = $foto;
				} else {
					$foto = $idElemento . '_' . time() . '_' . $campo . '.' . $model->file->extension;

					if ($campo == 'menique_izq') {
						$model->file->saveAs('img/fingerprints/' . $foto);
						$oElemento->menique_izq = $foto;
					} elseif ($campo == 'anular_izq'){
						$model->file->saveAs('img/fingerprints/' . $foto);
						$oElemento->anular_izq = $foto;
					} elseif ($campo == 'medio_izq'){
						$model->file->saveAs('img/fingerprints/' . $foto);
						$oElemento->medio_izq = $foto;                        
					} elseif ($campo == 'indice_izq'){
						$model->file->saveAs('img/fingerprints/' . $foto);
						$oElemento->indice_izq = $foto;
					} elseif ($campo == 'pulgar_izq'){
						$model->file->saveAs('img/fingerprints/' . $foto);
						$oElemento->pulgar_izq = $foto;
					} elseif ($campo == 'menique_der'){
						$model->file->saveAs('img/fingerprints/' . $foto);
						$oElemento->menique_der = $foto;
					} elseif ($campo == 'anular_der'){
						$model->file->saveAs('img/fingerprints/' . $foto);
						$oElemento->anular_der = $foto;
					} elseif ($campo == 'medio_der'){
						$model->file->saveAs('img/fingerprints/' . $foto);
						$oElemento->medio_der = $foto;
					} elseif ($campo == 'indice_der'){
						$model->file->saveAs('img/fingerprints/' . $foto);
						$oElemento->indice_der = $foto;
					} elseif ($campo == 'pulgar_der'){
						$model->file->saveAs('img/fingerprints/' . $foto);
						$oElemento->pulgar_der = $foto;
					}
				}

				$oElemento->save();

				$oElemento = $this->findModel($idElemento);
			} else if ($campo == 2) {
				$arrData   = Yii::$app->request->post('Elementos');
				$oElemento = $this->findModel($idElemento);

				$oElemento->cuip             = $arrData['cuip'];
				$oElemento->rfc              = $arrData['rfc'];
				$oElemento->curp             = $arrData['curp'];
				$oElemento->estado_civil     = $arrData['estado_civil'];
				$oElemento->fecha_nac        = $arrData['fecha_nac'];
				$oElemento->edad             = $arrData['edad'];
				$oElemento->idEstudios       = $arrData['idEstudios'];
				$oElemento->nombre           = $arrData['nombre'];
				$oElemento->apellido_paterno = $arrData['apellido_paterno'];
				$oElemento->apellido_materno = $arrData['apellido_materno'];
				$oElemento->sexo             = $arrData['sexo'];
				$oElemento->estado_nac       = $arrData['estado_nac'];
				$oElemento->municipio_nac    = $arrData['municipio_nac'];
				$oElemento->telefono         = $arrData['telefono'];
				$oElemento->movil            = $arrData['movil'];
				$oElemento->email            = $arrData['email'];
				$oElemento->domicilio_edo    = $arrData['domicilio_edo'];
				$oElemento->domicilio_mpo    = $arrData['domicilio_mpo'];
				$oElemento->domicilio_col    = $arrData['domicilio_col'];
				$oElemento->domicilio_cp     = $arrData['domicilio_cp'];
				$oElemento->domicilio_calle  = $arrData['domicilio_calle'];
				$oElemento->domicilio_next   = $arrData['domicilio_next'];
				$oElemento->domicilio_nint   = $arrData['domicilio_nint'];
                                $oElemento->idTipo           = $arrData['idTipo'];

				$oElemento->update(false, [
					'cuip', 'rfc', 'curp', 'estado_civil', 
					'fecha_nac', 'edad', 'idEstudios',
					'nombre', 'apellido_paterno', 'apellido_materno',
					'sexo', 'estado_nac', 'municipio_nac',
					'telefono', 'movil', 'email',
					'domicilio_edo', 'domicilio_mpo', 'domicilio_col',
					'domicilio_cp', 'domicilio_calle', 'domicilio_next',
					'domicilio_nint','idTipo'
				]);
			}
		}

		if ($campo == '-1') {
			$this->redirect('index');
		} else {
			$this->redirect('detalle?elemento=' . $idElemento);
		}
	}

	public function actionCargar() {
		if (Yii::$app->request->isPost) {
			$model = new CargaPdf();

			$model->file = UploadedFile::getInstance($model, 'file');

			if ($model->file) {
				$idElemento     = (!is_null(Yii::$app->request->post('idElemento'))) ? (int) Yii::$app->request->post('idElemento') : 0;
				$strFileName    = $this->_toAscii($model->file->name);
				$strNewFileName = $idElemento . '-' . $strFileName;

				$bSaved = $model->file->saveAs('documentos_elementos/' . $strNewFileName);

				$strNombreDocumento = (!is_null(Yii::$app->request->post('nombreDocumento'))) ? Yii::$app->request->post('nombreDocumento') : '';

				if ($strNombreDocumento != '-1') {
					$oRequerimiento = Requerimientos::findOne($strNombreDocumento);
					$strNombreDocumento = $oRequerimiento->nombre;
				} else {
					$strNombreDocumento = Yii::$app->request->post('otroDescripcion');
				}

				$oLogDocumentos = new LogDocumentos();

				$oLogDocumentos->nombreDocumento = $strNombreDocumento;
				$oLogDocumentos->fechaSubida     = date('Y-m-d');
				$oLogDocumentos->nombreArchivo   = $strNewFileName;
				$oLogDocumentos->idElemento      = $idElemento;

				$bRetLogDocumentos = $oLogDocumentos->insert();

				if ($bSaved && $bRetLogDocumentos) {
					$strMessage = 'Documento "' . $strNombreDocumento . '" cargado correctamente';
					Yii::$app->getSession()->setFlash('success', $strMessage, true);
				} else {
					$strMessage = 'Ocurrio un error al cargar el documento "' . $strNombreDocumento . '", por favar intente de nuevo';
					Yii::$app->getSession()->setFlash('error', $strMessage, true);
				}
			}
			
			$this->redirect('detalle?elemento=' . $idElemento . '&tab=2');
		}
	}

	public function actionEliminar($elemento = 0, $documento = 0) {
		if ($elemento > 0 && $documento > 0) {
			$model = LogDocumentos::findOne($documento);
			$bRet = $model->delete();

			if ($bRet) {
				unlink('documentos_elementos/' . $model->nombreArchivo);
				$strMessage = 'Documento "' . $model->nombreDocumento . '" borrado correctamente';
				Yii::$app->getSession()->setFlash('success', $strMessage, true);
			} else {
				$strMessage = 'Ocurrio un error al borrar el documento, por favar intente de nuevo';
				Yii::$app->getSession()->setFlash('error', $strMessage, true);
			}

			$this->redirect('detalle?elemento=' . $elemento . '&tab=2');
		} else {
			$this->redirect('index');
		}
	}

	protected function findModel($id)
	{
		if (($model = Elementos::findOne($id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}

	private function _toAscii( $str, $replace = array(), $delimiter = '_' ) {
		if( !empty($replace) ) {
			$str = str_replace((array)$replace, ' ', $str);
		}

		$clean = str_replace(array('á', 'é', 'í', 'ó', 'ú'), array('a', 'e', 'i', 'o', 'u'), $str);
		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $clean);
		$clean = preg_replace("/[^a-zA-Z0-9\/_.|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

		return $clean;
	}

}