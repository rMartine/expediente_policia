<?php

namespace app\controllers;

use app\models\Cursos;
use app\models\cursosSearch;
use app\models\FormSearch;
use app\models\Elementos;
use app\models\ElementosSearch;
use app\models\LogCursos;
use app\models\LogEvaluaciones;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\Html;

class PlanesController extends Controller
{	
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
	/**
     * Muestra la primera página.
     * @return mixed
     */
    public function actionIndex()
    {
        $model        = new Elementos();
        $arrElementos = $model->findActivos();
        $form         = new FormSearch;
        $search       = null;

        if ($form->load(Yii::$app->request->post())) {
            if ($form->validate()) {
                $search       = Html::encode($form->search_element);
                $arrElementos = $model->searchActivos($search);
            } else {
                $form->getErrors();
            }
        }
        return $this->render('index', [
            'arrElementos' => $arrElementos,
            'form' => $form,
            'search' => $search
        ]);
    }
	
	/**
     * Muestra resultado de la búsqueda.
     * @return mixed
     */
    public function actionBuscar()
    {
        //$searchModel = new ElementosSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('buscar');
    }
	
	/**
     * Muestra formulario de creación de nuevo registro.
     * @return mixed
     */
    public function actionNuevo()
    {
        $model = new Elementos;
        try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('nuevo', ['model' => $model,]);
    }

    /**
     * Lists all Elementos models.
     * @return mixed
     */
    public function actionIndexCrud()
    {
        $searchModel = new ElementosSearch;
        $dataProvider = $searchModel->search($_GET);

        Url::remember();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Elementos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        \Yii::$app->language = 'es_ES';
        Url::remember();
        $model = new LogCursos();
        $logEvaluacion = new logEvaluaciones();
        try {
            if ($model->load($_POST) && $model->save()) {
                $model = new LogCursos();
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
        }

        $form = new cursosSearch;
        $cursos = new Cursos();
        $elemento = Elementos::findOne($id);

        $arrCursos = $model->getLogCursosByElementoAndEtapa($id);
        
        return $this->render('view', [
            'model' => $model,
            'form' => $form,
            'cursos' => $cursos,
            'elemento' => $elemento,
            'logEvaluacion' => $logEvaluacion,
            'arrCursos' => $arrCursos
        ]);
    }

    /**
     * Creates a new Elementos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Elementos;

        try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('create', ['model' => $model,]);
    }

    /**
     * Updates an existing Elementos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Elementos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(Url::previous());
    }

    /**
     * Finds the Elementos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Elementos the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Elementos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
    }
}
