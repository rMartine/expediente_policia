<?php

namespace app\controllers;

use Yii;
use app\models\LogLicencias;
use app\models\LogLicenciasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use app\models\FormSearch;
use app\models\Elementos;

/**
 * LogLicenciasController implements the CRUD actions for LogLicencias model.
 */
class LogLicenciasController extends Controller
{
	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

	/**
	 * Lists all LogLicencias models.
	 * @return mixed
	 */
	public function actionIndex($id)
	{
		Url::remember();
		$model = new LogLicencias();
		 try {
            if ($model->load($_POST) && $model->save()) {
            	$model = new LogLicencias();
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
        }

        $form     = new FormSearch;
        $elemento = Elementos::findOne($id);

		return $this->render('index', [
			'model' => $model,
            'form' => $form,
            'elemento' => $elemento
		]);
	}

	/**
	 * Displays a single LogLicencias model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new LogLicencias model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new LogLicencias;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing LogLicencias model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->renderAjax('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing LogLicencias model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id, $idElemento)
	{
		$this->findModel($id)->delete();
		return $this->redirect(['index?id=' . $idElemento]);
	}

	/**
	 * Finds the LogLicencias model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return LogLicencias the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = LogLicencias::findOne($id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
