<?php

namespace app\controllers;

use app\models\PaquetesCursos;
use app\models\paquetesCursosSearch;
use app\models\etapas;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * PaquetesCursosController implements the CRUD actions for paquetesCursos model.
 */
class PaquetesCursosController extends Controller
{
	/**
	 * Lists all paquetesCursos models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new paquetesCursosSearch;
		$dataProvider = $searchModel->search($_GET);

                Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single paquetesCursos model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new paquetesCursos model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new paquetesCursos;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing paquetesCursos model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing paquetesCursos model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the paquetesCursos model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return paquetesCursos the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
                $model = paquetesCursos::find()
                            ->where(['paquetes_cursos.id'=>$id])
                            ->innerJoinWith('etapas')
                            ->all();
		if ($model !== null) {
			return $model[0];
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
