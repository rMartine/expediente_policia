<?php

namespace app\controllers;

use yii\web\Controller;
use Yii;
use app\models\Elementos;
use app\models\Motivos;
use app\models\LogBajas;
use app\models\BajasFormSearch;
use yii\helpers\Html;

class BajasController extends Controller
{

	public function actionIndex ()
	{

		$oElementosModel = new Elementos();
		$arrElementos    = $oElementosModel->getElementosForBajas();
		$form            = new BajasFormSearch;

		//Obtener motivos
		$oMotivosModel               = new Motivos();
		$arrMotivosOrdinarios        = $oMotivosModel->find()->where(['ordinaria' => Motivos::MOTIVO_ORDINARIA])->all();
		$arrMotivosExtraordinarias   = $oMotivosModel->find()->where(['ordinaria' => Motivos::MOTIVO_EXTRAORDINARIA])->all();
		$arrMotivoExtraordinarioOtro = $oMotivosModel->find()->where(['motivo' => Motivos::MOTIVO_EXTRAORDINARIO_OTRO])->all();
		$numMotivoExtraordinarioOtro = $arrMotivoExtraordinarioOtro[0]->id;
		
		$search = null;

		if ($form->load(Yii::$app->request->post())) {
			if ($form->validate()) {
				$search       = Html::encode($form->search_element);
				$arrElementos = $oElementosModel->searchElements($search);
			} else {
				$form->getErrors();
			}
		}

		return $this->render('index', [
			'arrElementos'                => $arrElementos,
			'form'                        => $form,
			'arrMotivosOrdinarios'        => $arrMotivosOrdinarios,
			'arrMotivosExtraordinarias'   => $arrMotivosExtraordinarias,
			'search'                      => $search,
			'numMotivoExtraordinarioOtro' => $numMotivoExtraordinarioOtro
		]);
	}

	public function actionBaja()
	{
		$numIdElemento                 = (!is_null(Yii::$app->request->post('idElemento')))                 ? (int) Yii::$app->request->post('idElemento')                 : 0;
		$numIdTipoBaja                 = (!is_null(Yii::$app->request->post('tipo_baja')))                  ? (int) Yii::$app->request->post('tipo_baja')                  : 0;
		$numIdMotivoOrdinarioBaja      = (!is_null(Yii::$app->request->post('motivo_ordinario_baja')))      ? (int) Yii::$app->request->post('motivo_ordinario_baja')      : 0;
		$numIdMotivoExtraordinarioBaja = (!is_null(Yii::$app->request->post('motivo_extraordinario_baja'))) ? (int) Yii::$app->request->post('motivo_extraordinario_baja') : 0;
		$strFechaBaja                  = (!is_null(Yii::$app->request->post('fecha_baja')))                 ? Yii::$app->request->post('fecha_baja')                       : '';
		$strObservacionesBaja          = (!is_null(Yii::$app->request->post('observaciones_baja')))         ? Yii::$app->request->post('observaciones_baja')               : '';
		$strOtroMotivo                 = (!is_null(Yii::$app->request->post('otro_motivo')))                ? Yii::$app->request->post('otro_motivo')                      : '';
		$numIdMotivoExtraordinarioBaja = (!is_null(Yii::$app->request->post('motivo_extraordinario_baja'))) ? (int) Yii::$app->request->post('motivo_extraordinario_baja') : 0;

		if ( $numIdElemento > 0 && $numIdTipoBaja >= 0 && $numIdMotivoOrdinarioBaja > 0 && $numIdMotivoExtraordinarioBaja > 0 && $strFechaBaja != '' && $strObservacionesBaja != '' ) {
			$numIdMotivo    = ($numIdTipoBaja == 0) ? $numIdMotivoExtraordinarioBaja : $numIdMotivoOrdinarioBaja;

			//Hacer el insert en la tabla de log_bajas
			$oLogBajasModel = new LogBajas();
			
			$oLogBajasModel->idElemento    = $numIdElemento;
			$oLogBajasModel->idMotivo      = $numIdMotivo;
			$oLogBajasModel->fecha         = implode('-', array_reverse(explode('/', $strFechaBaja)));
			$oLogBajasModel->observaciones = $strObservacionesBaja;

			if ($numIdTipoBaja == 0 && strlen($strOtroMotivo) > 0) {
				$oLogBajasModel->otro_motivo = $strOtroMotivo;
			}
			
			$bRetLogBajas = $oLogBajasModel->insert();

			//Hacer el update del estatus en la tabla elementos
			$oElemento = Elementos::findOne($numIdElemento);

			$oElemento->idEstatus = Elementos::ELEMENTOS_INACTIVOS;

			$bRetElementos = $oElemento->update(false, ['idEstatus']);

			$strMessage = ( $bRetLogBajas && $bRetElementos ) ? 'Elemento dado de baja' : 'Ocurrio un error, favor de intentar nuevamente';
		}

		$this->redirect('index');
	}

}