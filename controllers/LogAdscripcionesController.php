<?php

namespace app\controllers;

use app\models\Elementos;
use app\models\LogAdscripciones;
use app\models\LogAdscripcionesSearch;
use app\models\FormSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * LogAdscripcionesController implements the CRUD actions for LogAdscripciones model.
 */
class LogAdscripcionesController extends Controller
{
	/**
	 * Lists all LogAdscripciones models.
	 * @return mixed
	 */
	public function actionIndex($id)
	{
		Url::remember();
        $model = new LogAdscripciones();
        try {
            if ($model->load($_POST) && $model->save()) {
            	$model = new LogAdscripciones();
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
        }

        $form     = new FormSearch;
        $elemento = Elementos::findOne($id);
        
        return $this->render('index', [
            'model' => $model,
            'form' => $form,
            'elemento' => $elemento,
        ]);
	}

	/**
	 * Displays a single LogAdscripciones model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new LogAdscripciones model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new LogAdscripciones;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing LogAdscripciones model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->renderAjax('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing LogAdscripciones model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the LogAdscripciones model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return LogAdscripciones the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = LogAdscripciones::findOne($id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
