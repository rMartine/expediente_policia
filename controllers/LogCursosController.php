<?php

namespace app\controllers;

use app\models\LogCursos;
use app\models\LogCursosSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * LogCursosController implements the CRUD actions for LogCursos model.
 */
class LogCursosController extends Controller
{
	/**
	 * Lists all LogCursos models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new LogCursosSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single LogCursos model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new LogCursos model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new LogCursos;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing LogCursos model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
                        
		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->renderAjax('update', [
				'model' => $model,
			]);
		}
	}
        
	/**
	 * Updates an existing LogCursos model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdateLog($id)
	{
		$model = $this->findModel($id);

                $numerico = 0;
                if(isset($model->idCurso) && $model->idCurso > 0) {
                    $Cursomodel =  \app\models\Cursos::findOne($model->idCurso); 
                    if(isset($Cursomodel->numerico) && $Cursomodel->numerico) {
                        $numerico = 1;
                    }
                }
                
		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->renderAjax('update', [
				'model' => $model,
                                'noCal' => 1,
                                'numerico' => $numerico
			]);
		}
	}
	/**
	 * Deletes an existing LogCursos model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the LogCursos model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return LogCursos the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = LogCursos::findOne($id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
        
        public function actionCurso($idCurso)
        {
            if($idCurso > 0) {
                $model =  \app\models\Cursos::findOne($idCurso);            

                if($model->numerico) {
                    echo '<div class="col-md-2">
                            <div class="form-group field-logcursos-calificacion has-success">
                            <label class="control-label" for="logcursos-calificacion">Calificacion</label>
                            <input type="number" min="0" max="10" step="0.01" id="logcursos-calificacion" class="form-control" name="LogCursos[calificacion]">

                            <p class="help-block help-block-error"></p>
                            </div>                    
                          </div>';
                }
            }
        }
}
