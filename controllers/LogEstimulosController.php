<?php

namespace app\controllers;

use app\models\Elementos;
use app\models\LogEstimulos;
use app\models\FormSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * LogEstimulosController implements the CRUD actions for LogEstimulos model.
 */
class LogEstimulosController extends Controller
{
	/**
	 * Lists all LogEstimulos models.
	 * @return mixed
	 */
	public function actionIndex($id)
	{
		Url::remember();
        $model = new LogEstimulos();
        try {
            if ($model->load($_POST) && $model->save()) {
            	$model = new LogEstimulos();
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
        }

        $form     = new FormSearch;
        $elemento = Elementos::findOne($id);
        
        return $this->render('index', [
            'model' => $model,
            'form' => $form,
            'elemento' => $elemento,
        ]);
	}

	/**
	 * Displays a single LogEstimulos model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        Url::remember();
        $model = new LogEstimulos();
        try {
            if ($model->load($_POST) && $model->save()) {
            	$model = new LogEstimulos();
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
        }

        $form = new FormSearch;
        $elemento = Elementos::findOne($id);
        return $this->render('view', [
            'model' => $model,
            'form' => $form,
            'elemento' => $elemento,
        ]);
	}

	/**
	 * Creates a new LogEstimulos model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new LogEstimulos;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing LogEstimulos model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->renderAjax('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing LogEstimulos model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the LogEstimulos model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return LogEstimulos the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = LogEstimulos::findOne($id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
        
        public function actionEstimulo($estimulo) {
            $model = new LogEstimulos();
            return $this->renderAjax('estimulo', [
                'model' => $model,
                'estimulo' => $estimulo
            ]);
        }
}
