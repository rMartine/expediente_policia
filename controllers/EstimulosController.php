<?php

namespace app\controllers;

use app\models\Elementos;
use app\models\estimulos;
use app\models\estimulosSearch;
use app\models\FormSearch;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * EstimulosController implements the CRUD actions for estimulos model.
 */
class EstimulosController extends Controller
{
	/**
	 * Lists all estimulos models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$model     = new Elementos();
        $form      = new FormSearch();
        $elementos = null;
        $search    = null;

        if ($form->load(Yii::$app->request->post())) {
        	$search    = Html::encode($form->search_element);
            $elementos = $model->searchActivos($search);
        } else {
        	$elementos = $model->findActivos();
        }
        Url::remember();
        return $this->render('index', [
            'elementos' => $elementos,
            'form' => $form,
            'search' => $search
        ]);
	}

	/**
	 * Displays a single estimulos model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new estimulos model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new estimulos;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing estimulos model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing estimulos model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the estimulos model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return estimulos the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = estimulos::findOne($id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
