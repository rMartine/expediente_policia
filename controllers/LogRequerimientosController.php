<?php

namespace app\controllers;

use app\models\LogRequerimientos;
use app\models\LogRequerimientosSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use Yii;

/**
 * LogRequerimientosController implements the CRUD actions for LogRequerimientos model.
 */
class LogRequerimientosController extends Controller
{
	//public $enableCsrfValidation = false;

	/**
	 * Lists all LogRequerimientos models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new LogRequerimientosSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single LogRequerimientos model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new LogRequerimientos model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate($ajax = false)
	{
		$model = new LogRequerimientos;
		$idElemento = $_POST['idElemento'];
		try {
            if ($model->load($_POST) && $model->save()) {
                if($ajax) {
                    return 1;
                } else {
                    return $this->redirect(array('aspirantes/detalle','idElemento'=>$idElemento, 'pestana'=>'reclutamiento'));
                }    
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing LogRequerimientos model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id, $ajax = false)
	{
		$model = $this->findModel($id);
		$idElemento = $_POST['idElemento'];
		if ($model->load($_POST) && $model->save()) {
                    if($ajax) {
                        return 1;
                    } else {
                        return $this->redirect(array('aspirantes/detalle','idElemento'=>$idElemento, 'pestana'=>'reclutamiento'));
                    } 
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing LogRequerimientos model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id, $ajax = false)
	{
		$this->findModel($id)->delete();
                if($ajax) {
                    return 1;
                }
		$idElemento = $_POST['idElemento'];
		return $this->redirect(array('aspirantes/detalle','idElemento'=>$idElemento, 'pestana'=>'reclutamiento'));
	}

	/**
	 * Finds the LogRequerimientos model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return LogRequerimientos the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = LogRequerimientos::findOne($id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
