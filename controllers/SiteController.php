<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],/*
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        \Yii::$app->language = 'es_ES';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $this->layout = 'login';
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            $oFile = UploadedFile::getInstance($model, 'keyfile');
            $strFileKey = (!empty($oFile))?file_get_contents($oFile->tempName):''; 
            $oUser = $model->getUser();
            if(!empty($oUser)) {
                $oUsuario = $oUser->getAttributes();
                $strMemberKey = sha1(Yii::$app->params['keyFileSeed'] . $oUsuario['creado']);
            } else {
                $strMemberKey = '';
            }
            
            //Temporal para que funcione cualquier llave
            //$strFileKey = $strMemberKey;
            
            if($strMemberKey == $strFileKey && $model->login()) {                
                $oUser->updateAttributes(['ultima_entrada'=>date('Y-m-d G:i:s')]);
                $oUser->save();
                return $this->goBack();
            } else {
                $fileError = $strMemberKey == $strFileKey?'':'<span class="error_message">Llave incorrecta/faltante</span>';
                return $this->render('login', [
                    'model' => $model,
                    'fileError' => $fileError
                ]);
            }            
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        \Yii::$app->language = 'es_ES';
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}
