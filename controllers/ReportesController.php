<?php

namespace app\controllers;

use yii\web\Controller;
use Yii;
use app\models\Reportes;
use app\models\Elementos;

class ReportesController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $oReportesModel = new Reportes();
        $arrReportes    = array();

        $numRole = \Yii::$app->user->identity->id_rol;

        switch ($numRole) {
            case Reportes::ROL_ADMINISTRADOR:
                $arrReportes = $oReportesModel->getReportesParaAdministrador();
                break;
            case Reportes::ROL_OFICIAL:
                $arrReportes = $oReportesModel->getReportesParaOficial();
                break;
            case Reportes::ROL_CIVIL:
                $arrReportes = $oReportesModel->getReportesParaCivil();
                break;
            case Reportes::ROL_VIALIDAD:
                $arrReportes = $oReportesModel->getReportesParaVialidad();
                break;
        }

        $oElementos = new Elementos();
        $arrElementos = $oElementos->getElementosForReportes();

        return $this->render('index', [
            'arrReportes'  => $arrReportes,
            'arrElementos' => $arrElementos
        ]);
    }

    public function actionPolicial()
    {
        return $this->render('policial');
    }

    public function actionVialidad()
    {
        return $this->render('vialidad');
    }

}
