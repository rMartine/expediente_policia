<?php

namespace app\controllers;

use app\models\LogCursos;
use app\models\LogCursosSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * LogCursosController implements the CRUD actions for LogCursos model.
 */
class AspirantesFormacionController extends Controller
{
	/**
	 * Lists all LogCursos models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new LogCursosSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single LogCursos model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new LogCursos model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new LogCursos;
		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(array('aspirantes/detalle','idElemento'=>$model->idElemento, 'pestana'=>'formacion'));
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
		return $this->redirect(array('aspirantes/detalle','idElemento'=>$model->idElemento, 'pestana'=>'formacion'));
	}

	/**
	 * Updates an existing LogCursos model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(array('aspirantes/detalle','idElemento'=>$model->idElemento, 'pestana'=>'formacion'));
		} else {
			return $this->renderPartial('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing LogCursos model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$idElemento = $this->findModel($id)->idElemento;
		$this->findModel($id)->delete();
		return $this->redirect(array('aspirantes/detalle','idElemento'=>$idElemento, 'pestana'=>'formacion'));
	}

	/**
	 * Finds the LogCursos model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return LogCursos the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = LogCursos::findOne($id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
