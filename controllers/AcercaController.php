<?php

namespace app\controllers;

use Yii;
use app\models\LogLicencias;
use app\models\LogLicenciasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use app\models\FormSearch;
use app\models\Elementos;

/**
 * LogLicenciasController implements the CRUD actions for LogLicencias model.
 */
class AcercaController extends Controller
{
	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

	/**
	 * Lists all LogLicencias models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		Url::remember();
		return $this->render('index');
	}
}
