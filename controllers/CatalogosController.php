<?php

namespace app\controllers;

use Yii;
use app\models\etapas;
use app\models\etapasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class CatalogosController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all etapas models.
     * @return mixed
     */
    public function actionIndex()
    {
        \Yii::$app->language = 'es_ES';
        $oCatalogos = [               
               'Adscripciones'=>'Adscripciones',
               'Convocatorias'=>'Convocatorias',
               'Cursos'=>'Cursos',  
               'Estimulos'=>'Est&iacute;mulos',  
               'Estudios'=>'Estudios',
               'Etapas'=>'Etapas',
               'Evaluaciones'=>'Evaluaciones', 
               'Motivos'=>'Motivos',
               'Nombramientos'=>'Nombramientos',
               'PaquetesCursos'=>'Paquetes de Cursos',
               'Sanciones'=>'Sanciones',
               'Estatus'=>'Estatus',
               'Requerimientos'=>'Requerimientos',
               'Roles'=>'Roles',
               'TiposEvaluacion'=>'Tipos de Evaluaci&oacute;n',
            ];
        
        return $this->render('index', []);
    }

    public function actionOverview($stage) {
        \Yii::$app->language = 'es_ES';
        switch($stage) {
            case 'Etapas':
                $searchModel = new etapasSearch();
                break;
            case 'PaquetesCursos':
                $searchModel = new \app\models\paquetesCursosSearch();
                break;
            case 'Adscripciones':
                $searchModel = new \app\models\adscripcionesSearch();                
                break;
            case 'Convocatorias':
                $searchModel = new \app\models\convocatoriasSearch();                
                break;
            case 'Cursos':
                $searchModel = new \app\models\cursosSearch();                
                break;
            case 'Estimulos':
                $searchModel = new \app\models\estimulosSearch();                
                break;
            case 'Estudios':
                $searchModel = new \app\models\estudiosSearch();                
                break;
            case 'Evaluaciones':
                $searchModel = new \app\models\evaluacionesSearch();                
                break;
            case 'Motivos':
                $searchModel = new \app\models\motivosSearch();                
                break;
            case 'Nombramientos':
                $searchModel = new \app\models\nombramientosSearch();                
                break;
            case 'Sanciones':
                $searchModel = new \app\models\sancionesSearch();                
                break;
            case 'Estatus':
                $searchModel = new \app\models\estatusSearch();                
                break;
            case 'Requerimientos':
                $searchModel = new \app\models\RequerimientosSearch();                
                break;
            case 'Roles':
                $searchModel = new \app\models\RolesSearch();                
                break;
            case 'TiposEvaluacion':
                $searchModel = new \app\models\tiposEvaluacionSearch();                
                break;
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->renderAjax('overview', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'stage'        => $stage,
        ]);
    }
    /**
     * Displays a single etapas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $action)
    {
        \Yii::$app->language = 'es_ES';
       $oModel = $this->getModel($action);        
        return $this->renderAjax('view', [
            'model' => $this->findModel($id,$oModel),
            'stage' => $action,
        ]);
    }

    /**
     * Creates a new etapas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($action)
    {
        \Yii::$app->language = 'es_ES';
        $model = $this->getModel($action);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $model->id;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
                'stage' => $action,
            ]);
        }
    }

    /**
     * Updates an existing etapas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $action)
    {
        \Yii::$app->language = 'es_ES';
        $oModel = $this->getModel($action);               
        $model = $this->findModel($id, $oModel);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $model->id;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
                'stage' => $action,
            ]);
        }
    }

    /**
     * Deletes an existing etapas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $action)
    {
        $oModel = $this->getModel($action);
        $this->findModel($id, $oModel)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the etapas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return etapas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $oModel)
    {
        if (($model = $oModel->findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function getModel($action) {
        switch($action) {
            case 'Etapas':
                $model = new \app\models\Etapas();
                break;
            case 'PaquetesCursos':
                $model = new \app\models\PaquetesCursos();
                break;
            case 'Adscripciones':
                $model = new \app\models\Adscripciones();                
                break;
            case 'Convocatorias':
                $model = new \app\models\Convocatorias();                
                break;
            case 'Cursos':
                $model = new \app\models\Cursos();                
                break;
            case 'Estimulos':
                $model = new \app\models\Estimulos();                
                break;
            case 'Estudios':
                $model = new \app\models\Estudios();                
                break;
            case 'Evaluaciones':
                $model = new \app\models\Evaluaciones();                
                break;
            case 'Motivos':
                $model = new \app\models\Motivos();                
                break;
            case 'Nombramientos':
                $model = new \app\models\Nombramientos();                
                break;
            case 'Sanciones':
                $model = new \app\models\Sanciones();                
                break;
            case 'Estatus':
                $model = new \app\models\Estatus();                
                break;
            case 'Requerimientos':
                $model = new \app\models\Requerimientos();                
                break;
            case 'Roles':
                $model = new \app\models\Roles();                
                break;
            case 'TiposEvaluacion':
                $model = new \app\models\TiposEvaluacion();                
                break;
        }
        return $model;
    }
}
