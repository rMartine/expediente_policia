<?php

namespace app\controllers;

use app\models\Convocatorias;
use app\models\RequerimientosConvocatoria;
use app\models\convocatoriasSearch;
use app\models\ConvocatoriasFormSearch;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * ConvocatoriasController implements the CRUD actions for convocatorias model.
 */
class ConvocatoriasController extends Controller
{
	/**
	 * Lists all convocatorias models.
	 * @return mixed
	 */
	public function actionIndex()
	{
        $model         = new Convocatorias();
        $form          = new ConvocatoriasFormSearch();
        $convocatorias = null;
        $search        = null;

        if ($form->load(Yii::$app->request->post())) {
        	$search        = Html::encode($form->search_element);
            $convocatorias = $model->searchConvocatorias($search);
        } else {
        	$convocatorias = Convocatorias::find()->all();
        }
        Url::remember();
        return $this->render('index', [
            'convocatorias' => $convocatorias,
            'form' => $form,
            'search' => $search
        ]);
	}

	/**
	 * Displays a single convocatorias model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        \Yii::$app->language = 'es_ES';
        Url::remember();
        $requerimientosConvocatoria = new RequerimientosConvocatoria();
        return $this->renderPartial('view', [
			'model' => $this->findModel($id),
			'requerimientosConvocatoria' => $requerimientosConvocatoria,
		]);
	}

	/**
	 * Creates a new convocatorias model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
                 \Yii::$app->language = 'es_ES';
		$model = new convocatorias;
		$requerimientosConvocatoria = new RequerimientosConvocatoria();
		try {
            if (isset($_POST['Convocatorias']) && isset($_POST['RequerimientosConvocatoria'])) {
            	$model->attributes = $_POST['Convocatorias'];
            	if($model->save()) {
                    $requerimientosConvocatoria->requerimientos = $_POST['RequerimientosConvocatoria']['requerimientos'];
                    foreach ($requerimientosConvocatoria->requerimientos as $key => $checked) {
                            if ($checked == 1) {
                                    $rc = new RequerimientosConvocatoria();
                                    $exists = RequerimientosConvocatoria::find()
                                            ->where(['idConvocatoria' => $model->id, 'idRequerimiento' => $key])
                                            ->exists();
                                            if ($exists) {
                                                    $rc = RequerimientosConvocatoria::findOne(['idConvocatoria' => $model->id, 'idRequerimiento' => $key]);
                                            }
                                    $rc->idRequerimiento = $key;
                                    $rc->idConvocatoria = $model->id;
                                    $rc->save();
                            }
                    }

                    return $this->redirect(Url::previous());
                } else {
                    return $this->render('create', [
                            'model' => $model,
                            'requerimientosConvocatoria' => $requerimientosConvocatoria
                            ]);
                }
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', [
        	'model' => $model,
        	'requerimientosConvocatoria' => $requerimientosConvocatoria
        	]);
	}

	/**
	 * Updates an existing convocatorias model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
                 \Yii::$app->language = 'es_ES';
		$model = $this->findModel($id);
		$requerimientosConvocatoria = new RequerimientosConvocatoria();

		if (isset($_POST['Convocatorias']) && isset($_POST['RequerimientosConvocatoria'])) {
        	$model->attributes = $_POST['Convocatorias'];
        	$model->save();

        	$requerimientosConvocatoria->requerimientos = $_POST['RequerimientosConvocatoria']['requerimientos'];
        	foreach ($requerimientosConvocatoria->requerimientos as $key => $checked) {
        		$rc = RequerimientosConvocatoria::findOne(['idConvocatoria' => $model->id, 'idRequerimiento' => $key]);
        		
        		if ($checked == 1) {
    				if ($rc == null) $rc = new RequerimientosConvocatoria();
        			$rc->idRequerimiento = $key;
                                $rc->idConvocatoria = $model->id;
                                $rc->save();
        		} else {
    				if ($rc != null) $rc->delete();
        		}
        	}

            return $this->redirect(Url::to(['convocatorias/']));
        } elseif (!\Yii::$app->request->isPost) {
            return $this->renderPartial('update', [
				'model' => $model,
				'requerimientosConvocatoria' => $requerimientosConvocatoria,
			]);
        }
	}

	/**
	 * Deletes an existing convocatorias model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
            $requerimientosConvocatoria = new RequerimientosConvocatoria();
            $requerimientosConvocatoria->deleteAll(['and', 'idConvocatoria = :id_convocatoria'], [
                ':id_convocatoria' => $id
            ]);
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
	}

	/**
	 * Finds the convocatorias model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return convocatorias the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = convocatorias::findOne($id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
