<?php

namespace app\controllers;

use app\models\BajasFormSearch;
use app\models\Elementos;
use app\models\ElementosSearch;
use app\models\LogRequerimientos;
use app\models\LogNombramientos;
use app\models\LogCursos;
use app\models\Cursos;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\CargaImagen;
use yii\web\UploadedFile;

/**
 * AspirantesController implements the CRUD actions for Elementos model.
 */
class AspirantesController extends Controller
{	
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
	/**
     * Muestra la primera página.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Elementos();
        $arrElementos = $model->findAspirantes();
        $form = new BajasFormSearch;
        $search = null;

        if ($form->load(Yii::$app->request->post())) {
            if ($form->validate()) {
                $search       = Html::encode($form->search_element);
                $arrElementos = $model->searchAspirantes($search);
            } else {
                $form->getErrors();
            }
        }
        return $this->render('inicio', [
            'arrElementos' => $arrElementos,
            'form' => $form,
            'search' => $search
        ]);
    }

    /**
     * Muestra en detalle el expediente de un elemento.
     * @return mixed
     */
    public function actionDetalle()
    {
        if(isset($_GET['pestana'])){
            $pestana = $_GET['pestana'];
        } else {
            $pestana = 'generales';
        }

        if(isset($_POST['idElemento'])){
            $idElemento = $_POST['idElemento'];
            $campo = $_POST['campo'];
        } else {
            $idElemento = $_GET['idElemento'];
        }
        $pagina = (isset($_GET['pagina']))?$_GET['pagina']:0;
        
        $logCursos = new LogCursos();
        $logNombramientos = new LogNombramientos();
        $aspirante = $this->findModel($idElemento);
        $model = new CargaImagen();
        $cursos = new Cursos();


        //$aspirante->fotografia = 'no.png';

        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->file) {
                if ($campo == 0) {
                    $foto = 'foto_' . $idElemento . '_' . time() . '.' . $model->file->extension;
                    $model->file->saveAs('img/fotos_elementos/' . $foto);
                    $aspirante->fotografia = $foto;
                } else {
                    $foto = $idElemento . '_' . time() . '_' . $campo . '.' . $model->file->extension;
                    if ($campo == 'menique_izq') {
                        $model->file->saveAs('img/fingerprints/' . $foto);
                        $aspirante->menique_izq = $foto;
                    } elseif ($campo == 'anular_izq'){
                        $model->file->saveAs('img/fingerprints/' . $foto);
                        $aspirante->anular_izq = $foto;
                    } elseif ($campo == 'medio_izq'){
                        $model->file->saveAs('img/fingerprints/' . $foto);
                        $aspirante->medio_izq = $foto;                        
                    } elseif ($campo == 'indice_izq'){
                        $model->file->saveAs('img/fingerprints/' . $foto);
                        $aspirante->indice_izq = $foto;
                    } elseif ($campo == 'pulgar_izq'){
                        $model->file->saveAs('img/fingerprints/' . $foto);
                        $aspirante->pulgar_izq = $foto;
                    } elseif ($campo == 'menique_der'){
                        $model->file->saveAs('img/fingerprints/' . $foto);
                        $aspirante->menique_der = $foto;
                    } elseif ($campo == 'anular_der'){
                        $model->file->saveAs('img/fingerprints/' . $foto);
                        $aspirante->anular_der = $foto;
                    } elseif ($campo == 'medio_der'){
                        $model->file->saveAs('img/fingerprints/' . $foto);
                        $aspirante->medio_der = $foto;
                    } elseif ($campo == 'indice_der'){
                        $model->file->saveAs('img/fingerprints/' . $foto);
                        $aspirante->indice_der = $foto;
                    } elseif ($campo == 'pulgar_der'){
                        $model->file->saveAs('img/fingerprints/' . $foto);
                        $aspirante->pulgar_der = $foto;
                    }
                }
                $aspirante->save();
                $aspirante = $this->findModel($idElemento);
            } elseif ($campo == 2) {
                if (!($aspirante->load($_POST) && $aspirante->save())) {
                    return $this->render('detalle', [
                        'aspirante' => $aspirante,
                        'pestana' => $pestana,
                        'logCursos'=>$logCursos,
                        'logNombramientos'=>$logNombramientos,
                        'cursos'=>$cursos,
                        'pagina'=>$pagina
                    ]);
                }
            }
        }
        return $this->render('detalle', [
            'aspirante' => $aspirante,
            'pestana' => $pestana,
            'logCursos'=>$logCursos,
            'logNombramientos'=>$logNombramientos,
            'cursos'=>$cursos,
            'pagina'=>$pagina
        ]);
    }
	
    /**
     * Muestra formulario de creación de nuevo registro.
     * @return mixed
     */
    public function actionHabilitaSeleccion($id) {
        //verificar si cumplió con los requisitos
        Elementos::setEtapa($id, 3);
        $aspirante = $this->findModel($id);
        $aspirante->idEtapa = 3;
        $aspirante->save();
        return $this->redirect(array('aspirantes/detalle','idElemento'=>$id, 'pestana'=>'seleccion'));
    }    

    /**
     * Modificar el estatus de un aspirante a rechazado.
     * @return mixed
     */
    public function actionRechazarAspirante($id, $ajax = false) {
        //verificar si cumplió con los requisitos
        $aspirante = $this->findModel($id);
        $aspirante->idEstatus = 5;
        if($aspirante->save()){
            return ($ajax)?1:$this->redirect(Yii::$app->homeUrl . 'personal');
        } else {
            return $this->redirect(array('aspirantes/detalle','idElemento'=>$id, 'pestana'=>'seleccion'));
        }
    }  

    /**
     * Muestra formulario de creación de nuevo registro.
     * @return mixed
     */
    public function actionHabilitaNombramiento($id) {
        //verificar si cumplió con los requisitos
        Elementos::setEtapa($id, 5);
        $aspirante = $this->findModel($id);
        $aspirante->idEtapa = 5;
        $aspirante->save();
        return $this->redirect(array('aspirantes/detalle','idElemento'=>$id, 'pestana'=>'nombramiento'));
    } 

    /**
     * Muestra formulario de creación de nuevo registro.
     * @return mixed
     */
    public function actionHabilitaFormacion($id) {
        //verificar si cumplió con los requisitos
        Elementos::setEtapa($id, 4);
        $aspirante = $this->findModel($id);
        $aspirante->idEtapa = 4;
        $aspirante->save();
        return $this->redirect(array('aspirantes/detalle','idElemento'=>$id, 'pestana'=>'formacion'));
    }

	/**
     * Muestra formulario de creación de nuevo registro.
     * @return mixed
     */
    public function actionNuevo() {
        $aspirante = new Elementos();
        $maxFolio = Elementos::find()->select('MAX(folio) as max_folio')->one();
        $next_folio = $maxFolio->max_folio;
        $next_folio++;
        $aspirante->folio = $next_folio;
        if (Yii::$app->request->isPost) {
            $op = $_POST['op'];
            if ($op == 1) {
                try {
                    if ($aspirante->load($_POST)) {
                        $edad = floor((time() - strtotime($aspirante->fecha_nac))/31536000); //Cálculo de edad
                        $aspirante->edad = $edad;
                        $aspirante->fecha_ingreso = date('Y-m-d', time());
                        $aspirante->idEtapa = 2; //Etapa de datos generales
                        $aspirante->idEstatus = 1; //Estatus de aspirante
                        $aspirante->fotografia = 'no.png';
                        $aspirante->menique_der = 'no.png';
                        $aspirante->anular_der = 'no.png';
                        $aspirante->medio_der = 'no.png';
                        $aspirante->indice_der = 'no.png';
                        $aspirante->pulgar_der = 'no.png';
                        $aspirante->pulgar_izq = 'no.png';
                        $aspirante->indice_izq = 'no.png';
                        $aspirante->medio_izq = 'no.png';
                        $aspirante->anular_izq = 'no.png';
                        $aspirante->menique_izq = 'no.png';
                        if ($aspirante->save()) {
                            Elementos::nuevosLogs($aspirante->id);
                            return $this->redirect(Yii::$app->homeUrl . 'aspirantes');
                        } elseif(!\Yii::$app->request->isPost) {
                            $aspirante->load($_GET);
                        }
                    }
                } catch (\Exception $e) {
                    $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
                    $aspirante->addError('_exception', $msg);
                }
                return $this->render('nuevo', [
                    'aspirante' => $aspirante
                ]);
            } else {
                return $this->redirect(Yii::$app->homeUrl . 'aspirantes');
            }
        } else {
            return $this->render('nuevo', [
                'aspirante' => $aspirante
            ]);
        }        
    }

    protected function findModel($id)
    {
        if (($model = Elementos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
    }
    
    public function actionGetFormReclutamiento($idElemento) {
        $aspirante = $this->findModel($idElemento);        
        return $this->renderAjax('form_reclutamiento', [
            'aspirante' => $aspirante,
        ]);
    }
    
    public function actionGetFormRechazo($idElemento) {
        $aspirante = $this->findModel($idElemento);        
        return $this->renderAjax('form_rechazo', [
            'aspirante' => $aspirante,
        ]);
    }
    
    public function actionGetFormSeleccion($idElemento) {
        $aspirante = $this->findModel($idElemento);        
        return $this->renderAjax('form_seleccion', [
            'aspirante' => $aspirante,
        ]);
    }
    
    public function actionGetFormFormacion($idElemento) {
        $aspirante = $this->findModel($idElemento);        
        return $this->renderAjax('form_formacion', [
            'aspirante' => $aspirante,
        ]);
    }
}
