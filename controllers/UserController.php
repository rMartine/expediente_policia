<?php

namespace app\controllers;

use Yii;
use app\models\Usuarios;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

use yii\filters\AccessControl;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['create', 'update', 'delete'],
                'only' => ['index', 'create', 'update', 'delete', 'view'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            \Yii::$app->language = 'es_ES';
                            $isAdmin = \Yii::$app->user->identity->id_rol <= 2 && \Yii::$app->user->identity->estatus_usr == 1;
                            return $isAdmin;
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {   
        \Yii::$app->language = 'es_ES';
        $searchModel = new UserSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        if(isset($_POST['reload']) && $_POST['reload'] == 1) {
            return $this->renderAjax('grid', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]);
        } else {
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]);
        }
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $partial = false)
    {
        return ($partial)?$this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]):$this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($partial = false)
    {        
        $model = new Usuarios;  
        $options = [
            'model' => $model,
        ];
        if ($model->load(Yii::$app->request->post())) {
            /*
            $oFile = UploadedFile::getInstance($model, 'avatar');
            if($oFile && ($oFile->extension == 'png' ||$oFile->extension == 'jpg' || $oFile->extension == 'jpeg')) {
                $model->avatar = sha1($model->username) . '.' . $oFile->extension;                
            }
             * */
             
            if(!empty($model->avatar)) {
                $uriPhp = 'data://' . substr($model->avatar, 5);
                $fp = fopen($uriPhp, 'r');
                $meta = stream_get_meta_data($fp);
                $ext = explode('/', $meta['mediatype']);
                $ext = $ext[1];
                $model->avatar = sha1($model->username) . '.' . $ext;
            } 
            
            if($model->save(true, ['username', 'password','id_cargo','id_adscripcion','id_rol','estatus_usr','avatar'])) { 
                /*if($oFile){
                    $oFile->saveAs('avatars/'.$model->avatar);
                } */
                if(!empty($model->avatar)) {
                    $binary = file_get_contents($uriPhp);
                    file_put_contents('avatars/'.$model->avatar, $binary);
                }
                return ($partial)?$model->id:$this->redirect(['view', 'id' => $model->id]); 
            } else {
                return ($partial)?$this->renderAjax('create', $options):$this->render('create', $options);
            }                       
        } else {
            return ($partial)?$this->renderAjax('create', $options):$this->render('create', $options);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $partial = false)
    {
        $model = $this->findModel($id);
        $options = [
            'model' => $model,
        ];
        $fields = ['username','id_cargo','id_adscripcion','id_rol','estatus_usr','avatar'];
        if ($model->load(Yii::$app->request->post())) { 
            if(!empty($model->password)){
                $fields[] = 'password';
            }
            /*
            $oFile = UploadedFile::getInstance($model, 'avatar');
            if($oFile && ($oFile->extension == 'png' ||$oFile->extension == 'jpg' || $oFile->extension == 'jpeg')) {
                $model->avatar = sha1($model->username) . '.' . $oFile->extension;                
            }*/
            
            if(!empty($model->avatar)) {
                $uriPhp = 'data://' . substr($model->avatar, 5);
                $fp = fopen($uriPhp, 'r');
                $meta = stream_get_meta_data($fp);
                $ext = explode('/', $meta['mediatype']);
                $ext = $ext[1];
                $model->avatar = sha1($model->username) . '.' . $ext;
            }            
            
            if($model->save(true, $fields)) {                
                //print_r(explode('/', $meta['mediatype'])[1]);exit;
                if(!empty($model->avatar)) {
                    $binary = file_get_contents($uriPhp);
                    file_put_contents('avatars/'.$model->avatar, $binary);
                }
                /*if($oFile){
                    $oFile->saveAs('avatars/'.$model->avatar);
                }*/
                return ($partial)?$model->id:$this->goBack(); 
            } else {
                return ($partial)?$this->renderAjax('update', $options):$this->render('update', $options);
            }  
        } else {
            return ($partial)?$this->renderAjax('update', $options):$this->render('update', $options);
        }
    }

    public function actionUpload($id) {
        echo $id; exit;
    }
    
    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Usuarios::find()
                    ->innerJoinWith('roles')
                    ->innerJoinWith('adscripciones')
                    ->innerJoinWith('nombramientos')
                    ->where(['usuarios.id'=>$id])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionGetkey($id)
    {   
        if($id > 0) {
            $model = $this->findModel($id);
            $oUser = $model->getAttributes();
            $keyFileSeed = Yii::$app->params['keyFileSeed'];
            $strMemberKey = sha1($keyFileSeed . $oUser['creado']);
            header("Content-disposition: attachment; filename=llave.rxt");
            header("Content-type: application/octet-stream");
            echo $strMemberKey;   
        }    
        exit;
    }
    
    public function actionMember(){
        \Yii::$app->language = 'es_ES';
        return $this->render('member', [
            'model' => $this->findModel(\Yii::$app->user->identity->id),
        ]);
    }
    
    public function actionMemberupdate(){
        \Yii::$app->language = 'es_ES';
        $model = $this->findModel(\Yii::$app->user->identity->id);
        $options = [
            'model' => $model,
        ];
        $fields = [];
        if ($model->load(Yii::$app->request->post())) { 
            if(!empty($model->password)){
                $fields[] = 'password';
            }
            if($model->save(true, $fields)) {                
                return $this->goBack();//$mode$l->id;
            } else {
                return $this->render('memberupdate', $options);
            }  
        } else {
            return $this->render('memberupdate', $options);
        }
    }
}