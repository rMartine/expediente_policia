<?php

namespace app\controllers;

use app\models\LogEvaluaciones;
use app\models\LogEvaluacionesSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * LogEvaluacionesController implements the CRUD actions for LogEvaluaciones model.
 */
class LogEvaluacionesController extends Controller
{
	/**
	 * Lists all LogEvaluaciones models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new LogEvaluacionesSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single LogEvaluaciones model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new LogEvaluaciones model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new LogEvaluaciones;

		try {
            if ($model->load($_POST) && $model->save()) {
                $tab = ($_POST['LogEvaluaciones']['idEvaluacion'] == 4)?
                        '#tab_'.($_POST['LogEvaluaciones']['idEvaluacion']):
                        '#tab_'.($_POST['LogEvaluaciones']['idEvaluacion']+1);
                return $this->redirect(Url::previous().$tab);
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing LogEvaluaciones model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load($_POST) && $model->save()) {
                $tab = ($_POST['LogEvaluaciones']['idEvaluacion'] == 4)?
                        '#tab_'.($_POST['LogEvaluaciones']['idEvaluacion']):
                        '#tab_'.($_POST['LogEvaluaciones']['idEvaluacion']+1);
                return $this->redirect(Url::previous().$tab);
		} else {
			return $this->renderAjax('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing LogEvaluaciones model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the LogEvaluaciones model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return LogEvaluaciones the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = LogEvaluaciones::findOne($id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
