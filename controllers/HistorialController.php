<?php

namespace app\controllers;

use yii\web\Controller;
use Yii;
use yii\filters\VerbFilter;
use app\models\Elementos;
use yii\helpers\Html;
use app\models\PersonalFormSearch;
use app\models\Requerimientos;
use app\models\CargaImagen;

class HistorialController extends Controller
{


	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	public function actionIndex()
	{
		$oElementosModel = new Elementos();
		$arrElementos    = $oElementosModel->getElementosForHistorial();
		$form            = new PersonalFormSearch;
		
		$search = null;

		if ($form->load(Yii::$app->request->post())) {
			if ($form->validate()) {
				$search       = Html::encode($form->search_element);
				$arrElementos = $oElementosModel->searchHistorial($search);
			} else {
				$form->getErrors();
			}
		}

		return $this->render('index', [
			'arrElementos' => $arrElementos,
			'form'         => $form,
			'search'       => $search
		]);
	}

	public function actionDetalle($elemento = 0)
	{
		if ($elemento > 0) {
			$oElemento         = Elementos::findOne($elemento);
			$arrRequerimientos = Requerimientos::find()->all();

			return $this->render('detalle', [
				'oElemento'         => $oElemento,
				'arrRequerimientos' => $arrRequerimientos
			]);
		} else {
			$this->redirect(Yii::$app->urlManager->getBaseUrl() . '/historial');
		}
	}

	public function actionSave()
	{
		$idElemento = (!is_null(Yii::$app->request->post('idElemento'))) ? (int) Yii::$app->request->post('idElemento') : 0;
		$campo = (!is_null(Yii::$app->request->post('campo'))) ? Yii::$app->request->post('campo') : '';

		$oElemento = $this->findModel($idElemento);

		if ($idElemento > 0 && Yii::$app->request->isPost) {
			if ($campo == 2) {
				if (!($oElemento->load($_POST) && $oElemento->save())) {
					return $this->render('detalle', [
						'oElemento' => $oElemento
					]);
				}
			}
		}

		$this->redirect('index');
	}

	protected function findModel($id)
	{
		if (($model = Elementos::findOne($id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}

}