<?php

namespace app\controllers;

use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * ConfiguracionController implements the CRUD actions for etapas model.
 */
class ConfiguracionController extends Controller
{
	/**
	 * Lists all etapas models.
	 * @return mixed
	 */
	public function actionIndex()
	{
                Url::remember();
		return $this->render('index');
	}

}
