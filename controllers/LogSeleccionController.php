<?php

namespace app\controllers;

use app\models\LogSeleccion;
use app\models\LogSeleccionSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * LogSeleccionController implements the CRUD actions for LogSeleccion model.
 */
class LogSeleccionController extends Controller
{
	/**
	 * Lists all LogSeleccion models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new LogSeleccionSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single LogSeleccion model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new LogSeleccion model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate($ajax, $ajax = false)
	{
		$model = new LogSeleccion;

		try {
            if ($model->load($_POST) && $model->save()) {
                if($ajax) {
                    return 1;
                } else {
                    return $this->redirect(Url::previous());
                }
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing LogSeleccion model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id, $ajax = false)
	{
		$model = $this->findModel($id);
		$idElemento = $_POST['idElemento'];
		if ($model->load($_POST) && $model->save()) {
                    if($ajax) {
                        return 1;
                    } else {
                        return $this->redirect(array('aspirantes/detalle','idElemento'=>$idElemento, 'pestana'=>'seleccion'));
                    }
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing LogSeleccion model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id, $ajax)
	{
		$this->findModel($id)->delete();
                if($ajax) {
                    return 1;
                }
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the LogSeleccion model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return LogSeleccion the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = LogSeleccion::findOne($id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
