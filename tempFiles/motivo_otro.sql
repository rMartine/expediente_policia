INSERT INTO `expediente_policia`.`motivos` (`id`, `motivo`, `descripcion`, `ordinaria`) VALUES (NULL, 'Otro', 'Motivo otro extraordinario', '0');

ALTER TABLE `log_bajas` ADD `otro_motivo` TEXT CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL AFTER `idMotivo`;