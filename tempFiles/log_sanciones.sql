#Removemos la relacion hacia causas:
ALTER TABLE `log_sanciones` DROP FOREIGN KEY `fk_log_sanciones_causas1`;

#Eliminamos el campo idCausa:
ALTER TABLE `log_sanciones` DROP `idCausa`;

#Agregamos el campo otra_sancion:
ALTER TABLE `log_sanciones` ADD `otra_sancion` TEXT CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL AFTER `idSancion`;

#Agregamos el campo causa:
ALTER TABLE `log_sanciones` ADD `causa` TEXT CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL AFTER `idSancion`;

#Eliminamos el campo duracion:
ALTER TABLE `log_sanciones` DROP `duracion`;

#Agregamos el campo fecha_inicio:
ALTER TABLE `log_sanciones` ADD `fecha_inicio` DATE NULL AFTER `fecha`;

#Agregamos el campo fecha_fin:
ALTER TABLE `log_sanciones` ADD `fecha_fin` DATE NULL AFTER `fecha_inicio`;