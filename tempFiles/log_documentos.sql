-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-04-2015 a las 10:49:11
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `expediente_policia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_documentos`
--

CREATE TABLE IF NOT EXISTS `log_documentos` (
`id` int(11) NOT NULL,
  `nombreDocumento` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `fechaSubida` date NOT NULL,
  `nombreArchivo` text NOT NULL,
  `idElemento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `log_documentos`
--
ALTER TABLE `log_documentos`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_log_documentos_elementos_idx` (`idElemento`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `log_documentos`
--
ALTER TABLE `log_documentos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `log_documentos`
--
ALTER TABLE `log_documentos`
ADD CONSTRAINT `fk_log_documentos_elementos` FOREIGN KEY (`idElemento`) REFERENCES `elementos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
