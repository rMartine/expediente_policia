    $('#ajax_div').on('submit','form',function(event){
        event.preventDefault();
        $.ajax({
               type     :'POST',
               cache    : false,
               url  : $(this).attr('action'),//'user/create?partial=true',
               data : $(this).serialize(),
               success  : function(response) {
                   if(parseInt(response) > 0) {
                       action = $('#ajax_div #action_pointer').attr('action');
                       //successUrl = $('#ajax_div #action_pointer').attr('url')+'/catalogos/view?id='+response +'&action='+ action;
                       //loadViewFromUrl(successUrl);
                       $('.close').click();
                       reloadGrid($('#base_url').attr('url')+'/catalogos/overview?&stage='+$('#ajax_div #action_pointer').attr('action'), action);
                   } else {
                       $('#ajax_div').html(response);
                       $('#ajaxModal').modal();
                   }                   
               }
               });
    });
    function loadViewFromId(id) {
        $.ajax({
               type     :'POST',
               cache    : false,
               url  : 'user/view?id='+id+'&partial=true',
               data : $(this).serialize(),
               success  : function(response) {                   
                   $('#ajax_div').html(response);
                   $('#ajaxModal').modal();
               }
               });        
    }
    function loadViewFromUrl(Url) {
        $.ajax({
               type     :'POST',
               cache    : false,
               url  : Url,
               data : $(this).serialize(),
               success  : function(response) {                   
                   $('#ajax_div').html(response);
                   $('#ajaxModal').modal();
               }
               });        
    }
    $('.panel-group').on('click','.grid-view td a[title=Ver], .grid-view td a[title=View]',function(event){
        event.preventDefault();
        parent = $('.panel-body').has(this).attr('parent');
        baseUrl = $(this).attr('href');
        target = $('.panel-body').has(this);
        url = baseUrl + '&action='+parent;
        loadViewFromUrl(url);
    });
    
    $('.panel-group').on('click','.grid-view td a[title=Update], .grid-view td a[title=Actualizar]',function(event){
        event.preventDefault();
        parent = $('.panel-body').has(this).attr('parent');
        baseUrl = $(this).attr('href');
        target = $('.panel-body').has(this);
        url = baseUrl + '&action='+parent;
        loadViewFromUrl(url);
    });
    
    $('.panel-group').on('click','.creator',function(event){
        event.preventDefault();
        parent = $(this).attr('parent');
        url = 'catalogos/create' + '?action='+parent;
        loadViewFromUrl(url);
    });
    
    $('#ajax_div').on('click','.updater',function(event){
        event.preventDefault();
        loadViewFromUrl($(this).attr('href')+'&action='+$(this).attr('stage'));
    });
    $('#ajax_div').on('click','.closer',function(event){
        $('.close').click();
    });
    function addLoading(targetId) {
        $(targetId).append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
    }
    
    function endLoading(targetId) {
        $('.overlay', targetId).remove();
    }
    
    $('.panel-group').on('click','.grid-view td a[title=Eliminar], .grid-view td a[title=Delete]',function(event){
        event.preventDefault();
        parent = $('.panel-body').has(this).attr('parent');
        baseUrl = $(this).attr('href');
        target = $('.panel-body').has(this);
        url = baseUrl + '&action='+parent;
        //loadViewFromUrl(url);
    });
    
    $(".panel-group").on('click', '.pagination a, thead a', function(){
        event.preventDefault();        
        baseUrl = $(this).attr('href');
        action = $('.panel-body').has(this).attr('parent');
        reloadGrid(baseUrl,action);        
    });
    
    function reloadGrid(url, action){
        action = action || '';
        target = $('.panel-body:visible');
        
        if(target == '' || action == $('.panel-body:visible').attr('parent')) {

            $.ajax({
                 type     :'POST',
                 cache    : false,
                 url  : url,
                 success  : function(response) {                   
                     $(target).html(response);
                     action = $('.panel-body:visible').attr('parent');
                     prepareActions(action);
                 }
            });  
        } 
    }   
    
    $('#catalogos .opener').on('click', function (e) {
        tab = $('#'+'collapse_'+$(this).attr('id')+' .panel-body');
        stage = $(this).attr('id');
        if(!$(tab).is(":visible")) {
            if($(tab).html()=='') {
               addLoading('.main_grid .box');
               $.ajax({
                    type     :'POST',
                    cache    : false,
                    url  : $('#base_url').attr('url')+'/catalogos/overview?&stage='+$(this).attr('id'),
                    data : $(this).serialize(),
                    success  : function(response) {
                        endLoading('.main_grid .box');
                        $(tab).html(response);
                        prepareActions(stage);                        
                    }
               });  
            } else {
                console.debug('Already loaded');
            }
        }        
    });
    
    function prepareActions(stage) {
        eachTarget = $(".panel-body:visible .grid-view td a[title=Eliminar] , .panel-body:visible .grid-view td a[title=Delete]");
        $(eachTarget).each(function(){
            $(this).attr('href', $(this).attr('href') + '&action=' + stage);
        })
    }