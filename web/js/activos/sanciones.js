$(document).ready(function () {
	oSanciones.start();
});

var oSanciones = (function sancionesClousure () {

	function initListeners () {
		if ($('#logsanciones-idsancion').val() == 2 || $('#logsanciones-idsancion').val() == 4) {
			$('.box-fecha-inicio').show();
			$('.box-fecha-fin').show();
		} else {
			$('.box-fecha-inicio').hide();
			$('.box-fecha-fin').hide();
		}

		if ($('#logsanciones-idsancion').val() == 6) {
			$('.box-otra-sancion').show();
		} else {
			$('.box-otra-sancion').hide();
		}

		$('#logsanciones-idsancion').on('change', function () {
			var value = $(this).val();

			if (value == 2 || value == 4) {
				$('.box-fecha-inicio').show();
				$('.box-fecha-fin').show();
			} else {
				$('.box-fecha-inicio').hide();
				$('.box-fecha-fin').hide();
			}

			if (value == 6) {
				$('.box-otra-sancion').show();
			} else {
				$('.box-otra-sancion').hide();
			}
		});
	}

	return {
		start : function () {
			initListeners();
		}
	}

}) ();