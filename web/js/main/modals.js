$(document).ready(function () {
	var modal = $("#modal-model");
	var modalContent = modal.find(".modal-content");
	var modalBody = modal.find(".modal-body");
	var buttonPost = modal.find("[data-post]");
	var modalType, url;
	var width = 700;

	$("body").on("click", 'a[data-action]', function (e) {
		e.preventDefault();
		url = $(this).attr("data-action");
		modalType = $(this).attr("data-modal-type");
		modalBehaviours[modalType](url);
	});

	var modalBehaviours = {
		edit: function (url) {
			$.get(url, function(data) {
				modalContent.width(width);
				modalBody.html(data);

				var form = modalBody.find("form");
				form.on('submit', function (e) {
					if (!e.isDefaultPrevented()) showSpinner("Guardando datos...", true);
				});
			});
			buttonPost.text("Guardar");
			showSpinner("Cargando datos...", true);
		},
		remove: function (url) {
			buttonPost.text("Eliminar");
			showSpinner("¿Deseas eliminar este registro?", false);
		},
		view: function () {
			$.get(url, function(data) {
				modalContent.width(width);
				modalBody.html(data);
			});
			buttonPost.attr("disabled", "");
			showSpinner("Cargando datos...", true);
		}
	};

	var modalActions = {
		edit: function (url) {
			var form = modalBody.find("form");
			form.find('button[type=submit]').click();
		},
		remove: function (url) {
			$.get(url, function () {});
			showSpinner("Eliminando registro...", true);
		}
	};

	var showSpinner = function (text, spin) {
		var i = spin ? $("<i></i>").addClass("fa fa-refresh fa-spin fa-2x") : "";
		var div = $("<div></div>").addClass("overlay")
			.append(text)
			.append(i);
		modalBody.html(div);
	};

	modal.on('hidden.bs.modal', function (e) {
		$("#modal-model .modal-body").html("");
		buttonPost.removeAttr("disabled");
	});

	buttonPost.on("click", function() {
		modalActions[modalType](url);
	});;
  
});