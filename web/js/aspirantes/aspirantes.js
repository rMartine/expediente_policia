
$(document).ready(function () {
	$(".aspirante-detalle-ui").addClass("disabled");
	$(":input.aspirante-detalle-ui").prop("disabled", true);
	
	$("#edit-button").click(function() {
		$(".aspirante-detalle-ui").removeClass("disabled");
		$(":input.aspirante-detalle-ui").prop("disabled", false);
		$("#edit-button").addClass("disabled");
	});
	
	$("#btn-guardar").click(function() {
		if(window.confirm("¿Deseas guardar los cambios?")) {
			$("#operacion").val ("2");
			$("#generales-form").submit();
		}
			
	});

	$("#btn-nombrar").click(function() {
		$("#asigna-nombramiento").submit();
	});

	$("#btn-cancelar").click(function() {
		if(window.confirm("¿Deseas cancelar la edición?")) {
			$("#operacion").val ("-1");
			$("#generales-form").submit();
		}
			
	});

	$('#elementos-idestatus').on('change', function () {
		var realEstatus = $(this).data('realestatus');
		var realIdEtapa = $(this).data('realidetapa');
		var value       = $(this).val();

		if (realEstatus != value) {
			$('#btn-guardar').attr('disabled', false);
		} else {
			$('#btn-guardar').attr('disabled', true);
		}

		if (realEstatus == 3 || realEstatus == 5) {
			if (value == 1) {
				$('#elementos-idconvocatoria').removeAttr('readonly');
				$('#input-id-etapa').val('2');
			} else {
				$('#elementos-idconvocatoria').attr('readonly', 'true');
				$('#input-id-etapa').val(realIdEtapa);
			}
		} else {
			$('#input-id-etapa').val(realIdEtapa);
		}
	});
});