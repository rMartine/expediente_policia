
$(document).ready(function () {
	$("#btn-nuevo").click(function() {
		if(window.confirm("¿Deseas guardar el nuevo registro?")) {
			$("#op").val("1");
			$("#generales-form").submit();
		}
			
	});

	$("#btn-cancelar").click(function() {
		if(window.confirm("¿Deseas cancelar la edición? Perderás los cambios realizados a excepción de las fotos e imagenes cargadas")) {
			$("#op").val("2");
			$("#generales-form").submit();
		}
			
	});
});