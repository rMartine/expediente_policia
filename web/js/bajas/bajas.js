$(document).ready(function () {
	oBajas.start();
});

var oBajas = (function bajasClousure () {

	function initListeners () {
		var idMotivoExtraordinarioOtro = $('#idMotivoExtraordinarioOtro').val();

		$('.dropdown-extraordinarias').hide();
		
		$('.tipo-baja').on('change', function () {
			numType = $(this).val();

			if (numType == 0) {
				if ($('.dropdown-extraordinarias').val() == idMotivoExtraordinarioOtro) {
					$('.otro-motivo-extraordinario').show();
				} else {
					$('#otro-motivo-error').hide();
					$('.otro-motivo-extraordinario').hide();
				}

				$('.dropdown-ordinarias').hide();
				$('.dropdown-extraordinarias').show();
			} else if (numType == 1) {
				$('#otro-motivo-error').hide();
				$('.otro-motivo-extraordinario').hide();
				$('.dropdown-ordinarias').show();
				$('.dropdown-extraordinarias').hide();
			};
		});

		$('.dropdown-extraordinarias').on('change', function () {
			var option = $(this).val();

			if (option == idMotivoExtraordinarioOtro) {
				$('.otro-motivo-extraordinario').show();
			} else {
				$('#otro-motivo-error').hide();
				$('.otro-motivo-extraordinario').hide();
			}
		});

		$('.baja-datepicker').datepicker().on('changeDate', function () {
			$('.datepicker').hide();
		});

		$('.btn-baja-elemento').on('click', function () {
			var fecha_baja    = $('.input-fecha-baja').val();
			var observaciones = $('.observaciones-baja').val();
			var numIdElemento = $(this).data('id');

			if (fecha_baja == '') {
				$('#fecha-error').show();
				$('.confirmacion-popup').modal('hide');
			} else if ($('.tipo-baja').val() == '0' && $('.dropdown-extraordinarias').val() == idMotivoExtraordinarioOtro && $('#otro-motivo').val() == '') {				
				$('#otro-motivo-error').show();
				$('.confirmacion-popup').modal('hide');
			} else if (observaciones == '') {
				$('#observaciones-error').show();
				$('.confirmacion-popup').modal('hide');
			} else {
				$('.fecha-baja').html(fecha_baja);
				$('#bajasConfirmacionModal-' + numIdElemento).modal();
			}
		});

		$('.input-fecha-baja').on('focus', function () {
			$('#fecha-error').hide();
		});

		$('.observaciones-baja').on('focus', function () {
			$('#observaciones-error').hide();
		});

		$('#otro-motivo').on('focus', function () {
			$('#otro-motivo-error').hide();
		});

		$('#elementos-table').dataTable({
			'bPaginate' : true,
			'bLengthChange' : false,
			'bFilter' : false,
			'bSort' : true,
			'bInfo' : true,
			'bAutoWidth' : false,
			'oLanguage' : {
				'sInfo' : 'Mostrando _START_ a _END_ registros de un total de _TOTAL_',
				'sInfoEmpty' : 'Mostrando 0 a 0 registros de un total de 0',
				"sEmptyTable":"Su búsqueda no arrojó resultados",
				'oPaginate' : {
					'sPrevious' : 'Anterior',
					'sNext' : 'Siguiente'
				}
			},
			'aoColumnDefs' : [
				{
					'bSortable' : false,
					'aTargets' : [ 6 ]
				}
			]
		});
	}

	return {
		start : function () {
			initListeners();
		}
	}

}) ();