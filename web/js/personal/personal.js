$(document).ready(function () {
	oPersonal.start();
});

var oPersonal = (function personalClousure () {

	function initListeners () {
		$('#personal-table').dataTable({
			'bPaginate' : true,
			'bLengthChange' : false,
			'bFilter' : false,
			'bSort' : true,
			'bInfo' : true,
			'bAutoWidth' : false,
			'oLanguage' : {
				'sEmptyTable' : 'No hay documentos entregados',
				'sInfo' : 'Mostrando _START_ a _END_ registros de un total de _TOTAL_',
				'sInfoEmpty' : 'Mostrando 0 a 0 registros de un total de 0',
				'oPaginate' : {
					'sPrevious' : 'Anterior',
					'sNext' : 'Siguiente'
				}
			},
			'aoColumnDefs' : [
				{
					'bSortable' : false,
					'aTargets' : [ 5 ]
				}
			]
		});

		$('#documentos-table').dataTable({
			'bPaginate' : true,
			'bLengthChange' : false,
			'bFilter' : false,
			'bSort' : true,
			'bInfo' : true,
			'bAutoWidth' : false,
			'oLanguage' : {
				'sEmptyTable' : 'No hay documentos entregados',
				'sInfo' : 'Mostrando _START_ a _END_ registros de un total de _TOTAL_',
				'sInfoEmpty' : 'Mostrando 0 a 0 registros de un total de 0',
				'oPaginate' : {
					'sPrevious' : 'Anterior',
					'sNext' : 'Siguiente'
				}
			},
			'aoColumnDefs' : [
				{
					'bSortable' : false,
					'aTargets' : [ 3 ]
				}
			],
		});

		$('#requerimientos').on('change', function () {
			$('#otroDescripcion').val('');
			var value = $(this).val();

			if (value == '-1') {
				$('.btn-carga-documento').addClass('disabled');
				$('.btn-carga-documento').prop('disabled', true);
				$('.nuevo-requerimiento').show();
			} else {
				$('.btn-carga-documento').removeClass('disabled');
				$('.btn-carga-documento').prop('disabled', false);
				$('.nuevo-requerimiento').hide();
			}
		});

		$('#descripcion-requerimiento').on('focus', function () {
			$('.btn-carga-documento').removeClass('disabled');
			$('.btn-carga-documento').prop('disabled', false);
		});

		$('.btn-carga-documento').on('click', function (event) {
			var requerimiento = $('#requerimientos').val();
			var otroNombre    = $('#descripcion-requerimiento').val();

			if (requerimiento == '-1' && otroNombre != '') {
				$('.btn-carga-documento').removeClass('disabled');
				$('.btn-carga-documento').prop('disabled', false);
				$('#cargar-documento').modal('show');
			} else if (requerimiento == '-1' && otroNombre == '') {
				bootbox.alert('Introduzca el nombre del documento');
				$('#cargar-documento').modal('hide');
			}

			if (requerimiento != '-1') {
				$('#cargar-documento').modal('show');
			}
		});

		$('.btn-delete-documento').on('click', function (event) {
			event.preventDefault();
			bootbox.confirm("¿Esta seguro que desea eliminar el documento?", function(result) {
			  	if (result) {
			  		window.location.href = $('.btn-delete-documento').attr('href');
			  	}
			});
		});

		$('#btn-upload-file').on('click', function (event) {
			event.preventDefault();
			var requerimiento = $('#requerimientos').val();

			if (requerimiento == '-1') {
				var otroDescripcion = $('#descripcion-requerimiento').val();
				$('#otroDescripcion').val(otroDescripcion);
			}

			$('#nombreDocumento').val(requerimiento);

			$('#frm-documentos').submit();
		});
	}

	return {
		start : function () {
			initListeners();
		}
	}

}) ();