$(document).ready(function () {
	oReportes.start();
});

var oReportes = (function reportesClousure () {

	function initListeners () {
		var strUrl = '';

		$('.btn-ver-reporte').on('click', function () {
			strUrl = '';
			strUrl = $(this).attr('url');
			$('#clave_empleado').val('');
		});

		$('#frm-agregar-parametro').validate({
			rules : {
				clave_empleado : {
					required : true
				}
			},
			messages : {
				clave_empleado : {
					required : 'Favor de agregar clave de empleado'
				}
			},
			submitHandler: function(form) {
				strUrl += '&claveEmpleado=' + $('#clave_empleado').val();
				openInNewTab(strUrl);
			}
		});

		$('#reporte-elementos-table').dataTable({
			'bPaginate' : true,
			'bLengthChange' : false,
			'bSort' : true,
			'bInfo' : true,
			'bAutoWidth' : false,
			'oLanguage' : {
				'sSearch' : 'Buscar elemento:',
				'sInfo' : 'Mostrando _START_ a _END_ registros de un total de _TOTAL_',
				'sInfoEmpty' : 'Mostrando 0 a 0 registros de un total de 0',
				"sEmptyTable":"Su búsqueda no arrojó resultados",
				'oPaginate' : {
					'sPrevious' : 'Anterior',
					'sNext' : 'Siguiente'
				}
			},
			'aoColumnDefs' : [
				{
					'bSortable' : false,
					'aTargets' : [ 3 ]
				}
			]
		});

		$('.btn-seleccionar-clave').on('click', function () {
			var numClaveEmpleado = $(this).attr('claveEmpleado');

			$('#clave_empleado').val(numClaveEmpleado);
		});
	}

	function openInNewTab (strUrl) {
		var win = window.open(strUrl, '_blank');
		win.focus();
		$('#agregarParametro').modal('hide');
	}

	return {
		start : function () {
			initListeners();
		}
	}

}) ();