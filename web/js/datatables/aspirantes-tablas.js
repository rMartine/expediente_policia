$(document).ready(function() {
        tables = {};
	$('.aspirante-tabla').each(function(element){
            table = $(this).dataTable({
                        "iDisplayLength": 5,
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": false,
                        "bSort": true,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "oLanguage":{
                                "oPaginate":{
                                        "sFirst":"Primero",
                                        "sLast":"Último",
                                        "sNext":"Siguiente",
                                        "sPrevious":"Anterior"},
                                "sEmptyTable":"Su búsqueda no arrojó resultados",
                                "sInfo":"Mostrando _START_ a _END_ registros de un total de _TOTAL_",
                                "sInfoEmpty":"Mostrando 0 a 0 registros de un total de 0",
                                "sInfoFiltered":"(filtrando de un total de _MAX_ registros)"
                        }
            });
            tables[$(this).attr('id')] = table;
        });
        lastPage = $('.main-aspirantes').attr('lastPage');
        if(lastPage > 0) {
            target = $('.main-aspirantes .tab-pane.active .aspirante-tabla').attr('id');
            tables[target].fnPageChange(parseInt(lastPage),true);
        }
});