    var view_url = '/validacions/web'; 
            
   $('#ajax_div').on('submit','form',function(event){
            event.preventDefault();
            addLoading('#ajax_div .box');
            $.ajax({
               type     :'POST',
               cache    : false,
               url  : $(this).attr('action'),//'user/create?partial=true',
               data : $(this).serialize(),
               success  : function(response) {
                   endLoading('#ajax_div .box');
                   if(parseInt(response) > 0) {
                       reloadGrid();
                       $('.close').click();
                       //loadViewFromId(response);
                   } else {
                       if(response.match(/^\d+$/)) {
                           //loadViewFromId(response);
                       }
                       $('#ajax_div').html(response);
                       $('#ajaxModal').modal();
                   }                   
               }
               });
    });
    function loadViewFromId(id) {
        addLoading('#ajax_div .box');
        $.ajax({
               type     :'POST',
               cache    : false,
               url  : view_url+'/user/view?id='+id+'&partial=true',
               data : $(this).serialize(),
               success  : function(response) {                   
                   $('#ajax_div').html(response);
                   $('#ajaxModal').modal();
                   endLoading('#ajax_div .box');
               }
               });        
    }
    function loadViewFromUrl(Url) {
        addLoading('#ajax_div .box');
        $.ajax({
               type     :'POST',
               cache    : false,
               url  : Url+'&partial=true',
               data : $(this).serialize(),
               success  : function(response) {                   
                   $('#ajax_div').html(response);
                   $('#ajaxModal').modal();
                   endLoading('.box');
               }
               });        
    }
    $('#grid_cont').on('click','.grid-view td a[title=View], .grid-view td a[title=Ver]', function(event){
        addLoading('#members_grid');
        event.preventDefault();
        loadViewFromUrl($(this).attr('href'));
    });
    
    $('#grid_cont').on('click', '.grid-view td a[title=Update],.grid-view td a[title=Actualizar]', function(event){
        addLoading('#members_grid');
        event.preventDefault();
        loadViewFromUrl($(this).attr('href'));
    });
    
    $('#ajax_div').on('click','.updater',function(event){
        event.preventDefault();
        loadViewFromUrl($(this).attr('href'));
    });

    $('#ajax_div').on('click','.closer',function(event){
        $('.close').click();
    });
    
    function reloadGrid(){
        $.ajax({
            type     :'POST',
            cache    : false,
            url  : window.location.href,
            data : {'reload':1},
            success  : function(response) {                   
                $('#grid_cont').html(response);
            }
            });   
    }
    
    
    function addLoading(targetId) {
        $(targetId).append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
    }
    
    function endLoading(targetId) {
        $('.overlay', targetId).remove();
    }