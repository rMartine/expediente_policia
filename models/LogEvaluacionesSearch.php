<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogEvaluaciones;

/**
 * LogEvaluacionesSearch represents the model behind the search form about LogEvaluaciones.
 */
class LogEvaluacionesSearch extends Model
{
	public $id;
	public $idElemento;
	public $idEvaluacion;
	public $cumplimiento;
	public $observaciones;
	public $fecha;

	public function rules()
	{
		return [
			[['id', 'idElemento', 'idEvaluacion', 'cumplimiento'], 'integer'],
			[['fecha', 'observaciones'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'idElemento' => 'Id Elemento',
			'idEvaluacion' => 'Id Evaluacion',
			'cumplimiento' => 'Cumplimiento',
			'observaciones' => 'Observaciones',
			'fecha' => 'Fecha',
		];
	}

	public function search($params)
	{
		$query = LogEvaluaciones::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'id' => $this->id,
            'idElemento' => $this->idElemento,
            'idEvaluacion' => $this->idEvaluacion,
            'cumplimiento' => $this->cumplimiento,
            'fecha' => $this->fecha,
        ]);

		$query->andFilterWhere(['like', 'observaciones', $this->observaciones]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
