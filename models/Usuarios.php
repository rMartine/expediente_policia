<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;
/**
 * This is the model class for table "usuarios".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property integer $id_cargo
 * @property integer $id_adscripcion
 * @property integer $id_rol
 * @property integer $estatus_usr
 * @property string $ultima_entrada
 * @property string $avatar
 * @property string $creado
 */
class Usuarios extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    public function attributes()
    {
        return array_merge(
            parent::attributes(),
            ['confirm_password']
        );
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password','confirm_password','id_cargo', 'id_adscripcion', 'id_rol'], 'required'],
            [['username'], 'unique'],
            [['id_cargo', 'id_adscripcion', 'id_rol', 'estatus_usr'], 'integer'],
            [['ultima_entrada', 'creado'], 'safe'],
            [['password'], 'compare', 'compareAttribute'=>'confirm_password'],
            [['avatar'], 'string'],
            [['username', 'password'], 'string', 'max' => 60]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'id_cargo' => 'Cargo',
            'id_adscripcion' => 'Adscripcion',
            'id_rol' => 'Rol',
            'estatus_usr' => 'Activo',
            'ultima_entrada' => 'Ultima Entrada',
            'avatar' => 'Avatar',
            'creado' => 'Creado',
        ];
    }
    
/** INCLUDE USER LOGIN VALIDATION FUNCTIONS**/
        /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
/* modified */
    public static function findIdentityByAccessToken($token, $type = null)
    {
          return static::findOne(['access_token' => $token]);
    }
 
    public function beforeSave($insert)
    {
        // hash new password if set
        $this->password = sha1($this->password);
        /*
        // ensure fields are null so they won't get set as empty string
        $nullAttributes = ["email", "username", "ban_time", "ban_reason"];
        foreach ($nullAttributes as $nullAttribute) {
            $this->$nullAttribute = $this->$nullAttribute ? $this->$nullAttribute : null;
        }
*/
        return parent::beforeSave($insert);
    }
    
/* removed
    public static function findIdentityByAccessToken($token)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
*/
    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Finds user by password reset token
     *
     * @param  string      $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    { 
        return $this->password === sha1($password);
    }

    /**
     * sets encrypted password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function setHashedPassword($password)
    { 
        return $this->password = sha1($password);
    }
    
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Security::generatePasswordHash($password);
        return true;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Security::generateRandomKey();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Security::generateRandomKey() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    /** EXTENSION MOVIE **/
    
    public function getRoles()
    {      
        return $this->hasOne(\app\models\Roles::className(), ['id' => 'id_rol']);
    }
    public function getAdscripciones()
    {      
        return $this->hasOne(\app\models\Adscripciones::className(), ['id' => 'id_adscripcion']);
    }
    public function getNombramientos()
    {      
        return $this->hasOne(\app\models\Nombramientos::className(), ['id' => 'id_cargo']);
    }
}

