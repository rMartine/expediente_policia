<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_evaluaciones".
 */
class LogEvaluaciones extends \app\models\base\LogEvaluaciones
{
	public function getEvaluacion() {
		return $this->hasOne(Evaluaciones::className(), ['id' => 'idEvaluacion']);
	}

	public function getCumplimientoText() {
		return $this->cumplimiento ? 'Si' : 'No';
	}

	public function getFechaText() {
		return date('d/m/Y', strtotime($this->fecha));
	}
}
