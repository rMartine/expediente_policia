<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_estimulos".
 */
class LogEstimulos extends \app\models\base\LogEstimulos
{
	public function getEstimulo() {
		return $this->hasOne(Estimulos::className(), ['id' => 'idEstimulo']);
	}
}
