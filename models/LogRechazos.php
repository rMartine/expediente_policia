<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_rechazos".
 */
class LogRechazos extends \app\models\base\LogRechazos
{
	public function getFechaText() {
		return date('d/m/Y', strtotime($this->fecha));
	}
}
