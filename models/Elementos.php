<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "elementos".
 */
class Elementos extends \app\models\base\Elementos
{
	const ELEMENTOS_ASPIRANTES = 1;
	const ELEMENTOS_ACTIVOS    = 2;
	const ELEMENTOS_INACTIVOS  = 3;
	const ELEMENTOS_ELIMINADOS = 4;
	const ELEMENTOS_RECHAZADOS = 5;

	public $folioConvocatoria;
	public $nombramiento;
	public $adscripcion;
	public $max_folio;
	public $nombre_estatus;
	
	/**
	 * Método para obtener todos los elementos activos con la información
	 * necesaria para la página de bajas
	 *
	 *  @return Array $arrElementos
	 */
	public function getElementosForBajas()
	{       
                $arrConditions = ['<>', 'idEstatus',self::ELEMENTOS_ASPIRANTES];
                if(\Yii::$app->user->identity->id_rol == 4) {                    
                    $arrElementos = $this->find()->where($arrConditions)->andWhere(['idTipo' => 2])->all();
                } else {
                    $arrElementos = $this->find()->where($arrConditions)->all();
                }		

		foreach ($arrElementos as $oElemento) {
			$elemento                = $this->findOne($oElemento->id);
			$oElemento->nombramiento = $elemento->getNombramientoOrderByDate();
			$oElemento->adscripcion  = $elemento->getAdscripcionOrderByDate();
		}

		return $arrElementos;
	}

	/**
	 * Metodo para realizar una busqueda de elementos activos
	 *
	 * @param String $strSearch
	 *
	 * @return Array $arrElementos
	 */
	public function searchElements ($strSearch = '')
	{
		$strQuery = "SELECT * FROM elementos WHERE ";
		$strQuery .= "idEstatus = " . self::ELEMENTOS_ACTIVOS . " AND (";
		$strQuery .= "nombre LIKE '%$strSearch%' OR ";
		$strQuery .= "apellido_paterno LIKE '%$strSearch%' OR ";
		$strQuery .= "apellido_materno LIKE '%$strSearch%' OR ";
		$strQuery .= "folio LIKE '%$strSearch%')";

		$arrElementos = $this->findBySql($strQuery)->all();

		foreach ($arrElementos as $oElemento) {	
			$elemento                = $this->findOne($oElemento->id);
			$oElemento->nombramiento = $elemento->getNombramientoOrderByDate();
			$oElemento->adscripcion  = $elemento->getAdscripcionOrderByDate();
		}

		return $arrElementos;
	}

	/**
	 * Método para obtener todos los elementos con la información
	 * necesaria para la página de personal
	 *
	 *  @return Array $arrElementos
	 */
	public function getElementosForPersonal()
	{
		$strQuery = "SELECT elm.*, est.nombre as nombre_estatus FROM elementos elm ";
		$strQuery .= "LEFT JOIN estatus est ON est.id = elm.idEstatus ";
		$strQuery .= "WHERE elm.idEstatus = " . self::ELEMENTOS_ACTIVOS;
                if(\Yii::$app->user->identity->id_rol == 4) {                    
                     $strQuery .= " AND elm.idTipo = 2 "; 
                }
                
		$arrElementos = $this->findBySql($strQuery)->all();

		foreach ($arrElementos as $oElemento) {
			$elemento                = $this->findOne($oElemento->id);
			$oElemento->nombramiento = $elemento->getNombramientoOrderByDate();
		}

		return $arrElementos;
	}

	/**
	 * Metodo para realizar una busqueda de elementos aspirantes, activos o inactivos
	 *
	 * @param String $strSearch
	 *
	 * @return Array $arrElementos
	 */
	public function searchPersonal ($strSearch = '')
	{
		$strQuery = "SELECT elm.*, est.nombre as nombre_estatus FROM elementos elm ";
		$strQuery .= "LEFT JOIN estatus est ON est.id = elm.idEstatus WHERE ";
                if(\Yii::$app->user->identity->id_rol == 4) {
                   $strQuery .= "elm.idTipo = 2 AND "; 
                }
		$strQuery .= "elm.idEstatus = " . self::ELEMENTOS_ACTIVOS . " AND (";
		$strQuery .= "elm.nombre LIKE '%$strSearch%' OR ";
		$strQuery .= "elm.apellido_paterno LIKE '%$strSearch%' OR ";
		$strQuery .= "elm.apellido_materno LIKE '%$strSearch%' OR ";
		$strQuery .= "elm.folio LIKE '%$strSearch%')";

		$arrElementos = $this->findBySql($strQuery)->all();

		foreach ($arrElementos as $oElemento) {	
			$elemento                = $this->findOne($oElemento->id);
			$oElemento->nombramiento = $elemento->getNombramientoOrderByDate();
		}

		return $arrElementos;
	}

	/**
	 * Método para obtener todos los elementos con la información
	 * necesaria para la página de historial
	 *
	 *  @return Array $arrElementos
	 */
	public function getElementosForHistorial ()
	{
		$strQuery = "SELECT elm.*, est.nombre as nombre_estatus FROM elementos elm ";
		$strQuery .= "LEFT JOIN estatus est ON est.id = elm.idEstatus ";
		$strQuery .= "WHERE elm.idEstatus = " . self::ELEMENTOS_INACTIVOS . " OR elm.idEstatus = " . self::ELEMENTOS_RECHAZADOS;

		$arrElementos = $this->findBySql($strQuery)->all();

		foreach ($arrElementos as $oElemento) {
			$elemento                = $this->findOne($oElemento->id);
			$oElemento->nombramiento = $elemento->getNombramientoOrderByDate();
		}

		return $arrElementos;
	}

	/**
	 * Metodo para realizar una busqueda de elementos aspirantes, activos o inactivos
	 *
	 * @param String $strSearch
	 *
	 * @return Array $arrElementos
	 */
	public function searchHistorial ($strSearch = '')
	{
		$strQuery = "SELECT elm.*, est.nombre as nombre_estatus FROM elementos elm ";
		$strQuery .= "LEFT JOIN estatus est ON est.id = elm.idEstatus WHERE ";
		$strQuery .= "(elm.idEstatus = " . self::ELEMENTOS_INACTIVOS . " OR elm.idEstatus = " . self::ELEMENTOS_RECHAZADOS . ") AND (";
		$strQuery .= "elm.nombre LIKE '%$strSearch%' OR ";
		$strQuery .= "elm.apellido_paterno LIKE '%$strSearch%' OR ";
		$strQuery .= "elm.apellido_materno LIKE '%$strSearch%' OR ";
		$strQuery .= "elm.folio LIKE '%$strSearch%')";

		$arrElementos = $this->findBySql($strQuery)->all();

		foreach ($arrElementos as $oElemento) {	
			$elemento                = $this->findOne($oElemento->id);
			$oElemento->nombramiento = $elemento->getNombramientoOrderByDate();
		}

		return $arrElementos;
	}

	/**
	 * Metodo para obtener todos los elementos activos para la página de reportes
	 *
	 * @return Array $arrElementos
	 */
	public function getElementosForReportes () {
		$strQuery = "SELECT clave_empleado, nombre, apellido_paterno, apellido_materno FROM elementos WHERE idEstatus = " . self::ELEMENTOS_ACTIVOS;

		$arrElementos = $this->findBySql($strQuery)->all();

		return $arrElementos;
	}

	public function findAspirantes() {
                $arrConditions = ['idEstatus' => 1];
                if(\Yii::$app->user->identity->id_rol == 4) {                    
                     $arrConditions['idTipo'] = 2; 
                }
		$arrElementos = $this->find()
			->select('elementos.*, convocatorias.folio as folioConvocatoria')
			->joinWith('idConvocatoria0')
			->where($arrConditions)
			->all();
		return $arrElementos;
	}

	public function reasignarRequerimientos($idElemento) {
		$db = Yii::$app->getDb();
    	$db->createCommand("update elementos set idEtapa=:idEtapa where id=:idElemento")
    	   ->bindValue(':idElemento', $idElemento)
    	   ->bindValue(':idEtapa', $idEtapa)
    	   ->execute();
    	return true;
	}

	public function setEtapa($idElemento, $idEtapa) {
		/*$strQuery = "CALL inserta_requerimientos(:idElemento)";
		$arrElementos = $this->findBySql($strQuery)->all();*/
    	$db = Yii::$app->getDb();
    	$db->createCommand("update elementos set idEtapa=:idEtapa where id=:idElemento")
    	   ->bindValue(':idElemento', $idElemento)
    	   ->bindValue(':idEtapa', $idEtapa)
    	   ->execute();
    	return true;
	}

	public function getFechaIngresoText() {
		return date('d/m/Y', strtotime($this->fecha_ingreso));
	}

	public function findActivos() {
                $arrConditions = ['idEstatus' => self::ELEMENTOS_ACTIVOS];
                if(\Yii::$app->user->identity->id_rol == 4) {                    
                     $arrConditions['idTipo'] = 2; 
                }
		$arrElementos = $this->find()
			->where($arrConditions)
			->all();
		return $arrElementos;
	}

	public function searchAspirantes($search = '') {
                
		$query = "select e.id, ";
		$query .= "concat(e.nombre, ' ', e.apellido_paterno, ' ', e.apellido_materno) as nombre,";
		$query .= "e.folio, c.folio as folioConvocatoria";
		$query .= " from elementos as e, convocatorias as c";
		$query .= " where e.idEstatus = 1";
		$query .= " and e.idConvocatoria = c.id";
		$query .= " and (e.nombre like '%$search%'";
                if(\Yii::$app->user->identity->id_rol == 4) {
                   $query .= " and e.idTipo = 2"; 
                }
		$query .= " or e.folio like '%$search%'";
		$query .= " or c.folio like '%$search%')"; 
		$arrElementos = $this->findBySql($query)->all();
		return $arrElementos;
	}

	public function searchActivos($search = '') {
		$strQuery = "SELECT * FROM elementos WHERE ";
                if(\Yii::$app->user->identity->id_rol == 4) {
                   $strQuery .= "idTipo = 2 AND "; 
                }
		$strQuery .= "idEstatus = " . self::ELEMENTOS_ACTIVOS . " AND (";
		$strQuery .= "nombre LIKE '%$search%' OR ";
		$strQuery .= "apellido_paterno LIKE '%$search%' OR ";
		$strQuery .= "apellido_materno LIKE '%$search%' OR ";
		$strQuery .= "clave_empleado LIKE '%$search%')";

		$arrElementos = $this->findBySql($strQuery)->all();
		return $arrElementos;
	}

	public static function findLogCursos($idElemento) {
		return Elementos::findOne($idElemento)->getLogCursos()->all();
	}

	public function getNombreText() {
		return $this->nombre . ' ' . $this->apellido_paterno . ' ' . $this->apellido_materno;
	}

	/**
	 * Método para obtener el ultimo nombramiento (por fecha) asignado a un elemento
	 *
	 * @return Object
	 */
	public function getNombramientoOrderByDate()
	{
		$lastLog = $this->getLogNombramientos()->orderBy(['fecha' => SORT_DESC])->limit(1)->one();
		return $lastLog ? $lastLog->nombramientos : new Nombramientos();
	}

	public function getNombramiento() {
		$lastLog = $this->getLogNombramientos()->orderBy(['fecha' => SORT_DESC])->limit(1)->one();
		return $lastLog ? $lastLog->nombramientos : new Nombramientos();
	}

	/**
	 * Realción para obtener los requerimientos en incumplimiento
	 */

	public function getLogRequerimientosNo() {
		return $this->hasMany(\app\models\LogRequerimientos::className(), ['idElemento' => 'id'])
					->where ('cumplimiento = 2');
	}

	/**
	 * Realción para obtener las evaluaciones en incumplimiento
	 */

	public function getLogSeleccionNo() {
		return $this->hasMany(\app\models\LogSeleccion::className(), ['idElemento' => 'id'])
					->where ('resultados = 3');
	}

	/**
	 * Realción para obtener los motivos de rechazo o desistimiento en SI
	 */

	public function getLogRechazosSi() {
		return $this->hasMany(\app\models\LogRechazos::className(), ['idElemento' => 'id'])
					->where ('rechazado = 1');
	}

	/**
	 *Función para insertar los nuevos registros a los logs para un elemento determinado
	 */
	public function nuevosLogs($id) {
    	$db = Yii::$app->getDb();
    	$db->createCommand("CALL inserta_logs(:id)")
    	   ->bindValue(':id', $id)
    	   ->execute();
    }

    /**
	 *Función para obtener el promedio obtenido en la formación inicial
	 */
	public function promedioFormacionInicial() {
    	$db = Yii::$app->getDb();
    	return $db->createCommand("CALL calcula_promedio_aspirantes(:id)")
    			  ->bindValue(':id', $this->id)
    			  ->queryScalar();
    }

    /**
	 *Función para obtener el numero de cursos reprobados
	 */
	public function reprobadoFormacionInicial() {
    	$db = Yii::$app->getDb();
    	return $db->createCommand("CALL materias_reprobadas_formacion_inicial(:id)")
    			  ->bindValue(':id', $this->id)
    			  ->queryScalar();
    }

	/**
	 * Método para obtener la ultima adscripcion (por fecha) asignado a un elemento
	 *
	 * @return Object
	 */
	public function getAdscripcionOrderByDate()
	{
		$lastLog = $this->getLogAdscripciones()->orderBy(['fecha' => SORT_DESC])->limit(1)->one();
		return $lastLog ? $lastLog->adscripcion : new Adscripciones();
	}

	public function getAdscripcion() {
		$lastLog = $this->getLogAdscripciones()->orderBy(['id' => SORT_DESC])->limit(1)->one();
		return $lastLog ? $lastLog->adscripcion : new Adscripciones();
	}

	public function getFilteredEvaluaciones($tipoEvaluacion) {
		$filteredLog = array_filter($this->logEvaluaciones, function($log) use ($tipoEvaluacion) {
			return $log->evaluacion->idTipo == $tipoEvaluacion;
		});
		return $filteredLog;
	}

	/**
     * Función para obtener unicamente los cursos que el elemento obtuvo como activo
     */
    public function getLogCursosActivo()
    {
        return $this->hasMany(\app\models\LogCursos::className(), ['idElemento' => 'id']);
    }
}
