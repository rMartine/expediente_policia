<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_seleccion".
 */
class LogSeleccion extends \app\models\base\LogSeleccion
{
	public function getFechaText() {
		return date('d/m/Y', strtotime($this->fecha));
	}
}
