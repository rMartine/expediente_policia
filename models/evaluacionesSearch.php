<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Evaluaciones;

/**
 * evaluacionesSearch represents the model behind the search form about evaluaciones.
 */
class evaluacionesSearch extends Model
{
	public $id;
	public $idTipo;
	public $nombre;
	public $duracion;
	public $importancia;

	public function rules()
	{
		return [
			[['id', 'idTipo', 'duracion', 'importancia'], 'integer'],
			[['nombre'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'idTipo' => 'Id Tipo',
			'nombre' => 'Nombre',
			'duracion' => 'Duracion',
			'importancia' => 'Importancia',
		];
	}

	public function search($params)
	{
		$query = evaluaciones::find()->innerJoinWith('tiposEvaluacion');
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'id' => $this->id,
            'idTipo' => $this->idTipo,
            'duracion' => $this->duracion,
            'importancia' => $this->importancia,
        ]);

		$query->andFilterWhere(['like', 'nombre', $this->nombre]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
