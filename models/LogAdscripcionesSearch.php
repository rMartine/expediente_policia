<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogAdscripciones;

/**
 * LogAdscripcionesSearch represents the model behind the search form about LogAdscripciones.
 */
class LogAdscripcionesSearch extends Model
{
	public $id;
	public $idElemento;
	public $idAdscripcion;
	public $fecha;
	public $oficio_comision;
	public $detalle_comision;
	public $observaciones;

	public function rules()
	{
		return [
			[['id', 'idElemento', 'idAdscripcion'], 'integer'],
			[['fecha', 'oficio_comision', 'detalle_comision', 'observaciones'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'idElemento' => 'Id Elemento',
			'idAdscripcion' => 'Id Adscripcion',
			'fecha' => 'Fecha',
			'oficio_comision' => 'Oficio Comision',
			'detalle_comision' => 'Detalle Comision',
			'observaciones' => 'Observaciones',
		];
	}

	public function search($params)
	{
		$query = LogAdscripciones::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'id' => $this->id,
            'idElemento' => $this->idElemento,
            'idAdscripcion' => $this->idAdscripcion,
            'fecha' => $this->fecha,
        ]);

		$query->andFilterWhere(['like', 'oficio_comision', $this->oficio_comision])
            ->andFilterWhere(['like', 'detalle_comision', $this->detalle_comision])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
