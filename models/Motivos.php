<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "motivos".
 */
class Motivos extends \app\models\base\Motivos
{

	const MOTIVO_ORDINARIA      = 1;
	const MOTIVO_EXTRAORDINARIA = 0;
	const MOTIVO_EXTRAORDINARIO_OTRO = 'Otro';

}
