<?php

namespace app\models;
use app\models\Adscripcion;

use Yii;

/**
 * This is the model class for table "log_adscripciones".
 */
class LogAdscripciones extends \app\models\base\LogAdscripciones
{
	public function getAdscripcion() {
		return $this->hasOne(Adscripciones::className(), ['id' => 'idAdscripcion']);
	}
}
