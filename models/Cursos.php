<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cursos".
 */
class Cursos extends \app\models\base\Cursos
{
	public function getDuracionText() {
		return $this->duracion . ' horas';
	}

	public function getObligatorioText() {
		return $this->obligatorio ? 'obligatorio' : 'opcional';
	}
}
