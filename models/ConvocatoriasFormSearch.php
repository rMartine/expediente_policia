<?php

namespace app\models;

use Yii;
use yii\base\model;

class ConvocatoriasFormSearch extends model
{
	
	public $search_element;

	public function rules()
	{
		return [
			['search_element', 'required']
		];
	}

	public function attributeLabels()
	{
		return [
			'search_element' => ''
		];
	}

}