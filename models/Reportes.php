<?php

namespace app\models;

use Yii;
use yii\db\Query;
use app\models\Roles;

/**
 * This is the model class for table "reportes".
 */
class Reportes extends \app\models\base\Reportes
{

    const REPORTES_ACTIVOS  = 1;
    const ROL_ADMINISTRADOR = 1;
    const ROL_OFICIAL       = 2;
    const ROL_CIVIL         = 3;
    const ROL_VIALIDAD      = 4;

    public $descripcion_role;

    /**
     * Metodo para obterner los reportes activos para administrador
     */
    public function getReportesParaAdministrador()
    {
        $arrReportes = $this->find()->where(['estatus' => self::REPORTES_ACTIVOS])->all();

        foreach ($arrReportes as $oReporte) {
            $oReporte->descripcion_role = $this->_getRolName($oReporte->rol);
        }

        return $arrReportes;
    }

    /**
     * Metodo para obterner los reportes activos para oficial
     */
    public function getReportesParaOficial()
    {
        $arrReportes = $this->find()->where(['estatus' => self::REPORTES_ACTIVOS])->andWhere(['rol' => self::ROL_OFICIAL])->orWhere(['rol' => self::ROL_CIVIL])->orWhere(['rol' => self::ROL_VIALIDAD])->all();

        foreach ($arrReportes as $oReporte) {
            $oReporte->descripcion_role = $this->_getRolName($oReporte->rol);
        }

        return $arrReportes;
    }

    /**
     * Metodo para obterner los reportes activos para civil
     */
    public function getReportesParaCivil()
    {
        $arrReportes = $this->find()->where(['estatus' => self::REPORTES_ACTIVOS])->andWhere(['rol' => self::ROL_CIVIL])->orWhere(['rol' => self::ROL_VIALIDAD])->all();

        foreach ($arrReportes as $oReporte) {
            $oReporte->descripcion_role = $this->_getRolName($oReporte->rol);
        }

        return $arrReportes;
    }

    /**
     * Metodo para obterner los reportes activos para vialidad
     */
    public function getReportesParaVialidad()
    {
        $arrReportes = $this->find()->where(['estatus' => self::REPORTES_ACTIVOS])->andWhere(['rol' => self::ROL_VIALIDAD])->all();

        foreach ($arrReportes as $oReporte) {
            $oReporte->descripcion_role = $this->_getRolName($oReporte->rol);
        }

        return $arrReportes;
    }

    private function _getRolName ( $numIdRol )
    {
        $oRoles = new Roles();
        $arrRol = $oRoles->find()->where(['id' => $numIdRol])->all();

        return $arrRol[0]->nombre;
    }

}