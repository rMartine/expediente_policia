<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "etapas".
 */
class Etapas extends \app\models\base\Etapas
{
	public function findEtapas() {
		$query = "select id, nombre";
		$query .= " from etapas";
		$query .= " order by orden";
		$arrElementos = $this->findBySql($query)->all();
		return $arrElementos;
	}
}