<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paquete_cursos_lista".
 *
 * @property integer $id
 * @property integer $idPaquete
 * @property integer $idCurso
 *
 * @property Cursos $idCurso0
 * @property PaquetesCursos $idPaquete0
 */
class paquetesCursosLista extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paquete_cursos_lista';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idPaquete', 'idCurso'], 'required'],
            [['idPaquete', 'idCurso'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idPaquete' => 'Id Paquete',
            'idCurso' => 'Id Curso',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCurso0()
    {
        return $this->hasOne(Cursos::className(), ['id' => 'idCurso']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPaquete0()
    {
        return $this->hasOne(PaquetesCursos::className(), ['id' => 'idPaquete']);
    }
}
