<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogSeleccion;

/**
 * LogSeleccionSearch represents the model behind the search form about LogSeleccion.
 */
class LogSeleccionSearch extends Model
{
	public $id;
	public $idElemento;
	public $idSeleccion;
	public $observaciones;
	public $resultados;
	public $fecha;

	public function rules()
	{
		return [
			[['id', 'idElemento', 'idSeleccion', 'resultados'], 'integer'],
			[['observaciones', 'fecha'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'idElemento' => 'Id Elemento',
			'idSeleccion' => 'Id Seleccion',
			'observaciones' => 'Observaciones',
			'resultados' => 'Resultados',
			'fecha' => 'Fecha',
		];
	}

	public function search($params)
	{
		$query = LogSeleccion::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'id' => $this->id,
            'idElemento' => $this->idElemento,
            'idSeleccion' => $this->idSeleccion,
            'resultados' => $this->resultados,
            'fecha' => $this->fecha,
        ]);

		$query->andFilterWhere(['like', 'observaciones', $this->observaciones]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
