<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogRequerimientos;

/**
 * LogRequerimientosSearch represents the model behind the search form about LogRequerimientos.
 */
class LogRequerimientosSearch extends Model
{
	public $id;
	public $fecha;
	public $observaciones;
	public $idRequerimiento;
	public $idElemento;
	public $cumplimiento;

	public function rules()
	{
		return [
			[['id', 'idRequerimiento', 'idElemento', 'cumplimiento'], 'integer'],
			[['fecha', 'observaciones'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'fecha' => 'Fecha',
			'observaciones' => 'Observaciones',
			'idRequerimiento' => 'Id Requerimiento',
			'idElemento' => 'Id Elemento',
			'cumplimiento' => 'Cumplimiento',
		];
	}

	public function search($params)
	{
		$query = LogRequerimientos::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'id' => $this->id,
            'fecha' => $this->fecha,
            'idRequerimiento' => $this->idRequerimiento,
            'idElemento' => $this->idElemento,
            'cumplimiento' => $this->cumplimiento,
        ]);

		$query->andFilterWhere(['like', 'observaciones', $this->observaciones]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
