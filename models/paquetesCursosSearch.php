<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PaquetesCursos;

/**
 * paquetesCursosSearch represents the model behind the search form about paquetesCursos.
 */
class paquetesCursosSearch extends Model
{
	public $id;
	public $nombre;
	public $idEtapa;
	public $descripcion;

	public function rules()
	{
		return [
			[['id', 'idEtapa'], 'integer'],
			[['nombre', 'descripcion'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'nombre' => 'Nombre',
			'idEtapa' => 'Id Etapa',
			'descripcion' => 'Descripcion',
		];
	}

	public function search($params)
	{
                $model = new paquetesCursos;
		$query = paquetesCursos::find()->innerJoinWith('etapas');
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'id' => $this->id,
            'idEtapa' => $this->idEtapa,
        ]);

		$query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
