<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "convocatorias".
 */
class Convocatorias extends \app\models\base\Convocatorias
{
	public $requerimientos = [];

	public function searchConvocatorias($search = '') {
		return $this->find()
			->where(['like', 'folio', $search])
			->orWhere(['like', 'apertura', $search])
			->all();
	}

	public function getRequerimientosConvocatoria() {
		return $this->hasMany(RequerimientosConvocatoria::className(), ['idConvocatoria' => 'id']);
	}

	public function getRequerimientos() {
		$requerimientosConvocatorias = $this->requerimientosConvocatoria;
		$ids = [];

		foreach ($requerimientosConvocatorias as $rc) {
			$key = $rc->idRequerimiento;
			$ids[$key] = $rc->requerimiento->nombre;
		}

		return $ids;
	}
}
