<?php

namespace app\models;


use Yii;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogNombramientos;

/**
 * LogNombramientosSearch represents the model behind the search form about LogNombramientos.
 */
class LogNombramientosSearch extends Model
{
	public $id;
	public $idElemento;
	public $idTipo;
	public $idAdscripcion;
	public $fecha;
	public $comision;
	public $formacion_inicial;
	public $oficio_nombramiento;
	public $observaciones;

	public function rules()
	{
		return [
			[['id', 'idElemento', 'idTipo', 'idAdscripcion'], 'integer'],
			[['fecha', 'comision', 'oficio_nombramiento', 'observaciones'], 'safe'],
			[['formacion_inicial'], 'number'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'idElemento' => 'Id Elemento',
			'idTipo' => 'Id Tipo',
			'idAdscripcion' => 'Id Adscripcion',
			'fecha' => 'Fecha',
			'comision' => 'Comision',
			'formacion_inicial' => 'Formacion Inicial',
			'oficio_nombramiento' => 'Oficio Nombramiento',
			'observaciones' => 'Observaciones',
		];
	}

	public function search($params)
	{
		$query = LogNombramientos::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'id' => $this->id,
            'idElemento' => $this->idElemento,
            'idTipo' => $this->idTipo,
            'idAdscripcion' => $this->idAdscripcion,
            'fecha' => $this->fecha,
            'formacion_inicial' => $this->formacion_inicial,
        ]);

		$query->andFilterWhere(['like', 'comision', $this->comision])
            ->andFilterWhere(['like', 'oficio_nombramiento', $this->oficio_nombramiento])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
