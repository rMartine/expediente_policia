<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipos_elementos".
 *
 * @property integer $id
 * @property string $nombre
 */
class TiposElementos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipos_elementos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }
    
    public static function find(){
        $arrConditions = [];
        if(\Yii::$app->user->identity->id_rol == 4) {                    
            $arrConditions['id'] = 2; 
        }
        return parent::find()->where($arrConditions);
    }
}
