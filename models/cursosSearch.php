<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cursos;

/**
 * cursosSearch represents the model behind the search form about cursos.
 */
class cursosSearch extends Model
{
	public $id;
	public $numerico;
	public $idEtapa;
	public $nombre;
	public $duracion;
	public $importancia;
	public $search_element;

	public function rules()
	{
		return [
			[['id', 'numerico', 'idEtapa', 'duracion', 'importancia'], 'integer'],
			[['nombre'], 'safe'],
			['search_element', 'match', 'pattern' => '/^[0-9a-záéíóúñ]+$/i', 'message' => 'Sólo se aceptan letras y números'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'numerico' => 'Numerico',
			'idEtapa' => 'Id Etapa',
			'nombre' => 'Nombre',
			'duracion' => 'Duracion',
			'importancia' => 'Importancia',
			'search_element' => '',
		];
	}

	public function search($params)
	{
		$query = cursos::find()->innerJoinWith('etapas')->innerJoinWith('categoriasCursos');
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'id' => $this->id,
            'numerico' => $this->numerico,
            'idEtapa' => $this->idEtapa,
            'duracion' => $this->duracion,
            'importancia' => $this->importancia,
        ]);

		$query->andFilterWhere(['like', 'nombre', $this->nombre]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
