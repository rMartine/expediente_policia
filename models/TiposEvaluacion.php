<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipos_evaluacion".
 */
class TiposEvaluacion extends \app\models\base\TiposEvaluacion
{
	const DESEMPENO = 1;
	const DESTREZAS_HABILIDADES = 2;
	const CONTROL_CONFIANZA = 3;
}
