<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_nombramientos".
 */
class LogNombramientos extends \app\models\base\LogNombramientos
{
	public function getNombramientos() {
		return $this->hasOne(Nombramientos::className(), ['id' => 'idTipo']);
	}
        
        public function getAdscripciones() {
                return $this->hasOne(Adscripciones::className(), ['id' => 'idAdscripcion']);
        }
}
