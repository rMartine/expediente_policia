<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "evaluaciones".
 */
class Evaluaciones extends \app\models\base\Evaluaciones
{
	public function getDuracionText() {
		return $this->duracion . ' dias';
	}

	public function getImportanciaText() {
		return $this->importancia ? 'obligatorio' : 'opcional';
	}
}
