<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogSanciones;

/**
 * LogSancionesSearch represents the model behind the search form about LogSanciones.
 */
class LogSancionesSearch extends Model
{
	public $id;
	public $idElemento;
	public $idSancion;
	public $otra_sancion;
	public $causa;
	public $fecha;
	public $fecha_inicio;
	public $fecha_fin;
	public $observaciones;

	public function rules()
	{
		return [
			[['id', 'idElemento', 'idSancion'], 'integer'],
			[['otra_sancion', 'causa', 'fecha', 'fecha_inicio', 'fecha_fin', 'observaciones'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'idElemento' => 'Id Elemento',
			'idSancion' => 'Id Sancion',
			'otra_sancion' => 'Otra Sancion',
			'causa' => 'Causa',
			'fecha' => 'Fecha',
			'fecha_inicio' => 'Fecha Inicio',
			'fecha_fin' => 'Fecha Fin',
			'observaciones' => 'Observaciones',
		];
	}

	public function search($params)
	{
		$query = LogSanciones::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'id' => $this->id,
            'idElemento' => $this->idElemento,
            'idSancion' => $this->idSancion,
            'otra_sancion' => $this->otra_sancion,
            'causa' => $this->causa,
            'fecha' => $this->fecha,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_fin' => $this->fecha_fin
        ]);

		$query->andFilterWhere(['like', 'otra_sancion', $this->otra_sancion]);
		$query->andFilterWhere(['like', 'causa', $this->causa]);
		$query->andFilterWhere(['like', 'observaciones', $this->observaciones]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
