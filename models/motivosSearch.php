<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Motivos;

/**
 * motivosSearch represents the model behind the search form about motivos.
 */
class motivosSearch extends Model
{
	public $id;
	public $motivo;
	public $descripcion;
	public $ordinaria;

	public function rules()
	{
		return [
			[['id', 'ordinaria'], 'integer'],
			[['motivo', 'descripcion'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'motivo' => 'Motivo',
			'descripcion' => 'Descripcion',
			'ordinaria' => 'Ordinaria',
		];
	}

	public function search($params)
	{
		$query = motivos::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'id' => $this->id,
            'ordinaria' => $this->ordinaria,
        ]);

		$query->andFilterWhere(['like', 'motivo', $this->motivo])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
