<?php

namespace app\models;

use Yii;
use yii\base\model;

class BajasFormSearch extends model
{
	
	public $search_element;

	public function rules()
	{
		return [
			['search_element', 'match', 'pattern' => '/^[0-9a-záéíóúñ ]+$/i', 'message' => 'Sólo se aceptan letras y números'],
			['search_element', 'required', 'message' => 'Favor de ingresar la busqueda']
		];
	}

	public function attributeLabels()
	{
		return [
			'search_element' => ''
		];
	}

}