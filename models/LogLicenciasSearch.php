<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogLicencias;

/**
 * LogLicenciasSearch represents the model behind the search form about LogLicencias.
 */
class LogLicenciasSearch extends Model
{
	public $id;
	public $idElemento;
	public $idLicencia;
	public $fecha_insercion;
	public $fecha_inicio;
	public $fecha_duracion;
	public $observaciones;

	public function rules()
	{
		return [
			[['id', 'idElemento', 'idLicencia'], 'integer'],
			[['fecha_insercion', 'fecha_inicio', 'fecha_duracion', 'observaciones'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'idElemento' => 'Id Elemento',
			'idLicencia' => 'Id Licencia',
			'fecha_insercion' => 'Fecha Insercion',
			'fecha_inicio' => 'Fecha Inicio',
			'fecha_duracion' => 'Fecha Duracion',
			'observaciones' => 'Observaciones',
		];
	}

	public function search($params)
	{
		$query = LogLicencias::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'id' => $this->id,
            'idElemento' => $this->idElemento,
            'idLicencia' => $this->idLicencia,
            'fecha_insercion' => $this->fecha_insercion,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_duracion' => $this->fecha_duracion,
        ]);

		$query->andFilterWhere(['like', 'observaciones', $this->observaciones]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
