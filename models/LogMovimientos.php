<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_movimientos".
 *
 * @property integer $id
 * @property integer $id_usuario
 * @property string $tabla
 * @property integer $id_campo
 * @property string $tipo_movimiento
 * @property string $fecha
 */
class LogMovimientos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_movimientos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_usuario', 'tabla', 'id_campo', 'tipo_movimiento'], 'required'],
            [['id_usuario', 'id_campo'], 'integer'],
            [['fecha'], 'safe'],
            [['tabla'], 'string', 'max' => 30],
            [['tipo_movimiento'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'tabla' => 'Tabla',
            'id_campo' => 'Id Campo',
            'tipo_movimiento' => 'Tipo Movimiento',
            'fecha' => 'Fecha',
        ];
    }
}
