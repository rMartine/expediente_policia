<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogCursos;

/**
 * LogCursosSearch represents the model behind the search form about LogCursos.
 */
class LogCursosSearch extends Model
{
	public $id;
	public $idElemento;
	public $idCurso;
	public $calificacion;
	public $cumplimiento;
	public $fecha;
	public $observaciones;

	public function rules()
	{
		return [
			[['id', 'idElemento', 'idCurso', 'cumplimiento'], 'integer'],
			[['calificacion'], 'number'],
			[['fecha', 'observaciones'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'idElemento' => 'Id Elemento',
			'idCurso' => 'Id Curso',
			'calificacion' => 'Calificacion',
			'cumplimiento' => 'Cumplimiento',
			'fecha' => 'Fecha',
			'observaciones' => 'Observaciones',
		];
	}

	public function search($params)
	{
		$query = LogCursos::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'id' => $this->id,
            'idElemento' => $this->idElemento,
            'idCurso' => $this->idCurso,
            'calificacion' => $this->calificacion,
            'cumplimiento' => $this->cumplimiento,
            'fecha' => $this->fecha,
        ]);

		$query->andFilterWhere(['like', 'observaciones', $this->observaciones]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
