<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "log_requerimientos".
 */
class LogRequerimientos extends \app\models\base\LogRequerimientos {
	public function setNuevosRequerimientos($idElemento) {
		/*$strQuery = "CALL inserta_requerimientos(:idElemento)";
		$arrElementos = $this->findBySql($strQuery)->all();*/
    	$db = Yii::$app->getDb();
    	$db->createCommand("CALL inserta_requerimientos(:idElemento)")
    	   ->bindValue(':idElemento', $idElemento)
    	   ->execute();
	}

	public function getRequerimiento() {
		return $this->hasOne(Requerimientos::className(), ['id' => 'idRequerimiento']);
	}

	public function getFechaText() {
		return date('d/m/Y', strtotime($this->fecha));
	}
}
