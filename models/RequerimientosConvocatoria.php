<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "requerimientos_convocatoria".
 */
class RequerimientosConvocatoria extends \app\models\base\RequerimientosConvocatoria
{
	public $requerimientos = [];
	
	public function getRequerimiento() {
		return $this->hasOne(Requerimientos::className(), ['id' => 'idRequerimiento']);
	}
}
