<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Convocatorias;

/**
 * convocatoriasSearch represents the model behind the search form about convocatorias.
 */
class convocatoriasSearch extends Model
{
	public $id;
	public $folio;
	public $apertura;
	public $cierre;
	public $fallo;
	public $contenido;

	public function rules()
	{
		return [
			[['id'], 'integer'],
			[['folio', 'apertura', 'cierre', 'fallo', 'contenido'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'folio' => 'Folio',
			'apertura' => 'Apertura',
			'cierre' => 'Cierre',
			'fallo' => 'Fallo',
			'contenido' => 'Contenido',
		];
	}

	public function search($params)
	{
		$query = convocatorias::find()->innerJoinWith('convocatorias');
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'id' => $this->id,
            'apertura' => $this->apertura,
            'cierre' => $this->cierre,
            'fallo' => $this->fallo,
        ]);

		$query->andFilterWhere(['like', 'folio', $this->folio])
            ->andFilterWhere(['like', 'contenido', $this->contenido]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
