<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Requerimientos;

/**
 * RequerimientosSearch represents the model behind the search form about Requerimientos.
 */
class RequerimientosSearch extends Model
{
	public $id;
	public $nombre;
	public $descripcion;
	public $idConvocatoria;

	public function rules()
	{
		return [
			[['id', 'idConvocatoria'], 'integer'],
			[['nombre', 'descripcion'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'nombre' => 'Nombre',
			'descripcion' => 'Descripcion',
			'idConvocatoria' => 'Id Convocatoria',
		];
	}

	public function search($params)
	{
		$query = Requerimientos::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'id' => $this->id,
            'idConvocatoria' => $this->idConvocatoria,
        ]);

		$query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
