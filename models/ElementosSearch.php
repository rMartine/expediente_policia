<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Elementos;

/**
 * ElementosSearch represents the model behind the search form about Elementos.
 */
class ElementosSearch extends Model
{
	public $id;
	public $folio;
	public $clave_empleado;
	public $nombre;
	public $apellido_paterno;
	public $apellido_materno;
	public $fecha_nac;
	public $municipio_nac;
	public $estado_nac;
	public $rfc;
	public $curp;
	public $edad;
	public $sexo;
	public $telefono;
	public $movil;
	public $domicilio_mpo;
	public $domicilio_edo;
	public $estado_civil;
	public $email;
	public $fotografia;
	public $pulgar_der;
	public $indice_der;
	public $medio_der;
	public $anular_der;
	public $menique_der;
	public $pulgar_izq;
	public $indice_izq;
	public $medio_izq;
	public $anular_izq;
	public $menique_izq;
	public $idEstatus;
	public $idEstudios;
	public $idEtapa;
	public $idConvocatoria;

	public function rules()
	{
		return [
			[['id', 'edad', 'idEstatus', 'idEstudios', 'idEtapa', 'idConvocatoria'], 'integer'],
			[['folio', 'clave_empleado', 'nombre', 'apellido_paterno', 'apellido_materno', 'fecha_nac', 'municipio_nac', 'estado_nac', 'rfc', 'curp', 'sexo', 'telefono', 'movil', 'domicilio_mpo', 'domicilio_edo', 'estado_civil', 'email', 'fotografia', 'pulgar_der', 'indice_der', 'medio_der', 'anular_der', 'menique_der', 'pulgar_izq', 'indice_izq', 'medio_izq', 'anular_izq', 'menique_izq'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'folio' => 'Folio',
			'clave_empleado' => 'Clave Empleado',
			'nombre' => 'Nombre',
			'apellido_paterno' => 'Apellido Paterno',
			'apellido_materno' => 'Apellido Materno',
			'fecha_nac' => 'Fecha Nac',
			'municipio_nac' => 'Municipio Nac',
			'estado_nac' => 'Estado Nac',
			'rfc' => 'Rfc',
			'curp' => 'Curp',
			'edad' => 'Edad',
			'sexo' => 'Sexo',
			'telefono' => 'Telefono',
			'movil' => 'Movil',
			'domicilio_mpo' => 'Domicilio Mpo',
			'domicilio_edo' => 'Domicilio Edo',
			'estado_civil' => 'Estado Civil',
			'email' => 'Email',
			'fotografia' => 'Fotografia',
			'pulgar_der' => 'Pulgar Der',
			'indice_der' => 'Indice Der',
			'medio_der' => 'Medio Der',
			'anular_der' => 'Anular Der',
			'menique_der' => 'Menique Der',
			'pulgar_izq' => 'Pulgar Izq',
			'indice_izq' => 'Indice Izq',
			'medio_izq' => 'Medio Izq',
			'anular_izq' => 'Anular Izq',
			'menique_izq' => 'Menique Izq',
			'idEstatus' => 'Id Estatus',
			'idEstudios' => 'Id Estudios',
			'idEtapa' => 'Id Etapa',
			'idConvocatoria' => 'Id Convocatoria',
		];
	}

	public function search($params)
	{
		$query = Elementos::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'id' => $this->id,
            'fecha_nac' => $this->fecha_nac,
            'edad' => $this->edad,
            'idEstatus' => $this->idEstatus,
            'idEstudios' => $this->idEstudios,
            'idEtapa' => $this->idEtapa,
            'idConvocatoria' => $this->idConvocatoria,
        ]);

		$query->andFilterWhere(['like', 'folio', $this->folio])
            ->andFilterWhere(['like', 'clave_empleado', $this->clave_empleado])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'apellido_paterno', $this->apellido_paterno])
            ->andFilterWhere(['like', 'apellido_materno', $this->apellido_materno])
            ->andFilterWhere(['like', 'municipio_nac', $this->municipio_nac])
            ->andFilterWhere(['like', 'estado_nac', $this->estado_nac])
            ->andFilterWhere(['like', 'rfc', $this->rfc])
            ->andFilterWhere(['like', 'curp', $this->curp])
            ->andFilterWhere(['like', 'sexo', $this->sexo])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'movil', $this->movil])
            ->andFilterWhere(['like', 'domicilio_mpo', $this->domicilio_mpo])
            ->andFilterWhere(['like', 'domicilio_edo', $this->domicilio_edo])
            ->andFilterWhere(['like', 'estado_civil', $this->estado_civil])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'fotografia', $this->fotografia])
            ->andFilterWhere(['like', 'pulgar_der', $this->pulgar_der])
            ->andFilterWhere(['like', 'indice_der', $this->indice_der])
            ->andFilterWhere(['like', 'medio_der', $this->medio_der])
            ->andFilterWhere(['like', 'anular_der', $this->anular_der])
            ->andFilterWhere(['like', 'menique_der', $this->menique_der])
            ->andFilterWhere(['like', 'pulgar_izq', $this->pulgar_izq])
            ->andFilterWhere(['like', 'indice_izq', $this->indice_izq])
            ->andFilterWhere(['like', 'medio_izq', $this->medio_izq])
            ->andFilterWhere(['like', 'anular_izq', $this->anular_izq])
            ->andFilterWhere(['like', 'menique_izq', $this->menique_izq]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
