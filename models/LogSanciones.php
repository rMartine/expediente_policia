<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_sanciones".
 */
class LogSanciones extends \app\models\base\LogSanciones
{

	public function getSancion() {
		return $this->hasOne(Sanciones::className(), ['id' => 'idSancion']);
	}
}
