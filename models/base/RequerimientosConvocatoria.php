<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "requerimientos_convocatoria".
 *
 * @property integer $id
 * @property integer $idRequerimiento
 * @property integer $idConvocatoria
 *
 * @property Requerimientos $idRequerimiento0
 * @property Convocatorias $idConvocatoria0
 */
class RequerimientosConvocatoria extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requerimientos_convocatoria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idRequerimiento', 'idConvocatoria'], 'required'],
            [['idRequerimiento', 'idConvocatoria'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idRequerimiento' => 'Id Requerimiento',
            'idConvocatoria' => 'Id Convocatoria',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRequerimiento0()
    {
        return $this->hasOne(\app\models\Requerimientos::className(), ['id' => 'idRequerimiento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdConvocatoria0()
    {
        return $this->hasOne(\app\models\Convocatorias::className(), ['id' => 'idConvocatoria']);
    }
}
