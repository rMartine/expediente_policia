<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "paquete_cursos_lista".
 *
 * @property integer $id
 * @property integer $idPaquete
 * @property integer $idCurso
 *
 * @property Cursos $idCurso0
 * @property PaquetesCursos $idPaquete0
 */
class PaqueteCursosLista extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paquete_cursos_lista';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idPaquete', 'idCurso'], 'required'],
            [['idPaquete', 'idCurso'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idPaquete' => 'Id Paquete',
            'idCurso' => 'Id Curso',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCurso0()
    {
        return $this->hasOne(\app\models\Cursos::className(), ['id' => 'idCurso']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPaquete0()
    {
        return $this->hasOne(\app\models\PaquetesCursos::className(), ['id' => 'idPaquete']);
    }
}
