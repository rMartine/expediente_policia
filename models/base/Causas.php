<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "causas".
 *
 * @property integer $id
 * @property string $causa_sancion
 *
 * @property LogSanciones[] $logSanciones
 */
class Causas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'causas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['causa_sancion'], 'required'],
            [['causa_sancion'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'causa_sancion' => 'Causa Sancion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogSanciones()
    {
        return $this->hasMany(\app\models\LogSanciones::className(), ['idCausa' => 'id']);
    }
}
