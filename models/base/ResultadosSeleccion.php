<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "resultados_seleccion".
 *
 * @property integer $id
 * @property string $valor
 * @property string $descripcion
 *
 * @property LogSeleccion[] $logSeleccions
 */
class ResultadosSeleccion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'resultados_seleccion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['valor', 'descripcion'], 'required'],
            [['descripcion'], 'string'],
            [['valor'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'valor' => 'Valor',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogSeleccions()
    {
        return $this->hasMany(\app\models\LogSeleccion::className(), ['resultados' => 'id']);
    }
}
