<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "estudios".
 *
 * @property integer $id
 * @property string $grado_estudios
 *
 * @property Elementos[] $elementos
 */
class Estudios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estudios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grado_estudios'], 'required'],
            [['grado_estudios'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'grado_estudios' => 'Grado Estudios',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElementos()
    {
        return $this->hasMany(\app\models\Elementos::className(), ['idEstudios' => 'id']);
    }
}
