<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "log_evaluaciones".
 *
 * @property integer $id
 * @property integer $idElemento
 * @property integer $idEvaluacion
 * @property integer $cumplimiento
 * @property string $observaciones
 * @property string $fecha
 *
 * @property Elementos $idElemento0
 * @property Evaluaciones $idEvaluacion0
 */
class LogEvaluaciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_evaluaciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idElemento', 'cumplimiento'], 'required'],
            [['idElemento', 'idEvaluacion', 'cumplimiento'], 'integer'],
            [['observaciones'], 'string'],
            [['fecha'], 'safe'],
            ['idEvaluacion', 'required', 'message' => 'Debes seleccionar una evaluacion'],
            ['fecha', 'required', 'message' => 'La fecha no puede quedar vacia'],
            ['observaciones', 'required', 'message' => 'Las observaciones no pueden quedar vacias'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idElemento' => 'Id Elemento',
            'idEvaluacion' => 'Id Evaluacion',
            'cumplimiento' => 'Cumplimiento',
            'observaciones' => 'Observaciones',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdElemento0()
    {
        return $this->hasOne(\app\models\Elementos::className(), ['id' => 'idElemento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEvaluacion0()
    {
        return $this->hasOne(\app\models\Evaluaciones::className(), ['id' => 'idEvaluacion']);
    }
}
