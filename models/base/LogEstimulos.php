<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "log_estimulos".
 *
 * @property integer $id
 * @property integer $idElemento
 * @property integer $idEstimulo
 * @property string $fecha
 * @property string $observaciones
 *
 * @property Elementos $idElemento0
 * @property Estimulos $idEstimulo0
 */
class LogEstimulos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_estimulos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idElemento'], 'required'],
            [['idElemento', 'idEstimulo'], 'integer'],
            [['fecha'], 'safe'],
            [['observaciones'], 'string'],
            ['idEstimulo', 'required', 'message' => 'Debes seleccionar un estimulo, reconocimiento o recompensa'],
            ['fecha', 'required', 'message' => 'La fecha no puede quedar vacia'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idElemento' => 'Id Elemento',
            'idEstimulo' => 'Id Estimulo',
            'fecha' => 'Fecha',
            'observaciones' => 'Observaciones',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdElemento0()
    {
        return $this->hasOne(\app\models\Elementos::className(), ['id' => 'idElemento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstimulo0()
    {
        return $this->hasOne(\app\models\Estimulos::className(), ['id' => 'idEstimulo']);
    }
}
