<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "log_licencias".
 *
 * @property integer $id
 * @property integer $idElemento
 * @property integer $idLicencia
 * @property string $motivo
 * @property string $fecha_insercion
 * @property string $fecha_inicio
 * @property string $fecha_fin
 * @property string $observaciones
 *
 * @property Elementos $idElemento0
 * @property Licencias $idLicencia0
 */
class LogLicencias extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_licencias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idElemento', 'idLicencia', 'motivo', 'fecha_insercion', 'fecha_inicio', 'fecha_fin'], 'required'],
            [['idElemento', 'idLicencia'], 'integer'],
            [['motivo', 'observaciones'], 'string'],
            [['fecha_insercion', 'fecha_inicio', 'fecha_fin'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idElemento' => 'Id Elemento',
            'idLicencia' => 'Id Licencia',
            'motivo' => 'Motivo',
            'fecha_insercion' => 'Fecha Insercion',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_fin' => 'Fecha Fin',
            'observaciones' => 'Observaciones',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdElemento0()
    {
        return $this->hasOne(\app\models\Elementos::className(), ['id' => 'idElemento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLicencia0()
    {
        return $this->hasOne(\app\models\Licencias::className(), ['id' => 'idLicencia']);
    }
}
