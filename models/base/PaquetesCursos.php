<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "paquetes_cursos".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $idEtapa
 * @property string $descripcion
 *
 * @property PaqueteCursosLista[] $paqueteCursosListas
 * @property Etapas $idEtapa0
 */
class PaquetesCursos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paquetes_cursos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'idEtapa', 'descripcion'], 'required'],
            [['idEtapa'], 'integer'],
            [['descripcion'], 'string'],
            [['nombre'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'idEtapa' => 'Id Etapa',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     *//*
    public function getPaqueteCursosListas()
    {
        return $this->hasMany(\app\models\PaqueteCursosLista::className(), ['idPaquete' => 'id']);
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEtapas()
    {      
        return $this->hasOne(\app\models\Etapas::className(), ['id' => 'idEtapa']);
    }
}
