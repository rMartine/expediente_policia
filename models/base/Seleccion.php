<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "seleccion".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property LogSeleccion[] $logSeleccions
 */
class Seleccion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seleccion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogSeleccions()
    {
        return $this->hasMany(\app\models\LogSeleccion::className(), ['idSeleccion' => 'id']);
    }
}
