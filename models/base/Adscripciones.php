<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "adscripciones".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property LogAdscripciones[] $logAdscripciones
 */
class Adscripciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adscripciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogAdscripciones()
    {
        return $this->hasMany(\app\models\LogAdscripciones::className(), ['idAdscripcion' => 'id']);
    }
}
