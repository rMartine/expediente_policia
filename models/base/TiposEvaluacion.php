<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "tipos_evaluacion".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Evaluaciones[] $evaluaciones
 */
class TiposEvaluacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipos_evaluacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvaluaciones()
    {
        return $this->hasMany(\app\models\Evaluaciones::className(), ['idTipo' => 'id']);
    }
}
