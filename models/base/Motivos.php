<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "motivos".
 *
 * @property integer $id
 * @property string $motivo
 * @property string $descripcion
 * @property integer $ordinaria
 *
 * @property LogBajas[] $logBajas
 */
class Motivos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'motivos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['motivo', 'descripcion'], 'required'],
            [['descripcion'], 'string'],
            [['ordinaria'], 'integer'],
            [['motivo'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'motivo' => 'Motivo',
            'descripcion' => 'Descripcion',
            'ordinaria' => 'Ordinaria',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogBajas()
    {
        return $this->hasMany(\app\models\LogBajas::className(), ['idMotivo' => 'id']);
    }
}
