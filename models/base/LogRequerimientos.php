<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "log_requerimientos".
 *
 * @property integer $id
 * @property string $fecha
 * @property string $observaciones
 * @property integer $idRequerimiento
 * @property integer $idElemento
 * @property integer $cumplimiento
 *
 * @property Cumplimiento $cumplimiento0
 * @property Elementos $idElemento0
 * @property Requerimientos $idRequerimiento0
 */
class LogRequerimientos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_requerimientos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha', 'idRequerimiento', 'idElemento', 'cumplimiento'], 'required'],
            [['fecha'], 'safe'],
            [['observaciones'], 'string'],
            [['idRequerimiento', 'idElemento', 'cumplimiento'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'observaciones' => 'Observaciones',
            'idRequerimiento' => 'Id Requerimiento',
            'idElemento' => 'Id Elemento',
            'cumplimiento' => 'Cumplimiento',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCumplimiento0()
    {
        return $this->hasOne(\app\models\Cumplimiento::className(), ['id' => 'cumplimiento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdElemento0()
    {
        return $this->hasOne(\app\models\Elementos::className(), ['id' => 'idElemento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRequerimiento0()
    {
        return $this->hasOne(\app\models\Requerimientos::className(), ['id' => 'idRequerimiento']);
    }
}
