<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "estatus".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Elementos[] $elementos
 */
class Estatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estatus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElementos()
    {
        return $this->hasMany(\app\models\Elementos::className(), ['idEstatus' => 'id']);
    }
}
