<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "estados".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Elementos[] $elementos
 */
class Estados extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estados';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElementos()
    {
        return $this->hasMany(\app\models\Elementos::className(), ['estado_nac' => 'id']);
    }
}
