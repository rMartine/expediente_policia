<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "convocatorias".
 *
 * @property integer $id
 * @property string $folio
 * @property string $apertura
 * @property string $cierre
 * @property string $fallo
 * @property string $contenido
 *
 * @property Elementos[] $elementos
 */
class Convocatorias extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'convocatorias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['folio', 'apertura', 'cierre', 'fallo','contenido'], 'required'],
            [['folio', 'contenido'], 'string'],
            [['folio'], 'string', 'max' => 30],
            [['apertura', 'cierre', 'fallo'], 'safe'],
            ['apertura', 
                function ($attr) {
                    if($this->$attr <= $this->cierre){
                        return true;
                    } else {
                        $this->addError('apertura', "La fecha de cierre debe ser mayor a la fecha de apertura.");
                        return false;
                    }                    
                },
            ],
            ['cierre', 
                function ($attr) {
                    if($this->$attr <= $this->fallo){
                        return true;
                    } else {
                        $this->addError('cierre', "La fecha de fallo debe ser mayor a la fecha de cierre.");
                        return false;
                    }                    
                },
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'folio' => 'Folio',
            'apertura' => 'Apertura',
            'cierre' => 'Cierre',
            'fallo' => 'Fallo',
            'contenido' => 'Contenido',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElementos()
    {
        return $this->hasMany(\app\models\Elementos::className(), ['idConvocatoria' => 'id']);
    }
}
