<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "cursos".
 *
 * @property integer $id
 * @property integer $numerico
 * @property integer $idEtapa
 * @property string $nombre
 * @property integer $duracion
 * @property integer $importancia
 *
 * @property Etapas $idEtapa0
 * @property LogCursos[] $logCursos
 * @property PaqueteCursosLista[] $paqueteCursosListas
 */
class Cursos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cursos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['numerico', 'idEtapa', 'nombre', 'duracion', 'obligatorio', 'idCategoria'], 'required'],
            [['numerico', 'idEtapa', 'duracion', 'obligatorio'], 'integer'],
            [['nombre'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numerico' => 'Numerico',
            'idEtapa' => 'Etapa',   
            'nombre' => 'Nombre',
            'duracion' => 'Duracion',
            'obligatorio' => 'Obligatorio',
            'idCategoria' => 'Categoria'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEtapas()
    {      
        return $this->hasOne(\app\models\Etapas::className(), ['id' => 'idEtapa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogCursos()
    {
        return $this->hasMany(\app\models\LogCursos::className(), ['idCurso' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaqueteCursosListas()
    {
        return $this->hasMany(\app\models\PaqueteCursosLista::className(), ['idCurso' => 'id']);
    }
    
    public function getCategoriasCursos()
    {
        return $this->hasOne(\app\models\CategoriasCursos::className(), ['id' => 'idCategoria']);
    }
}
