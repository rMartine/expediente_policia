<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "sexo".
 *
 * @property string $id
 * @property string $valor
 */
class Sexo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sexo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'valor'], 'required'],
            [['id'], 'string', 'max' => 1],
            [['valor'], 'string', 'max' => 45],
            [['valor'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'valor' => 'Valor',
        ];
    }
}
