<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "sanciones".
 *
 * @property integer $id
 * @property string $sancion
 *
 * @property LogSanciones[] $logSanciones
 */
class Sanciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sanciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sancion'], 'required'],
            [['sancion'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sancion' => 'Sancion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogSanciones()
    {
        return $this->hasMany(\app\models\LogSanciones::className(), ['idSancion' => 'id']);
    }
}
