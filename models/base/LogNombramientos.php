<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "log_nombramientos".
 *
 * @property integer $id
 * @property integer $idElemento
 * @property integer $idTipo
 * @property integer $idAdscripcion
 * @property string $fecha
 * @property string $comision
 * @property string $formacion_inicial
 * @property string $oficio_nombramiento
 * @property string $observaciones
 *
 * @property Elementos $idElemento0
 * @property Nombramientos $idTipo0
 * @property Adscripciones $idAdscripcion0
 */
class LogNombramientos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_nombramientos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idElemento', 'idTipo', 'idAdscripcion', 'fecha', 'comision', 'oficio_nombramiento'], 'required'],
            [['idElemento', 'idTipo', 'idAdscripcion'], 'integer'],
            [['fecha'], 'safe'],
            [['comision', 'observaciones'], 'string'],
            [['formacion_inicial'], 'number'],
            [['oficio_nombramiento'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idElemento' => 'Id Elemento',
            'idTipo' => 'Id Tipo',
            'idAdscripcion' => 'Id Adscripcion',
            'fecha' => 'Fecha',
            'comision' => 'Comision',
            'formacion_inicial' => 'Formacion Inicial',
            'oficio_nombramiento' => 'Oficio Nombramiento',
            'observaciones' => 'Observaciones',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdElemento0()
    {
        return $this->hasOne(\app\models\Elementos::className(), ['id' => 'idElemento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipo0()
    {
        return $this->hasOne(\app\models\Nombramientos::className(), ['id' => 'idTipo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAdscripcion0()
    {
        return $this->hasOne(\app\models\Adscripciones::className(), ['id' => 'idAdscripcion']);
    }
}
