<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "rechazo".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property LogRechazos[] $logRechazos
 */
class Rechazo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rechazo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogRechazos()
    {
        return $this->hasMany(\app\models\LogRechazos::className(), ['idRechazo' => 'id']);
    }
}
