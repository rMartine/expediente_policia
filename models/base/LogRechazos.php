<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "log_rechazos".
 *
 * @property integer $id
 * @property integer $idElemento
 * @property integer $idRechazo
 * @property string $observaciones
 * @property integer $rechazado
 * @property string $fecha
 *
 * @property Cumplimiento $rechazado0
 * @property Elementos $idElemento0
 * @property Rechazo $idRechazo0
 */
class LogRechazos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_rechazos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idElemento', 'idRechazo', 'rechazado', 'fecha'], 'required'],
            [['idElemento', 'idRechazo', 'rechazado'], 'integer'],
            [['observaciones'], 'string'],
            [['fecha'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idElemento' => 'Id Elemento',
            'idRechazo' => 'Id Rechazo',
            'observaciones' => 'Observaciones',
            'rechazado' => 'Rechazado',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRechazado0()
    {
        return $this->hasOne(\app\models\Cumplimiento::className(), ['id' => 'rechazado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdElemento0()
    {
        return $this->hasOne(\app\models\Elementos::className(), ['id' => 'idElemento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRechazo0()
    {
        return $this->hasOne(\app\models\Rechazo::className(), ['id' => 'idRechazo']);
    }
}
