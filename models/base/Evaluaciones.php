<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "evaluaciones".
 *
 * @property integer $id
 * @property integer $idTipo
 * @property string $nombre
 * @property integer $duracion
 * @property integer $importancia
 *
 * @property TiposEvaluacion $idTipo0
 * @property LogEvaluaciones[] $logEvaluaciones
 */
class Evaluaciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'evaluaciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idTipo', 'nombre', 'duracion', 'importancia'], 'required'],
            [['idTipo', 'duracion', 'importancia'], 'integer'],
            [['nombre'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idTipo' => 'Id Tipo',
            'nombre' => 'Nombre',
            'duracion' => 'Duracion',
            'importancia' => 'Importancia',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTiposEvaluacion()
    {
        return $this->hasOne(\app\models\TiposEvaluacion::className(), ['id' => 'idTipo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogEvaluaciones()
    {
        return $this->hasMany(\app\models\LogEvaluaciones::className(), ['idEvaluacion' => 'id']);
    }
}
