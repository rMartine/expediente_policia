<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "log_documentos".
 *
 * @property integer $id
 * @property string $nombreDocumento
 * @property string $fechaSubida
 * @property string $nombreArchivo
 * @property integer $idElemento
 *
 * @property Elementos $idElemento0
 */
class LogDocumentos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_documentos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombreDocumento', 'fechaSubida', 'nombreArchivo', 'idElemento'], 'required'],
            [['nombreDocumento', 'nombreArchivo'], 'string'],
            [['fechaSubida'], 'safe'],
            [['idElemento'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombreDocumento' => 'Nombre Documento',
            'fechaSubida' => 'Fecha Subida',
            'nombreArchivo' => 'Nombre Archivo',
            'idElemento' => 'Id Elemento',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdElemento0()
    {
        return $this->hasOne(Elementos::className(), ['id' => 'idElemento']);
    }
}
