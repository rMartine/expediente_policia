<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "log_bajas".
 *
 * @property integer $id
 * @property integer $idElemento
 * @property integer $idMotivo
 * @property string $otro_motivo
 * @property string $fecha
 * @property string $observaciones
 *
 * @property Elementos $idElemento0
 * @property Motivos $idMotivo0
 */
class LogBajas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_bajas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idElemento', 'idMotivo', 'fecha'], 'required'],
            [['idElemento', 'idMotivo'], 'integer'],
            [['fecha'], 'safe'],
            [['otro_motivo', 'observaciones'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idElemento' => 'Id Elemento',
            'idMotivo' => 'Id Motivo',
            'otro_motivo' => 'Otro Motivo',
            'fecha' => 'Fecha',
            'observaciones' => 'Observaciones',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdElemento0()
    {
        return $this->hasOne(\app\models\Elementos::className(), ['id' => 'idElemento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMotivo0()
    {
        return $this->hasOne(\app\models\Motivos::className(), ['id' => 'idMotivo']);
    }
}
