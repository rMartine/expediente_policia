<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "cumplimiento".
 *
 * @property integer $id
 * @property string $valor
 *
 * @property LogRechazos[] $logRechazos
 * @property LogRequerimientos[] $logRequerimientos
 */
class Cumplimiento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cumplimiento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['valor'], 'required'],
            [['valor'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'valor' => 'Valor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogRechazos()
    {
        return $this->hasMany(\app\models\LogRechazos::className(), ['rechazado' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogRequerimientos()
    {
        return $this->hasMany(\app\models\LogRequerimientos::className(), ['cumplimiento' => 'id']);
    }
}
