<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "elementos".
 *
 * @property integer $id
 * @property integer $folio
 * @property string $clave_empleado
 * @property string $nombre
 * @property string $apellido_paterno
 * @property string $apellido_materno
 * @property string $fecha_nac
 * @property string $municipio_nac
 * @property integer $estado_nac
 * @property string $rfc
 * @property string $curp
 * @property integer $edad
 * @property string $sexo
 * @property string $telefono
 * @property string $movil
 * @property integer $domicilio_next
 * @property integer $domicilio_nint
 * @property string $domicilio_calle
 * @property string $domicilio_col
 * @property integer $domicilio_cp
 * @property string $domicilio_mpo
 * @property string $domicilio_edo
 * @property string $estado_civil
 * @property string $email
 * @property string $fotografia
 * @property string $pulgar_der
 * @property string $indice_der
 * @property string $medio_der
 * @property string $anular_der
 * @property string $menique_der
 * @property string $pulgar_izq
 * @property string $indice_izq
 * @property string $medio_izq
 * @property string $anular_izq
 * @property string $menique_izq
 * @property integer $idEstatus
 * @property integer $idEstudios
 * @property integer $idEtapa
 * @property integer $idConvocatoria
 * @property string $fecha_ingreso
 * @property string $cuip
 *
 * @property Convocatorias $idConvocatoria0
 * @property Estados $estadoNac
 * @property Estatus $idEstatus0
 * @property Estudios $idEstudios0
 * @property Etapas $idEtapa0
 * @property Sexo $sexo0
 * @property LogAdscripciones[] $logAdscripciones
 * @property LogBajas[] $logBajas
 * @property LogCursos[] $logCursos
 * @property LogEstimulos[] $logEstimulos
 * @property LogEvaluaciones[] $logEvaluaciones
 * @property LogNombramientos[] $logNombramientos
 * @property LogRequerimientos[] $logRequerimientos
 * @property LogSanciones[] $logSanciones
 */
class Elementos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'elementos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            /*[['folio', 'nombre', 'apellido_paterno', 'apellido_materno', 'fecha_nac', 'municipio_nac', 'estado_nac', 'curp', 'edad', 'sexo', 'domicilio_calle', 'domicilio_col', 'domicilio_cp', 'domicilio_mpo', 'domicilio_edo', 'estado_civil', 'fotografia', 'pulgar_der', 'indice_der', 'medio_der', 'anular_der', 'menique_der', 'pulgar_izq', 'indice_izq', 'medio_izq', 'anular_izq', 'menique_izq', 'idEstatus', 'idEstudios', 'idEtapa', 'idConvocatoria', 'fecha_ingreso'], 'required'],
            [['folio', 'estado_nac', 'edad', 'domicilio_next', 'domicilio_nint', 'domicilio_cp', 'idEstatus', 'idEstudios', 'idEtapa', 'idConvocatoria'], 'integer'],
            [['fecha_nac', 'fecha_ingreso'], 'safe'],
            [['fotografia', 'pulgar_der', 'indice_der', 'medio_der', 'anular_der', 'menique_der', 'pulgar_izq', 'indice_izq', 'medio_izq', 'anular_izq', 'menique_izq', 'cuip'], 'string'],
            [['clave_empleado', 'estado_civil'], 'string', 'max' => 20],
            [['nombre'], 'string', 'max' => 60],
            [['apellido_paterno', 'apellido_materno', 'municipio_nac', 'domicilio_mpo', 'domicilio_edo'], 'string', 'max' => 30],
            [['rfc'], 'string', 'max' => 13],
            [['curp', 'cuip'], 'string', 'max' => 18],
            [['sexo'], 'string', 'max' => 1],
            [['telefono', 'movil'], 'string', 'max' => 10],
            [['domicilio_calle', 'domicilio_col'], 'string', 'max' => 45],
            [['email'], 'string', 'max' => 40],
            [['folio'], 'unique'],
            [['curp', 'cuip'], 'unique'],
            ['curp', 'match', 'pattern' => '/^[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9]$/', 'message' => 'CURP invalido'],
            [['clave_empleado'], 'unique'],
            [['rfc'], 'unique'],
            ['rfc', 'match', 'pattern' => '/^[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?$/', 'message' => 'RFC invalido.']
        ];*/

        [['folio', 'nombre', 'apellido_paterno', 'apellido_materno', 'fecha_nac', 'municipio_nac', 'estado_nac', 'curp', 'edad', 'sexo', 'domicilio_calle', 'domicilio_col', 'domicilio_cp', 'domicilio_mpo', 'domicilio_edo', 'estado_civil', 'fotografia', 'pulgar_der', 'indice_der', 'medio_der', 'anular_der', 'menique_der', 'pulgar_izq', 'indice_izq', 'medio_izq', 'anular_izq', 'menique_izq', 'idEstatus', 'idEstudios', 'idEtapa', 'idConvocatoria', 'fecha_ingreso'], 'required'],
        [['folio', 'estado_nac', 'edad', 'domicilio_next', 'domicilio_nint', 'domicilio_cp', 'idEstatus', 'idEstudios', 'idEtapa', 'idConvocatoria', 'idTipo'], 'integer'],
        [['fecha_nac', 'fecha_ingreso'], 'safe'],
        [['fotografia', 'pulgar_der', 'indice_der', 'medio_der', 'anular_der', 'menique_der', 'pulgar_izq', 'indice_izq', 'medio_izq', 'anular_izq', 'menique_izq', 'cuip'], 'string'],
        [['clave_empleado', 'estado_civil'], 'string', 'max' => 20],
        [['nombre'], 'string', 'max' => 60],
        [['apellido_paterno', 'apellido_materno', 'municipio_nac', 'domicilio_mpo', 'domicilio_edo'], 'string', 'max' => 30],
        [['rfc'], 'string', 'max' => 13],
        [['curp', 'cuip'], 'string', 'max' => 18],
        [['sexo'], 'string', 'max' => 1],
        [['telefono', 'movil'], 'string', 'max' => 10],
        [['domicilio_calle', 'domicilio_col'], 'string', 'max' => 45],
        [['email'], 'string', 'max' => 40],
        [['folio'], 'unique'],
        [['curp', 'cuip'], 'unique'],
        [['clave_empleado'], 'unique'],
        [['rfc'], 'unique']

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'folio' => 'Folio',
            'clave_empleado' => 'Clave Empleado',
            'nombre' => 'Nombre',
            'apellido_paterno' => 'Apellido Paterno',
            'apellido_materno' => 'Apellido Materno',
            'fecha_nac' => 'Fecha Nac',
            'municipio_nac' => 'Municipio Nac',
            'estado_nac' => 'Estado Nac',
            'rfc' => 'Rfc',
            'curp' => 'Curp',
            'edad' => 'Edad',
            'sexo' => 'Sexo',
            'telefono' => 'Telefono',
            'movil' => 'Movil',
            'domicilio_next' => 'Domicilio Next',
            'domicilio_nint' => 'Domicilio Nint',
            'domicilio_calle' => 'Domicilio Calle',
            'domicilio_col' => 'Domicilio Col',
            'domicilio_cp' => 'Domicilio Cp',
            'domicilio_mpo' => 'Domicilio Mpo',
            'domicilio_edo' => 'Domicilio Edo',
            'estado_civil' => 'Estado Civil',
            'email' => 'Email',
            'fotografia' => 'Fotografia',
            'pulgar_der' => 'Pulgar Der',
            'indice_der' => 'Indice Der',
            'medio_der' => 'Medio Der',
            'anular_der' => 'Anular Der',
            'menique_der' => 'Menique Der',
            'pulgar_izq' => 'Pulgar Izq',
            'indice_izq' => 'Indice Izq',
            'medio_izq' => 'Medio Izq',
            'anular_izq' => 'Anular Izq',
            'menique_izq' => 'Menique Izq',
            'idEstatus' => 'Id Estatus',
            'idEstudios' => 'Id Estudios',
            'idEtapa' => 'Id Etapa',
            'idConvocatoria' => 'Id Convocatoria',
            'fecha_ingreso' => 'Fecha Ingreso',
            'cuip' => 'Cuip',
            'idTipo' => 'Elemento'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdConvocatoria0()
    {
        return $this->hasOne(\app\models\Convocatorias::className(), ['id' => 'idConvocatoria']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoNac()
    {
        return $this->hasOne(\app\models\Estados::className(), ['id' => 'estado_nac']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstatus0()
    {
        return $this->hasOne(\app\models\Estatus::className(), ['id' => 'idEstatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstudios0()
    {
        return $this->hasOne(\app\models\Estudios::className(), ['id' => 'idEstudios']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEtapa0()
    {
        return $this->hasOne(\app\models\Etapas::className(), ['id' => 'idEtapa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSexo0()
    {
        return $this->hasOne(\app\models\Sexo::className(), ['id' => 'sexo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogAdscripciones()
    {
        return $this->hasMany(\app\models\LogAdscripciones::className(), ['idElemento' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogBajas()
    {
        return $this->hasMany(\app\models\LogBajas::className(), ['idElemento' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogCursos()
    {
        return $this->hasMany(\app\models\LogCursos::className(), ['idElemento' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogEstimulos()
    {
        return $this->hasMany(\app\models\LogEstimulos::className(), ['idElemento' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogEvaluaciones()
    {
        return $this->hasMany(\app\models\LogEvaluaciones::className(), ['idElemento' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogNombramientos()
    {
        return $this->hasMany(\app\models\LogNombramientos::className(), ['idElemento' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogLicencias()
    {
        return $this->hasMany(\app\models\LogLicencias::className(), ['idElemento' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogRequerimientos()
    {
        return $this->hasMany(\app\models\LogRequerimientos::className(), ['idElemento' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogSanciones()
    {
        return $this->hasMany(\app\models\LogSanciones::className(), ['idElemento' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogRechazos()
    {
        return $this->hasMany(\app\models\LogRechazos::className(), ['idElemento' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogSeleccion()
    {
        return $this->hasMany(\app\models\LogSeleccion::className(), ['idElemento' => 'id']);
    }
}
