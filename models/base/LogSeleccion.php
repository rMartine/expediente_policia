<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "log_seleccion".
 *
 * @property integer $id
 * @property integer $idElemento
 * @property integer $idSeleccion
 * @property string $observaciones
 * @property integer $resultados
 * @property string $fecha
 *
 * @property ResultadosSeleccion $resultados0
 * @property Elementos $idElemento0
 * @property Seleccion $idSeleccion0
 */
class LogSeleccion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_seleccion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idElemento', 'idSeleccion', 'resultados', 'fecha'], 'required'],
            [['idElemento', 'idSeleccion', 'resultados'], 'integer'],
            [['observaciones'], 'string'],
            [['fecha'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idElemento' => 'Id Elemento',
            'idSeleccion' => 'Id Seleccion',
            'observaciones' => 'Observaciones',
            'resultados' => 'Resultados',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultados0()
    {
        return $this->hasOne(\app\models\ResultadosSeleccion::className(), ['id' => 'resultados']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdElemento0()
    {
        return $this->hasOne(\app\models\Elementos::className(), ['id' => 'idElemento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSeleccion0()
    {
        return $this->hasOne(\app\models\Seleccion::className(), ['id' => 'idSeleccion']);
    }
}
