<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "estimulos".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $recompensa
 *
 * @property LogEstimulos[] $logEstimulos
 */
class Estimulos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estimulos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo', 'concepto'], 'required'],
            [['tipo', 'concepto'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
       return [ 
           'id' => 'ID', 
           'tipo' => 'Tipo', 
           'concepto' => 'Concepto', 
       ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogEstimulos()
    {
        return $this->hasMany(\app\models\LogEstimulos::className(), ['idEstimulo' => 'id']);
    }
}
