<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "requerimientos".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $descripcion
 *
 * @property LogRequerimientos[] $logRequerimientos
 * @property RequerimientosConvocatoria[] $requerimientosConvocatorias
 */
class Requerimientos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requerimientos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'descripcion'], 'required'],
            [['descripcion'], 'string'],
            [['nombre'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogRequerimientos()
    {
        return $this->hasMany(\app\models\LogRequerimientos::className(), ['idRequerimiento' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequerimientosConvocatorias()
    {
        return $this->hasMany(\app\models\RequerimientosConvocatoria::className(), ['idRequerimiento' => 'id']);
    }
    
    public function getConvocatorias()
    {
        return $this->hasOne(\app\models\Convocatorias::className(), ['id' => 'idConvocatoria']);
    }
}

