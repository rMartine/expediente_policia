<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "nombramientos".
 *
 * @property integer $id
 * @property string $grado
 *
 * @property LogAdscripciones[] $logAdscripciones
 * @property LogNombramientos[] $logNombramientos
 */
class Nombramientos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nombramientos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grado'], 'required'],
            [['grado'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'grado' => 'Grado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogAdscripciones()
    {
        return $this->hasMany(\app\models\LogAdscripciones::className(), ['idNombramiento' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogNombramientos()
    {
        return $this->hasMany(\app\models\LogNombramientos::className(), ['idNombramiento' => 'id']);
    }
}
