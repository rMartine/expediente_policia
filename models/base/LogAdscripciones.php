<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "log_adscripciones".
 *
 * @property integer $id
 * @property integer $idElemento
 * @property integer $idAdscripcion
 * @property string $fecha
 * @property string $observaciones
 *
 * @property Adscripciones $idAdscripcion0
 * @property Elementos $idElemento0
 */
class LogAdscripciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_adscripciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idElemento', 'idNombramiento'], 'required'],
            [['idElemento', 'idAdscripcion', 'idNombramiento'], 'integer'],
            [['fecha'], 'safe'],
            [['observaciones'], 'string'],
            ['oficio_comision', 'required', 'message' => 'El oficio no puede quedar vacio'],
            ['idAdscripcion', 'required', 'message' => 'Debes seleccionar una adscripción'],
            ['fecha', 'required', 'message' => 'La fecha no puede quedar vacia'],
            ['detalle_comision', 'required', 'message' => 'La comisión no puede quedar vacia'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idElemento' => 'Id Elemento',
            'idAdscripcion' => 'Id Adscripcion',
            'idNombramiento' => 'Id Nombramiento',
            'fecha' => 'Fecha',
            'observaciones' => 'Observaciones',
            'oficio_comision' => 'Oficio',
            'detalle_comision' => 'Comisión',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAdscripcion0()
    {
        return $this->hasOne(\app\models\Adscripciones::className(), ['id' => 'idAdscripcion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdElemento0()
    {
        return $this->hasOne(\app\models\Elementos::className(), ['id' => 'idElemento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdNombramiento0()
    {
        return $this->hasOne(\app\models\Nombramientos::className(), ['id' => 'idNombramiento']);
    }
}
