<?php

namespace app\models\base;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "reportes".
 *
 * @property integer $id_reporte
 * @property string $nombre
 * @property string $url
 * @property integer $rol
 * @property integer $parametros
 * @property integer $estatus
 */
class Reportes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reportes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'rol'], 'required'],
            [['rol', 'parametros', 'estatus'], 'integer'],
            [['nombre', 'url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_reporte' => 'Id Reporte',
            'nombre' => 'Nombre',
            'url' => 'Url',
            'rol' => 'Rol',
            'parametros' => 'Parametros',
            'estatus' => 'Estatus',
        ];
    }
}
