<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "log_cursos".
 *
 * @property integer $id
 * @property integer $idElemento
 * @property integer $idCurso
 * @property string $calificacion
 * @property integer $cumplimiento
 * @property string $fecha
 * @property string $observaciones
 *
 * @property Cursos $idCurso0
 * @property Elementos $idElemento0
 */
class LogCursos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_cursos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idElemento', 'cumplimiento'], 'required'],
            [['idElemento', 'idCurso', 'cumplimiento'], 'integer'],
            [['calificacion'], 'number'],
            ['calificacion', 
                function ($attr) {
                    if($this->$attr >= 0 && $this->$attr <= 10){
                        return true;
                    } else {
                        $this->addError('calificacion', "Calificacion debe ser 1.00-10.00");
                        return false;
                    }                    
                },
            ],
            [['fecha','fecha_fin'], 'safe'],
            [['observaciones'], 'string'],
            ['idCurso', 'required', 'message' => 'Debes seleccionar un curso'],
            [['fecha','fecha_fin'], 'required', 'message' => 'La fecha no puede quedar vacia'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idElemento' => 'Id Elemento',
            'idCurso' => 'Id Curso',
            'calificacion' => 'Calificacion',
            'cumplimiento' => 'Cumplimiento',
            'fecha' => 'Fecha Inicio',
            'fecha_fin' => 'Fecha Final',
            'observaciones' => 'Observaciones',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCurso0()
    {
        return $this->hasOne(\app\models\Cursos::className(), ['id' => 'idCurso']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdElemento0()
    {
        return $this->hasOne(\app\models\Elementos::className(), ['id' => 'idElemento']);
    }
}
