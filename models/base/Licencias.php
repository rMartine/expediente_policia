<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "licencias".
 *
 * @property integer $id
 * @property string $tipo
 *
 * @property LogLicencias[] $logLicencias
 */
class Licencias extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'licencias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo'], 'required'],
            [['tipo'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo' => 'Tipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogLicencias()
    {
        return $this->hasMany(\app\models\LogLicencias::className(), ['idLicencia' => 'id']);
    }
}
