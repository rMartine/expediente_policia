<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "log_sanciones".
 *
 * @property integer $id
 * @property integer $idElemento
 * @property integer $idSancion
 * @property string $otra_sancion
 * @property string $causa
 * @property string $fecha
 * @property string $fecha_inicio
 * @property string $fecha_fin
 * @property string $observaciones
 *
 * @property Elementos $elementos
 * @property Sanciones $sanciones
 */
class LogSanciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_sanciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idElemento'], 'required'],
            [['idElemento', 'idSancion'], 'integer'],
            [['otra_sancion', 'causa'], 'string'],
            [['fecha', 'fecha_inicio', 'fecha_fin'], 'safe'],
            [['observaciones'], 'string'],
            ['idSancion', 'required', 'message' => 'Debes seleccionar una sanción'],
            ['causa', 'required', 'message' => 'La causa no puede quedar vacia'],
            ['fecha', 'required', 'message' => 'La fecha no puede quedar vacia'],
            ['observaciones', 'required', 'message' => 'Las observaciones no pueden quedar vacias'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idElemento' => 'Id Elemento',
            'idSancion' => 'Id Sancion',
            'otra_sancion' => 'Otra Sancion',
            'causa' => 'Causa',
            'fecha' => 'Fecha',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_fin' => 'Fecha Fin',
            'observaciones' => 'Observaciones',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElementos()
    {
        return $this->hasOne(\app\models\Elementos::className(), ['id' => 'idElemento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSanciones()
    {
        return $this->hasOne(\app\models\Sanciones::className(), ['id' => 'idSancion']);
    }
}
