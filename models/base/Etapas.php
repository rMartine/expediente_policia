<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "etapas".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $orden
 *
 * @property Cursos[] $cursos
 * @property Elementos[] $elementos
 * @property PaquetesCursos[] $paquetesCursos
 */
class Etapas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'etapas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'orden'], 'required'],
            [['orden'], 'integer'],
            [['nombre'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'orden' => 'Orden',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCursos()
    {
        return $this->hasMany(\app\models\Cursos::className(), ['idEtapa' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElementos()
    {
        return $this->hasMany(\app\models\Elementos::className(), ['idEtapa' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetesCursos()
    {
        return $this->hasMany(\app\models\PaquetesCursos::className(), ['idEtapa' => 'id']);
    }
}
