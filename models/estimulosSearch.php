<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Estimulos;

/**
 * estimulosSearch represents the model behind the search form about estimulos.
 */
class estimulosSearch extends Model
{
	public $id;
	public $nombre;
	public $recompensa;

	public function rules()
	{
		return [
			[['id'], 'integer'],
			[['nombre', 'recompensa'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'nombre' => 'Nombre',
			'recompensa' => 'Recompensa',
		];
	}

	public function search($params)
	{
		$query = Estimulos::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'id' => $this->id,
        ]);

		$query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'recompensa', $this->recompensa]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
