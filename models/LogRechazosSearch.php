<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogRechazos;

/**
 * LogRechazosSearch represents the model behind the search form about LogRechazos.
 */
class LogRechazosSearch extends Model
{
	public $id;
	public $idElemento;
	public $idRechazo;
	public $comentarios;
	public $rechazado;
	public $fecha;

	public function rules()
	{
		return [
			[['id', 'idElemento', 'idRechazo', 'rechazado'], 'integer'],
			[['comentarios', 'fecha'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'idElemento' => 'Id Elemento',
			'idRechazo' => 'Id Rechazo',
			'comentarios' => 'Comentarios',
			'rechazado' => 'Rechazado',
			'fecha' => 'Fecha',
		];
	}

	public function search($params)
	{
		$query = LogRechazos::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'id' => $this->id,
            'idElemento' => $this->idElemento,
            'idRechazo' => $this->idRechazo,
            'rechazado' => $this->rechazado,
            'fecha' => $this->fecha,
        ]);

		$query->andFilterWhere(['like', 'comentarios', $this->comentarios]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
