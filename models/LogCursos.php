<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_cursos".
 */
class LogCursos extends \app\models\base\LogCursos
{
	const ETAPA_ACTIVO = 6;

	public $nombre_curso;
	public $duracion_curso;
	public $obligatorio_curso;

    public function getCurso() {
		return $this->hasOne(Cursos::className(), ['id' => 'idCurso']);
	}

	public function getCumplimientoText() {
		return $this->cumplimiento ? 'Si' : 'No';
	}

	public function getFechaText() {
		return date('d/m/Y', strtotime($this->fecha));
	}
        
	public function getFechaFinText() {
		return date('d/m/Y', strtotime($this->fecha_fin));
	}

	public function getDuracionText() {
		return $this->duracion_curso . ' horas';
	}

	public function getObligatorioText() {
		return $this->obligatorio_curso ? 'obligatorio' : 'opcional';
	}

	/**
     * Función para obtener todos los cursos en la etapa Activo
     */
    public function getLogCursosByElementoAndEtapa($numElementoId)
    {
    	$strQuery = "SELECT ";
    	$strQuery .= "lcursos.id, cur.nombre AS nombre_curso, cur.duracion AS duracion_curso, ";
    	$strQuery .= "cur.obligatorio AS obligatorio_curso, lcursos.cumplimiento, ";
    	$strQuery .= "lcursos.calificacion, lcursos.fecha, lcursos.fecha_fin ";
		$strQuery .= "FROM log_cursos lcursos ";
		$strQuery .= "LEFT JOIN cursos cur ON lcursos.idCurso = cur.id ";
		$strQuery .= "WHERE lcursos.idElemento = " . $numElementoId . " AND cur.idEtapa = " . self::ETAPA_ACTIVO;
                
		$arrCursos = $this->findBySql($strQuery)->all();

        return $arrCursos;
    }
}
